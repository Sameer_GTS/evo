//
//  AppDelegate.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/9/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.


// client dev
// com.evo.application

// Gts Dev
// com.evotestgts.app


import UIKit
import IQKeyboardManager
import FBSDKCoreKit
import GoogleSignIn
import Crashlytics
import Fabric
import UserNotifications
import Stripe
import GooglePlaces
import SendBirdSDK
import Mixpanel
import Flurry_iOS_SDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var tabBarController = CustomizedTabBarController()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        WebConnect.getGymList(type: "A", completion: { (_, _) in })

        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
        
        track.start = Date()
        
        //Mixpanel.init(token: MIXPANEL_TOKEN, launchOptions: launchOptions, flushInterval: 60, trackCrashes: true, automaticPushTracking: true)
        
        //----->
        let builder = FlurrySessionBuilder.init()
            .withAppVersion("1.0.2")
            .withLogLevel(FlurryLogLevelAll)
            .withCrashReporting(true)
            .withSessionContinueSeconds(10)
        
        // Replace YOUR_API_KEY with the api key in the downloaded package
        Flurry.startSession(Flurry_ApiKey_iOS, with: builder)
        //----->
        
      //  GMSPlacesClient.provideAPIKey(KEY_GOOGLE_LOCATION)
        
        Fabric.with([Crashlytics.self])
        
        if  IK_DEFAULTS.value(forKey: SPORTS) == nil {
            WebConnect.getSport(type:"A",completion:{ status , arr in
            })
        }
        
        IQKeyboardManager.shared().isEnabled = true
        //IQKeyboardManager.shared().canAdjustAdditionalSafeAreaInsets = true
        
        //------- Enable FBSDK to handel login with facebook.
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
      //  WebConnect.getCities(keyword:"")
        WebConnect.getGymList(type: "C",completion: { _, _ in })
        WebConnect.getPTLevel()
        
        IK_DELEGATE.notificationPermission()
        
        TabBarConfiguration(false)
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if url.absoluteString.contains("google") {
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        }
        
        return ApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        guard SBDMain.getCurrentUser()?.userId != nil else {return}
        ChatManager.shared.saveChannels()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0;
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

//MARK:- Notification
//MARK:-

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    /// Adk for notification permission
    func notificationPermission() {
        /**** Register Push Notification ****/
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in}
            UIApplication.shared.registerForRemoteNotifications()
        }
        else {
            let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
            let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(pushNotificationSettings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        //print("Token:-  " + token)
        IK_DEFAULTS.set(token as Any, forKey: DEVICE_TOKEN)
        IK_DEFAULTS.synchronize()
        
        guard let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary else { return }
        //Mixpanel.sharedInstance()?.identify(IK_USER.profile_id, usePeople: true)
        //Mixpanel.sharedInstance()?.people.addPushDeviceToken(deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
//        print("---------------------------------------------------")
//        print("didReceiveRemoteNotification :-",userInfo)
        
        if userInfo["sendbird"] != nil {
            let sendBirdPayload = userInfo["sendbird"] as! Dictionary<String, Any>
            let channel = (sendBirdPayload["channel"]  as! Dictionary<String, Any>)["channel_url"] as! String
            Utils.openChat(channelUrl: channel)
        }
        else {
            if userInfo["aps"] != nil {
                let aps = userInfo["aps"] as! Dictionary<String, Any>
                if let alert = aps["alert"] as? Dictionary<String, Any> {
                    if alert["type"] != nil {
                        let type =  Int(String(describing:alert["type"]!))
                        if let unType = NotificationType(rawValue: type!) {
                            if alert["profile_id"] != nil {
                                let userId = String(describing:alert["profile_id"]!)
                                Utils.handelNotification(type: unType, aceId: userId)
                            }
                            else {
                                Utils.handelNotification(type: unType)
                            }
                        }
                    }
                }
            }
        }
    }
    
    @available(iOS 9.0, *)
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
//        print("---------------------------------------------------")
//        print("action handler :-",userInfo)
        
        if userInfo["sendbird"] != nil {
            let sendBirdPayload = userInfo["sendbird"] as! Dictionary<String, Any>
            let channel = (sendBirdPayload["channel"]  as! Dictionary<String, Any>)["channel_url"] as! String
            Utils.openChat(channelUrl: channel)
        }
        else {
            if userInfo["aps"] != nil {
                let aps = userInfo["aps"] as! Dictionary<String, Any>
                if let alert = aps["alert"] as? Dictionary<String, Any> {
                    if alert["type"] != nil {
                        let type =  Int(String(describing:alert["type"]!))
                        if let unType = NotificationType(rawValue: type!) {
                            if alert["profile_id"] != nil {
                                let userId = String(describing:alert["profile_id"]!)
                                Utils.handelNotification(type: unType, aceId: userId)
                            }
                            else {
                                Utils.handelNotification(type: unType)
                            }
                        }
                    }
                }
            }
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        print("---------------------------------------------------")
//        print("willPresent :-",notification.request.content.userInfo)
        UIApplication.shared.applicationIconBadgeNumber = 0;
        completionHandler(.alert)
        
        if notification.request.content.userInfo["aps"] != nil {
            let aps = notification.request.content.userInfo["aps"] as! Dictionary<String, Any>
            if let alert = aps["alert"] as? Dictionary<String, Any> {
                if alert["type"] != nil {
                    let type =  Int(String(describing:alert["type"]!))
                    if let unType = NotificationType(rawValue: type!) {
                        if alert["profile_id"] != nil {
                            let userId = String(describing:alert["profile_id"]!)
                            //Utils.handelNotification(type: unType, aceId: userId)
                        } else {
                            //Utils.handelNotification(type: unType)
                        }
                    }
                }
            }
        }
    }
}

//MARK:- Tab Bar Setup
//MARK:-


extension AppDelegate
{
    
    func TabBarConfiguration(_ needAnimation : Bool) {
        
        if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
            
            let navigation = UINavigationController()
            navigation.isNavigationBarHidden = true
            var views: [UIViewController] = []
            
            /*
            Mixpanel.sharedInstance()?.registerSuperProperties([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
                                                                 "First Name":IK_USER.first_name,
                                                                 "Last Name":IK_USER.last_name,
                                                                 "Email":IK_USER.email,*/
                                                            "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                                                 "Fitness Level":arrFitness[IK_USER.fitness],
                                                                 "Age Group":arrAge[IK_USER.ageGroup]])
            */
        }
        
        tabBarController = CustomizedTabBarController()
        
        let homeObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AceList") as! AceList
        let homeAceObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Home") as! Home
        
        let historyObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Workout") as! Workout
        let moreObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "More") as! More
        
        let lockObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Unlock") as! Unlock
        let notificationObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Notifications") as! Notifications
        notificationObj.isShowNotification = false
        
        homeAceObj.tabBarItem.image = UIImage(named:  "tab_home")?.withRenderingMode(.alwaysOriginal)
        homeAceObj.tabBarItem.selectedImage = UIImage(named:  "tab_home_selected")?.withRenderingMode(.alwaysTemplate)
        homeAceObj.tabBarItem.title = "Hjem"
        
        homeObj.tabBarItem.image = UIImage(named:  "tab_home")?.withRenderingMode(.alwaysOriginal)
        homeObj.tabBarItem.selectedImage = UIImage(named:  "tab_home_selected")?.withRenderingMode(.alwaysTemplate)
        homeObj.tabBarItem.title = "Hjem"

        historyObj.tabBarItem.image = UIImage(named:  "tab_exercise")?.withRenderingMode(.alwaysOriginal)
        historyObj.tabBarItem.selectedImage = UIImage(named:  "tab_exercise_selected")?.withRenderingMode(.alwaysTemplate)
        historyObj.tabBarItem.title = "PT timer"
        
        moreObj.tabBarItem.image = UIImage(named:  "tab_more")?.withRenderingMode(.alwaysOriginal)
        moreObj.tabBarItem.selectedImage = UIImage(named:  "tab_more_selected")?.withRenderingMode(.alwaysTemplate)
        moreObj.tabBarItem.title = "Mer"

        lockObj.tabBarItem.image = UIImage(named:  "tab_lock")?.withRenderingMode(.alwaysOriginal)
        lockObj.tabBarItem.selectedImage = UIImage(named:  "tab_lock")?.withRenderingMode(.alwaysOriginal)
        lockObj.tabBarItem.title = "PORT"

        
        notificationObj.tabBarItem.image = UIImage(named:  "tab_notif")?.withRenderingMode(.alwaysOriginal)
        notificationObj.tabBarItem.selectedImage = UIImage(named:  "tab_notif_set")?.withRenderingMode(.alwaysTemplate)
        notificationObj.tabBarItem.title = "Yarsler"

        let homeAceNav = UINavigationController.init(rootViewController: homeAceObj)
        let homeNav = UINavigationController.init(rootViewController: homeObj)
        let historyNav = UINavigationController.init(rootViewController: historyObj)
        let moreNav = UINavigationController.init(rootViewController: moreObj)
        
        let lockNav = UINavigationController.init(rootViewController: lockObj)
        let notificationNav = UINavigationController.init(rootViewController: notificationObj)

        
        homeAceNav.navigationBar.isHidden  = true
        homeNav.navigationBar.isHidden  = true
        historyNav.navigationBar.isHidden  = true
        moreNav.navigationBar.isHidden  = true

        lockNav.navigationBar.isHidden  = true
        notificationNav.navigationBar.isHidden  = true
        
        //UITabBar.appearance().barTintColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        UITabBar.appearance().tintColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)

        
        //homeObj.tabBarItem.title = "dashboard"
        if IK_USER.userType == .ace {
            ChatManager.shared.connect(userId:IK_USER.profile_id, accessToken: IK_USER.sendbirdToken)
            
            let controllers = [homeAceNav,lockNav,moreNav]
            tabBarController.viewControllers = controllers
        }
        else if (IK_USER.sportId.count > 0 || IK_USER.nutritionist == "Y" ) && IK_USER.ageGroup > 0 && IK_USER.fitness > 0 && !(IK_USER.consentAgreed == consentAgree.consentAgreed.rawValue) { 
            ChatManager.shared.connect(userId:IK_USER.profile_id, accessToken: IK_USER.sendbirdToken)
//            let controllers = [homeNav,historyNav,moreNav]
            let controllers = [homeNav,historyNav,lockNav,notificationNav,moreNav]
            tabBarController.viewControllers = controllers
        }
        else {
            IK_DEFAULTS.removeObject(forKey: USER_DETAIL)
            IK_DEFAULTS.synchronize()
            return
        }
        
        IK_DELEGATE.window?.makeKeyAndVisible()
        
        //let navi = UINavigationController.init(rootViewController: tabBarController)
        //navi.navigationBar.isHidden = true
        
        let mainNav = UINavigationController.init(rootViewController: self.tabBarController)
        mainNav.navigationBar.isHidden = true
        
        if needAnimation {
            UIView.transition(from: (IK_DELEGATE.window?.rootViewController!.view)!, to: tabBarController.view, duration: 0.3, options: [.transitionCrossDissolve], completion: {
                _ in
                IK_DELEGATE.window?.rootViewController = mainNav
            })
        }
        else {
            IK_DELEGATE.window?.rootViewController = mainNav
        }

        
//        if needAnimation {
//            UIView.transition(from: (IK_DELEGATE.window?.rootViewController!.view)!, to: tabBarController.view, duration: 0.3, options: [.transitionCrossDissolve], completion: {
//                _ in
//                IK_DELEGATE.window?.rootViewController = self.tabBarController
//            })
//        }
//        else {
//            IK_DELEGATE.window?.rootViewController = tabBarController
//        }
        tabBarController.selectedIndex = 0
    }
}
