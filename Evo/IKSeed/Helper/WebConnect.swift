//
//  WebConnect.swift
//  FlightBuddy
//
//  Created by Chanchal Warde on 2/9/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.

import UIKit
import Alamofire
import CoreLocation

class WebConnect: NSObject {
    
    static let AceTerms = "https://evo.ikseed.com/ikseed/contact"
//    "http://ikseed.com/live/ikseed/contact"
    static let TermsNCondition = "https://evo.ikseed.com/ikseed/termsofuse"
    //"http://ikseed.com/live/ikseed/termsofuse"
    static let PrivacyPolicy = "https://evo.ikseed.com/ikseed/privacypolicies"
    //"http://ikseed.com/live/ikseed/privacypolicies"
    static let MinEvo = "https://me.evofitness.no/login"
    
    fileprivate struct WebURL {
        
        static let baseUrl =  "https://evo.ikseed.com/api/"
        
        static let login = baseUrl + "ikseed_users/login"
        static let register = baseUrl + "ikseed_users/register"
        static let sports = baseUrl + "ikseed_categories"
        static let saveSport = baseUrl + "ikseed_categories/save"
        static let facebook = baseUrl + "ikseed_users/fblogin"
        static let google = baseUrl + "ikseed_users/gmaillogin"
        static let updateEmail = baseUrl + "ikseed_user/update_email"
        static let cities = baseUrl + "ikseed_cities"
        static let addAge = baseUrl + "ikseed_user/update_agegroup"
        static let addFitness = baseUrl + "ikseed_user/update_fitnesslevel"
        static let updateImage = baseUrl + "ikseed_user/update_image"
        static let becomeAce = baseUrl + "ikseed_ace_user/create"
        static let checkEligibility = baseUrl + "ikseed_user/check_eligibility"
        static let aceList = baseUrl + "ikseed_user/ace_search"
        static let forgotPassword = baseUrl + "ikseed_users/forget_password"
        static let resetPassword = baseUrl + "ikseed_users/reset_password"
        static let searchAvailabelAce = baseUrl + "ikseed_user/avaliable_ace"
        static let requestStatus = baseUrl + "check_request"
        static let rateCalculation = baseUrl + "rate_calculation"
        static let bookAce = baseUrl + "user/ace_booking"
        static let getCards = baseUrl + "ikseed_user/show_card"
        static let addCard = baseUrl + "ikseed_user/add_card"
        static let deleteCard = baseUrl + "ikseed_user/remove_card"
        static let makeCardPrimary = baseUrl + "ikseed_user/make_card_primary"
        static let userSchedule = baseUrl + "user/booking"
        static let help =  baseUrl + "help"
        static let updateUserProfile = baseUrl + "ikseed_user/update_profile"
        static let rateAce = baseUrl + "add_rating"
        static let cancelWorkout = baseUrl + "ikseed_users/reject_upcoming_session"
        static let availabilityBadge = baseUrl + "ikseed_user/count_request"
        static let clearBadge = baseUrl + "ikseed_user/read_request"
        static let checkActivityStatus = baseUrl + "ace/ace_booking_compelete"
        static let aceProfileDetail = baseUrl + "ikseed_user/user_profile"
        
        static let trackProfileView = baseUrl + "ikseed_user/visitprofile"
        static let trackChat = baseUrl + "ikseed_user/savechat"
        
        static let getEKeys = baseUrl + "user/ekey"
        
        static let updateBooking = baseUrl + "user/update_booking"

        ///---- Comman
        static let getNotification = baseUrl + "ikseed_user/notifications"
        static let setting = baseUrl + "ikseed_user/view_settings"
        static let updateSetting = baseUrl + "ikseed_user/update_settings"
        
        ///---- ACE
        static let getPendingRequests = baseUrl + "ace/pending_request"
        static let replyAvailabilty = baseUrl + "ace/request_accept_decline"
        static let replyBooking = baseUrl + "ace/booking_accept_decline"
        static let addBankDetail = baseUrl + "ikseed_user/add_payout_recipient"
        static let getBankDetail = baseUrl + "ace/bank_details"
        static let getAceProfile = baseUrl + "ikseed_user/acer_profile"
        static let updateAceProfile = baseUrl + "ikseed_ace_user/ace_update_profile"
        static let cancelBooking = baseUrl + "ikseed_user/acer_cancel_booking"
        
        ///--- Chat
        static let createChannel = baseUrl + "ikseed_users/channel"
        
        ///--- Logout
        static let logout = baseUrl + "ikseed_user/logout"
        
        ///--- Marathon Event
        static let marathonType = baseUrl + "ikseed_trainingtype"
        static let marathonWeek = baseUrl + "ikseed_trainingweek"
        static let traningSchedule = baseUrl + "ikseed_trainingpackage"
        static let purchasePlan = baseUrl + "ikseed_trainingbooking"
        static let personalTrainer = baseUrl + "ikseed_training/get_ace"
        static let weekSchedule = baseUrl + "ikseed_training/get_schedule"
        static let weekDemoSchedule = baseUrl + "ikseed_training/sample_schedule"
        static let currentActivity = baseUrl + "ikseed_training/activity"
        static let saveCurrentActivity = baseUrl + "ikseed_training/saveactivity"
        
        ///--- EVO
        
        static let gymList = baseUrl + "ikseed_gyms"
        static let addGym = baseUrl + "ikseed_user/update_gym"
        static let ptLevels = baseUrl + "ikseed_pt_levels"

        static let requestBlogs = baseUrl + "blogs"

        static let gymsList = baseUrl + "gyms"

        static let getExoToken = baseUrl + "exo_token"
        
        //Sameer
        static let updateUserConsent = baseUrl + "ikseed_user/update_consent"
    }
    
    fileprivate static var header : HTTPHeaders {
        
        var head = [ "device-type": "IOS",
                     "app-version": Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "0",
                     "time-zone":TimeZone.current.identifier ,
                     "device-token": IK_DEFAULTS.object(forKey: DEVICE_TOKEN) as? String ?? "a8050f9ade63192a391fafcf288fbfa06d07f3c26c3c2d05633d12a668166af0",
                     "device-os-version": "11.2"]//UIDevice.current.systemVersion]
        
        if IK_DEFAULTS.object(forKey: USER_DETAIL) != nil {
            head["api-token"] = IK_USER.api_token
            head["user_id"] = String(describing: IK_USER.profile_id!)
        }
        
        //print("\n\n-------------------")
        //print("Header ",head)
        
        return head
    }
    
    static func login(email:String, password : String ,completion:@escaping(_ status: Bool, _ message: String) -> Void) {
        
        let param = ["user_email":email,
                     "password":password]
        
        post(url: WebURL.login, parameter: param) { (response, status) in
            if status {
                if let userData = response["data"] as? NSDictionary {
                    //consent_agreed
                    
                    print(userData["Consent_agreed"] as? Int ?? 0)
                    
                    if let userProfileId = userData["profile_id"] as? Int
                    {
                        if Defaults.integer(forKey: DefaultsKeys.profileId.rawValue) != userProfileId
                        {
                            Defaults.removeObject(forKey: DefaultsKeys.eKey.rawValue)
                            Defaults.removeObject(forKey: DefaultsKeys.eKeyReference.rawValue)
                            Defaults.set(userProfileId, forKey: DefaultsKeys.profileId.rawValue)
                        }
                    }
                    
                    var urData = [String : Any]()
                    urData = userData as! [String : Any]
                    if let tmp = urData["exo_id"] as? String
                    {
                        urData["exo_id"] = tmp
                    }
                    else
                    {
                        urData["exo_id"] = ""
                    }
                    IK_DEFAULTS.set(urData, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                }
                
                if let userData = response["serialNumbers"] as? NSArray {
                    Defaults.set(userData, forKey: DefaultsKeys.serialNumbers.rawValue)
                }
            }
            
            completion(status,response["message"] as! String)
        }
    }
    
    static func register(firstName:String, lastName: String, email: String , password:String ,completion:@escaping(_ status: Bool, _ message: String ) -> Void) {
        
        let param = [ "first_name":firstName,
                      "last_name":lastName,
                      "user_email":email,
                      "password":password,
                      "login_mode":"1",
                      "profile_id" : "0"]
        
        post(url: WebURL.register, parameter: param) { (response, status) in
            if status {
                if let userData = response["data"] as? NSDictionary {
                    
                    if let userProfileId = userData["profile_id"] as? Int
                    {
                        if Defaults.integer(forKey: DefaultsKeys.profileId.rawValue) != userProfileId
                        {
                            Defaults.removeObject(forKey: DefaultsKeys.eKey.rawValue)
                            Defaults.removeObject(forKey: DefaultsKeys.eKeyReference.rawValue)
                            Defaults.set(userProfileId, forKey: DefaultsKeys.profileId.rawValue)
                        }
                    }
                    
                    IK_DEFAULTS.set(userData, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                }
                
                if let userData = response["serialNumbers"] as? NSArray {
                    Defaults.set(userData, forKey: DefaultsKeys.serialNumbers.rawValue)
                }            }
            completion(status,response["message"] as! String)
        }
    }
    
    static func loginWithFB(email:String, name:String, profileId:String, imageURL: String, completion:@escaping(_ status: Bool, _ message: String ) -> Void ) {
        let param = [   "user_email":email,
                        "first_name" : name,
                        "last_name": "",
                        "profile_id": profileId,
                        "profile_image_url": imageURL]
        
        post(url: WebURL.facebook, parameter: param) { (response, status) in
            if status {
                if let userData = response["data"] as? NSDictionary {
                    
                    if let userProfileId = userData["profile_id"] as? Int
                    {
                        if Defaults.integer(forKey: DefaultsKeys.profileId.rawValue) != userProfileId
                        {
                            Defaults.removeObject(forKey: DefaultsKeys.eKey.rawValue)
                            Defaults.removeObject(forKey: DefaultsKeys.eKeyReference.rawValue)
                            Defaults.set(userProfileId, forKey: DefaultsKeys.profileId.rawValue)
                        }
                    }
                    
                    IK_DEFAULTS.set(userData, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                }
                
                if let userData = response["serialNumbers"] as? NSArray {
                    Defaults.set(userData, forKey: DefaultsKeys.serialNumbers.rawValue)
                }            }
            completion(status,response["message"] as! String)
        }
    }
    
    static func loginWithGoogle(email:String, name:String, profileId:String, imageURL: String, completion:@escaping(_ status: Bool, _ message: String ) -> Void ) {
        let param = [   "user_email":email,
                        "first_name" : name,
                        "last_name": "",
                        "profile_id": profileId,
                        "profile_image_url": imageURL]
        
        post(url: WebURL.google, parameter: param) { (response, status) in
            if status {
                if let userData = response["data"] as? NSDictionary {
                    
                    if let userProfileId = userData["profile_id"] as? Int
                    {
                        if Defaults.integer(forKey: DefaultsKeys.profileId.rawValue) != userProfileId
                        {
                            Defaults.removeObject(forKey: DefaultsKeys.eKey.rawValue)
                            Defaults.removeObject(forKey: DefaultsKeys.eKeyReference.rawValue)
                            Defaults.set(userProfileId, forKey: DefaultsKeys.profileId.rawValue)
                        }
                    }
                    
                    IK_DEFAULTS.set(userData, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                }
                
                if let userData = response["serialNumbers"] as? NSArray {
                    Defaults.set(userData, forKey: DefaultsKeys.serialNumbers.rawValue)
                }            }
            completion(status,response["message"] as! String)
        }
    }
    
    static func getSport(type:String , completion:@escaping(_ status : Bool, _ arrSport : [Sport]) -> Void) {
        post(url: WebURL.sports, parameter: [ "type":type]) { (response, status) in
            if status {
                let arrData = response["data"] as! [NSDictionary]
                IK_DEFAULTS.set(arrData, forKey: SPORTS)
                IK_DEFAULTS.synchronize()
                
                var arr : [Sport] = []
                
                for dic in arrData {
                    var obj = Sport()
                    obj.id = Int(String(describing:dic["cat_id"]!) )!
                    obj.name = dic["cat_name"] as! String
                    obj.image = SportImage[obj.name] ?? #imageLiteral(resourceName: "icn_golf")
                    obj.isSelected =  IK_USER.sportId.contains(obj.id)
                    arr.append(obj)
                }
                
                completion(status,arr)
            } else {
                completion(status,[])
            }
        }
    }
    
    static func saveInterest(sportId: [Int], nutrition: Bool, completion: @escaping(_ status : Bool, _ message : String) -> Void ) {
        var param = ["user_id":IK_USER.profile_id,
                     "cat_ids":sportId,
                     "nutrition":nutrition ? "Y" : "N"] as [String:Any]
        
        post(url: WebURL.saveSport, parameter: param) { (response, status) in
            if status {
            }
            completion(status,response["message"] as! String)
        }
    }
    
    static func getCities(keyword:String) {
        post(url: WebURL.cities, parameter: [ "keywords":keyword]) { (response, status) in
            if status {
                let data = response["data"] as! [NSDictionary]
                let ace = response["ace_cities"] as! [NSDictionary]
                
                IK_DEFAULTS.set(ace, forKey: ACE_CTIES)
                IK_DEFAULTS.set(data, forKey: CITIES)
                IK_DEFAULTS.synchronize()
            }
        }
    }
    
    static func getGymList(type:String , completion : @escaping(Bool , [(id:Int, name:String , address : String)]) -> Void ) {
        post(url: WebURL.gymList, parameter: ["type": type] , indicator: false ) { (response, status) in
            if status {
                let data = response["data"] as! [NSDictionary]
                
                var arrList : [(id:Int, name:String , address : String)] = []
             
                if type == "C" {
                    IK_DEFAULTS.set(data, forKey: GYM_LIST)
                    IK_DEFAULTS.synchronize()
                    
                    for dic in data {
                        arrList.append((id:Int(String(describing: dic["id"]!))!,
                                        name:dic["name"] as! String,
                                        address : String(describing: dic["address"]!)))
                    }
                    completion(status,arrList)
                }
                else {
                    
                    IK_DEFAULTS.set(data, forKey: GYM_LIST_ALL)
                    IK_DEFAULTS.synchronize()
                    for dic in data {
                        arrList.append((id:Int(String(describing: dic["id"]!))!,
                                        name:dic["name"] as! String,
                                        address : String(describing: dic["address"]!)))
                    }
                    completion(status,arrList)
                }
            }
        }
    }
    
    static func getPTLevel() {
        post(url: WebURL.ptLevels, parameter: [:]) { (response, status) in
            if status {
                let data = response["data"] as! [NSDictionary]
                
                IK_DEFAULTS.set(data, forKey: PT_LEVELS)
                IK_DEFAULTS.synchronize()
            }
        }
    }
    
    static func updateAgeGroup(ageId:Int , completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        
        let param = ["user_id":IK_USER.profile_id,
                     "age_groups":ageId] as [String : Any]
        
        post(url: WebURL.addAge, parameter: param ) { (response, status) in
            if status {
                
            }
            
            completion(status,response["message"] as! String)
        }
    }
    
    static func updateUserConsent(consentAgreed:Int , completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        let param = ["user_id":IK_USER.profile_id ?? "",
                     "consent_agreed":consentAgreed] as [String : Any]
        
        post(url: WebURL.updateUserConsent, parameter: param ) { (response, status) in
            
            if let userData = response["data"] as? NSDictionary {
                
                let consentID = userData["consent_agreed"] as? Int
                
                if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                    let newDic = NSMutableDictionary(dictionary: dict)
                    newDic.setValue(consentID, forKey: "consent_agreed")
                    
                    IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                }
                
            }
            
            completion(status,response["message"] as! String)
        }
    }
    
    
    static func updateGym(gymId:Int , completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        
        let param = ["user_id":IK_USER.profile_id,
                     "gym_id":gymId] as [String : Any]
        post(url: WebURL.addGym, parameter: param ) { (response, status) in
            if status {
                
            }
            completion(status,response["message"] as! String)
        }
    }
    
    
    static func updateFitnessLevel(levelID:Int , completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        
        let param = ["user_id":IK_USER.profile_id,
                     "fitness_level":levelID] as [String : Any]
        post(url: WebURL.addFitness, parameter: param ) { (response, status) in
            if status {
                
            }
            completion(status,response["message"] as! String)
        }
    }
    
    static func updateEmail(email:String , completion:@escaping(_ status : Bool,  _ message : String) -> Void) {
        
        let param = ["email":email,
                     "profile_id":IK_USER.profile_id ] as [String : Any]
        
        post(url: WebURL.updateEmail, parameter: param) { (response, status) in
            if status {
                
            }
            completion(status,response["message"] as! String)
        }
    }
    
    static func updateImage(image: UIImage , completion:@escaping(_ status : Bool,  _ message : String , _ imageUrl : String) -> Void) {
        
//        upload(url: WebURL.updateImage, parameter: ["user_id":String(describing:IK_USER.profile_id!)],uploadings: ["profile_image":FileManager.default.saveToDocs(data: UIImageJPEGRepresentation(image, 0.6)!, name: "profileImage.png")], completion: { (response, status) in

        upload(url: WebURL.updateImage, parameter: ["user_id":String(describing:IK_USER.profile_id!)],uploadings: ["profile_image":FileManager.default.saveToDocs(data: image.jpegData(compressionQuality: 0.6)!, name: "profileImage.png")], completion: { (response, status) in

            if status {
                completion(status,response["message"] as! String,(response["data"] as! NSDictionary) ["image"] as! String)
            }
            else {
                completion(status,response["message"] as! String,"")
            }
        })
    }
    
    static func becomeAce(param : [String:Any] , coverImage : UIImage?, completion:@escaping(_ status : Bool,  _ message : String) -> Void) {
       
        if coverImage != nil {
//            upload(url: WebURL.becomeAce,parameter: param, uploadings: ["bg_image":FileManager.default.saveToDocs(data: UIImageJPEGRepresentation(coverImage!, 1)!, name: "abc.jpeg")],
//                   completion: { (response, status) in
//                    completion(status,response["message"] as! String)
//            })

            upload(url: WebURL.becomeAce,parameter: param, uploadings: ["bg_image":FileManager.default.saveToDocs(data: coverImage!.jpegData(compressionQuality: 1)!, name: "abc.jpeg")],
                   completion: { (response, status) in
                    completion(status,response["message"] as! String)
            })

        } else {
            post(url: WebURL.becomeAce, parameter: param) { (response, status) in
                completion(status,response["message"] as! String)
            }
        }
    }
    
    static func checkCanAce(completion:@escaping(_ status : Bool,  _ message : String) -> Void) {
        post(url: WebURL.checkEligibility, parameter: ["user_id":IK_USER.profile_id]) { (response, status) in
            if status {
                
            }
            completion(status,response["message"] as! String)
        }
    }
    
    static func getAceList(centerId:Int,completion:@escaping(_ status : Bool, _ arrAce : [Ace], _ message : String , _ event:(active:Bool,packageId:Int)) -> Void) {
        
        let param = ["gym_id": centerId ] as [String : Any]
       
        post(url: WebURL.aceList, parameter: param) { (response, status) in
            
            var event = (active:false,packageId:0)
            
            if let marathon = response["marathon"] as? NSDictionary {
                event.active = marathon["is_active"] as! Bool
                event.packageId = marathon["package_id"] as! Int
            }
            
            
            if status {
                let arrData = response["data"] as! [NSDictionary]
                var arr : [Ace] = []
                
                for dic in arrData {
                    let obj = Ace(dict: dic)
                    arr.append(obj)
                }
                completion(status, arr,"",event)
            }
            else {
                completion(status, [],response["message"] as! String,event)
            }
        }
    }
    
    static func getLockGymsList(completion:@escaping(_ status : Bool, _ arrAce : [LockGymModel], _ message : String) -> Void) {
        
        let param = ["user_id": IK_USER.profile_id, "type" : "L" ] as [String : Any]
        
        post(url: WebURL.gymsList, parameter: param) { (response, status) in
            
            if status {
                let arrData = response["data"] as! [NSDictionary]
                var arr : [LockGymModel] = []
                
                for dic in arrData {
                    let obj = LockGymModel(dict: dic)
                    arr.append(obj)
                }
                
                IK_DEFAULTS.set(arrData, forKey: LOCK_GYM_LIST)
                IK_DEFAULTS.synchronize()
                
                if let currentId = Defaults.object(forKey: LOCK_GYM_CURRENT)
                {
                }
                else
                {
                    if !arr.isEmpty
                    {
                        Defaults.set(String.init(describing: arr.first!.id), forKey: LOCK_GYM_CURRENT)
                        Defaults.synchronize()
                    }
                }
                
                completion(status, arr,"")
            }
            else {
                completion(status, [],response["message"] as! String)
            }
        }
    }
    
    static func getTraningProgramUrl(completion:@escaping(_ status : Bool, _ urlPath : String, _ message : String) -> Void) {
        
        let param = ["profile_id": IK_USER.profile_id] as [String : Any]
        post(url: WebURL.getExoToken, parameter: param) { (response, status) in
            
            if status {
                if let urlPath = response["redirect_url"] as? String
                {
                    completion(status, urlPath,"")
                    return
                }
                completion(status, "" , response["message"] as? String ?? "No traning program.")
            }
            else {
                completion(status, "", response["message"] as? String ?? "No traning program.")
            }
        }
    }
    
    
    static func requestOTP(email:String , completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        
        let param = ["user_email":email]
        
        post(url: WebURL.forgotPassword, parameter: param ) { (response, status) in
            if status {
                
            }
            completion(status,response["message"] as! String)
        }
    }
    
    static func resetPassword(otp:String, password :String , completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        
        let param = ["otp":otp, "password":password]
        
        post(url: WebURL.resetPassword, parameter: param ) { (response, status) in
            if status {
                
            }
            completion(status,response["message"] as! String)
        }
    }
    
    static func requestAvailability(centerId:Int,
                                    date : String,
                                    startTime: String,
                                    endTime : String,
                                    help : String,
                                    person:Int,
                                    completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        
        let param : [String : Any] = ["profile_id":IK_USER.profile_id,
//                                      "nutritionist":nutrition,
//                                      "cat_id":sportId,
                                      "gym_id": centerId,
                                      "date":date,
                                      "start_time":startTime,
                                      "end_time":endTime,
                                      "no_of_people":person,
                                      "help_text":help,
                                      "meeting_point":"ABC",
                                    ]
        
//        "latitude":coordinate.latitude,
//        "longitude":coordinate.longitude,
//        "address":address
        
        post(url: WebURL.searchAvailabelAce, parameter: param) { (response, status) in
            completion(status, response["message"] as! String)
        }
    }
    
    static func requestStatus( completion:@escaping(_ status : Bool,_ arrAce : [Ace], _ message : String) -> Void) {
        post(url: WebURL.requestStatus, parameter: ["profile_id":IK_USER.profile_id]) { (response, status) in
            
            if status {
                
                let data = response["data"] as! NSDictionary
                
                if data["ace"] != nil {
                    let arrData = data["ace"] as! [NSDictionary]
                    var arr : [Ace] = []
                    
                    for dic in arrData {
                        let obj = Ace(dict: dic)
                        arr.append(obj)
                    }
                    completion(status, arr ,response["message"] as! String)
                }
                else {
                    completion(status, [] ,response["message"] as! String)
                }
            } else {
                completion(status, [] ,response["message"] as! String)
            }
        }
    }
    
    static func getRequest( completion:@escaping(_ status : Bool,_ arrRequest : [Request], _ arrBooking: [Request] , _ arrComplete : [Request], _ message : String) -> Void) {
        post(url: WebURL.getPendingRequests, parameter: ["profile_id":IK_USER.profile_id]) { (response, status) in
            if status {
                if let data = response["data"] as? NSDictionary {
                    var arr : [Request] = []
                    var arrBook : [Request] = []
                    var arrCompl : [Request] = []
                    
                    if let arrData = data["request"] as? [NSDictionary] {
                        for dic in arrData {
                            let obj = Request(dict: dic)
                            arr.append(obj)
                        }
                    }
                    
                    if let bookings = data["booked"] as? [NSDictionary] {
                        for dic in bookings {
                            let obj = Request(dict: dic)
                            arrBook.append(obj)
                        }
                    }
                    
                    if let completes = data["completed"] as? [NSDictionary] {
                        for dic in completes {
                            let obj = Request(dict: dic)
                            arrCompl.append(obj)
                        }
                    }
                    
                    completion(status, arr ,arrBook,arrCompl ,response["message"] as! String)
                }
                else {
                    completion(status, [] ,[],[],response["message"] as! String)
                }
            } else {
                completion(status, [] ,[],[],response["message"] as! String)
            }
        }
    }

    // jitendra
    static func requestBlogs( completion:@escaping(_ status : Bool,_ arrBlog : [Blog]) -> Void) {
        post(url: WebURL.requestBlogs, parameter: [:]) { (response, status) in
            
            if status {
                let arrData = response["data"] as! [NSDictionary]
                
                let data: Data = NSKeyedArchiver.archivedData(withRootObject: arrData)
                IK_DEFAULTS.set(data, forKey: BLOGS_LIST)
                IK_DEFAULTS.synchronize()
                
                var arr : [Blog] = []
                for dic in arrData {
                    let dictData = dic as! NSDictionary
                    let obj = Blog(dict: dictData)
                    arr.append(obj)
                }
                completion(status, arr)
            }
            else {
                completion(status, [])
            }
        }
    }
    
    static func replyBooking(requestId:Int, isAccept : Bool ,completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        
        let param : [String : Any] = ["profile_id":IK_USER.profile_id,
                                      "request_id":requestId,
                                      "status": isAccept ? "A" : "D"]
        
        post(url: WebURL.replyBooking, parameter: param) { (response, status) in
            completion(status, response["message"] as! String)
        }
    }
    
    static func replyAvailabilty(requestId:Int, isAccept : Bool ,completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        
        let param : [String : Any] = ["profile_id":IK_USER.profile_id,
                                      "request_id":requestId,
                                      "status": isAccept ? "A" : "D"]
        
        post(url: WebURL.replyAvailabilty, parameter: param) { (response, status) in
            completion(status, response["message"] as! String)
        }
    }
    
    
    static func rateCalculation(sportId:Int, person:Int,hour:Int,completion:@escaping(_ status : Bool, _ amount : String, _ acePrice: String) -> Void) {
        let param : [String : Any] = ["cat_id":sportId,
                                      "no_of_persons":person,
                                      "hours":hour]
        
        post(url: WebURL.rateCalculation, parameter: param) { (response, status) in
            if let data = response["data"] as? NSDictionary {
                let amount = String(describing:data["price"]!)
                let aceAmount = String(describing:data["ace_price"]!)
                completion(status, amount, aceAmount)
            }
            else {
                completion(status, "0","0")
            }
        }
    }
    
    //Update Booking webService
    static func updateAce(bookingId: Int, date: String, startTime: String, endTime: String, completion:@escaping(_ status: Bool, _ message: String, _ noCard: Bool) -> Void) {
        
        let param : [String:Any] = ["profile_id" : IK_USER.profile_id ?? "",
                                    "booking_id" : bookingId,
                                    "date"       : date,
                                    "start_time" : startTime,
                                    "end_time"   : endTime]
        
        post(url: WebURL.updateBooking, parameter: param) { (response, status) in
            guard !status else {
                completion(status, response["message"] as! String,false)
                return
            }
            
            if let data = response["data"] as? NSDictionary {
                if let card = data["card_saved"] as? Int {
                    completion(status, response["message"] as! String, card == 0)
                } else {
                    completion(status, response["message"] as! String, false)
                }
            }
            else {
                completion(status, response["message"] as! String, false)
            }
            
        }
        
    }
    
    static func bookAce(aceId:String, center : Int , date : String, startTime: String , endTime : String ,person:Int, amount:String, acePrice:String , meetingPoint: String, helpText: String , completion:@escaping(_ status : Bool, _ message : String, _ noCard : Bool) -> Void) {
        
        let param : [String : Any] =  ["profile_id":IK_USER.profile_id,
                                       "ace_profile_id":aceId,
                                     //  "cat_id":sportId,
                                     //  "city_id":locationId,
                                       "gym_id" : center,
                                       "date":date,
                                       "start_time":startTime,
                                       "end_time":endTime,
                                       "no_of_people":person,
                                      // "nutritionist":nutrition ,
                                       "price":amount,
                                       "ace_price":acePrice,
                                       "meeting_point":meetingPoint,
                                      // "latitude":coordinate.latitude,
                                      // "longitude":coordinate.longitude,
                                      // "address":address
            "help_text" : helpText
        ]
        
        post(url: WebURL.bookAce, parameter: param) { (response, status) in
            guard !status else {
                completion(status, response["message"] as! String,false)
                return
            }
            
            if let data = response["data"] as? NSDictionary {
                if let card = data["card_saved"] as? Int {
                    completion(status, response["message"] as! String, card == 0)
                } else {
                    completion(status, response["message"] as! String, false)
                }
            }
            else {
                completion(status, response["message"] as! String, false)
            }
        }
    }
    
    static func addAcePaymentDetail(param:[String:Any], identity:UIImage,  completion:@escaping(_ status : Bool, _ message : String) -> Void) {
//        upload(url: WebURL.addBankDetail,
//               parameter: param,
//               uploadings: ["doc_image":FileManager.default.saveToDocs(data: UIImageJPEGRepresentation(identity, 1)!, name: "abc.jpeg")], completion: { (response, status) in
//                completion(status, response["message"] as! String)
//        })

        upload(url: WebURL.addBankDetail,
               parameter: param,
               uploadings: ["doc_image":FileManager.default.saveToDocs(data: identity.jpegData(compressionQuality: 1)!, name: "abc.jpeg")], completion: { (response, status) in
                completion(status, response["message"] as! String)
        })
    }
    
    static func addCardDetail(token:String,completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        
        let param = ["user_id":IK_USER.profile_id,
                     "email":IK_USER.email,
                     "customer_token":token]
        
        post(url: WebURL.addCard, parameter: param as [String : Any]) { (response, status) in
            completion(status, response["message"] as! String)
        }
    }
    
    static func getCardList(isBackground:Bool = false , completion:@escaping(_ status : Bool,_ arrCard : [Cards], _ message : String) -> Void) {
        post(url: WebURL.getCards, parameter: ["user_id":IK_USER.profile_id] , indicator:!isBackground) { (response, status) in
            
            if status {
                let arrData = response["data"] as! [NSDictionary]
                var arr : [Cards] = []
                
                for dic in arrData {
                    let obj = Cards(dict: dic)
                    arr.append(obj)
                }
                completion(status, arr,"")
            }
            else {
                completion(status, [],response["message"] as! String)
            }
        }
    }
    
    static func removeCard(card_token:String,completion:@escaping(_ status : Bool , _ message : String) -> Void) {
        
        let param = ["customer_token":card_token,"user_id":IK_USER.profile_id]
        
        post(url: WebURL.deleteCard, parameter: param) { (response, status) in
            completion(status, response["message"] as! String)
        }
    }
    
    static func cancelWorkout(bookingId:String,completion:@escaping(_ status : Bool , _ message : String) -> Void) {
        let param = ["user_id":IK_USER.profile_id ,"booking_id":bookingId]
        
        post(url: WebURL.cancelWorkout, parameter: param) { (response, status) in
            completion(status, response["message"] as! String)
        }
    }
    
    static func cancelBooking(bookingId:Int,completion:@escaping(_ status : Bool , _ message : String) -> Void) {
        let param = ["user_id":IK_USER.profile_id ,"request_id":bookingId] as [String : Any]
        
        post(url: WebURL.cancelBooking, parameter: param) { (response, status) in
            completion(status, response["message"] as! String)
        }
    }
    
    static func makePrimary(card_token:String,completion:@escaping(_ status : Bool , _ message : String) -> Void) {
        
        let param = ["customer_token":card_token,"user_id":IK_USER.profile_id]
        post(url: WebURL.makeCardPrimary, parameter: param) { (response, status) in
            completion(status, response["message"] as! String)
        }
    }
    
    static func getUserSchedule(completion:@escaping(_ status :Bool, _ arrBooking : [Bookings] ,_ arrComplete : [Bookings]  , _ message : String) -> Void) {
        
        post(url: WebURL.userSchedule, parameter: ["profile_id":IK_USER.profile_id]) { (response, status) in
            
            if status {
                var booking : [Bookings] = []
                var complete : [Bookings] = []
                
                if let data = response["data"] as? NSDictionary {
                    if let arrBooking = data["booked"] as? [NSDictionary] {
                        for b in arrBooking {
                            booking.append(Bookings(dict: b))
                        }
                    }
                    
                    if let arrComplete = data["completed"] as? [NSDictionary] {
                        for c in arrComplete {
                            complete.append(Bookings(dict: c))
                        }
                    }
                    completion(status,booking, complete, response["message"] as! String)
                } else {
                    completion(status, [], [], response["message"] as! String)
                }
            }
            else {
                completion(status, [], [], response["message"] as! String)
            }
        }
    }
    
    static func sumitHelp(comment:String,completion:@escaping(_ status : Bool , _ message : String) -> Void) {
        
        let param = ["message":comment,"user_id":IK_USER.profile_id]
        post(url: WebURL.help, parameter: param) { (response, status) in
            completion(status, response["message"] as! String)
        }
    }
    
    static func updateAceProfile(param : [String:Any], images : [String : URL] , completion:@escaping(_ status : Bool,  _ message : String, _ imageUrl : String?) -> Void) {
        
        if !images.isEmpty {
            upload(url: WebURL.updateAceProfile,parameter: param,uploadings: images,
                   completion: { (response, status) in
                    if status {
                        let data = response["data"] as! NSDictionary
                        completion(status,response["message"] as! String , data["profile_image"] as! String)
                    } else {
                        completion(status, response["message"] as! String , nil )
                    }
            })
        } else {
            post(url: WebURL.updateAceProfile, parameter: param, completion: { (response, status) in
                if status {
                    let data = response["data"] as! NSDictionary
                    completion(status, response["message"] as! String, nil)
                } else {
                    completion(status, response["message"] as! String, nil)
                }
            })
        }
    }
    
    static func updateUserProfile( firstName:String ,lastName: String, profileImage : UIImage? ,completion:@escaping(_ status : Bool, _ message : String , _ imageUrl : String?) -> Void) {
        
        let param = [ "user_id" : IK_USER.profile_id,
                      "first_name" : firstName,
                      "last_name" : lastName ]
        
        if profileImage != nil {
//            upload(url: WebURL.updateUserProfile,parameter: param,uploadings: ["profile_image" : FileManager.default.saveToDocs(data:UIImageJPEGRepresentation(profileImage!, 0.6)!, name: "profileImage.png")],

            upload(url: WebURL.updateUserProfile,parameter: param,uploadings: ["profile_image" : FileManager.default.saveToDocs(data:profileImage!.jpegData(compressionQuality: 0.6)!, name: "profileImage.png")],
                   completion: { (response, status) in
                    if status {
                        let data = response["data"] as! NSDictionary
                        completion(status,response["message"] as! String ,data["image"] as! String)
                    } else {
                        completion(status, response["message"] as! String,nil)
                    }
            })
        } else {
            post(url: WebURL.updateUserProfile, parameter: param, completion: { (response, status) in
                if status {
                    let data = response["data"] as! NSDictionary
                    completion(status, response["message"] as! String, nil)
                } else {
                    completion(status, response["message"] as! String,nil)
                }
            })
        }
    }
    
    static func getBankingDetail( completion:@escaping(_ detail: BankingDetails?, _ message : String) -> Void) {
        post(url: WebURL.getBankDetail, parameter: ["user_id":IK_USER.profile_id]) { (response, status) in
            
            if status {
                let arrData = response["data"] as! NSDictionary
                
                completion(BankingDetails(dict: arrData), "")
            }
            else {
                completion(nil,response["message"] as! String)
            }
        }
    }
    
    static func getAceProfile(completion:@escaping(_ response : NSDictionary? ,_ status : Bool , _ message : String?) -> Void) {
        post(url: WebURL.getAceProfile, parameter: ["user_id":IK_USER.profile_id,"acer_user_id":IK_USER.profile_id]) { (response, status) in
            if status {
                let data = response["data"] as! NSDictionary
                completion(data,status,nil)
            } else {
                completion(nil,status,(response["message"] as! String))
            }
        }
    }
    
    static func rateAceSession(aceId:String, bookingId: String, rating:Int , feedback:String,completion:@escaping(_ status : Bool , _ message : String?) -> Void) {
        
        let param =  ["user_id" : IK_USER.profile_id,
                      "who_receive" : aceId,
                      "request_id" : bookingId,
                      "rate_count" : rating,
                      "feedback" : feedback] as [String : Any]
        
        post(url: WebURL.rateAce, parameter: param) { (response, status) in
            completion(status,(response["message"] as! String))
        }
    }
    
    static func getSetting(completion:@escaping( _ language:String?, _ status:Bool) -> Void) {
        post(url: WebURL.setting, parameter: ["user_id" : IK_USER.profile_id], indicator : false ) { (response, status) in
            if status {
                let data = response["data"] as! NSDictionary
                let ln = data["language"] as? String ?? "EN"
                IK_DEFAULTS.set(ln, forKey: LANGUAGE)
                IK_DEFAULTS.synchronize()
                settingLn = ln
                completion( ln, status)
            } else {
                completion("EN" , status)
            }
        }
    }
    
    static func updateSetting(language : String ,completion:@escaping( _ status:Bool) -> Void) {
        post(url: WebURL.updateSetting, parameter: ["user_id" : IK_USER.profile_id,"language":language], indicator : false ) { (response, status) in
            completion(status)
            guard status else {return}
            
            IK_DEFAULTS.set(language, forKey: LANGUAGE)
            IK_DEFAULTS.synchronize()
            settingLn = language
        }
    }
    
    static func getNotification(completion:@escaping(_ status:Bool ,_ notification:[UserNotification] , _ message:String ) -> Void) {
        post(url: WebURL.getNotification, parameter: ["user_id" : IK_USER.profile_id]) { (response, status) in
            if status {
                let arrData = response["data"] as! [NSDictionary]
                var arr : [UserNotification] = []
                
                for dic in arrData {
                    let obj = UserNotification(dict: dic)
                    arr.append(obj)
                }
                completion(status, arr,"")
            }
            else {
                completion(status, [],response["message"] as! String)
            }
        }
    }
    
    static func checkStatus(completion :@escaping( _ status : Bool , _ arrBooking : [RatingDetails] , _ isAce : Bool) -> Void ) {
        
        post(url: WebURL.checkActivityStatus, parameter: ["profile_id" : IK_USER.profile_id], indicator: false) { (response, status) in
            if status {
                let data = response["data"] as! NSDictionary
                var arrBooking : [RatingDetails] = []
                
                if let arr = data["booking"] as? [NSDictionary] {
                    for dic in arr {
                        let obj = RatingDetails(dict: dic)
                        arrBooking.append(obj)
                    }
                }
                
                let isAce = Int( String(describing:data["is_ace"]!))
                completion(true,arrBooking, isAce == 1)
            }
            else {
                completion(false, [],false)
            }
        }
    }
    
    static func trackActivity(aceID:String, isChat: Bool = false) {
        post(url: isChat ? WebURL.trackChat : WebURL.trackProfileView, parameter: ["ace_profile":aceID,"customer_profile":IK_USER.profile_id], indicator: false) { (response, status) in
        }
    }
    
    //MARK:- Chatting
    //MARK:-
    
    static func createChannel(with user : String, completion :@escaping( _ status : Bool , _ channelUrl : String , _ message : String) -> Void ) {
        post(url: WebURL.createChannel, parameter: ["sender_profile":IK_USER.profile_id,"receiver_profile":user]) { (response, status) in
            if status {
                let data = response["data"] as! NSDictionary
                completion(status, data["channel_url"] as! String, response["message"] as! String)
            } else {
                completion(status,"",response["message"] as! String)
            }
        }
    }
    
    static func logout(completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        
        let param : [String : Any] = ["user_id":IK_USER.profile_id]
        
        post(url: WebURL.logout, parameter: param) { (response, status) in
            completion(status, response["message"] as! String)
        }
    }
    
    static func avilabilityBadge(completion:@escaping(_ status : Bool , _ count : Int) -> Void) {
        post(url: WebURL.availabilityBadge, parameter: ["user_id":IK_USER.profile_id], indicator: false) { (response, status) in
            let count = response["data"] as? Int ?? 0
            completion(status,count)
        }
    }
    
    static func clearBadge(completion:@escaping(_ status : Bool) -> Void) {
        post(url: WebURL.clearBadge, parameter: ["user_id":IK_USER.profile_id], indicator: false) { (response, status) in
            completion(status)
        }
    }
    
    static func getAceDetail(aceId: String ,completion:@escaping(_ status : Bool, _ objAce : Ace) -> Void) {
        post(url: WebURL.aceProfileDetail, parameter: ["profile_id":aceId], indicator: false) { (response, status) in
            if status {
                completion(status,Ace(dict: response["data"] as! NSDictionary))
            }
            else {
                completion(status,Ace())
            }
        }
    }
    
    //MARK:- Event
    
    static func getEventType(completion:@escaping(_ status : Bool, _ type: [(id:Int, title:String)]) -> Void) {
        get(url: WebURL.marathonType, indicator: true) { (response, status) in
            if status {
                let data = response["data"] as! [NSDictionary]
                var arrType : [(id:Int, title:String)] = []
                
                for dic in data {
                    let type = (id:dic["id"] as! Int,
                                title:dic["title"] as! String)
                    arrType.append(type)
                }
                completion(status,arrType)
            } else {
                completion(status,[])
            }
        }
    }
    
    static func getWeekDays(completion:@escaping(_ status : Bool, _ day: [(id:Int, title:String)]) -> Void) {
        get(url: WebURL.marathonWeek, indicator: true) { (response, status) in
            if status {
                let data = response["data"] as! [NSDictionary]
                var arrDay : [(id:Int, title:String)] = []
                
                for dic in data {
                    let type = (id:dic["id"] as! Int,
                                title:dic["exercise"] as! String)
                    arrDay.append(type)
                }
                completion(status,arrDay)
            } else {
                completion(status,[])
            }
        }
    }
    
    static func getPlans(completion:@escaping(_ status : Bool, _ day: [(id:Int, price:Int)]) -> Void) {
        get(url: WebURL.traningSchedule, indicator: true) { (response, status) in
            if status {
                let data = response["data"] as! [NSDictionary]
                var arrDay : [(id:Int, price:Int)] = []
                
                for dic in data {
                    let type = (id:dic["id"] as! Int,
                                price:dic["price"] as! Int)
                    arrDay.append(type)
                }
                completion(status,arrDay)
            } else {
                completion(status,[])
            }
        }
    }

  //  {"profile_id":"51526626121","training_type":"3","training_week":"2"}

    static func purchasePlan(type: Int, day : Int, plan:Int, code:String? ,completion:@escaping(_ status : Bool, _ message : String) -> Void) {
        let param =  ["profile_id" : IK_USER.profile_id,
                      "training_type" : type,
                      "training_week" : day,
                      "training_package" : plan,
                      "promo_code":code ?? ""] as [String : Any]
        
        post(url: WebURL.purchasePlan, parameter: param, indicator: true) { (response, status) in
            if status {
                completion(status, response["message"] as? String ?? "")
            } else {
                completion(status, response["message"] as! String)
            }
        }
    }
    
    static func getPersonalTrainers(completion:@escaping(_ status : Bool, _ arrAce : [Ace]) -> Void) {
        get(url: WebURL.personalTrainer, indicator: true) { (response, status) in
            if status {
                
                let arrData = response["data"] as! [NSDictionary]
                var arr : [Ace] = []
                
                for dic in arrData {
                    let obj = Ace(dict: dic)
                    arr.append(obj)
                }
                completion(status, arr)
            } else {
                completion(status,[])
            }
        }
    }
    
    static func getWeeklySchedule(completion:@escaping(_ status : Bool,_ arrSchedule:[WeekSchedule]) -> Void) {
    
        post(url: WebURL.weekSchedule, parameter: ["profile_id" : IK_USER.profile_id]) { (response, status) in
            if status {
                let arrData = response["data"] as! [NSDictionary]
                var arr : [WeekSchedule] = []
                
                for dic in arrData {
                    let obj = WeekSchedule(dict: dic)
                    arr.append(obj)
                }
                completion(status, arr)
            } else {
                completion(status,[])
            }
        }
    }
    
    static func getDemoSchedule( type:Int , weekInDay:Int ,  completion:@escaping(_ status : Bool,_ arrSchedule:[WeekSchedule]) -> Void) {
        let param = ["training_type":type,
                     "training_week":weekInDay,
                     "profile_id" : IK_USER.profile_id] as [String : Any]
        
        post(url: WebURL.weekDemoSchedule, parameter: param) { (response, status) in
            if status {
                let arrData = response["data"] as! [NSDictionary]
                var arr : [WeekSchedule] = []
                
                for dic in arrData {
                    let obj = WeekSchedule(dict: dic)
                    arr.append(obj)
                }
                completion(status, arr)
            } else {
                completion(status,[])
            }
        }
    }
    
    static func getCurrentActivityList(completion:@escaping(_ status : Bool, _ arrActivity: [Acitvity]) -> Void) {
        
        post(url: WebURL.currentActivity, parameter: ["profile_id":IK_USER.profile_id]) { (response, status) in
            if status {
                let arrData = response["data"] as! [NSDictionary]
                var arr : [Acitvity] = []
                
                for dic in arrData {
                    let obj = Acitvity(dict: dic)
                    arr.append(obj)
                }
                completion(status, arr)
            } else {
                completion(status,[])
            }
        }
    }
    
    static func saveCurrentActivity(activity : [[String:Int]], completion:@escaping(_ status : Bool,_ message: String) -> Void ) {
        
        let param: [String:Any] = ["profile_id": IK_USER.profile_id,
                                   "activity":activity]
        
//        {"profile_id": 41525668848,"activity":[{"sykling":1},{"svomming":1},{"langrenn":1},{"rulleski":1},{"styrke":1},{"fotball":4},{"squash":2}]}
        
        post(url: WebURL.saveCurrentActivity, parameter: param) { (response , status) in
            completion(status,response["message"] as! String)
        }
    }
    
    //MARK:-
    //MARK: EKeys
    static func getEKeys(phoneno : String ,completion:@escaping(_ status: Bool, _ message: String, _ delayTime : Int64, _ serverMsg:String) -> Void) {
        
        let param = ["user_id":IK_USER.profile_id,
                     "phone_number":phoneno]
        
        post(url: WebURL.getEKeys, parameter: param, indicator: false) { (response, status) in
            if status {
                if let userData = response["eKey"] {
                    Defaults.set(userData, forKey: DefaultsKeys.eKey.rawValue)
                }
                if let userData = response["eKeyReference"] {
                    Defaults.set(userData, forKey: DefaultsKeys.eKeyReference.rawValue)
                }
                if let userData = response["serialNumbers"] as? NSArray {
                    Defaults.set(userData, forKey: DefaultsKeys.serialNumbers.rawValue)
                }
                completion(status, TextMessage.keySuccessfuly, response["delay_time"] as! Int64, response["message"] as! String)
            }
            else if let msg = response["message"]
            {
                completion(status, msg as? String ?? "", 0, "" )
            }
            else
            {
                completion(status, "Some think went wrong.\nPlease try again.", 0, "")
            }
        }
    }

    
}

extension WebConnect {
    
    /// Post method to call post API.
    ///
    /// - Parameters:
    ///   - url: Api url to call
    ///   - parameter: Parameter to pass in body.
    ///   - indicator: Boolean value to maintain should show indicator or call api in background
    ///   - completion: Completion handler to handel response.
    ///                 Completion  handler will return received Json response and Api status for success/failure
    fileprivate static func post(url : String , parameter : [String : Any] , indicator : Bool = true , completion : @escaping(_ result : NSDictionary , _ status : Bool) -> Void) {
        
        
        guard  Utils.shared.checkInternet() else {
            let result = ["status" : false as Any , "message" : "Sjekk nettverkskoblingen din." as Any]
            completion(result as NSDictionary , false)
            return
        }
        
        if indicator { FUProgressView.showProgress()  }
        
        //print("\n\n ######################################### \n\(parameter)\n-----------------------------")
        //print("\n API  = ",url)
        Alamofire.request( url,
                           method: .post,
                           parameters: parameter,
                           encoding: JSONEncoding.default,
                           headers: header)
            .responseJSON(completionHandler: { response in
                if indicator {  FUProgressView.hideProgress() }
                
                DispatchQueue.main.async {
                    
                    if WebConnect.isHeaderExpire(response: response)
                    {
                        return
                    }
                    
                    if let JSON = response.result.value {
                        //print("-----------------------------\n\(response)\n#########################################")
                        let status = Int( String(describing:(JSON as! NSDictionary)["success"]!))
                        completion(JSON as! NSDictionary , status == 1)
                    }
                    else {
                        //print(String(data: response.data!, encoding: .utf8))
                        let result = ["status" : false as Any , "message" : "Some error occoured. Please try again." as Any]
                        completion(result as NSDictionary , false)
                    }
                }
                
            })
    }
    
    /// Get method to call get api
    ///
    /// - Parameters:
    ///   - url:  Api url to call
    ///   - indicator:  Boolean value to maintain should show indicator or call api in background
    ///   - completion: Completion handler to handel response.
    ///                 Completion  handler will return received Json response and Api status for success/failure
    fileprivate static func get(url : String , indicator : Bool = true , completion : @escaping(_ result : NSDictionary , _ status : Bool) -> Void) {
        
        guard  Utils.shared.checkInternet() else {
            let result = ["status" : false as Any , "message" : "Sjekk nettverkskoblingen din." as Any]
            completion(result as NSDictionary , false)
            return
        }
        
        if indicator { FUProgressView.showProgress()  }
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            DispatchQueue.main.async {
                if indicator {  FUProgressView.hideProgress() }
                
                if WebConnect.isHeaderExpire(response: response)
                {
                    return
                }
                
                if let JSON = response.result.value {
                    //print("-----------------------------\n\(response)\n-----------------------------")
                    let status = Int( String(describing:(JSON as! NSDictionary)["success"]!))
                    completion(JSON as! NSDictionary , status == 1)
                }
                else {
                    //print(String(data: response.data!, encoding: .utf8))
                    let result = ["status" : false as Any , "message" : "Some error occoured. Please try again." as Any]
                    completion(result as NSDictionary , false)
                }
            }
        }
    }
    
    /// Post method to call post api and upload images/videos/files in mulyipart form data.
    ///
    /// - Parameters:
    ///   - url:  Api url to call
    ///   - parameter: Parameter to pass in body.
    ///   - uploadings: Key,Value pair array to upload attachments on server. Every key will be paramtere name for file and URL is path of file to attach.
    ///   - indicator:  Boolean value to maintain should show indicator or call api in background
    ///   - completion: Completion handler to handel response.
    ///                 Completion  handler will return received Json response and Api status for success/failure
    ///   - progressHandler: Progress handler to handel uploading progress of attached files.
    ///                 Completion  handler will return received Completed unit fom total and Total unit to upload.
    fileprivate static func upload(url : String , parameter : [String : Any] , uploadings : [String:URL] , indicator : Bool = true , completion:  @escaping(_ result : NSDictionary , _ status : Bool) -> Void, progressHandler: @escaping (Int, Int) -> Void = { _,_  in })  {
        
        guard  Utils.shared.checkInternet() else {
            let result = ["status" : false as Any , "message" : "Sjekk nettverkskoblingen din." as Any]
            completion(result as NSDictionary , false)
            return
        }
        
        if indicator { FUProgressView.showProgress()  }
        //print("######################################### \n\(parameter)\n-----------------------------")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameter {
                if let str = value as? String {
                    multipartFormData.append(str.data(using: .utf8 )! , withName: key)
                }
                else if var inte = value as? Int {
                    let data = Data(buffer: UnsafeBufferPointer(start: &inte, count: 1))
                    multipartFormData.append(data , withName: key)
                }
                else {
                    for str in value as! [String] {
                        multipartFormData.append(str.data(using: .utf8 )! , withName: key)
                    }
                }
            }
            for (key,value) in uploadings {
                multipartFormData.append(value, withName: key)
            }
        }, to: url, method: .post, headers: header , encodingCompletion: { encodingResult in
            switch encodingResult {
                
            case .success(let upRequest, _, _):
                
                upRequest.uploadProgress(closure: { (progress) in
                    progressHandler(Int(progress.completedUnitCount), Int(progress.totalUnitCount))
                })
                
                upRequest.responseJSON {  response in
                    DispatchQueue.main.async {
                        if indicator {  FUProgressView.hideProgress() }
                        
                        if let JSON = response.result.value {
                            //print("-----------------------------\n\(response)\n#########################################")
                            let status = Int( String(describing:(JSON as! NSDictionary)["success"]!))
                            completion(JSON as! NSDictionary , status == 1)
                        }
                        else {
                            //print(String(data: response.data!, encoding: .utf8))
                            let result = ["status" : false as Any , "message" : "Some error occoured. Please try again." as Any]
                            completion(result as NSDictionary , false)
                        }
                    }
                }
                break
            case .failure( _):
                if indicator {  FUProgressView.hideProgress() }
            }
        })
    }
    
    static func isHeaderExpire(response : DataResponse<Any>) -> Bool
    {
        if response.response?.statusCode == 401
        {
            if let JSON = response.result.value as? NSDictionary
            {
                if let msg = JSON["message"] as? String
                {
                    Utils.shared.headerExpireForLogout(message: msg)
                    return true
                }
            }
        }
        return false
    }
}
