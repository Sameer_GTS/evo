//
//  Constant.swift
//  StyleOnQ
//
//  Created by Chanchal Warde on 3/7/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import CoreLocation

///--- Defaults
let DEVICE_TOKEN = "DEVICE_TOKEN"
let USER_DETAIL = "USER_DETAIL"
let CITIES = "CITIES"
let ACE_CTIES = "ACE_CTIES"
let SPORTS = "SPORTS"
let LANGUAGE = "LANGUAGE"
let MARATHON_PLAN = "MARATHON_PLAN"
let GYM_LIST = "GYM_LIST"
let PT_LEVELS = "PT_LEVELS"
let GYM_LIST_ALL = "GYM_LIST_ALL"
let BLOGS_LIST = "BLOGS_LIST"

let LOCK_GYM_LIST = "LOCK_GYM_LIST"
let LOCK_GYM_CURRENT = "LOCK_GYM_CURRENT"

let trackEvent = "trackEvent"


///--- Constant objects
/// AppDelegate object to access comman objects.
let IK_DELEGATE =  UIApplication.shared.delegate as! AppDelegate
/// User Defaults constant object
let IK_DEFAULTS =  UserDefaults.standard
/// Notification default object
let IK_NOTIFICATION =  NotificationCenter.default
/// Current Loggedin user detail
var IK_USER : CurrentUser {
    return CurrentUser(dict: IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary)
}

var DATE_FORMAT = "MMMM dd, yyyy"
var TIME_FORMAT = "hh:mm a"

let NUTRITION : Int = 65235

///- Global Data

var MarathonScheduleDetail : (type:Int, days:Int, plan:Int) = (type:0, days:0, plan:0)
var track : (start:Date,end:Date) =  (start:Date(),end:Date())

///- Notification

let NOTIFICATION_HOME = NSNotification.Name(rawValue: "RefreshHome")
let NOTIFICATION_WORKOUT = NSNotification.Name(rawValue: "RefreshWorkout")
let NOTIFICATION_BADGE = NSNotification.Name(rawValue: "RefreshBadge")

//---- Messages

//Error

let ERROR_PHONE = "Ditt telefonnr."
let ERROR_OTP = "Skriv inn riktig OTP som du har fått på mail."
let ERROR_NAME = "Fyll inn navn."
let ERROR_EMAIL = "Fyll inn e-post."
let ERROR_VALID_EMAIL = "Skriv riktig e-post."
let ERROR_INTEREST = "Velg interesse"
let ERROR_AGE = "Velg aldersgruppe"
let ERROR_FITNESS = "Velg hvilket nivå du er på"
let ERROR_IMAGE = "Last opp profilbilde."
let ERROR_HELP = "Skriv hva du kan hjelpe andre med."
let ERROR_HELP_EXISTS = "Legg til flere."
let ERROR_TIME = "Velg starttid og sluttid ."
let ERROR_NO_CARD = "Legg til kortinformasjon for å fullføre bestillingen."
let ERROR_MAX_SPORT = "Du kan velge opp til 3 interesser"
let ERROR_INVALID_CARD = "Please enter a valid card details"

let ALERT_INTERNET = "Sjekk nettverkskoblingen din."
let ALERT_APPLIED = "Vi sender deg en bekreftelse innen 48t."
let ALERT_ALREADY_APPLIED = "Du har allerede søkt om å bli EVO-trener. Vennligst vent til vi svarer deg."

let CONFIRM_UPDATE = "Vil du lagre endringer?"
let CONFIRM_LOGOUT = "Er du sikker på at du vil logge ut?"
let CONFIRM_DELETE_CARD = "Er du sikker på at du vil fjerne dette kortet?"
let CONFIRM_CANCEL_WORKOUT = "Er du sikker på at du vil kansellere bestillingen?"

let REJECT_AVAIL = "Bekreft tilgjengeligheten din"
let ACCEPT_AVAIL = "Kunden vil få beskjed om at du er ledig."
let REJECT_BOOKING = "Er du sikker på at du vil avvise bestillingen?"
let ACCEPT_BOOKING = "Er du sikker på at du vil akseptere bestillingen?"

//--- Keys

let KEY_GOOGLE_LOCATION = "AIzaSyCVWDsdA6GRiVKRK-EVpEJI7HSlJdHtbQ8" //"AIzaSyAJad98vAofq1pLcnmK7txnRTcAOz1UwpQ"
let KEY_GOOGLE = "201768177705-s71qf370tv9jn58979upgu23idae8l5o.apps.googleusercontent.com"

let KEY_STRIPE = "pk_live_0iQeWxGG36MdMpMiByVIkpjm"         /////"pk_test_OTKwKJP0OPyGCi3iTxrsOp6F" pk_live_0iQeWxGG36MdMpMiByVIkpjm
let SECRET_STRIPE = "sk_live_dehOiQTvN3NG5ugrpIJIOayA"   /////"sk_test_5pMxnnEtiEsQSwPtHmUTFy9G"  sk_live_dehOiQTvN3NG5ugrpIJIOayA
let SENDBIRD_ID = "9350AA49-C61D-4C03-B8D6-9F9E1A3574C7"

let MIXPANEL_TOKEN = "c3b5e3857abc85564080c2439e5f920e"
let MIXPANEL_SECRET = "a70d7ca81c019e4583237908dc9d9899"

let Flurry_ApiKey_iOS = "9G4RCK9WBMZBQSF2857W"

/// Sports icons
let SportImage = ["Langrenn" : #imageLiteral(resourceName: "icn_skiing"),
                  "Løping" : #imageLiteral(resourceName: "icn_running"),
                  "Sykling" : #imageLiteral(resourceName: "icn_cycling"),
                  "Styrke" : #imageLiteral(resourceName: "icn_barbell"),
                  "Svømming" : #imageLiteral(resourceName: "icn_swimming"),
                  "Fotball" : #imageLiteral(resourceName: "icn_soccer"),
                  "Tennis": #imageLiteral(resourceName: "icn_tennis"),
                  "Golf" : #imageLiteral(resourceName: "icn_golf"),
                  "Basketball" : #imageLiteral(resourceName: "icn_basketball"),
                  "Amerikansk fotball" : #imageLiteral(resourceName: "icn_football"),
                  "Innebandy" : #imageLiteral(resourceName: "icn_filed_hockey"),
                  "Ishockey" : #imageLiteral(resourceName: "icn_ice_hockey") ,
                  "Alpint": #imageLiteral(resourceName: "icn_alpine_ski"),
                  "Boksing":#imageLiteral(resourceName: "icn_boxing"),
                  "MMA": #imageLiteral(resourceName: "icn_mma") ,
                  "Massør":#imageLiteral(resourceName: "icn_massage"),
                  "Fysioterapeut": #imageLiteral(resourceName: "icn_physio")]

/// Sports icons
let SportSmImage = ["Langrenn" :#imageLiteral(resourceName: "icn_small_skiing") ,
                    "Løping" : #imageLiteral(resourceName: "icn_small_running"),
                    "Sykling" : #imageLiteral(resourceName: "icn_small_cycling"),
                    "Styrke" : #imageLiteral(resourceName: "icn_small_barbell"),
                    "Svømming" : #imageLiteral(resourceName: "icn_small_swimming"),
                    "Fotball" : #imageLiteral(resourceName: "icn_small_soccer"),
                    "Tennis": #imageLiteral(resourceName: "icn_small_tennis"),
                    "Golf" : #imageLiteral(resourceName: "icn_small_golf"),
                    "Basketball" : #imageLiteral(resourceName: "icn_small_basketball"),
                    "Amerikansk fotball" : #imageLiteral(resourceName: "icn_small_football"),
                    "Innebandy" : #imageLiteral(resourceName: "icn_small_filed_hockey") ,
                    "Ishockey" : #imageLiteral(resourceName: "icn_small_ice_hockey") ,
                    "Alpint":  #imageLiteral(resourceName: "icn_small_skiing") ,
                    "Boksing": #imageLiteral(resourceName: "icn_small_boxing") ,
                    "MMA": #imageLiteral(resourceName: "icn_small_mma") ,
                    "Massør":#imageLiteral(resourceName: "icn_small_massage"),
                    "Fysioterapeut": #imageLiteral(resourceName: "icn_small_physio")]

/// Sports icons
let CardImages = [
    "Visa" :#imageLiteral(resourceName: "card_visa") ,
    "MasterCard": #imageLiteral(resourceName: "card_mastercard"),
    "American E" : #imageLiteral(resourceName: "card_amex") ,
    "Discover" : #imageLiteral(resourceName: "card_discover"),
    "Diners Club" :  #imageLiteral(resourceName: "card_diners") ,
    "JCB" : #imageLiteral(resourceName: "card_jcb") ,
    "Unknown" : #imageLiteral(resourceName: "card_unknown") ,
    "None" : #imageLiteral(resourceName: "icn_card.png"),]

///Age
let arrAge = ["","under 20 år gammel","21 - 40 år gammel","41 - 50 år gammel","51 år gammel eller eldre"]
///Fitness
let arrFitness = ["","Lite aktiv","På snittet","Mosjonist","Elite"]

///Country Code
var arrCountry :[(id:Int, name:String)] = {
    return   NSLocale.isoCountryCodes.sorted().map({ (code) -> (id:Int, name:String) in
        return (id:1,name:  code)
    })
}()

/// Settings
var settingLn : String = {
    if let ln = IK_DEFAULTS.value(forKey: LANGUAGE) as? String {
        return ln
    } else {0
        WebConnect.getSetting(completion: { (_, _) in
            
        })
        return "EM"
    }
}()


/// City List
var arrGyms : [(id:Int, name:String , address : String )] = {
    if let arr = IK_DEFAULTS.value(forKey: GYM_LIST) as? [NSDictionary] {
        var arrList : [(id:Int, name:String , address : String)] = []
        for dic in arr {
            arrList.append((id:Int(String(describing: dic["id"]!))!,
                            name:dic["name"] as! String,
                            address : String(describing: dic["address"]!)))
        }
        return arrList
    } else {
        WebConnect.getGymList(type: "C", completion: { (_, _) in
        })
        
        return []
    }
}()

/// Lock Gym List
var arrLockGyms : [LockGymModel] = {
    if let arr = IK_DEFAULTS.value(forKey: LOCK_GYM_LIST) as? [NSDictionary] {
        var arrList : [LockGymModel] = []
        for dic in arr {
            let obj = LockGymModel.init(dict: dic)
            arrList.append(obj)
        }
        return arrList
    } else {
        WebConnect.getLockGymsList(completion: { (_, _,_)  in
        })
        
        return []
    }
}()


//Surendra
let Defaults                    =       UserDefaults.standard

//Sameer
let defaultConsent = "consent"

enum consentAgree : Int {
    case consentAgreed = 0
    case Accept = 1
    case Decline = 2
}

enum DefaultsKeys : String {
    case isScanned_gymId   =  "isScanned_"
    case inHardwareId_gymId   =  "inHardwareId_"
    case outHardwareId_gymId   =  "outHardwareId_"

    case profileId      =  "profileId"
    case eKey           =  "eKey"
    case eKeyReference  =  "eKeyReference"
    case serialNumbers  =  "serialNumbers"

}
enum LockTypes : Int {
    case lockIn   =  0
    case lockOut   =  1
}
enum bleStatus : String {
    case connecting =  "Connecting..."
    case opening    =  "Opening..."
    case connected  =  "Connected"
    case doorOpened =  "Door Opened"
    case doorLocked =  "Door Locked"
    case notFound   =  "No lock found.\nTry again!"
    case failed     =  "Failed\nTry again!"
    case disconnected     =  "Disconnected\nTry again!"
}
//enum LockIds : String {
//    case lockInId   =  "1834050006"
//    case lockOutId   =  "1834050003"
//}
struct TextMessage {
    static let pleaseWait   =  "Please wait while we're connecting. it might take up to 60 second."
    static let notFound   =  "No Lock found. Try again!"
    static let doneAllDevice   =  "Done All Devices"
    static let notConnectingPlaseScan = "Unable to connect lock. Please scan again."
    //static let noKeySetup = "You don\'t have access yet. Click on the Key button on header to insert your phone number and get access."
    static let noKeySetup = "Du har ikke tilgang ennå."
    
    static let keySuccessfuly = "Your key is successfully generated. You can now access the lock to open doors."
}



// Jitendra

var isNotchAvailable : Bool = {
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 2436,2688,1792:
            //print("iPhone X")
            return true
        default:
            return false
        }
    }
    return false
}()

/// Blogs List
var arrBlogs : [Blog] = {
    if let data = IK_DEFAULTS.value(forKey: BLOGS_LIST) as? Data {
        
        let arrDict: NSArray = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSArray
        var arrList : [Blog] = []
        for dic in arrDict {
            let obj = Blog(dict: dic as? NSDictionary)
            arrList.append(obj)
        }
        return arrList
    } else {
        return [Blog]()
    }
}()


/// City List
var arrAllGyms : [(id:Int, name:String , address : String )] = {
    if let arr = IK_DEFAULTS.value(forKey: GYM_LIST_ALL) as? [NSDictionary] {
        var arrList : [(id:Int, name:String , address : String)] = []
        for dic in arr {
            arrList.append((id:Int(String(describing: dic["id"]!))!,
                            name:dic["name"] as! String,
                            address : String(describing: dic["address"]!)))
        }
        return arrList
    } else {
        WebConnect.getGymList(type: "A", completion: { (_, _) in
        })
        
        return []
    }
}()


var arrPTLevel : [(id:Int, name:String )] = {
    if let arr = IK_DEFAULTS.value(forKey: PT_LEVELS) as? [NSDictionary] {
        var arrList : [(id:Int, name:String)] = []
        for dic in arr {
            arrList.append((id:Int(String(describing: dic["id"]!))!,
                            name:dic["name"] as! String))
        }
        return arrList
    } else {
        WebConnect.getPTLevel()
        return []
    }
}()

/// City List
var arrCities : [(id:Int, name:String )] = {
    if let arr = IK_DEFAULTS.value(forKey: CITIES) as? [NSDictionary] {
        var arrList : [(id:Int, name:String)] = []
        for dic in arr {
            arrList.append((id:Int(String(describing: dic["id"]!))!,
                            name:dic["name"] as! String))
        }
        return arrList
    } else {
        WebConnect.getCities(keyword:"")
        return []
    }
}()

var arrAceCities : [(id:Int, name:String )] = {
    if let arr = IK_DEFAULTS.value(forKey: ACE_CTIES) as? [NSDictionary] {
        var arrList : [(id:Int, name:String)] = []
        for dic in arr {
            arrList.append((id:Int(String(describing: dic["id"]!))!,
                            name:dic["name"] as! String))
        }
        return arrList
    } else {
        WebConnect.getCities(keyword:"")
        return []
    }
}()

/// Sport List
var arrSports : [Sport] = {
    if let arr = IK_DEFAULTS.value(forKey: SPORTS) as? [NSDictionary] {
        var arrList : [Sport] = []
        for dic in arr {
            var obj = Sport()
            obj.id = Int(String(describing:dic["cat_id"]!) )!
            obj.name = dic["cat_name"] as! String
            obj.image = SportImage[obj.name] ?? #imageLiteral(resourceName: "icn_golf")
            obj.isSelected =   IK_USER.sportId.contains(obj.id)
            arrList.append(obj)
        }
        return arrList
    } else {
        WebConnect.getSport(type:"C",completion:{ status , arr in
        })
        return []
    }
}()


class Constant: NSObject {
    
}

//MARK:- Notification Type
//1. booking notification to acer user    type=2
//2. Acer accept booking request  type=3
//3. Acer decline booking request type=4
//4.If Acer cancels 48hrs before the session  type=5
//5.user cancelled booking             type=6
//6.Acer is approved by admin      type=1
//7.Acer is rejected by admin         type=7
//8.searchAvailabelAce notification to acer           type=8
//9.Accept searchAvailabelAce request             type=9
//10. Reject searchAvailabelAce request       type=10

enum NotificationType : Int {
    case aceApproved = 1
    case bookingrequest = 2
    case bookingAccept = 3
    case bookingReject = 4
    case bookingCancelAce = 12
    case bookingCancel = 6
    case availbilityRequest = 8
    case availbilityAccept = 9
    case availbilityReject = 10
    case newAce = 11
    case sessionComplete = 14
    case seeAce48 = 18
    case seeAce  = 19

    case none = 25
}

//MARK:- Enum

enum UserType : Int {
    case none = 0
    case customer
    case ace
    case requestedForAcer
}

enum CardType : String {
    case visa = "Visa"
    case master = "MasterCard"
    case american = "American E"
    case discover = "Discover"
    case diners = "Diners Club"
    case jcb = "JCB"
    case unknown = "Unknown"
    case none = "None"
}

struct RatingDetails {
    
    var profileId = ""
    var name = ""
    var image = ""
    var bookingId = ""
    var date = ""
    var amount = ""
    
    init(dict : NSDictionary) {
        profileId = String(describing: dict["profile_id"]!)
        bookingId = String(describing: dict["request_id"]!)
        name = String(describing: dict["name"]!)
        image = String(describing: dict["profile_image"]!)
        date = String(describing:dict["booking_date"]!).date!.string(format: DATE_FORMAT)
        amount = String(describing: dict["amount"]!)
    }
}

//MARK:- Structure

struct  Bookings {
    
    var bookingId = ""
    var firstName = ""
    var lastName = ""
    var sportId  = 0
    var sportName = ""
    var date = ""
    var startTime = Date()
    var endTime =  Date()
    var image = ""
    var isNutrition = false
    var amount = ""
    var aceDetail : Ace = Ace()
    var gymName =  ""
    
    init() {
        
    }
    
    init(dict : NSDictionary) {
        bookingId = String(describing: dict["booking_id"]!)
        firstName = String(describing: dict["first_name"]!)
        lastName =  String(describing: dict["last_name"]!)
        sportId  = dict["cat_id"] as! Int
        sportName = arrSports.filter({$0.id == sportId}).first?.name ?? ""
        date = (String(describing:dict["booking_date"]!) + " 00:00:00").date!.string(format: DATE_FORMAT)
        startTime = (String(describing:dict["booking_date"]!) + " " + String(describing:dict["booking_start_time"]!)).date!
        endTime = (String(describing:dict["booking_date"]!) + " " + String(describing:dict["booking_end_time"]!)).date!
        image = String(describing: dict["profile_image"]!)
        
        gymName = String(describing: dict["gym"]!)
        
        if let am =  dict["amount"] as? Int{
            amount = String(describing:am)
        }
        else {
            amount = "0"
        }
        
        if let aceDic = dict["ace"] as? NSDictionary {
            aceDetail = Ace(dict: aceDic)
            aceDetail.sportId = Int(String(describing:dict["cat_id"]!))!
        }
    }
}

struct Sport {
    var id : Int = 0
    var name : String = ""
    var image : UIImage = UIImage()
    var isSelected = false
}

struct Cards {
    var country : String = ""
    var number : String = ""
    var type : CardType = .none
    var expMonth :  String = ""
    var expYear : String = ""
    var isPrimary : Bool = false
    var stId : String = ""
    var stToken : String = ""
    
    init() {
        country  = ""
        number = "Legg til betalingskort"
        expMonth  = ""
        expYear  = ""
        isPrimary  = false
        stId  = ""
        stToken = ""
    }
    
    init(dict : NSDictionary) {
        country = String(describing: dict["card_country"]!)
        number  = "****" + String(describing: dict["card_last_four_digit"]!)
        
        if let ty = CardType(rawValue: String(describing: dict["card_type"]!)) {
            type = ty
        }
        
        isPrimary  = dict["is_primary"] as! Int == 0 ? false : true
        stId  = String(describing: dict["stripe_customer_cardid"]!)
        stToken  = String(describing: dict["stripe_customer_token"]!)
        
        let date =  String(describing: dict["expire_date"]!)
        expMonth  = date.components(separatedBy: "/").first ?? "0"
        expYear  = date.components(separatedBy: "/")[1]
    }
}

struct Request {
    
    var amount : String = ""
    var location : String = ""
    var date : String = ""
    var startTime : Date = Date()
    var endTime : Date = Date()
    var name : String = ""
    var lat : Double = 0.0
    var categoryId: Int = 0
    var categoryName: String = ""
    var long : Double = 0.0
    var noPeople : Int = 0
    var image : String = ""
    var requestId : Int = 0
    var isBooking : Bool = false
    var fitness : Int = 0
    var age : Int = 0
    var profileId : String = ""
    var helpWith : String  = ""
    
    init(dict : NSDictionary) {
        amount = String(describing: dict["ace_price"]!)
        location  = String(describing: dict["gym"]!)
        date = (String(describing:dict["booking_date"]!) + " 00:00:00").date!.string(format: DATE_FORMAT)
        
        startTime = (String(describing:dict["booking_date"]!) + " " + String(describing:dict["booking_start_time"]!)).date!
        endTime = (String(describing:dict["booking_date"]!) + " " + String(describing:dict["booking_end_time"]!)).date!
        
//        startTime = (String(describing:dict["booking_date"]!) + " " +
//            String(describing:dict["booking_start_time"]!)).date!.string(format: TIME_FORMAT)
//        endTime = (String(describing:dict["booking_date"]!) + " " +
//            String(describing:dict["booking_end_time"]!)).date!.string(format: TIME_FORMAT)
        
        name = String(describing:dict["first_name"]!)
        lat = Double(String(describing:dict["lat"]!))!
        long = Double(String(describing:dict["lng"]!))!
        noPeople = Int(String(describing:dict["no_of_people"]!))!
        image = String(describing:dict["profile_image"]!)
        requestId = Int(String(describing:dict["request_id"]!))!
        let status = dict["booking_status"] as! Int
        isBooking = status == 0 ? false : true
        fitness = Int(String(describing:dict["fitness_level"]!))!
        age = Int(String(describing:dict["age_groups"]!))!
        categoryId = Int(String(describing:dict["cat_id"]!))!
        categoryName = String(describing:dict["cat_name"]!)
        profileId = String(describing:dict["profile_id"]!)
        
        if  let help  = dict["help_text"] as? String {
            helpWith = help
        }
    }
    
    /*
     "ace_price" = 2;
     amount = 3040;
     "booking_date" = "2018-03-31";
     "booking_end_time" = "19:00:00";
     "booking_start_time" = "14:00:00";
     "booking_status" = 1;
     city = Oslo;
     "city_id" = 2;
     "first_name" = Kane;
     lat = 0;
     lng = 0;
     "meeting_point" = Ruth;
     name = "Kane Williams";
     "no_of_people" = 2;
     nutritionist = N;
     "profile_image" = "";
     "request_id" = 42;  */
    
    //    address = "Frognerparken,kirkeveien,oslo,norge";
    //    amount = 0;
    //    "booking_date" = "2018-04-02";
    //    "booking_end_time" = "17:00:00";
    //    "booking_start_time" = "16:00:00";
    //    "booking_status" = 0;
    //    "cat_id" = "";
    //    "cat_name" = "";
    //    city = Oslo;
    //    "city_id" = 2;
    //    "first_name" = colin;
    //    "last_name" = munro;
    //    lat = 0;
    //    lng = 0;
    //    "meeting_point" = rjrj;
    //    name = "colin munro";
    //    "no_of_people" = 0;
    //    nutritionist = Y;
    //    "profile_image" = "http://34.193.201.83/ikseed/public/profiles/41522144548/1522144591.jpg";
    //    "request_id" = 270;
}

struct Ace  {
    
    var aboutExp : String = ""
    var adminVerify : Int = 0
    var aceReason : String = ""
    var firstName : String = ""
    var lastName : String = ""
    var helpInputs : String = ""
    var location : String = ""
    var memberof : String = ""
    var profileId : String = ""
    var profileImage : String = ""
    var backgroundImage : String = ""
    var rating : Double = 0.0
    var sportId : Int = 0
    var isNutritionist : Bool = false
    var isChatEnabled : Bool = true
    var isBookEnabled : Bool = true
    
    var ptlevel : String = ""
    
    var gymId : Int = 0

    ///Chanchal #Update this
    
    init() {
        
    }
    
    init(dict : NSDictionary?) {
        aboutExp = dict!["about_experience_work"] as! String
        adminVerify = Int(String(describing:dict!["admin_verify"]!))!
        aceReason  = dict!["become_ace_reasons"] as! String
        firstName  = dict!["first_name"] as! String
        lastName  = dict!["last_name"] as! String
        helpInputs  = dict!["help_inputs"] as! String
        location  = dict!["gym"] as! String
        memberof  = dict!["member_of"] as! String
        profileId  = String(describing:dict!["profile_id"]!)
        profileImage  = dict!["profile_image"] as! String
        
        ptlevel = dict!["pt_level"] as! String
        
        if let gymIdd = dict!["gym_id"]{
            gymId  =  Int(String(describing:gymIdd))!
        }

        let nutrition = dict!["i_am_nutritionist"] as! String
        isNutritionist = nutrition == "Y"
        
        let book = dict!["isBook"] as! Int
        isBookEnabled = !(book == 0)
        
        let chat = dict!["isChat"] as! Int
        isChatEnabled = !(chat == 0)
        
        if let bg = dict!["bg_img"] as? String {
            backgroundImage = bg
        } else {
            backgroundImage = "ab"
        }
        
        if let rate = Double(String(describing:dict!["rating"]!)) {
            rating  = rate
        }
        
        if let catId = dict!["cat_id"]{
            sportId  =  Int(String(describing:catId))!
        }
    }
}


struct Blog  {
    
    var title : String = ""
    var authorName : String = ""
    var html : String = ""
    var leadIn : String = ""
    var publicationEnd : String = ""
    var publicationStart : String = ""
    var thumb : String = ""
    var url : String = ""
    
    init(dict : NSDictionary?) {
        if let strTitle = dict!["title"] as? String {
            title = strTitle
        }
        if let strAuthorName = dict!["author_name"] as? String {
            authorName = strAuthorName
        }
        if let strHtml = dict!["content_html"] as? String {
            html = strHtml
        }
        if let strLeadIn = dict!["lead_in"] as? String {
            leadIn = strLeadIn
        }
        if let strPublicationEnd = dict!["publication_end"] as? String {
            publicationEnd = strPublicationEnd
        }
        if let strPublicationStart = dict!["publication_start"] as? String {
            publicationStart = strPublicationStart
        }
        if let strThumb = dict!["thumbnail"] as? String {
            thumb = strThumb
        }
        if let strUrl = dict!["url"] as? String {
            url = strUrl
        }
    }
}

struct CurrentUser  {
    
    var profile_id: String!
    var email: String!
    var first_name: String!
    var last_name: String!
    var user_name: String!
    var phone_number: String!
    var search_range: String!
    var login_mode: Int!
    var profile_image: String!
    var add_invitation: Int!
    var api_token: String!
    var userType: UserType!
    var user_lat: String!
    var user_long: String!
    var created_at: String!
    var updated_at: String!
    var sportId: [Int]
    var fitness : Int!
    var gym : Int!
    var ageGroup : Int!
    var nutritionist : String!
    var sendbirdToken  : String!
    var consentAgreed : Int!
    
    init() {
        profile_id = ""
        email = ""
        first_name = ""
        last_name = ""
        user_name = ""
        phone_number = ""
        search_range = ""
        login_mode = 0
        profile_image = ""
        add_invitation = 0
        api_token = ""
        userType = .none
        user_lat = ""
        //        user_long = ""
        created_at = ""
        //        updated_at = ""
        sportId = []
        fitness = 0
        ageGroup = 0
        gym = 0
        nutritionist  = "N"
        sendbirdToken = ""
        consentAgreed = 0
    }
    
    init(dict : NSDictionary?) {
        self.init()
        guard dict != nil else {
            return
        }
        
        profile_id =  String(describing:dict!["profile_id"]!)
        email = dict!["user_email"] as! String
        first_name = dict!["first_name"] as! String
        last_name = dict!["last_name"] as! String
        //user_name = dict!["user_name"] as! String
        phone_number = String(describing: dict!["phone_number"] ?? "")
        // search_range = dict!["search_range"] as! String
        login_mode = Int(String(describing:dict!["login_mode"]!))
        profile_image = dict!["profile_image"] as? String ?? "av"
        //   add_invitation = Int(String(describing:dict!["add_invitation"]!))
        api_token = dict!["api_token"] as! String
        let role : Int! = Int(String(describing:dict!["role"]!))!
        userType = UserType(rawValue:role)
        //        user_lat = dict!["user_lat"] as! String
        //        user_long = dict!["user_long"] as! String
        created_at = dict!["created_at"] as! String
        //        updated_at = dict!["updated_at"] as! String
        sendbirdToken = dict!["s_token"] as! String
        
        if let arr = dict!["categories"]  as? [Any] {
            for value in arr {
                sportId.append( Int(String(describing:value))!)
            }
        } else {
            sportId = []
        }
        
        if let value = dict!["fitness_level"] {
            fitness = Int(String(describing:value)) ?? 0
        } else {
            fitness = 0
        }
        
        if let value = dict!["gym_id"] {
            gym = Int(String(describing:value)) ?? 0
        } else {
            gym = 0
        }
        
        
        if let value = dict!["age_groups"] {
            ageGroup = Int(String(describing:value)) ?? 0
        } else {
            ageGroup = 0
        }
        
        if let value = dict!["i_need_nutritionist"] {
            nutritionist = value as! String
        } else {
            nutritionist = "N"
        }
        
        if let value = dict!["consent_agreed"] {
            consentAgreed = value as! Int
        }else {
            consentAgreed = 0
        }
        
        //
        //        fitness = Int(String(describing:dict!["fitness_level"]!)) ?? 0
        //        ageGroup = Int(String(describing:dict!["age_groups"]!))! ?? 0
        //        nutritionist  = dict!["i_need_nutritionist"] as! String
    }
}

struct BankingDetails {
    var firstName: String = ""
    var lastName: String = ""
    var address: String = ""
    var city: String = ""
    var postalCode: String = ""
    var dob: String = ""
    var ibanNumber: String = ""
    var type: String = ""
    var idNumber: String = ""
    var idType : String = ""
    var docImage : String = ""
    
    init(dict: NSDictionary?) {
        if let data = dict  {
            firstName = data["first_name"] as! String
            lastName = data["last_name"] as! String
            address = data["address"] as! String
            city = data["city"] as! String
            postalCode = data["postal"] as! String
            dob = data["dob"] as! String
            ibanNumber = "****\(data["account_number"] as! String)"
            type = data["type"] as! String
            idNumber =  String(describing:data["doc_number"]!) 
            idType = data["doc_type"] as! String
            docImage = data["doc_image"] as! String
        }
    }
}

struct Acitvity {
    
    var name : String = ""
    var key : String = ""
    var total : Int = 0
    
    init(dict:NSDictionary) {
        name = dict["name"] as! String
        key = dict["activity_key"] as! String
       // total = Int( String(describing: dict["total"]!))!
    }
}

struct WeekSchedule {
    
    var title: String = ""
    var comment: String = ""
    var expanded: Bool = false
    var days: [(title:String,description: String, day:Int)] = []
    
    init(dict: NSDictionary) {
        title = dict["title"] as! String
        comment = dict["comment"] as! String
        
        if let arrDays = dict["days"] as? [NSDictionary] {
            for data in arrDays {
                let dayData = (title:data["title"] as! String,
                               description: data["description"] as! String,
                               day:data["days_no"] as! Int)
                
                days.append(dayData)
            }
        }
    }
}

struct UserNotification {
    
    var createdDate : String = ""
    var notifyId : String = ""
    var profileImage : String = ""
    var message : String = ""
    var type : NotificationType = .none
    var aceId : String = ""
    
    init(dict: NSDictionary?) {
        if let data = dict  {
            createdDate = (data["created_date"] as? String)?.date?.string(format: DATE_FORMAT + " " + TIME_FORMAT) ?? Date().string(format: DATE_FORMAT)
            notifyId = data["notification_id"] as! String
            profileImage = data["profile_image"] as! String
            message = data["notification_message"] as! String
            aceId =  String(describing:data["ace_id"]!)
            
            if let no = data["notification_type"] as? Int {
                if let ty = NotificationType(rawValue: no) {
                    type = ty
                }
            }
        }
    }
    
//    "created_date" = 1524220259;
//    "notification_id" = 336ad4eb7deb4d9bf4720b5d05674849;
//    "notification_message" = "Congratulation, you are now an Ace. Log out then sign in and start setting up dates you can make money";
//    "profile_id" = 41522144548;
//    "profile_image" = "http://34.193.201.83/profiles/41522144548/1524117594.png";
}


struct LockGymModel  {
    
    var id : Int = 0
    var name : String = ""
    var address : String = ""
    var serialNo : String = ""
    
    ///Chanchal #Update this
    
    init() {
        
    }
    
    init(dict : NSDictionary?) {
        id = dict!["id"] as! Int
        name = dict!["name"] as! String
        address  = dict!["address"] as! String
        serialNo  = dict!["sr_no"] as! String
    }
    
    func objectToDict() -> [String : Any] {
        var dict = [String  : Any]()
        dict["id"] = self.id
        dict["name"] = self.name
        dict["address"] = self.address
        dict["sr_no"] = self.serialNo
        return dict
    }
    
}
