//
//  customizedView.swift
//  KeptByK
//
//  Created by Surendra Sharma on 04/07/18.
//  Copyright © 2018 GTS. All rights reserved.
//

import UIKit

@IBDesignable final class customizedView: UIView {

    @IBInspectable var startColor: UIColor = UIColor.clear
    @IBInspectable var endColor: UIColor = UIColor.clear
    
    @IBInspectable var horizontal: Bool = false
    let gradient: CAGradientLayer = CAGradientLayer()

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0
    @IBInspectable var borderColor: UIColor = UIColor.clear
    
    @IBInspectable var shadowColor: UIColor = UIColor.clear
    @IBInspectable var shadowOpacity: Float = 0
    @IBInspectable var shadowRadius: CGFloat = 0
    @IBInspectable var shadowOffset: CGSize = CGSize(width: -1, height: 1)
    @IBInspectable var shadowScale: Bool = false
    @IBInspectable var canShowShadow: Bool = false
    @IBInspectable var isDottedborder: Bool = false

    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect)
    {
        gradient.frame = CGRect(x: CGFloat(0),
                                y: CGFloat(0),
                                width: self.frame.size.width,
                                height: self.frame.size.height)
        gradient.colors = [startColor.cgColor ,endColor.cgColor]
      
        if horizontal {
            gradient.startPoint = CGPoint(x: 0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1, y: 0.5)
        }
        
      //  gradient.zPosition = -1
        layer.addSublayer(gradient)
        
        if cornerRadius > 0 {
            self.layer.cornerRadius = cornerRadius
            self.clipsToBounds = true
        }
        
        if canShowShadow{
            showShadow()
        }
        
        if isDottedborder {
            ShowDottedBorder(rect)
        }
        else {
            if borderWidth > 0 {
                self.layer.borderWidth = borderWidth
                self.layer.borderColor = borderColor.cgColor
            }
        }
    }
    
    
    
    /// To show shadow if enabled
    func showShadow()
    {
        self.layer.masksToBounds = false
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        //self.layer.rasterizationScale = shadowScale ? UIScreen.main.scale : 1
    }
    
    /// To Animate color
    func animateColor()
    {
        let animation = CABasicAnimation(keyPath: "colors")
        animation.toValue = [startColor.cgColor, endColor.cgColor]
        animation.duration = 2
        animation.isRemovedOnCompletion = false
        animation.fillMode  = CAMediaTimingFillMode.forwards
        
        gradient.add(animation, forKey: nil)
    }
    
    func ShowDottedBorder(_ rect: CGRect)
    {
        let path = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
        
        UIColor.white.setFill()
        path.fill()
        
        borderColor.setStroke()
        path.lineWidth = borderWidth
        
        let dashPattern : [CGFloat] = [4, 4]
        path.setLineDash(dashPattern, count: 2, phase: 0)
        path.stroke()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = self.bounds
    }
    
    func updateChanges() {
        gradient.colors = [startColor.cgColor,endColor.cgColor]
        self.layoutSubviews()
    }
}
