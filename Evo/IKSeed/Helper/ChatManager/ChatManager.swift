//
//  ChatManager.swift
//  IKSeed
//
//  Created by Chanchal Warde on 4/10/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import SendBirdSDK

protocol ChatChannelDelegate : class {
    func channelUpdate(channel:SBDGroupChannel)
    func channelListUpdated()
    func channelShouldReload()
}

protocol ChatMessageDelegate : class {
    func didRecieveMessage(in channel : SBDGroupChannel , message : SBDUserMessage)
    func messageShouldRefresh()
}

protocol UnreadMessageDelegate : class {
    func unreadCountUpdate(count:Int)
}

class ChatManager: NSObject {
    
    static let shared  = ChatManager()
    static var channelList : [SBDGroupChannel] = {
       return ChatCaching.loadGroupChannels()
    }()
    
    static var unreadCount : Int = 0
    
    private var groupChannelListQuery: SBDGroupChannelListQuery?
    
    weak var messageDelegate : ChatMessageDelegate?
    weak var channelDelegate : ChatChannelDelegate?
    weak var unreadDelegate : UnreadMessageDelegate?
    
    override init() {
        super.init()
        SBDMain.initWithApplicationId(SENDBIRD_ID)
        SBDMain.setLogLevel(SBDLogLevel.none)
        SBDOptions.setUseMemberAsMessageSender(true)
        
        SBDMain.add(self as SBDChannelDelegate, identifier: self.description)
        SBDMain.add(self as SBDConnectionDelegate, identifier: self.description)
    }
    
    private func getChannelList() {
        self.groupChannelListQuery = SBDGroupChannel.createMyGroupChannelListQuery()
        self.groupChannelListQuery?.limit = 100
        self.groupChannelListQuery?.order = SBDGroupChannelListOrder.latestLastMessage
        
        self.groupChannelListQuery?.loadNextPage(completionHandler: { (channels, error) in
//            print("-----------------------------")
//            print("Channels --- ",channels)
//            print("Error", error)
//            print("-----------------------------")
            
            if error != nil {
                return
            }
            ChatManager.channelList.removeAll()
            ChatManager.channelList =  channels!
            self.loadNextChannel()
        })
    }
    
    private func loadNextChannel() {
        if self.groupChannelListQuery != nil {
            if self.groupChannelListQuery?.hasNext == false {
                channelDelegate?.channelListUpdated()
                return
            }
            
            self.groupChannelListQuery?.loadNextPage(completionHandler: { (channels, error) in
                if error != nil {
                    return
                }
                ChatManager.channelList =  ChatManager.channelList + channels!
                //ChatManager.channelList.append(contentsOf: channels)
                self.loadNextChannel()
            })
        }
    }
}

//MARK:- User Methods
//MARK:-

extension ChatManager  {
    
    func connect(userId:String,accessToken : String) {
        SBDMain.connect(withUserId: userId, accessToken: accessToken) { (user, error) in
//            print("?????????????????????")
//            print(user?.userId," --------- ",user?.nickname)
            self.getChannelList()
            self.totalUnread{_ in }
        }
    }
    
    func disconnect()  {
        SBDMain.disconnect {
          ChatManager.channelList.removeAll()
        }
    }
    
    func createChannel(userIds:[String],completion:@escaping(_ channel: SBDGroupChannel?, _ error : SBDError?) -> Void) {
        SBDGroupChannel.createChannel(withUserIds: userIds, isDistinct: true) { (channel, error) in
          completion(channel,error)
        }
    }
    
    func getChannel(channelUrl : String , completion:@escaping(_ channel : SBDGroupChannel? , _ error : SBDError?) -> Void )  {
        SBDGroupChannel.getWithUrl(channelUrl) { (groupChannel, error) in
            completion(groupChannel, error)
            self.refreshChannelList()
        }
    }
    
    func sendMessage(channel : SBDGroupChannel , message: String, completion:@escaping(_ userMessage : SBDBaseMessage? , _ error : SBDError?) -> Void) -> SBDUserMessage {
        return channel.sendUserMessage(message, data: "", customType: "", targetLanguages: [], completionHandler: { (userMessage, error) in
            DispatchQueue.main.async {
                if error != nil {
                    return
                }
                completion(userMessage , error)
            }
        })
    }
    
    func loadMessage(channel : SBDGroupChannel, timestamp : Int64, initial : Bool , completion:@escaping(_ messages : [SBDBaseMessage]? , _ error: SBDError?) -> Void) {
        channel.getPreviousMessages(byTimestamp: timestamp, limit: 100, reverse: !initial, messageType: SBDMessageTypeFilter.user, customType: "") { (messages, error) in
            completion(messages,error)
        }
    }
    
    func getUserChannel(userid:String, otherUser : String, completion:@escaping(_ channel : SBDGroupChannel? ) -> Void ) {
        
        let query = SBDGroupChannel.createMyGroupChannelListQuery()
        query?.limit = 10
        query?.setUserIdsIncludeFilter([userid,otherUser], queryType: SBDGroupChannelListQueryType.and)
        
        guard (query != nil) else { completion(nil); return}
        query!.loadNextPage(completionHandler: { (channels, error) in
            if error != nil {
                completion(nil)
                return
            }
            guard channels!.count > 0 else {   completion(nil); return}
            completion(channels?.first )
        })
    }
    
    func refreshChannelList() {
        getChannelList()
    }
    
    func saveChannels() {
        ChatCaching.dumpChannels(channels: ChatManager.channelList)
    }
    
    func totalUnread(completion:@escaping(_ count : Int) -> Void = { _ in } )  {
        SBDGroupChannel.getTotalUnreadMessageCount(completionHandler: { (count, error) in
            ChatManager.unreadCount = Int(count)
            completion(Int(count) ?? 0)
            
            guard self.unreadDelegate != nil else { return }
            self.unreadDelegate?.unreadCountUpdate(count: Int(count))
        })
    }
}

//MARK:- Connection Delegate
//MARK:-

extension ChatManager : SBDConnectionDelegate {
    func didSucceedReconnection() {
        for channel in ChatManager.channelList {
            channel.refresh(completionHandler: { (error) in  })
        }
        
        totalUnread{_ in }
        
        if channelDelegate != nil {
            channelDelegate?.channelShouldReload()
        }
        
        if messageDelegate != nil {
            messageDelegate?.messageShouldRefresh()
        }
    }
    
    func didStartReconnection() {
    
    }
    
    func didFailReconnection() {
        
    }
    
    func didCancelReconnection() {
        
    }
}

//MARK:- Channel Delegate
//MARK:-

extension ChatManager : SBDChannelDelegate {
    
    func channel(_ sender: SBDBaseChannel, didReceive message: SBDBaseMessage) {
        if sender is SBDGroupChannel {
            totalUnread{_ in }
            let messageReceivedChannel = sender as! SBDGroupChannel
            if ChatManager.channelList.index(of: messageReceivedChannel) != nil {
                ChatManager.channelList.remove(at: ChatManager.channelList.index(of: messageReceivedChannel)!)
            }
            
             //print("------------ \n \n Recieved Message :- \((message as! SBDUserMessage).message) \n Time:- \(message.createdAt) \n \n -------------")
            
            ChatManager.channelList.insert(messageReceivedChannel, at: 0)
            if channelDelegate != nil {
                channelDelegate?.channelUpdate(channel: messageReceivedChannel)
            }
            
            if messageDelegate != nil {
                messageDelegate?.didRecieveMessage(in: messageReceivedChannel, message: message as! SBDUserMessage)
            }
        }
        
    }
    
    func channelWasChanged(_ sender: SBDBaseChannel) {
        if sender is SBDGroupChannel {
            totalUnread{_ in }
            let messageReceivedChannel = sender as! SBDGroupChannel
            if ChatManager.channelList.index(of: messageReceivedChannel) != nil {
                ChatManager.channelList.remove(at: ChatManager.channelList.index(of: messageReceivedChannel)!)
            }
            ChatManager.channelList.insert(messageReceivedChannel, at: 0)
        }
        
    }
    
    func channel(_ sender: SBDGroupChannel, userDidJoin user: SBDUser) {
       totalUnread{_ in }
        DispatchQueue.main.async {
            if ChatManager.channelList.index(of: sender) == nil {
                ChatManager.channelList.append(sender)
            }
        }
        
    }
    
    func channelDidUpdateReadReceipt(_ sender: SBDGroupChannel) {   }
    
    func channelDidUpdateTypingStatus(_ sender: SBDGroupChannel) {   }
    
    func channel(_ sender: SBDGroupChannel, userDidLeave user: SBDUser) { }
    
    func channel(_ sender: SBDOpenChannel, userDidEnter user: SBDUser) {   }
    
    func channel(_ sender: SBDOpenChannel, userDidExit user: SBDUser) {    }
    
    func channel(_ sender: SBDOpenChannel, userWasMuted user: SBDUser) {    }
    
    func channel(_ sender: SBDOpenChannel, userWasUnmuted user: SBDUser) {    }
    
    func channel(_ sender: SBDOpenChannel, userWasBanned user: SBDUser) {    }
    
    func channel(_ sender: SBDOpenChannel, userWasUnbanned user: SBDUser) {    }
    
    func channelWasFrozen(_ sender: SBDOpenChannel) {    }
    
    func channelWasUnfrozen(_ sender: SBDOpenChannel) {    }
    
    func channelWasDeleted(_ channelUrl: String, channelType: SBDChannelType) {    }
    
    func channel(_ sender: SBDBaseChannel, messageWasDeleted messageId: Int64) {    }
}


