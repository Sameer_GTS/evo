//
//  Methods.swift
//  OneTap
//
//  Created by Chanchal Warde on 12/12/17.
//  Copyright © 2017 Chanchal Warde. All rights reserved.


import UIKit
import LGSideMenuController
import Spring
import Reachability
import Flurry_iOS_SDK

class Utils: NSObject {
    
    /// Shared object to access methods
    static var shared  = Utils()
    
    fileprivate override init() {
        super.init()
    }
    
    /// Validate any email address format.
    ///
    /// - Parameter Email: Email string to validate.
    /// - Returns: Boolean for result.
    func isValid(_ Email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let valid = emailTest.evaluate(with:Email)
        return valid
    }
    
    //Sameer
    //Flurry for event tracking
    func logUserIdOnLogin() {
        guard IK_USER.consentAgreed == consentAgree.Accept.rawValue else {
            return
        }
        
        Flurry.setUserID(IK_USER.profile_id)
    }
    
    func logEventWithFlurry(EventName: String, EventParam:[String:Any]) {
        guard IK_USER.consentAgreed == consentAgree.Accept.rawValue else {
            return
        }
        
        Flurry.logEvent(EventName, withParameters: EventParam)
    }
    
    func logFlurrySession(sessionParam : [String:Any]) {
        guard IK_USER.consentAgreed == consentAgree.Accept.rawValue else {
            return
        }
        
        Flurry.sessionProperties(sessionParam)
    }
    
    func startEventDurationForEvents(eventName: String, eventParam:[String:Any]) {
        guard IK_USER.consentAgreed == consentAgree.Accept.rawValue else {
            return
        }
        
        Flurry.logEvent(eventName, withParameters: eventParam, timed: true)
    }
    
    func closeEventDurationForEvents(eventName: String, eventParam : [String:Any]) {
        guard IK_USER.consentAgreed == consentAgree.Accept.rawValue else {
            return
        }
        
        Flurry.endTimedEvent(eventName, withParameters: eventParam)
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (String) {
        return "\(seconds / 3600)"
        //return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    /// Present alert viewcontroller. It will modally present alert on given view controller and return completion handler on action.
    ///
    /// - Parameters:
    ///   - vc: View controller on alert is to be displayed.
    ///   - title: Title string for alert.
    ///   - message: Message string for the user.
    ///   - affirmButton: Confirmation button text.
    ///   - cancelButton: Cancellation button text.
    ///   - dismissWith: Completion handler which return selected index.
    func alert (on vc: UIViewController, title: String = "", message: String, affirmButton: String, cancelButton : String? , dismissWith: @escaping(_ index : Int) -> Void = { _ in } )  {
        
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertView.addAction(UIAlertAction(title: affirmButton, style: UIAlertAction.Style.default, handler:{(action) in
            dismissWith(0)
        }))
        
        if cancelButton != nil {
            alertView.addAction(UIAlertAction(title: cancelButton, style: UIAlertAction.Style.default, handler: {(action) in
                dismissWith(1)
            }))
        }
        
        vc.present(alertView, animated: true, completion: nil)
    }
    
    func headerExpireForLogout(message : String)
    {
        let alertView = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        alertView.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:{(action) in

            WebConnect.logout(completion: { (_, _) in
                ChatManager.shared.disconnect()
                
                IK_DEFAULTS.removeObject(forKey: USER_DETAIL)
                IK_DEFAULTS.synchronize()
                
                let mainSt = UIStoryboard.init(name: "Main", bundle: nil)
                let register = mainSt.instantiateViewController(withIdentifier: "Landing") as! Landing
                let navigation = UINavigationController(rootViewController:register )
                navigation.isNavigationBarHidden = true
                
                UIView.transition(from: IK_DELEGATE.window!.rootViewController!.view,
                                  to: navigation.view,
                                  duration: 0.45,
                                  options: .transitionCrossDissolve,
                                  completion: { (_) in
                                    IK_DELEGATE.window!.rootViewController = navigation
                })
            })
            
        }))
        IK_DELEGATE.window?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
    
    /// Fade out animation for images.
    ///
    /// - Parameter imgCheck: Imageview object on which animation will be done.
    func animateFadeOut( _ imageView : SpringImageView  ,_ duration : CGFloat = 0.3) {
        imageView.animation =  Spring.AnimationPreset.FadeOut.rawValue
        imageView.scaleX =  0
        imageView.scaleY =  0
        imageView.damping = 0.4
        imageView.velocity = 0.2
        imageView.duration = duration
        imageView.curve = Spring.AnimationCurve.Linear.rawValue
        imageView.animate()
    }
    
    /// Pop animation for images.
    ///
    /// - Parameters:
    ///   - imgCheck: Imageview object on which animation will be done.
    ///   - image: Image to set in passed imageview. It can be check confirm or error alert image based on entered values.
    func animatePop( _ imageView : SpringImageView ) {
        imageView.animation = Spring.AnimationPreset.Pop.rawValue
        imageView.scaleX = 1.0
        imageView.scaleY = 1.0
        imageView.damping = 0.4
        imageView.velocity = 0.2
        imageView.duration = 0.3
        imageView.curve = Spring.AnimationCurve.Linear.rawValue
        imageView.animate()
    }
    
    /// Setup Ace home screen with side menu
    func setupAceHome() {
        IK_DELEGATE.TabBarConfiguration(true)
        /*
        let home = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AceList") as! AceList
        let navigation = UINavigationController(rootViewController:home )
        navigation.isNavigationBarHidden = true
        
        UIView.transition(from: IK_DELEGATE.window!.rootViewController!.view,
                          to: navigation.view,
                          duration: 0.65,
                          options: .transitionCrossDissolve,
                          completion: { (_) in
                            IK_DELEGATE.window!.rootViewController = navigation
        })*/
    }
    
    //MARK:- Edit Date
    //MARK:-
    
  /*  func editHourMinute(timeString:String) -> NSDate
    {
        let convertTime = self.convertStringToStringDate(timeString, currentFormat: "hh:mm a", convertToFormat: "HH:mm" , isGMT: false)
        
        let arrHour : NSArray = convertTime.componentsSeparatedByString(":")
        
        let arrMinute : NSArray = arrHour[1].componentsSeparatedByString(" ")
        
        let hour =  Int(arrHour[0] as! String)!
        
        let minute = Int(arrMinute[0] as! String)!
        
        let gregorian = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let now = NSDate()
        let components = gregorian.components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: now)
        
        components.hour = hour
        components.minute = minute
        components.second = 0
        
        let date = gregorian.dateFromComponents(components)!
        
        return date
    }
    
    //    func createDateWithComponent(day: Int , month: Int , year: Int) -> String
    //    {
    //        let gregorian = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
    //        let now = NSDate()
    //        let components = gregorian.components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: now)
    //
    //        components.day = day
    //        components.month = month
    //        components.year = year
    //        let date = gregorian.dateFromComponents(components)!
    //
    //        let strDate = self.convertDateToString(date, convertToFormat: AppConstant().kDateFromat)
    //
    //        return strDate;
    //    }
    //
    func createDateWithTime(hour: Int , minute: Int ) -> NSDate
    {
        let gregorian = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let now = NSDate()
        let components = gregorian.components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: now)
        
        components.hour = hour
        components.minute = minute
        
        let date = gregorian.dateFromComponents(components)!
        
        return date;
    } */
    
//    case bookingrequest = 3
//    case bookingAccept = 3
//    case sessionComplete
//    case aceApproved
//    case availbilityConfirm
//    case none
    
    
    
    /// Check if intrnet connection is active
    ///
    /// - Returns: Boolean to indicate true false
    func checkInternet() -> Bool
    {
        do {
            let reachability = Reachability(hostname: "https://www.google.com")

            if (reachability?.isReachable)!
            {
                if (reachability?.isReachableViaWiFi)! {      ////print("Reachable via WiFi")
                }
                else { ////print("Reachable via Cellular")
                }
                return true
            }
            else {   return false   }
        }
    }
    
    
    /// Handel Notification
    ///
    /// - Parameter info: Notification user info
    static func handelNotification(type: NotificationType, aceId: String? = nil) {
        
        guard let navigation = (((IK_DELEGATE.window?.rootViewController as? UINavigationController)?.viewControllers[0] as? CustomizedTabBarController)?.selectedViewController as? UINavigationController) else {
            return
        }

        if type == .bookingrequest || type == .availbilityRequest || type == .bookingCancel {
            IK_NOTIFICATION.post(name: NOTIFICATION_HOME, object: nil)
//            guard !(navigation.viewControllers.last is Home) else {
//                IK_NOTIFICATION.post(name: NOTIFICATION_HOME, object: nil) ; return
//            }
            navigation.popToRootViewController(animated: true)
        }
        else if type == .bookingAccept {
//            guard !(navigation.viewControllers.last is Workout)   else {
                IK_NOTIFICATION.post(name: NOTIFICATION_WORKOUT, object: nil) ; return
//            }
            
            let workout = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Workout") as! Workout
            workout.canGoBack = true
            navigation.pushViewController(workout, animated: true)
        }
        else if type == .sessionComplete {
            guard !(navigation.viewControllers.last is Rating) else {return}
            
            WebConnect.checkStatus(completion: { (status, arrCompleted, isAce) in
                if isAce {
                    if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                        
                        let newDic = NSMutableDictionary(dictionary: dict)
                        newDic.setValue("2", forKey: "role")
                        
                        IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                        IK_DEFAULTS.synchronize()
                        
                        IK_DELEGATE.TabBarConfiguration(true)

                    }
                }
                else
                    
                    if arrCompleted.count > 0 {
                    let rating = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Rating") as! Rating
                    rating.arrRating = arrCompleted
                    navigation.viewControllers.last?.present(rating, animated: true, completion: nil)
                    //self.present(rating, animated: true, completion: nil)
                }
            })

        }
        else if type == .aceApproved {

            if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                
                let newDic = NSMutableDictionary(dictionary: dict)
                newDic.setValue("2", forKey: "role")
                
                IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                IK_DEFAULTS.synchronize()
                
                IK_DELEGATE.TabBarConfiguration(true)

            }
        }
        else if type == .availbilityAccept || type == .bookingCancelAce || type == .bookingReject || type == .bookingCancel {
            guard !(navigation.viewControllers.last is AceList)   else {
               IK_NOTIFICATION.post(name: NOTIFICATION_BADGE, object: nil) ; return
            }
            navigation.popToRootViewController(animated: true)
        } else if type == .newAce || type == .seeAce48 || type == .seeAce {
            guard !(navigation.viewControllers.last is AceDetail)   else {
                return
            }
            WebConnect.getAceDetail(aceId: aceId! , completion: { (status, ace) in
                guard status else {return}

                let detail = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AceDetail") as! AceDetail
                detail.objAce = ace
                navigation.pushViewController(detail, animated: true)
            })
        }
    }
    
    /// Handel Notification
    ///
    /// - Parameter info: Notification user info
    static func notifyViews(type: NotificationType) {
        guard let navigation = IK_DELEGATE.window?.rootViewController as? UINavigationController else { return }
        
        if type == .bookingrequest || type == .availbilityRequest  || type == .bookingReject {
                IK_NOTIFICATION.post(name: NOTIFICATION_HOME, object: nil) ; return
        }
        else if type == .bookingAccept  || type == .bookingCancelAce  {
                IK_NOTIFICATION.post(name: NOTIFICATION_WORKOUT, object: nil) ; return
        }
        else if type == .sessionComplete {
            guard !(navigation.viewControllers.last is Rating)   else {return}
            
        }
        else if type == .availbilityAccept {
                IK_NOTIFICATION.post(name: NOTIFICATION_BADGE, object: nil) ; return
        }
    }
    
    
    /// Open chat screen when recieve notification
    ///
    /// - Parameter channelUrl: Channel Url in which message recieved
    static func openChat(channelUrl : String) {
        ChatManager.shared.getChannel(channelUrl: channelUrl) { (channel, error) in
            guard channel != nil else { return }
            if let navigation = IK_DELEGATE.window?.rootViewController as? UINavigationController {

                guard !(navigation.viewControllers.last is Chat)   else {return}
                
                let chat = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as! Chat
                chat.channel = channel
                navigation.pushViewController(chat, animated: true)
            }
            
            ChatManager.shared.totalUnread()
            ChatManager.shared.refreshChannelList()
        }
    }
}

extension String {
    /// Property to get trimimmed string with white space
    var trimString:String {
        get {
            return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        }
        set {
            self = newValue
        }
    }
    
    /// Property to get converted date object from string
    var date : Date? {
        get {
            let  requiredDateFormatter = DateFormatter()
            requiredDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return requiredDateFormatter.date(from: self) ?? Date()
        }
        set {
            date = Date()
        }
    }
    
    /// Validate any email address format.
    ///
    /// - Parameter Email: Email string to validate.
    /// - Returns: Boolean for result.
    func isEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let valid = emailTest.evaluate(with:self)
        return valid
    }
    
    /// Check if string is blank or contains only white space.
    ///
    /// - Returns: Boolean for result.
    func isEmpty() -> Bool {
        let WhiteSpaceSet  =   NSCharacterSet.whitespacesAndNewlines
        return self.trimmingCharacters(in: WhiteSpaceSet).count == 0
    }
    
    /// Change color of string
    ///
    /// - Parameter color: Color needed
    /// - Returns: Colored attributed string
    func changeColor(_ color:UIColor) -> NSMutableAttributedString {
        let changeAtt = NSMutableAttributedString(string: self)
        changeAtt.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSMakeRange(0, changeAtt.length))
        return  changeAtt
    }
    
    /// Get url encoded string
    ///
    /// - Returns: String with url encoding
    func urlencoding() -> String {
        var output: String = ""
        
        for thisChar in self {
            if thisChar == " " {
                output += "+"
            }
            else if thisChar == "." ||
                thisChar == "-" ||
                thisChar == "_" ||
                thisChar == "~" ||
                (thisChar >= "a" && thisChar <= "z") ||
                (thisChar >= "A" && thisChar <= "Z") ||
                (thisChar >= "0" && thisChar <= "9") {
                let code = String(thisChar).utf8.map{ UInt8($0) }[0]
                output += String(format: "%c", code)
            }
            else {
                let code = String(thisChar).utf8.map{ UInt8($0) }[0]
                output += String(format: "%%%02X", code)
            }
        }
        
        return output;
    }
}

extension UIView {
    ///
    func roundCorners() {
        
        let path = UIBezierPath(arcCenter: CGPoint(x: frame.height*0.4, y: frame.midX ),
                                radius: frame.width*0.4,
                                startAngle: 0,
                                endAngle: 90,
                                clockwise: true) //UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft,.bottomRight], cornerRadii: CGSize(width: 0, height: 100))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    /// Method to set border and corner radius of any view.
    ///
    /// - Parameters:
    ///   - cornerRadius: Corner radius
    ///   - borderWidth: Width to set border
    ///   - borderColor: UIColor for border
    func setBorder(cornerRadius: CGFloat = 3, borderWidth : CGFloat = 1 , borderColor: UIColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1) ) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
    }
    
    /// Method to set border and corner radius of any view.
    ///
    /// - Parameters:
    ///   - cornerRadius: Corner radius
    ///   - borderWidth: Width to set border
    ///   - borderColor: UIColor for border
    func setBorderMustRequir(cornerRadius: CGFloat = 3, borderWidth : CGFloat = 1 , borderColor: UIColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1) ) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
    }
    
    /// Add shadow to view.
    ///
    /// - Parameters:
    /// -  opacity: Opacity of shadow should show under view.
    /// -  radius: Radius of shadow.
    func dropShadow(_ opacity : Float = 0.15 , radius: CGFloat = 7.5 , offset : CGSize = CGSize(width: 0, height: 2) , color : UIColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1) ) {
        self.layer.shadowOffset = offset  // CGSizeMake(0, 1)
        self.layer.shadowColor =  color.cgColor
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.clipsToBounds = true
        self.layer.masksToBounds = false
    }
    
    func gradient()  {
        
    }
}

extension UITableView {
    
    /// Animate table view with reload data.
    func animateReload() {
        
        let cells = self.visibleCells
        
        //        for i in cells {
        //            let cell: UITableViewCell = i as UITableViewCell
        //            cell.transform = CGAffineTransform(translationX: 0, y: 0)
        //        }
        
        var index = 0
        
        for a in cells.reversed() {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 0.5, delay: 0.2 * Double(index), usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: .transitionCrossDissolve, animations:{
                    cell.transform = CGAffineTransform(translationX: self.frame.size.width, y: 0);
            }, completion: { _ in
                self.deleteRows(at: [self.indexPath(for: cell)!], with: .none)
            })
            index += 1
        }
        
        //  self.reloadData()
        // self.setContentOffset(CGPoint.zero, animated: false)
    }
}

extension Date {
    
    /// Enum to get comparison result of two date.
    ///
    /// - equal: First date is equal to second
    /// - greater: First date is greater then second
    /// - less: First date is less then second
    enum DateOrder {
        case equal
        case greater
        case less
    }
    
    /// Function to compare two date with matching level.
    ///
    /// - Parameters:
    ///   - date: Second date object to compaire with date.
    ///   - granularity: Granularity to compare date object till like hour, minute etc
    /// - Returns: DateOrder object
    func compareTo(date: Date , granularity : Calendar.Component = .second ) -> DateOrder {
        let order = Calendar.current.compare(self, to: date, toGranularity: granularity)
        if order == ComparisonResult.orderedDescending   {
            return .equal
        }
        else if order == ComparisonResult.orderedAscending {
            return .less
        }
        else {
            return .greater
        }
    }
    
    /// Funcation to get string value of date object with specified format.
    ///
    /// - Parameter format: Format to in which date is required as string/
    /// - Returns: String value of date in given format
    func string(format:String) -> String {
        let  requiredDateFormatter = DateFormatter()
        requiredDateFormatter.dateFormat = format
        return requiredDateFormatter.string(from: self)
    }


    /// Get any component from date object like hour, minute etc
    ///
    /// - Parameter request: Required component
    /// - Returns: Component value in integer
    func getComponent(request:Calendar.Component) -> Int {
       return Calendar.current.component(request, from: self)
    }

    func getDifference(to:Date) -> Int
    {
        let startTime = self.timeIntervalSinceReferenceDate
        let endTime = to.timeIntervalSinceReferenceDate
        
        /* ----------------------------------------------------------------
         Find the difference between current time and start time.
         ---------------------------------------------------------------- */
        
        var elapsedTime: TimeInterval = endTime - startTime
        
        /* ------------------------------------------------
         calculate the hours in elapsed time.
         ------------------------------------------------ */
        
        return Int( elapsedTime / 3500.0 )
    }


}

public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        default:                                        return identifier
        }
    }
    
}

extension FileManager {
    
    /// Temporary Directory URL
    static let tempDir: URL = {
        var tempDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        tempDir.appendPathComponent(".temp")
        if !FileManager.default.fileExists(atPath: tempDir.path) {
            do {
                try FileManager.default.createDirectory(
                    at: tempDir, withIntermediateDirectories: false, attributes: nil
                )
            } catch {
                print(error)
            }
        }
        return tempDir
    }()
    
    /// Save file to document directory
    ///
    /// - Parameters:
    ///   - data: Data to write in directory
    ///   - name: File name with extension
    /// - Returns: Path of saved file
    func saveToDocs(data: Data, name: String) -> URL {
        var path = FileManager.tempDir
        path.appendPathComponent(name)
        do {
            if self.fileExists(atPath: path.path) {
                try self.removeItem(atPath: path.path)
            }
            try data.write(to: path)
        } catch {
            print(error)
        }
        
        return path
    }
}
