//
//  NSString+NSString_SHA256.h
//  InstagramPrivateApi
//
//  Created by Anil Simsek on 18/05/16.
//  Copyright © 2016 MobileArts. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SHA256)
-(NSData*) SHA256;
@end
