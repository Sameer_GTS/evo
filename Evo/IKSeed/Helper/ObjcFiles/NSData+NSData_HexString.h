//
//  NSData+NSData_HexString.h
//  
//
//  Created by Anil Simsek on 12/04/16.
//
//

#import <Foundation/Foundation.h>

@interface NSData (NSData_HexString)
- (NSString *)hexadecimalString ;
-(NSData*) SHA256;
@end
