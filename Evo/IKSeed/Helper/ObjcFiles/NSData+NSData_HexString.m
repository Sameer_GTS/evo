//
//  NSData+NSData_HexString.m
//  
//
//  Created by Anil Simsek on 12/04/16.
//
//

#import "NSData+NSData_HexString.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSData (NSData_HexString)
- (NSString *)hexadecimalString {
    /* Returns hexadecimal string of NSData. Empty string if data is empty.   */
    
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger          dataLength  = [self length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}

-(NSData*) SHA256 {
//  const char *s=[self cStringUsingEncoding:NSASCIIStringEncoding];
//  NSData *keyData=[NSData dataWithBytes:s length:strlen(s)];
  
  uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
  
  CC_SHA256(self.bytes, (CC_LONG)self.length, digest);
  return [NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];//.hexadecimalString;
}

@end
