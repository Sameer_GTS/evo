//
//  NSString+NSString_SHA256.m
//  InstagramPrivateApi
//
//  Created by Anil Simsek on 18/05/16.
//  Copyright © 2016 MobileArts. All rights reserved.
//

#import "NSString+SHA256.h"
#import <CommonCrypto/CommonCrypto.h>
#import "NSData+NSData_HexString.h"

@implementation NSString (SHA256)

-(NSData*) SHA256{
    const char *s=[self cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData=[NSData dataWithBytes:s length:strlen(s)];
    
    uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
    
    CC_SHA256(keyData.bytes, (CC_LONG)keyData.length, digest);
  return [NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];//.hexadecimalString;
}
@end
