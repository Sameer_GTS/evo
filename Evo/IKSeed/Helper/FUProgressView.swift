//
//  FUProgressView.swift
//  SocialPoint_Swift
//
//  Created by Fuad on 26/07/17.
//  Copyright © 2017 Fuad. All rights reserved.


import UIKit

class FUProgressView: UIView, CAAnimationDelegate {
    
    var progress: CGFloat = 0
    fileprivate static var progressView : FUProgressView!
    fileprivate static var transparentView : UIView!
    fileprivate static var App_Delegate = UIApplication.shared.delegate as! AppDelegate
    
    fileprivate static var progressLabel: UILabel = {
        $0.center = CGPoint(x: App_Delegate.window!.frame.midX, y: App_Delegate.window!.frame.midY * 0.7)
        $0.textAlignment = .center
        $0.textColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        $0.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 0.3))
        $0.text = ""
        return $0
    }(UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 30)))
    
    fileprivate static var messageLabel: UILabel = {
//        $0.center = CGPoint(x: App_Delegate.window!.frame.midX, y: (App_Delegate.window!.frame.midY * 0.7) + 50)
        $0.center = CGPoint(x: App_Delegate.window!.frame.midX, y: (App_Delegate.window!.frame.height * 0.5) + 50)
        $0.textAlignment = .center
        $0.textColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        $0.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight(rawValue: 0.3))
        $0.text = ""
        return $0
    }(UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 30)))
    
    override var layer: CAShapeLayer {
        get {
            return super.layer as! CAShapeLayer
        }
    }
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    static func new(on view: UIView) -> FUProgressView {
        let progressView = FUProgressView(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        progressView.center = CGPoint(x: view.frame.width / 2, y: view.frame.height / 2)
        view.addSubview(progressView)
        return progressView
    }
    
    static func loadProgress(_ message: String = "",isWebView:Bool = true ,isUseLayer : Bool = true, baseView : UIView) {
        
        if progressView != nil {
            progressView.layer.removeAllAnimations()
            progressView.removeFromSuperview()
            transparentView.removeFromSuperview()
            messageLabel.removeFromSuperview()
        }
        
        progressView = FUProgressView(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        
        //progressView.center = CGPoint.init(x: baseView.frame.midX, y: App_Delegate.window!.frame.height * 0.25)
        //messageLabel.center = CGPoint.init(x: baseView.frame.midX, y: App_Delegate.window!.frame.height * 0.25 + 50.0)
        
        if isWebView {
            progressView.center = CGPoint.init(x: baseView.frame.midX, y: App_Delegate.window!.frame.height * 0.25)
            messageLabel.center = CGPoint.init(x: baseView.frame.midX, y: App_Delegate.window!.frame.height * 0.25 + 50.0)
        }else {
            progressView.center = CGPoint.init(x: baseView.frame.midX, y: App_Delegate.window!.frame.height * 0.5)
            messageLabel.center = CGPoint.init(x: baseView.frame.midX, y: App_Delegate.window!.frame.height * 0.5 + 50.0)
            
            messageLabel.textColor = UIColor.white
        }
        
        progressView.animate()
        
        if isUseLayer
        {
            //transparentView = UIView(frame: App_Delegate.window!.frame)
            transparentView = UIView(frame: baseView.frame)
            transparentView.backgroundColor = UIColor.clear // #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
            transparentView.addSubview(progressView)
            //VC.view.addSubview(transparentView)
            baseView.addSubview(transparentView)
            transparentView.addSubview(messageLabel)
        }
        else
        {
            App_Delegate.window?.addSubview(progressView)
            App_Delegate.window?.addSubview(messageLabel)
        }
        messageLabel.text = message
    }
    
    static func unloadProgress() {
        
    }
    
    static func showProgress(_ message: String = "", isUseLayer : Bool = true, isFromKeyView : Bool = false)  {
        if progressView != nil {
            progressView.layer.removeAllAnimations()
            progressView.removeFromSuperview()
            transparentView.removeFromSuperview()
            messageLabel.removeFromSuperview()
        }
        
        progressView = FUProgressView(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        progressView.center = CGPoint(x: App_Delegate.window!.frame.midX, y: App_Delegate.window!.frame.height * 0.5)
        progressView.animate()
        
        if isFromKeyView {
           messageLabel.textColor = UIColor.white
        }
        
        if isUseLayer
        {
            transparentView = UIView(frame: App_Delegate.window!.frame)
            transparentView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
            transparentView.addSubview(progressView)
            App_Delegate.window?.addSubview(transparentView)
            transparentView.addSubview(messageLabel)
        }
        else
        {
            App_Delegate.window?.addSubview(progressView)
            App_Delegate.window?.addSubview(messageLabel)
        }
        messageLabel.text = message
    }
    
    static func hideProgress() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            progressView.layer.removeAllAnimations()
            progressView.removeFromSuperview()
            transparentView.removeFromSuperview()
            messageLabel.removeFromSuperview()
        })
    }
    
    static func progress(_ percent: CGFloat, _ message: String = "") {
        DispatchQueue.main.async {
            transparentView.addSubview(progressLabel)
            transparentView.addSubview(messageLabel)
            progressLabel.text = String(format: "%0.0f %%", (percent*100))
            messageLabel.text = message
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createProgressLayer()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createProgressLayer()
    }
    
    func createProgressLayer() {
        layer.fillColor = nil
        layer.strokeColor =  #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1).cgColor
        layer.lineWidth = 2
        setPath()
    }
    
    private func setPath() {
        layer.path = UIBezierPath(ovalIn: bounds.insetBy(dx: layer.lineWidth / 2, dy: layer.lineWidth / 2)).cgPath
        layer.strokeEnd = 0.0
    }
    
    func animate() {
        let times = [0, 0.3, 0.6, 1.0]
        let totalSeconds: CFTimeInterval = 2
        let rotations: [CGFloat] = [CGFloat.pi, CGFloat.pi * 2, CGFloat.pi * 4.5, CGFloat.pi * 11]
        let strokeEnds: [CGFloat] = [0, 0.5 , 0.9, 0.0]
        
        let colorHues = [#colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1).cgColor, #colorLiteral(red: 0.8510329286, green: 0.1725490196, blue: 0.4, alpha: 1).cgColor , #colorLiteral(red: 0.777528005, green: 0, blue: 0.2588235294, alpha: 1).cgColor , #colorLiteral(red: 0.7264403824, green: 0, blue: 0.231372549, alpha: 1).cgColor ]
        
        self.isHidden = false
        animateKeyPath(keyPath: "strokeEnd", duration: totalSeconds, times: times, values: strokeEnds)
        animateKeyPath(keyPath: "transform.rotation", duration: totalSeconds, times: times, values: rotations)
        animateKeyPath(keyPath: "strokeColor", duration: totalSeconds, times: times, values: colorHues)
    }
    
    func animateWithProgress(_ progress: CGFloat) {
        guard progress > self.progress else { return }
        let centerPoint = (progress - layer.strokeEnd) / 2
        // print("\nProgressView\n", layer.strokeEnd, centerPoint, progress)
        animateKeyPath(keyPath: "strokeEnd", duration: 0.2, times: [0.0, 0.5, 1.0], values: [layer.strokeEnd, centerPoint, progress], repeated: false)
        self.progress = progress
    }
    
    func stopAnimating(remove: Bool = true, hide: Bool = false) {
        DispatchQueue.main.async {
            self.layer.removeAllAnimations()
            self.isHidden = hide
            if remove {
                self.removeFromSuperview()
            }
        }
    }
    
    private func animateKeyPath(keyPath: String, duration: CFTimeInterval, times: [CFTimeInterval], values: [Any], repeated: Bool = true) {
        let animation = CAKeyframeAnimation(keyPath: keyPath)
        animation.keyTimes = times as [NSNumber]?
        animation.values = values
        animation.calculationMode = CAAnimationCalculationMode.linear
        animation.duration = duration
        animation.delegate = self
        animation.repeatCount = repeated ? Float.infinity : 1
        layer.add(animation, forKey: animation.keyPath)
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        layer.strokeEnd = progress
    }
}
