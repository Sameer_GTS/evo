//
//  PickImage.swift
//  FlightBuddy
//
//  Created by Chanchal Warde on 2/12/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.

import UIKit
//Fahad
import CropViewController
//

protocol ImagePickerDelegate : class {
    func didSelect(image:UIImage)
}

/// Media type to add video or image type of media.
///
/// - image: Image to b selected with picker.
/// - video: Video to b selected with picker.
enum MediaType {
    case image
    case video
}

class MediaPicker: NSObject , UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    
    var tag = 0
    /// UIImagePicker controller object
    let picker = UIImagePickerController()
    /// Delegate
    weak var delegate : ImagePickerDelegate?
    //Fahad
    weak var targetVC : UIViewController?
    
    var isSquared: Bool = false
    
    /// Function to show image picker with selected media type
    ///
    /// - Parameters:
    ///   - vc: View controller object to present image picker controller
    ///   - media: Media Type to select image or video with picker.
    func showPicker(vc:UIViewController, media : MediaType = .image , isEditing : Bool = false)  {
        picker.delegate = self
        let actionSheet: UIAlertController = UIAlertController(title: "Pick image from", message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Avbryt", style: .cancel ,handler: nil ))
        actionSheet.addAction( UIAlertAction(title: "Camera", style: .default) { _ in
            self.picker.allowsEditing = isEditing
            self.picker.sourceType = .camera
            if media == .video {
                self.picker.mediaTypes =  ["public.movie"]    // [kUTTypeMovie]
                
            }
            vc.present(self.picker, animated: true, completion: nil)
        })
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default) { _ in
            self.picker.allowsEditing = isEditing
            self.picker.sourceType = .photoLibrary
            if media == .video {
                self.picker.mediaTypes =  ["public.movie"]    // [kUTTypeMovie]
            }
            
            vc.present(self.picker, animated: false, completion: nil)
        })
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    /// UIImagePicker delegate, called if user selects or captures any image.
    ///
    /// - Parameters:
    ///   - picker: Image picker object presented on view controller
    ///   - info: Information of picked media like origial image, edited image etc
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: {
            
            guard self.delegate != nil else {
                return
            }
            
            if let image = info[.editedImage] as? UIImage {
                //Fahad
                if let vc = self.targetVC {
                    self.presentCropViewController(vc: vc, image: image, isSquare: self.isSquared)
                } else {
                    self.delegate?.didSelect(image: image)
                }
            }
            else {
                let image = info[.originalImage] as! UIImage
                //Fahad
                if let vc = self.targetVC {
                    self.presentCropViewController(vc: vc, image: image, isSquare: self.isSquared)
                } else {
                    self.delegate?.didSelect(image: image)
                }
            }
        })
    }
    
    /// UIImagePicker delegate called if user cancel to select media
    ///
    /// - Parameter picker: Image picker object presented on view controller
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}


//Fahad
extension MediaPicker: CropViewControllerDelegate {
    func presentCropViewController(vc: UIViewController, image: UIImage, isSquare: Bool = true) {
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        cropViewController.aspectRatioPreset = isSquare ? .presetSquare : .preset16x9
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.rotateButtonsHidden = true
        cropViewController.rotateClockwiseButtonHidden = false 
        cropViewController.aspectRatioPickerButtonHidden = true
        cropViewController.resetAspectRatioEnabled = false 
        vc.present(cropViewController, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        cropViewController.dismiss(animated: true) {
            self.delegate?.didSelect(image: image)
        }
    }
}
