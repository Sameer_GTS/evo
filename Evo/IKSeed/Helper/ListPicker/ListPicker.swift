//
//  ListPicker.swift
//  FlightBuddy
//
//  Created by Chanchal Warde on 2/16/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Spring

class ListPicker: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    /// Static object of view controller to call dynamically.
    fileprivate static let picker = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListPicker") as! ListPicker
    /// Cell height
    fileprivate let ROW_HEIGHT : CGFloat = 48
    
    //MARK:- IBOutlet
    //MARK:-
    
    /// Button to dismiss
    @IBOutlet var btnDismiss: UIButton!
    /// Title view for picker
    @IBOutlet var viewTitle: UIView!
    /// Title label to show for list
    @IBOutlet var lblTitle: UILabel!
    /// Textfield to search in options
    @IBOutlet var tfSearch: UITextField! {
        didSet {
            tfSearch.attributedPlaceholder = NSAttributedString(string: "Søk",attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
        }
    }
    /// Table view to show scrollable list
    @IBOutlet var tblList: UITableView!
    /// Search button to show/hide search textfield
    @IBOutlet var btnSearch: UIButton!
    /// Constraint object to increase/decrease search textfield width for hide/show functionality
    @IBOutlet var consSearchWidth: NSLayoutConstraint!
    
    //MARK:- Variable
    //MARK:-
    
    /// Option list to load table
    fileprivate var arrOptions : [(id:Int, name:String)] = []
    /// Copy of option list to produce searched array in option array and restore all value.
    fileprivate var arrTemp : [(id:Int, name:String )] = []
    /// Copy of option list to produce searched array in option array and restore all value.
    fileprivate var arrSelected : [(id:Int, name:String )] = []
    /// Dynamic width for search textfiled
    fileprivate var searchWidth : CGFloat  {
        return tfSearch.frame.origin.x - 17
    }
    /// Menu title
    fileprivate var menuTitle : String!
    /// If multiselection is enabled
    fileprivate var isMultiSelect = false
    
    /// On select clouser to call selected method when any cell is selected.
    public var onSelectItem: ((id:Int, name:String )) -> () = { _ in }
    /// Multiple select clouser call when multiple item is selected
    public var multiSelectItem : ([(id:Int, name:String )]) -> () = { _ in }
    public var selected : (_ index :Int ) -> () = { _ in }
    
    //MARK:- View Method
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblList.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.btnSearch.setBorder(cornerRadius: self.btnSearch.bounds.height/2, borderWidth: 0, borderColor: .clear)
        tfSearch.setBorder(cornerRadius: self.tfSearch.bounds.height/2, borderWidth: 1, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        tblList.reloadData()
        lblTitle.text = menuTitle
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.btnSearch.setBorder(cornerRadius: self.btnSearch.bounds.height/2, borderWidth: 0, borderColor: .clear)
        tfSearch.setBorder(cornerRadius: self.tfSearch.bounds.height/2, borderWidth: 1, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        tblList.reloadData()
    }
    
    //MARK:- IBAction Method
    //MARK:-
    
    /// Search button function to show/hide search bar textfield
    ///
    /// - Parameter sender: Search button
    @IBAction func clickSearch(_ sender: UIButton) {
        if sender.isSelected {
            arrOptions = arrTemp
            tfSearch.text = nil
            tfSearch.resignFirstResponder()
            tblList.reloadData()
        }
        
        sender.isSelected = !sender.isSelected
        consSearchWidth.constant = searchWidth
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    /// Dismiss action to hide picker.
    ///
    /// - Parameter sender: Dismiss action button
    @IBAction func clickDismiss(_ sender: UIButton) {
        hidePicker()
    }
    
    //MARK:- Other Method
    //MARK:-
    
    /// Show picker on view controller
    ///
    /// - Parameters:
    ///   - on: View controller on which picker will be shown
    ///   - title: Title of picker
    ///   - arrList: Array to show list
    ///   - multipleSelection: Is picker allow multi selection
    ///   - arrSelected: Already selected options
    /// - Returns: Object of picker class
    static func showPicker(on:UIViewController,
                           title : String = "Søk by" ,
                           arrList : [(id:Int, name:String)] ,
                           multipleSelection : Bool = false ,
                           arrSelected: [(id:Int, name:String )] = [] ) -> ListPicker {
        picker.menuTitle = title
        picker.arrOptions = arrList
        picker.arrTemp = arrList
        picker.arrSelected = arrSelected
        picker.isMultiSelect = multipleSelection
        
        on.present(picker, animated: true, completion: {
            UIView.transition(with:picker.btnDismiss,
                              duration: 0.20,
                              options: [.transitionCrossDissolve],
                              animations: {
                                picker.btnDismiss.alpha = 1
            }, completion:nil)
        })
        
        return picker
    }
    
    /// Function to hide picker from superview
    fileprivate func hidePicker() {
        if isMultiSelect {  multiSelectItem(arrSelected)  }
        ListPicker.picker.dismiss(animated: true, completion: {
            ListPicker.picker.btnDismiss.alpha = 0
        })
    }
}

//MARK:- Textfield Delegate
//MARK:-

extension ListPicker : UITextFieldDelegate {
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        arrOptions = arrTemp
        tblList.reloadData()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let rangeT = Range(range, in: textField.text!) else { return true }
        let searchText: String = textField.text!.replacingCharacters(in: rangeT, with: string)
        
        self.arrOptions =  self.arrTemp.filter({ $0.name.lowercased().contains(searchText.lowercased()) })
        tblList.reloadData()
        
        return true
    }
}

//MARK:- Tableview Delegate
//MARK:-

extension ListPicker : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellListPicker", for: indexPath) as! CellListPicker
        cell.setup(multi: isMultiSelect, data: arrOptions[indexPath.row], arrSelected: arrSelected)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ROW_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isMultiSelect {
            onSelectItem(arrOptions[indexPath.row])
            selected(indexPath.row)
            hidePicker()
        }
        else {
            let cell = tableView.cellForRow(at: indexPath) as! CellListPicker
            
            let data = arrOptions[indexPath.row]
            if let index = arrSelected.index(where: {$0.id == data.id}) {
                cell.btnAdd.isSelected = false
                arrSelected.remove(at:index)
            }
            else {
                cell.btnAdd.isSelected = true
                arrSelected.append(data)
            }
            
            cell.btnAdd.animation = Spring.AnimationPreset.Pop.rawValue
            cell.btnAdd.curve = Spring.AnimationCurve.Linear.rawValue
            cell.btnAdd.scaleX = 0.7
            cell.btnAdd.scaleY = 0.7
            cell.btnAdd.duration = 0.10
            cell.btnAdd.animate()
        }
        //print("Select --->> ",indexPath.row, "   ------->",arrOptions[indexPath.row].name)
    }
}

//MARK:- Tableview Cell
//MARK:-

class CellListPicker : UITableViewCell {
    
    /// Show short name of airport
    @IBOutlet var lblAbbreviation: UILabel!
    /// Show options
    @IBOutlet var lblOption: UILabel!
    /// Button to select option in multiple list
    @IBOutlet var btnAdd: SpringButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /// Setup cell to show multi selected and data
    ///
    /// - Parameters:
    ///   - multi: Boolean is multiple selection allowed
    ///   - data: Object to set data in labels
    ///   - arrSelected: Selected array to match
    func setup(multi:Bool , data: (id:Int, name:String ), arrSelected : [(id:Int, name:String )] ) {
        
        lblOption.text = data.name
     //   lblAbbreviation.text = data.short
        
      //  lblAbbreviation.isHidden = multi
        btnAdd.isHidden = !multi
        
        if arrSelected.first(where: { $0.id == data.id }) != nil {
            btnAdd.isSelected = true
        }
        else {
            btnAdd.isSelected = false
        }
    }
    
}
