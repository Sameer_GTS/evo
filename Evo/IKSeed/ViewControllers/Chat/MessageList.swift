//
//  MessageList.swift
//  IKSeed
//
//  Created by Chanchal Warde on 4/7/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import SendBirdSDK
import Spring

class MessageList: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    fileprivate let refresh = UIRefreshControl()
    fileprivate let ROW_HEIGHT : CGFloat = 70
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var tblMessage: UITableView!
    @IBOutlet var conBackHeight: NSLayoutConstraint!
    @IBOutlet var viewNavigation: UIView!
    @IBOutlet var viewError: SpringView!
    @IBOutlet var lblErrorMessage: UILabel!
    
    //MARK:- Variable
    //MARK:-
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utils.shared.logEventWithFlurry(EventName: "chat_list_screen", EventParam: [:])
        
        refresh.attributedTitle = "Oppdater".changeColor(#colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        refresh.tintColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        refresh.addTarget(self, action: #selector(reloadData) , for: UIControl.Event.valueChanged)
        tblMessage.addSubview(refresh)
        
        if IK_USER.userType == .ace {
            lblErrorMessage.text =   "Chat med kundene og svar på det de lurer på."
        }
        
        
        if #available(iOS 11.0, *) {
            var top = UIApplication.shared.keyWindow?.safeAreaInsets.top
            conBackHeight.constant = conBackHeight.constant + top!
            var frame = viewNavigation.frame
            frame.size.height =  frame.size.height + top!
            viewNavigation.frame = frame
            //   tblMessage.contentInset = UIEdgeInsets(top: CGFloat(Float(top!)), left: 0, bottom: 0, right: 0)
        }
        else {
            let top = UIApplication.shared.statusBarFrame.size.height
            //  tblMessage.contentInset = UIEdgeInsets(top: CGFloat(-Float(top)), left: 0, bottom: 0, right: 0)
        }
        
        ChatManager.shared.channelDelegate = self
        setPlaceholder ()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        tblMessage.reloadData()
        setPlaceholder ()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Other
    //MARK:-
    
    @IBAction func reloadData() {
        refresh.endRefreshing()
        ChatManager.shared.refreshChannelList()
        setPlaceholder ()
    }
    
    fileprivate func setPlaceholder () {
        UIView.animate(withDuration: 0.25) {
            self.viewError.alpha = ChatManager.channelList.count > 0 ?  0 : 1
        }
    }
}

extension MessageList : ChatChannelDelegate {
    func channelListUpdated() {
        tblMessage.reloadData()
        setPlaceholder ()
    }
    
    func channelUpdate(channel: SBDGroupChannel) {
        tblMessage.reloadData()
        setPlaceholder ()
    }
    
    func channelShouldReload() {
        tblMessage.reloadData()
        setPlaceholder ()
    }
}

extension MessageList : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ChatManager.channelList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellMessage", for: indexPath) as! CellMessage
        cell.setModel(aChannel: ChatManager.channelList[indexPath.row])
        
        //cell.lblUnreadCount.setBorder(cornerRadius: cell.lblUnreadCount.frame.height/2, borderWidth: 0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ROW_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chat = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! Chat
        chat.channel = ChatManager.channelList[indexPath.row]
        self.navigationController?.pushViewController(chat, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
}

class CellMessage : UITableViewCell {
    
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblUnreadCount: UILabel!
    var channel : SBDGroupChannel!
    
    override func awakeFromNib() {
        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 0)
    }
    
    func setModel(aChannel: SBDGroupChannel) {
        
        self.channel = aChannel
        
        //self.typingImageView.isHidden = true
        // self.typingLabel.isHidden = true
        
        if self.channel.memberCount == 2 {
            for member in self.channel.members! as NSArray as! [SBDUser] {
                if member.userId == IK_USER.profile_id { continue }// SBDMain.getCurrentUser()?.userId { continue  }
                
                self.imgUser.kf.indicatorType = .activity
                self.imgUser.kf.setImage(with: URL(string: member.profileUrl!),
                                         placeholder: #imageLiteral(resourceName: "thumb_menu"),
                                         options: nil,
                                         progressBlock: nil,
                                         completionHandler: nil)
                self.lblUserName.text = member.nickname!
            }
        }
        
        guard  self.channel.lastMessage is SBDUserMessage else { return }
        
        let lastMessage = (self.channel.lastMessage as! SBDUserMessage)
        let lastMessageTimestamp = Int64(lastMessage.createdAt)
        
        var lastMessageDate: Date?
        if String(format: "%lld", lastMessageTimestamp).count == 10 {
            lastMessageDate = Date.init(timeIntervalSince1970: Double(lastMessageTimestamp))
        }  else {
            lastMessageDate = Date.init(timeIntervalSince1970: Double(lastMessageTimestamp) / 1000.0)
        }
        
        self.lblDate.text = lastMessageDate?.string(format: DATE_FORMAT)
        self.lblTime.text = lastMessageDate?.string(format: TIME_FORMAT)
        self.lblUnreadCount.isHidden = self.channel.unreadMessageCount == 0
        
        if self.channel.unreadMessageCount <= 9 {
            self.lblUnreadCount.text = String(format: " %ld", self.channel.unreadMessageCount)
        } else {
            self.lblUnreadCount.text = " 9+"
        }
        
    }
}
