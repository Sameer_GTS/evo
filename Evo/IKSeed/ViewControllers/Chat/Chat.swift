
//
//  Chat.swift
//  IKSeed
//
//  Created by Chanchal Warde on 4/7/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import IQKeyboardManager
import SendBirdSDK

class Chat: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var tblMessage: UITableView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var tvMessage: UITextView!
    
    @IBOutlet var conTypeBottom: NSLayoutConstraint!
    @IBOutlet var conTypeHeight: NSLayoutConstraint!
    
    //MARK:- Variable
    //MARK:-
    
    var otherUser : (id:String , name: String, image:String)!
    var channel : SBDGroupChannel!
    private var messages: [SBDBaseMessage] = []
    private var hasNext: Bool = true
    private var minMessageTimestamp: Int64 = Int64.max
    private var isLoading: Bool = false
    private var userDetail =  ( name: "",ageGroup:0,fitness:0,image:"" , category : "")
    private var userId = ""
    private var isPendingRefresh = false
    
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        ChatManager.shared.messageDelegate = self
        setUserData()
        setChannel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        IQKeyboardManager.shared().isEnabled = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        IQKeyboardManager.shared().isEnabled = true
        
        guard channel != nil && SBDMain.getCurrentUser()?.userId != nil else { return }
        ChatCaching.dumpMessages(messages: messages, channelUrl: channel.channelUrl)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //let eventParams = ["ace_id"   : self.otherUser.id] as [String : Any]
        //Utils.shared.closeEventDurationForEvents(eventName: "chat_with_ace", eventParam: eventParams)
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickShowProfile(_ sender: UIButton) {
        
        if IK_USER.userType != .ace {
            guard channel != nil else {
                return
            }
            
            WebConnect.getAceDetail(aceId: userId , completion: { (status, ace) in
                guard status else {return}
                
                let detail = self.storyboard?.instantiateViewController(withIdentifier: "AceDetail") as! AceDetail
                detail.isBack = true
                detail.objAce = ace
                self.navigationController?.pushViewController(detail, animated: true)
            })
        }
        else {
            let detail = self.storyboard?.instantiateViewController(withIdentifier: "RequesterDetail") as! RequesterDetail
            detail.userDetail = userDetail
            self.navigationController?.pushViewController(detail, animated: true)
        }
    }
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickSend(_ sender: UIButton) {
        if channel == nil {
            initateChat {
                self.sendMessage()
            }
        } else {
            sendMessage()
        }
    }
    
    fileprivate func sendMessage() {
        
        guard !tvMessage.text.isEmpty() && tvMessage.text!.trimString != "Skriv..." else { tvMessage.resignFirstResponder() ; return}
        
        // channel.endTyping()
        let message = tvMessage.text.trimString
        tvMessage.text = ""
        // tvMessage.resignFirstResponder()
        
        var tempMessage : SBDUserMessage!
        tempMessage =  ChatManager.shared.sendMessage(channel: channel, message: message) { (userMessage, error) in
            if error != nil {
                return
            }
            
            let index = IndexPath(row: self.messages.index(of: tempMessage)!, section: 0)
            self.tblMessage.beginUpdates()
            UIView.setAnimationsEnabled(false)
            self.tblMessage.reloadRows(at: [index], with: UITableView.RowAnimation.none)
            UIView.setAnimationsEnabled(true)
            self.tblMessage.endUpdates()
            
            DispatchQueue.main.async {
                self.scrollToBottom()
            }
        }
        
        self.tblMessage.beginUpdates()
        self.messages.append(tempMessage!)
        UIView.setAnimationsEnabled(false)
        self.tblMessage.insertRows(at: [IndexPath(row:  self.messages.count-1, section: 0)], with: UITableView.RowAnimation.none)
        UIView.setAnimationsEnabled(true)
        self.tblMessage.endUpdates()
        
        DispatchQueue.main.async {
            self.scrollToBottom()
        }
    }
    
    fileprivate func loadPreviousMessage(initial: Bool) {
        
        let timestamp =  initial ? Int64.max : self.minMessageTimestamp
        
        guard self.hasNext && !self.isLoading else { return }
        self.isLoading = true
        
        ChatManager.shared.loadMessage(channel: channel, timestamp: timestamp, initial: initial) { (messages, error) in
            self.isLoading = false
            guard error == nil else { return }
            
            self.hasNext = (messages!.count > 0)
            
            if initial {
                self.messages = []
                for item in messages! {
                    let message: SBDBaseMessage = item as SBDBaseMessage
                    self.messages.append(message)
                    if self.minMessageTimestamp > message.createdAt {
                        self.minMessageTimestamp = message.createdAt
                    }
                }
                
                DispatchQueue.main.async {
                    //                    self.messages.sort(by: { (a,b ) -> Bool in
                    //                        a.createdAt < b.createdAt
                    //                    })
                    self.tblMessage.reloadData()
                    self.scrollToBottom()
                }
            }
            else {
                for item in messages!.reversed() {
                    let message: SBDBaseMessage = item as SBDBaseMessage
                    self.messages.insert(message, at: 0)
                    if self.minMessageTimestamp > message.createdAt {
                        self.minMessageTimestamp = message.createdAt
                    }
                }
                
                DispatchQueue.main.async {
                    //                    self.messages.sort(by: { (a,b ) -> Bool in
                    //                        a.createdAt < b.createdAt
                    //                    })
                    self.tblMessage.reloadData()
                }
            }
            self.channel.markAsRead()
            guard self.isPendingRefresh && !self.isLoading else {return}
            self.loadPreviousMessage(initial: true)
        }
    }
    
    //MARK:- Other Methods
    //MARK:-
    
    fileprivate func setChannel() {
        if (channel != nil){
            messages = ChatCaching.loadMessagesInChannel(channelUrl: channel.channelUrl)
            tblMessage.reloadData()
            loadPreviousMessage(initial: true)
        } else {
            ChatManager.shared.getUserChannel(userid: IK_USER.profile_id, otherUser: otherUser.id, completion: { channel in
                guard (channel != nil) else {return}
                DispatchQueue.main.async {
                    self.channel = channel
                    self.messages = ChatCaching.loadMessagesInChannel(channelUrl: channel!.channelUrl)
                    self.tblMessage.reloadData()
                    self.loadPreviousMessage(initial: true)
                    
                    for member in self.channel.members! as NSArray as! [SBDUser] {
                        if member.userId == SBDMain.getCurrentUser()?.userId { continue  }
                        self.userDetail.name = member.nickname!
                        self.userDetail.image = member.profileUrl!
                        self.userDetail.ageGroup =  Int(member.metaData!["age groups"]!) ?? 1
                        self.userDetail.fitness =  Int( member.metaData!["fitness level"]!) ??  1
                    }
                }
            })
            return
        }
    }
    
    fileprivate func setUserData() {
        
        if channel != nil {
            for member in self.channel.members! as NSArray as! [SBDUser] {
                if member.userId == SBDMain.getCurrentUser()?.userId { continue  }
                
                self.imgUser.kf.indicatorType = .activity
                self.imgUser.kf.setImage(with: URL(string: member.profileUrl!),
                                         placeholder: #imageLiteral(resourceName: "thumb_menu"),
                                         options: nil,
                                         progressBlock: nil,
                                         completionHandler: nil)
                self.lblName.text = member.nickname!
                
                userDetail.name = member.nickname!
                userDetail.image = member.profileUrl!
                
                if let data = member.metaData!["age groups"] {
                    userDetail.ageGroup =  Int(data) ?? 1
                }
                
                if let data = member.metaData!["fitness level"] {
                    userDetail.fitness =  Int(data) ??  1
                }
                
                
                guard member.userId != nil else {return}
                userId = member.userId
                
                guard IK_USER.userType != .ace else {return}
                WebConnect.trackActivity(aceID: userId,isChat: true)
            }
        } else {
            self.imgUser.kf.indicatorType = .activity
            self.imgUser.kf.setImage(with: URL(string: otherUser.image),
                                     placeholder: #imageLiteral(resourceName: "thumb_menu"),
                                     options: nil,
                                     progressBlock: nil,
                                     completionHandler: nil)
            self.lblName.text = otherUser.name
            userId = otherUser.id
            userDetail.name = otherUser.name
            userDetail.image = otherUser.image
            
            guard IK_USER.userType != .ace else {return}
            WebConnect.trackActivity(aceID: userId,isChat: true)
        }
    }
    
    fileprivate func scrollToBottom() {
        guard self.messages.count != 0 else { return }
        tblMessage.scrollToRow(at: IndexPath.init(row: self.messages.count - 1, section: 0), at: UITableView.ScrollPosition.bottom, animated: false)
    }
    
    fileprivate func initateChat(completion:@escaping () -> Void) {
        ChatManager.shared.createChannel(userIds: [otherUser.id,IK_USER.profile_id]) { (channel, error) in
            guard error == nil else { print("Error creating channel ",error); return }
            self.channel = channel
            completion()
        }
        //
        //        WebConnect.createChannel(with: otherUser.id, completion: { (status, channelUrl, message) in
        //            ChatManager.shared.getChannel(channelUrl: channelUrl, completion: { (channel, error) in
        //                guard error == nil else {return}
        //                self.channel = channel
        //                completion()
        //            })
        //        })
    }
    
    //MARK:- Keyboard Observer
    //MARK:-
    
    @objc func keyboardWillShow(_ notification: NSNotification)
    {
        /* -----------------------------------------------------------------
         Get keyboard infoamtion by notification object.
         Get keyboard frame and animation time.
         ----------------------------------------------------------------- */
        
        let keyboardFrame:CGRect = ( notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        /* ----------------------------------------------------------------------------------------------
         Use animation and update input view, table view frame to move up with keyboard.
         ---------------------------------------------------------------------------------------------- */
        
        if let animationDurarion = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let animationCurve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber {
            let options = UIView.AnimationOptions(rawValue: UInt((animationCurve).intValue << 16))
            
            DispatchQueue.main.async(execute: {
                let Height = keyboardFrame.size.height
                //print("--------------- \n Keyboard Height ",Height)
                self.conTypeBottom.constant = -Height
                
                UIView.animate(withDuration: animationDurarion, delay: 0, options: options, animations: {
                    self.view.layoutIfNeeded()
                    /* --------------------------------------------------------------------------------------------------
                     If we have messges and table view is scrolling then , will scroll it to bottom.
                     ---------------------------------------------------------------------------------------------------- */
                    // self.scrollTableBottom()
                    
                })
            })
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification)
    {
        /* ----------------------------------------------------------
         -  Get keyboard infoamtion by notification object.
         -  Get animation time.
         ---------------------------------------------------------- */
        
        if let animationDurarion = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let animationCurve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber {
            let options = UIView.AnimationOptions(rawValue: UInt((animationCurve).intValue << 16))
            /* ----------------------------------------------------------------------------------------------
             Use animation and update input view, table view frame to move down with keyboard.
             ---------------------------------------------------------------------------------------------- */
            DispatchQueue.main.async(execute: {
                // self.channel.endTyping()
                UIView.animate(withDuration: animationDurarion, delay: 0, options: options, animations: {
                    self.conTypeBottom.constant = 0
                    // self.ConstraintBottomSafeArea.isActive = true
                    self.view.layoutIfNeeded()
                })
            })
        }
    }
}

extension Chat : ChatMessageDelegate {
    func didRecieveMessage(in channel: SBDGroupChannel, message: SBDUserMessage) {
        if channel == self.channel {
            channel.markAsRead()
            messages.append(message)
            tblMessage.reloadData()
            scrollToBottom()
        }
    }
    
    func messageShouldRefresh() {
        
        guard channel != nil && !isLoading else {
            isPendingRefresh = true
            return
        }
        
        loadPreviousMessage(initial: true)
        // channel.markAsRead()
        // messages.append(message)
        // tblMessage.reloadData()
        // scrollToBottom()
    }
}

extension Chat : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        //channel.startTyping()
        guard textView.text! == "Skriv..."  else { return }
        textView.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        guard textView.text!.trimString == ""  else { return }
        textView.text = "Skriv..."
        
        conTypeHeight.constant = 60
        
        UIView.animate(withDuration: 0.10, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //        guard text != "\n" else {
        //            textView.resignFirstResponder()
        //            return true
        //        }
        //
        let height = textView.contentSize.height
        
        guard height < 160 && height > 60  else { return true }
        
        conTypeHeight.constant = height + 10
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        return true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Fuad
        // guard scrollView.contentOffset.y < 5 else {return}
        guard
            scrollView.contentOffset.y < 5 &&
                channel != nil
            else {return}
        
        loadPreviousMessage(initial: false)
    }
    
}

//MARK:- TableView Delegate || DataSource
//MARK:-

extension Chat: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let msg = self.messages[indexPath.row] as! SBDUserMessage
        
        let identifier = msg.sender?.userId == IK_USER.profile_id ? "ChatCellOutgoing" : "ChatCellIncoming"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ChatCell else {
            return UITableViewCell()
        }
        
        cell.setModel(message: msg)
        return cell
    }
}

class ChatCell: UITableViewCell {
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var lblTime: UILabel!
    
    override func awakeFromNib() {
    }
    
    func setModel(message:SBDUserMessage) {
        
        lblMessage.text = message.message
        lblMessage.sizeToFit()
        
        let messageTimestamp = Int64(message.createdAt)
        
        var lastMessageDate: Date?
        
        if String(format: "%lld", messageTimestamp).count == 10 {
            lastMessageDate = Date.init(timeIntervalSince1970: Double(messageTimestamp))
        }  else {
            lastMessageDate = Date.init(timeIntervalSince1970: Double(messageTimestamp) / 1000.0)
        }
        
        lblTime.text = lastMessageDate?.string(format: TIME_FORMAT)
    }
}

