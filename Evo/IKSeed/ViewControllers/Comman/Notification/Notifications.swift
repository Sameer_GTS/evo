//
//  Notification.swift
//  IKSeed
//
//  Created by Chanchal Warde on 4/7/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import  Spring

class Notifications : UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var tblNotification: UITableView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var btnBack: UIButton!

    @IBOutlet var conNavHeight: NSLayoutConstraint!
    
    @IBOutlet var viewError: SpringView!
    //MARK:- Variable
    //MARK:-
    
    var isShowNotification : Bool = true
    
    fileprivate var arrNotification : [UserNotification] = []
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnBack.isHidden = !isShowNotification
        
        if #available(iOS 11.0, *) {
            var top = UIApplication.shared.keyWindow?.safeAreaInsets.top
            conNavHeight.constant = conNavHeight.constant + top!
            var frame = viewHeader.frame
            frame.size.height += top!
            viewHeader.frame = frame
        }
        else {
            let top = UIApplication.shared.statusBarFrame.size.height
            //            conNavHeight.constant = conNavHeight.constant + top
            //            tblNotification.contentInset = UIEdgeInsets(top: CGFloat(-Float(top)), left: 0, bottom: 0, right: 0)
        }
        
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.extendedLayoutIncludesOpaqueBars = false
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        WebConnect.getNotification { (status, arrNo, message) in
            self.arrNotification = arrNo
            self.tblNotification.reloadData()
            
            UIView.animate(withDuration: 0.25) {
                self.viewError.alpha = arrNo.count > 0 ?  0 : 1
            }
        }
        
        if IK_USER.userType == .ace {
            Utils.shared.logEventWithFlurry(EventName: "ace_notification_screen", EventParam: [:])
        }else {
            Utils.shared.logEventWithFlurry(EventName: "notification_tab", EventParam: [:])
        }
        
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Other
    //MARK:-
}

//MARK:- Tableview Delegate || Datasource
//MARK:-

extension Notifications : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellNotification", for: indexPath) as! CellNotification
        
        let obj = arrNotification[indexPath.row]
        
        cell.imgUser.kf.indicatorType = .activity
        cell.imgUser.kf.setImage(with: URL(string: obj.profileImage),
                                 placeholder: #imageLiteral(resourceName: "thumb_menu"),
                                 options: nil,
                                 progressBlock: nil,
                                 completionHandler: nil)
        cell.lblMessage.text = obj.message
        cell.lblDate.text = obj.createdDate
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard IK_USER.userType != .ace else {
            self.tabBarController?.selectedIndex = 0
            return
        }
        
        let obj = arrNotification[indexPath.row]
        
        if obj.type == .bookingAccept || obj.type == .bookingReject {
            let workout = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Workout") as! Workout
            workout.isBack = true
            workout.canGoBack = true
            navigationController?.pushViewController(workout, animated: true)
        }
        else if obj.type == .availbilityAccept {
            WebConnect.requestStatus(completion: { (status, arrAce , message) in
                if status {
                    if arrAce.count > 0 {
                        let list = self.storyboard?.instantiateViewController(withIdentifier: "AvailableList") as! AvailableList
                        list.arrAce = arrAce
                        self.navigationController?.pushViewController(list, animated: true)
                    }
                    else {
                        let wait = self.storyboard?.instantiateViewController(withIdentifier: "Waiting") as! Waiting
                        wait.message = message
                        self.navigationController?.pushViewController(wait, animated: true)
                    }
                }
            })
        }
        else if obj.type == .aceApproved {
            if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                
                let newDic = NSMutableDictionary(dictionary: dict)
                newDic.setValue("2", forKey: "role")

                IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                IK_DEFAULTS.synchronize()
                
                IK_DELEGATE.TabBarConfiguration(true)
            }
        }
        else if obj.type == .newAce || obj.type == .seeAce48 || obj.type == .seeAce {
            WebConnect.getAceDetail(aceId: obj.aceId , completion: { (status, ace) in
                
                guard status else {return}
                
                let detail = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AceDetail") as! AceDetail
                detail.objAce = ace
                self.navigationController?.pushViewController(detail, animated: true)
            })
        }
    }
    
}

//MARK:- Cell
//MARK:-

class CellNotification : UITableViewCell {
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblDate: UILabel!
    
    override func awakeFromNib() {
        
    }
}
