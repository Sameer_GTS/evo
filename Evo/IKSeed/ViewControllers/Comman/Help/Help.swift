//
//  Help.swift
//  IKSeed
//
//  Created by Chanchal Warde on 4/2/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.


import UIKit
import Spring

class Help: UIViewController {
    
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var lblError: SpringLabel! {
        didSet {
            lblError.animation = Spring.AnimationPreset.FadeOut.rawValue
            lblError.curve = Spring.AnimationCurve.Linear.rawValue
            lblError.duration = 0
            lblError.animate()
        }
    }
    @IBOutlet var tvHelp: UITextView!
    @IBOutlet var btnSubmit: UIButton!{
        didSet {
            btnSubmit.dropShadow()
        }
    }
    @IBOutlet var viewThank: SpringView! {
        didSet {
            viewThank.animation = Spring.AnimationPreset.FadeOut.rawValue
            viewThank.curve = Spring.AnimationCurve.Linear.rawValue
            viewThank.duration = 0
            viewThank.animate()
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utils.shared.logEventWithFlurry(EventName: "help_screen", EventParam: [:])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickSubmit(_ sender: UIButton) {
        
        guard !tvHelp.text!.isEmpty() else {
            lblError.animation = Spring.AnimationPreset.FadeIn.rawValue
            lblError.curve = Spring.AnimationCurve.Linear.rawValue
            lblError.duration = 0.35
            lblError.animate()
            return
        }
        
        self.view.endEditing(true)
        
        WebConnect.sumitHelp(comment: tvHelp.text!.trimString) { (status, message) in
            // Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            guard status else {return}
            
            self.viewThank.animation = Spring.AnimationPreset.FadeIn.rawValue
            self.viewThank.curve = Spring.AnimationCurve.Linear.rawValue
            self.viewThank.duration = 0.35
            self.viewThank.animate()
        }
    }
    
    //MARK:- Other
    //MARK:-
}

//MARK:- Textview Delegate
//MARK:-

extension Help : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblError.animation = Spring.AnimationPreset.FadeOut.rawValue
        lblError.curve = Spring.AnimationCurve.Linear.rawValue
        lblError.duration = 0
        lblError.animate()
    }
}
