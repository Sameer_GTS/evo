//
//  Terms.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/13/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit

class Terms: UIViewController, UIWebViewDelegate {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var webview: UIWebView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var bgView: UIView!
    
    //MARK:- Variable
    //MARK:-
    
    var isHideTabBar : Bool = false
    var isFirstLoader : Bool = true
    var isSecondLoader : Bool = true
    
    var url : String!
    var strTitle : String!
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = strTitle
        webview.loadRequest(URLRequest(url: URL(string: url)!))
        
        bgView.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false;

        if isHideTabBar
        {
            IK_DELEGATE.tabBarController.tabBar.isHidden = true
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isHideTabBar
        {
            IK_DELEGATE.tabBarController.tabBar.isHidden = false
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isHideTabBar
        {
            IK_DELEGATE.tabBarController.tabBar.isHidden = true
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        DispatchQueue.main.async {
            self.bgView.isHidden = true
            FUProgressView.hideProgress()
        }
        
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        //_ = navigationController?.popViewController(animated: true)
        self.dismiss(animated: true) {
            
        }
    }
    
    //MARK:- Other
    //MARK:-
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("webViewDidStartLoad")
        
        guard isFirstLoader else {
            return
        }
        
        DispatchQueue.main.async {
            self.isFirstLoader = false
            //FUProgressView.showProgress("Et lite øyeblikk...",isUseLayer: false)
            self.bgView.isHidden = false
            FUProgressView.loadProgress("Et lite øyeblikk...", isUseLayer: true, baseView: self.bgView)
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("webViewDidFinishLoad")
        
        DispatchQueue.main.async {
            //self.isFirstLoader = true
            //self.isSecondLoader = true
            FUProgressView.hideProgress()
            self.btnBack.isUserInteractionEnabled = true
            self.bgView.isHidden = true
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("didFailLoadWithError")
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        print("shouldStartLoadWith")

        //DispatchQueue.main.async {
            //FUProgressView.showProgress("Et lite øyeblikk...",isUseLayer: false)
        //}
        
        if navigationType == .linkClicked || navigationType == .formSubmitted || navigationType == .formResubmitted
        {
            self.isFirstLoader = true
        }
        
        return true
    }
    
}
