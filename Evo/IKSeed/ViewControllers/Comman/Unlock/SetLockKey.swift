//
//  SetLockKey.swift
//  IKSeed
//
//  Created by Chanchal Warde on 4/2/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.


import UIKit
import Spring

class SetLockKey: UIViewController {
    
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    var timer = Timer()
    var counter = 0
    var msg = ""
    @IBOutlet var bgView: UIView!
    
    @IBOutlet var lblError: SpringLabel! {
        didSet {
            lblError.animation = Spring.AnimationPreset.FadeOut.rawValue
            lblError.curve = Spring.AnimationCurve.Linear.rawValue
            lblError.duration = 0
            lblError.animate()
        }
    }
    @IBOutlet var tfPhoneNo: UITextField!
    @IBOutlet var btnSubmit: UIButton!{
        didSet {
            btnSubmit.dropShadow()
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let eventParam = ["profile_id":IK_USER.profile_id] as [String:String]
        Utils.shared.logEventWithFlurry(EventName: "ekey_phone_no_screen", EventParam: [:])
        
        tfPhoneNo.isUserInteractionEnabled = false
        tfPhoneNo.text = "Your phone #: " + IK_USER.phone_number
        
        bgView.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickSubmit(_ sender: UIButton) {
        
        guard !IK_USER.phone_number.isEmpty() else {
            lblError.animation = Spring.AnimationPreset.FadeIn.rawValue
            lblError.curve = Spring.AnimationCurve.Linear.rawValue
            lblError.duration = 0.35
            lblError.animate()
            return
        }
        
        self.view.endEditing(true)
        
        bgView.isHidden = false
        //FUProgressView.loadProgress("Kobler til adgang",isWebView: false, isUseLayer: true, baseView: bgView)
        FUProgressView.showProgress("Kobler til adgang", isUseLayer: true,isFromKeyView: true)
        
        WebConnect.getEKeys(phoneno: IK_USER.phone_number.trimString) { (status, message, delay, respMessage) in
            
            guard status else {
                self.bgView.isHidden = true
                Utils.shared.alert(on: self, message: message, affirmButton: "OK", cancelButton: nil)
                return
            }
            
            //self.msg = "Vi aktiverer mobil adgang til deg. Når denne meldingen forsvinner er alt klart til bruk."
            
            //FUProgressView.loadProgress("Aktiverer adgang",isWebView: false, isUseLayer: true, baseView: self.bgView)
            FUProgressView.showProgress("Aktiverer adgang", isUseLayer: true, isFromKeyView: true)
            self.msg = respMessage
            self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(delay), target: self, selector: #selector(self.startTimer), userInfo: nil, repeats: true)
            
        }
        
        /*
        WebConnect.getEKeys(phoneno: IK_USER.phone_number.trimString) { (status, message) in
            /*
            Utils.shared.alert(on: self, title: "", message: message, affirmButton: "OK", cancelButton: nil, dismissWith: { (val) in
                self.clickBack(UIButton())
            })
            */
        }*/
        
    }
    
    @objc func startTimer() {
        
        timer.invalidate()
        
        bgView.isHidden = true
        FUProgressView.hideProgress()
        
        Utils.shared.alert(on: self, title: "", message: msg, affirmButton: "OK", cancelButton: nil, dismissWith: { (val) in
            self.clickBack(UIButton())
        })
        
    }
    
    //MARK:- Other
    //MARK:-
}

//MARK:- Textview Delegate
//MARK:-

extension SetLockKey : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblError.animation = Spring.AnimationPreset.FadeOut.rawValue
        lblError.curve = Spring.AnimationCurve.Linear.rawValue
        lblError.duration = 0
        lblError.animate()
    }
}
