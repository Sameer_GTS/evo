/**
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import CoreBluetooth
import Mixpanel

class HRMViewController: UIViewController  {
    
    //MARK:- Constant
    let GENERIC_ACCESS_STRING  = "180A"
    
    // Search devices for this services only
    let SERVICE_STRING = "9DB9222E-2F9D-4435-B42D-FAC1BCC2568B"
    
    // read request to get serial number (Inside GENERIC_ACCESS_STRING service)
    let CHARACTERISTIC_LOCK_SERIAL_NO_STRING = CBUUID(string: "2A25")
    
    // read request to get lock state
    let CHARACTERISTIC_LOCK_STATE_STRING = CBUUID(string: "7FA7C4BD-8D34-4358-B028-1157F564F040")
    
    // read request to get lock nonce value
    let CHARACTERISTIC_LOCK_NOUNCE_STRING = CBUUID(string: "F3A111AA-D7C9-4FD9-B017-530A9896A7FC")
    
    // write request to send command to open
    let CHARACTERISTIC_LOCK_OPEN_STRING = CBUUID(string: "34E63C26-DE68-4FF7-AE99-8132E243F22A")
    
    //MARK:- IBOutlets
    
    //@IBOutlet var tblList: UITableView!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblComment: UILabel!
    
    @IBOutlet var txtArea: UITextView!
    
    @IBOutlet var btnBgRound: UIButton!
    
    
    //MARK:- Variables
    
    var arrItemPeripheral = [CBPeripheral]()
    var centralManager: CBCentralManager!
    
    var blePeripheral: CBPeripheral!
    
    
    var chLOCK_SERIAL_NO_STRING : CBCharacteristic!
    var chLOCK_STATE : CBCharacteristic!
    var chLOCK_NOUNCE : CBCharacteristic!
    var chLOCK_OPEN : CBCharacteristic!
    
    var searchType = LockTypes.lockIn
    var isScaning = false
    var isReadingSerialNumbers = false
    var nonceValue : String = ""
    
    var timer = Timer()

    var gymId = ""

    var inSerialNumber = ""
    var outSerialNumber = ""
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var currentObj = LockGymModel()
        if let a = (arrLockGyms.filter { $0.id == Int(gymId)}).first {
            currentObj = a
        }
        let arrIds = currentObj.serialNo.split(separator: ",")
        inSerialNumber = String(arrIds.first!)
        outSerialNumber = String(arrIds.last!)
        
        if searchType == .lockIn
        {
            /*
            Mixpanel.sharedInstance()?.identify(IK_USER.profile_id, usePeople: true)
            
            Mixpanel.sharedInstance()?.track("Lock In", properties:["Gym Id":currentObj.id, "Gym Name":currentObj.name, "User Id" : IK_USER.profile_id ?? "", "Serial Number In" : inSerialNumber, "Serial Number Out" : outSerialNumber])
            */
            
            /*
            let eventParams = ["Gym Id":currentObj.id,
                               "Gym Name":currentObj.name,
                               "User Id" : IK_USER.profile_id ?? "",
                               "Serial Number In" : inSerialNumber,
                               "Serial Number Out" : outSerialNumber] as [String : Any]
            */
            
            let eventParam = ["gym":currentObj.name,
                              //"serial_numbers":inSerialNumber
                            ] as [String:String]
            Utils.shared.logEventWithFlurry(EventName: "lock_In", EventParam: eventParam)
        }
        else
        {
            /*
            Mixpanel.sharedInstance()?.identify(IK_USER.profile_id, usePeople: true)
            
            Mixpanel.sharedInstance()?.track("Lock Out", properties:["Gym Id":currentObj.id, "Gym Name":currentObj.name, "User Id" : IK_USER.profile_id ?? "", "Serial Number In" : inSerialNumber, "Serial Number Out" : outSerialNumber])
            */
            
            /*
            let eventParams = ["Gym Id":currentObj.id,
                               "Gym Name":currentObj.name,
                               "User Id" : IK_USER.profile_id ?? "",
                               "Serial Number In" : inSerialNumber,
                               "Serial Number Out" : outSerialNumber] as [String : Any]
            */
            
            let eventParam = ["gym":currentObj.name,
                              //"serial_numbers":outSerialNumber
                            ] as [String:String]
            Utils.shared.logEventWithFlurry(EventName: "lock_Out", EventParam: eventParam)
        }
        
        printLogs(data: "Initializing...")
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    //MARK:- Action handle
    @IBAction func btnBackTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sacnActionsClicked(_ sender: UIButton) {
        printLogs(data: "Scan clicked")
        Defaults.set(false, forKey: DefaultsKeys.isScanned_gymId.rawValue + gymId)
        startScaning()
    }
    
    @IBAction func tabActionsClicked(_ sender: UIButton) {
        printLogs(data: "Start clicked")
        timer.invalidate()
        
        if Defaults.bool(forKey: DefaultsKeys.isScanned_gymId.rawValue + gymId)
        {
            openLock()
        }
        else
        {
            startScaning()
        }
    }
    
    @IBAction func clearActionsClicked(_ sender: UIButton) {
        txtArea.text = ""
    }
    
    @IBAction func copyActionsClicked(_ sender: UIButton) {
        UIPasteboard.general.string = txtArea.text
    }
    
    func openLock()
    {
        printLogs(data: "openLock")
        btnBgRound.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7411764706, blue: 0.7490196078, alpha: 1)
        lblStatus.text = bleStatus.opening.rawValue
        lblComment.text = ""

        if searchType == LockTypes.lockIn
        {
            printLogs(data: "Opening lock in")
            let hdInId = Defaults.object(forKey: DefaultsKeys.inHardwareId_gymId.rawValue + gymId)
            startToConnectWithHardware(hardwareId: hdInId as! String)
        }
        else if searchType == LockTypes.lockOut
        {
            printLogs(data: "Opening lock out")
            let hdOutId = Defaults.object(forKey: DefaultsKeys.outHardwareId_gymId.rawValue + gymId)
            startToConnectWithHardware(hardwareId: hdOutId as! String)
        }
    }
    
    func startToConnectWithHardware(hardwareId : String)
    {
        printLogs(data: "startToConnectWithHardware")
        printLogs(data: "hardwareId")
        printLogs(data: hardwareId)
       
        let hardwareUUID = UUID.init(uuidString: hardwareId)!
        let peripheralsDevices = centralManager.retrievePeripherals(withIdentifiers: [hardwareUUID])
        printLogs(data: peripheralsDevices)
        
        if !peripheralsDevices.isEmpty
        {
            let per = peripheralsDevices.first!
            blePeripheral = per
            printLogs(data: blePeripheral)
            blePeripheral.delegate = self
            centralManager.connect( blePeripheral, options: nil)
        }
        else{
            btnBgRound.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.007843137255, blue: 0.3333333333, alpha: 1)
            lblStatus.text = bleStatus.failed.rawValue
            lblComment.text = ""
        }
    }
    
    func startScaning()
    {
        printLogs(data: "startScaning")
        
        isScaning = true
        isReadingSerialNumbers = true

        btnBgRound.backgroundColor = #colorLiteral(red: 0.662745098, green: 0.662745098, blue: 0.662745098, alpha: 1)
        lblStatus.text = bleStatus.connecting.rawValue
        lblComment.text = TextMessage.pleaseWait

        arrItemPeripheral.removeAll()
        let serviceStringCBUUID = CBUUID(string: SERVICE_STRING)
        centralManager.scanForPeripherals(withServices: [serviceStringCBUUID], options: nil)
        
        timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(stopScanning), userInfo: nil, repeats: false)
    }
    
    @objc func stopScanning()
    {
        printLogs(data: "stopScanning")
        timer.invalidate()
        
        centralManager.stopScan()
        lblComment.text = ""
        if arrItemPeripheral.isEmpty
        {
            isScaning = false

            btnBgRound.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.007843137255, blue: 0.3333333333, alpha: 1)
            lblStatus.text = bleStatus.notFound.rawValue
            lblComment.text = ""
            
        } else {
            processScannedDevices()
        }
    }
    
    func bluetoothIsOff()
    {
        printLogs(data: "bluetooth Is Off.")
        centralManager.stopScan()
        arrItemPeripheral.removeAll()
        
        Utils.shared.alert(on: self, title: "", message: "Please turn ON Bluetooth of this device.", affirmButton: "OK", cancelButton: nil, dismissWith: { (val) in
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    func printLogs(data : Any)
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss:SSS"
//        formatter.timeStyle = .medium
//        formatter.dateStyle = .full
        let dateStr = formatter.string(from: Date())
        txtArea.text = txtArea.text + "\n" + dateStr + " :: " + String.init(describing: data)
    }
    
    func processScannedDevices()
    {
        printLogs(data: "processScannedDevices")
        
        if arrItemPeripheral.isEmpty
        {
            isScaning = false
            isReadingSerialNumbers = false
            printLogs(data: "Processing completed")
            Defaults.set(true, forKey: DefaultsKeys.isScanned_gymId.rawValue + gymId)
            openLock()
            return
        }
        
        printLogs(data: "connecting...")
        
        let peripheral = arrItemPeripheral.first
        blePeripheral = peripheral
        blePeripheral.delegate = self
        centralManager.connect(blePeripheral, options: nil)
    }
}

extension HRMViewController: CBCentralManagerDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            printLogs(data: "central.state is .unknown")
        case .resetting:
            printLogs(data: "central.state is .resetting")
        case .unsupported:
            printLogs(data: "central.state is .unsupported")
        case .unauthorized:
            printLogs(data: "central.state is .unauthorized")
        case .poweredOff:
            printLogs(data: "central.state is .poweredOff")
            printLogs(data: "Bluetooth poweredOff")
            bluetoothIsOff()
        case .poweredOn:
            printLogs(data: "Bluetooth powerOn")
            tabActionsClicked(UIButton())
        //centralManager.scanForPeripherals(withServices: nil, options: nil)
        default:
            printLogs(data: "default used")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        printLogs(data: "getting Periphal : ")
        printLogs(data: peripheral)
        
        if !arrItemPeripheral.contains(peripheral)
        {
            arrItemPeripheral.append(peripheral)
        }
    }
    
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        printLogs(data: "Connected")
        printLogs(data: peripheral)
        
        printLogs(data: "discoverServices")
        if isScaning
        {
            blePeripheral.discoverServices([CBUUID(string: GENERIC_ACCESS_STRING)])
        }
        else
        {
            blePeripheral.discoverServices([CBUUID(string: SERVICE_STRING)])
        }
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        printLogs(data: "didFailToConnect")
        printLogs(data: peripheral)
        
        if isScaning
        {
            btnBgRound.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.007843137255, blue: 0.3333333333, alpha: 1)
            lblStatus.text = bleStatus.disconnected.rawValue
            lblComment.text = ""
        }
        else{
            btnBgRound.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.007843137255, blue: 0.3333333333, alpha: 1)
            lblStatus.text = bleStatus.failed.rawValue
            lblComment.text = ""
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        printLogs(data: "didDisconnectPeripheral")
        printLogs(data: peripheral)
        
        if !isScaning && !isReadingSerialNumbers {
            btnBgRound.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.007843137255, blue: 0.3333333333, alpha: 1)
            lblStatus.text = bleStatus.disconnected.rawValue
            lblComment.text = ""
        }
        
        if isReadingSerialNumbers
        {
            processScannedDevices()
        }
    }
}


extension HRMViewController: CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else
        {
            return
        }
        
        printLogs(data: "Services Found---")
        for service in services
        {
            printLogs(data: "service = ")
            printLogs(data: service)
            printLogs(data: "uuid = ")
            printLogs(data: service.uuid.uuidString)
        }
        
        printLogs(data: "Discovering Characteristics")
        for service in services
        {
            if isScaning
            {
                if service.uuid.uuidString == GENERIC_ACCESS_STRING
                {
                    printLogs(data: "Discovering Characteristics for serial number")
                    blePeripheral.discoverCharacteristics([CHARACTERISTIC_LOCK_SERIAL_NO_STRING], for: service)
                }
            }
            else if (service.uuid.uuidString == SERVICE_STRING)
            {
                printLogs(data: "Discovering Characteristics for lock nonce, nonce & state")
                blePeripheral.discoverCharacteristics([CHARACTERISTIC_LOCK_NOUNCE_STRING, CHARACTERISTIC_LOCK_OPEN_STRING, CHARACTERISTIC_LOCK_STATE_STRING], for: service)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?)
    {
        guard let characteristics = service.characteristics else { return }
        
        printLogs(data: "Characteristics Found---")
        for characteristic in characteristics {
            printLogs(data: characteristic)
            printLogs(data: "uuid = ")
            printLogs(data: characteristic.uuid.uuidString)
            printLogs(data: "properties")
            printLogs(data: characteristic.properties)
        }
        
        for characteristic in characteristics
        {
            if isScaning && characteristic.uuid == CHARACTERISTIC_LOCK_SERIAL_NO_STRING
            {
                printLogs(data: "reading serial number")
                chLOCK_SERIAL_NO_STRING = characteristic
                blePeripheral.readValue(for: characteristic)
            }
            else{
                switch characteristic.uuid
                {
                case CHARACTERISTIC_LOCK_NOUNCE_STRING:
                    if !isScaning
                    {
                        printLogs(data: "reading nonce value")
                        chLOCK_NOUNCE = characteristic
                        blePeripheral.readValue(for: characteristic)
                    }
                    break
                    
                case CHARACTERISTIC_LOCK_OPEN_STRING:
                    if !isScaning
                    {
                        chLOCK_OPEN = characteristic
                        //blePeripheral.readValue(for: characteristic)
                    }
                    break
                    
                case CHARACTERISTIC_LOCK_STATE_STRING:
                    if !isScaning
                    {
                        chLOCK_STATE = characteristic
                        //blePeripheral.readValue(for: characteristic)
                    }
                    break
                    
                default:
                    printLogs(data: "Unhandled didDiscoverCharacteristicsFor : \(characteristic.uuid)")
                }
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        printLogs(data: "didUpdateValueForCharacteristic = ")
        printLogs(data: characteristic)
        printLogs(data: "uuid = ")
        printLogs(data: characteristic.uuid.uuidString)
        switch characteristic.uuid        {
        case CHARACTERISTIC_LOCK_SERIAL_NO_STRING:
            printLogs(data: "CHARACTERISTIC_LOCK_SERIAL_NO_STRING")
            let charValue = characteristic.value
            let serialNo = String(data: charValue!, encoding: String.Encoding.utf8)
            printLogs(data: "serialNo = ")
            printLogs(data: serialNo?.trimmingCharacters(in: .whitespacesAndNewlines))
            if inSerialNumber.trimString == serialNo?.trimString            {
                printLogs(data: "Found serial number for IN")
                Defaults.set(blePeripheral.identifier.uuidString, forKey: DefaultsKeys.inHardwareId_gymId.rawValue + gymId)
                Defaults.synchronize()
                centralManager.cancelPeripheralConnection(blePeripheral)
                if !arrItemPeripheral.isEmpty
                {
                    arrItemPeripheral.removeFirst()
                }
                //processScannedDevices()
            }
            else if outSerialNumber.trimString == serialNo?.trimString            {
                printLogs(data: "Found serial number for OUT")
                Defaults.set(blePeripheral.identifier.uuidString, forKey: DefaultsKeys.outHardwareId_gymId.rawValue + gymId)
                Defaults.synchronize()
                centralManager.cancelPeripheralConnection(blePeripheral)
                if !arrItemPeripheral.isEmpty
                {
                    arrItemPeripheral.removeFirst()
                }
                //processScannedDevices()
            }
            break
            
        case CHARACTERISTIC_LOCK_NOUNCE_STRING:
            printLogs(data: "CHARACTERISTIC_LOCK_NOUNCE_STRING")
            
            let rxDatat = characteristic.value
            nonceValue = (characteristic.value?.hexEncodedString())!
            encodeDataForWrite()
            break
            
        case CHARACTERISTIC_LOCK_STATE_STRING:
            printLogs(data: "CHARACTERISTIC_LOCK_STATE_STRING")
            
            let rxDatat = characteristic.value
            let dataValue = String(data: rxDatat!, encoding: String.Encoding.utf8)!
            printLogs(data: dataValue)
            
            if dataValue == "\0"
            {
                printLogs(data: "inside if")
                btnBgRound.backgroundColor = #colorLiteral(red: 0.4823529412, green: 0.737254902, blue: 0.3333333333, alpha: 1)
                lblStatus.text = bleStatus.doorOpened.rawValue
                lblComment.text = ""
            }
            else{
                printLogs(data: "inside else")
                btnBgRound.backgroundColor = #colorLiteral(red: 0.4823529412, green: 0.737254902, blue: 0.3333333333, alpha: 1)
                lblStatus.text = bleStatus.doorLocked.rawValue
                lblComment.text = ""
            }
            DispatchQueue.main.asyncAfter(deadline: .now()+3.0)
            {
                self.centralManager.cancelPeripheralConnection(self.blePeripheral)
                self.navigationController?.popViewController(animated: true)
            }
            break
            
        default:
            printLogs(data: "Unhandled Characteristic UUID: \(characteristic.uuid)")
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        printLogs(data: "didWriteValueForcharacteristic = ")
        printLogs(data: characteristic)
        printLogs(data: "uuid = ")
        printLogs(data: characteristic.uuid)
        
        switch characteristic.uuid
        {
        case CHARACTERISTIC_LOCK_OPEN_STRING:
            if (error != nil) {
                printLogs(data: "Command write successfully")
                blePeripheral.readValue(for: chLOCK_STATE)
            }
            else{
                printLogs(data: "Command failed")
            }
            break
        default:
            printLogs(data: "Unhandled Characteristic UUID: \(characteristic.uuid)")
        }
    }
    
    func encodeDataForWrite()
    {
        printLogs(data: "sending command to open")
        let ref = Defaults.object(forKey: DefaultsKeys.eKeyReference.rawValue) as! String
        //"snPieu"
        let refHex = ref.hexEncodedStringFromString()
        
        printLogs(data: "ref = ")
        printLogs(data: ref)
        printLogs(data: "refHex = ")
        printLogs(data: refHex)
        
        let eKey = Defaults.object(forKey: DefaultsKeys.eKey.rawValue) as! String
        //"9aYaX7yplpBj1qxI8MlIag=="
        let decodedData = Data(base64Encoded: eKey)!
        let eKeyDecoded = String(decoding: decodedData, as: UTF8.self)
        //String(data: decodedData, encoding: .utf8)!
        let eKeyHex = decodedData.hexEncodedString()
        
        printLogs(data: "eKey = ")
        printLogs(data:  eKey)
        
        printLogs(data: "eKeyDecoded = ")
        printLogs(data:  eKeyDecoded)
        
        printLogs(data: "ekeyHex = ")
        printLogs(data:  eKeyHex)
        
        let nonceHex = nonceValue
        
        printLogs(data: "nonceHex = ")
        printLogs(data:  nonceHex)
        
        let nonceAndEkey = nonceHex + eKeyHex
        printLogs(data: "nonceAndEkey = ")
        printLogs(data:  nonceAndEkey)
        
        let nonceData = nonceAndEkey.hexDecodedDataFromString()
        let sha256Data = nonceData!.sha256()
        let sha256hex = sha256Data!.hexEncodedString()
        let sha25HexSubstring = sha256hex.substring(to: 16)
        
        printLogs(data: "sha256hex = ")
        printLogs(data:  sha256hex)
        
        printLogs(data: "sha25HexSubstring = ")
        printLogs(data:  sha25HexSubstring)
        
        let commandTxt = refHex + sha25HexSubstring
        
        printLogs(data: "commendTxt = ")
        printLogs(data:  commandTxt)
        
        let commandData = commandTxt.hexDecodedDataFromString()
        
        printLogs(data: "writeWithoutResponse")
        printLogs(data: chLOCK_OPEN.properties.contains(CBCharacteristicProperties.writeWithoutResponse))
        printLogs(data: "write")
        printLogs(data: chLOCK_OPEN.properties.contains(CBCharacteristicProperties.write))
        printLogs(data: "authenticatedSignedWrites")
        printLogs(data: chLOCK_OPEN.properties.contains(CBCharacteristicProperties.authenticatedSignedWrites))
        
        if chLOCK_OPEN.properties.contains(CBCharacteristicProperties.writeWithoutResponse) {
            printLogs(data: "Command write without response")
            blePeripheral.writeValue(commandData! as Data, for: chLOCK_OPEN, type: .withoutResponse)

            DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
                self.printLogs(data: "Command for read")
                self.blePeripheral.readValue(for: self.chLOCK_STATE)
            }
        }
        else{
            printLogs(data: "Command write with response")
            blePeripheral.writeValue(commandData! as Data, for: chLOCK_OPEN, type: .withResponse)
        }
        //    String eKey = MyApplication.getStringPrefs(ConstantUtills.USER_EKEY);
        //    String ekeyDecoded = StringUtils.decodeString(eKey);
        //    String ekeyHex = new String(Hex.encodeHex(Base64.decode(eKey, Base64.DEFAULT)));
        //
        //    print("ref = ", ref)
        //    print("refHex = ", refHex)
        //
        //    print("eKey = ", eKey)
        //    print("eKeyDecoded = ", ekeyDecoded)
        //    print("ekeyHex = ", ekeyHex)
        //
        //    String nonceCharValue = new String(Hex.encodeHex(nonceCharacteristic.getValue()));
        //    print("nonceCharValue ", nonceCharValue)
        //
        //    String nonceAndEkey = mNonceValue + ekeyHex;
        //    print("nonceAndEkey = ", nonceAndEkey)
        //
        //    byte[] nonceHexBytes = Hex.decodeHex(nonceAndEkey.toCharArray());
        //    byte[] sha256Bytes = DigestUtils.sha256(nonceHexBytes);
        //    String sha256hex = new String(Hex.encodeHex(sha256Bytes));
        //
        //    //      MessageDigest digest = MessageDigest.getInstance("SHA-256");
        //    //      byte[] sha256Bytes = digest.digest(nonceAndEkey.getBytes(StandardCharsets.UTF_8));
        //    //      String sha256hex = StringUtils.getHex(sha256Bytes);
        //    print("sha256hex = ", sha256hex)
        //    print("sha256hex subStr = ", sha256hex.substring(0, 16))
        //
        //    String commendTxt = refHex + sha256hex.substring(0, 16);
        //    print("commendTxt: ", commendTxt)
        //
        //    characteristicLockOpen.setValue(Hex.decodeHex(commendTxt.toCharArray()));
    }
    
    
    
}


extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
    
    public func sha256Hash()  {
        //-> Data
        /*
         let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
         var hash = [UInt8](repeating: 0, count: digestLength)
         CC_SHA256(input.bytes, UInt32(input.length), &hash)
         return NSData(bytes: hash, length: digestLength)
         */
        
        //    let transform = SecDigestTransformCreate(kSecDigestSHA2, 256, nil)
        //    SecTransformSetAttribute(transform, kSecTransformInputAttributeName, self as CFTypeRef, nil)
        //    return SecTransformExecute(transform, nil) as! Data
    }
}

extension String
{
    func hexEncodedStringFromString() -> String {
        let dataTmp = self.data(using: String.Encoding.utf8)
        return dataTmp!.hexEncodedString()
    }
    
    func hexDecodedDataFromString() -> NSData! {
        var data = Data(capacity: self.count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            data.append(num)
        }
        
        guard data.count > 0 else { return nil }
        
        return data as NSData
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
    
}

//MARK:- Cell Definition
//MARK:-
class cellList: UITableViewCell {
    @IBOutlet var lblItem: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var loading : UIActivityIndicatorView!
}

