//
//  Unlock.swift
//  Ikseed
//
//  Created by Fahad Mohammed Firoz Khan on 09/01/19.
//  Copyright © 2019 Chanchal Warde. All rights reserved.
//

import UIKit
import Mixpanel

class Unlock: UIViewController {

    @IBOutlet var btnSelectGym: UIButton!
    var currentLockGym = LockGymModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadUI()
        getGymsList(showProgess: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utils.shared.logEventWithFlurry(EventName: "unlock_tab", EventParam: [:])
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }

    func loadUI()  {
        let lockId = Defaults.object(forKey: LOCK_GYM_CURRENT)
        if lockId != nil, let a = (arrLockGyms.filter { $0.id == Int(lockId as! String)}).first {
            self.currentLockGym = a
        }

        if !currentLockGym.name.isEmpty {
            btnSelectGym.setTitle(self.currentLockGym.name, for: .normal)
        }
    }
}

extension Unlock
{
    @IBAction func clickInTab(_ sender: UIButton) {
        
        
        if Defaults.object(forKey: DefaultsKeys.eKey.rawValue) != nil
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HRMViewController") as! HRMViewController
            vc.searchType = LockTypes.lockIn
            vc.gymId = String.init(describing: currentLockGym.id)
            // send gym id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            
            Utils.shared.alert(on: self, title: "", message: TextMessage.noKeySetup, affirmButton: "Få tilgang", cancelButton: "Kanseller") { (val) in
                if val == 0
                {
                    self.clickSetKeys(UIButton())
                }
            }
        }
    }
    
    @IBAction func clickOutTab(_ sender: UIButton) {
        
        
        if Defaults.object(forKey: DefaultsKeys.eKey.rawValue) != nil
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HRMViewController") as! HRMViewController
            vc.searchType = LockTypes.lockOut
            vc.gymId = String.init(describing: currentLockGym.id)
            // send gym id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            Utils.shared.alert(on: self, title: "", message: TextMessage.noKeySetup, affirmButton: "Få tilgang", cancelButton: "Kanseller") { (val) in
                if val == 0
                {
                    self.clickSetKeys(UIButton())
                }
            }
        }
    }
    
    @IBAction func clickSetKeys(_ sender: UIButton) {
        let notification = self.storyboard?.instantiateViewController(withIdentifier: "SetLockKey") as! SetLockKey
        self.navigationController?.pushViewController(notification, animated: true)
    }
    
    @IBAction func clickLocation(_ sender: UIButton) {
        
        let gym = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LockGym_List") as! LockGym_List
        
        // jitendra
        for i in 0..<arrLockGyms.count {
            if self.currentLockGym.name == arrLockGyms[i].name {
                gym.selectedIndex = i
                gym.selectedGymId = arrLockGyms[i].id
                break
            }
        }
        
        gym.isHandler = true
        gym.onSelect = { obj in
            self.currentLockGym = obj
            
            Defaults.set(String.init(describing: obj.id), forKey: LOCK_GYM_CURRENT)
            Defaults.synchronize()

            DispatchQueue.main.async {
                self.loadUI()
            }
        }
        
        self.navigationController?.pushViewController(gym , animated: true)
        
        //        let picker =  ListPicker.showPicker(on: self, arrList: arrAceCities)
        //        picker.onSelectItem = { obj in
        //            self.filter.cityName = obj.name
        //            self.filter.cityId = obj.id
        //            sender.setTitle(obj.name + ", Norge", for: .normal)
        //            self.getAces()
        //        }
    }
}

extension Unlock
{
    @objc fileprivate func getGymsList(showProgess : Bool) {
        
        if showProgess {
            FUProgressView.showProgress()
        }
        
        WebConnect.getLockGymsList() { (status, aces , message)  in
            
            FUProgressView.hideProgress()
            
            UIView.animate(withDuration: 0.35) {
                self.view.layoutIfNeeded()
            }
            
            DispatchQueue.main.async {
                self.loadUI()
            }
        }
    }
}

