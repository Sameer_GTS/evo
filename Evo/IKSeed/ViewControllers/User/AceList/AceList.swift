//
//  AceList.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/10/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.

import UIKit
import Kingfisher
import Spring
//import FSPagerView


class AceList: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    let ROW_HEIGHT : CGFloat = 130
    fileprivate let refresh = UIRefreshControl()
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var lblAvailableBadge: UILabel!
    @IBOutlet var icnUnread: UIImageView! {
        didSet {
            icnUnread.isHidden = true
        }
    }
    
    @IBOutlet var btnSelectGym: UIButton!
    
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var conMenuLeading: NSLayoutConstraint!
    @IBOutlet var viewSideMenu: UIView! {
        didSet {
            let gesture = UISwipeGestureRecognizer.init(target: self, action: #selector(sideMenuSwipped))
            gesture.direction = .left
            viewSideMenu.addGestureRecognizer(gesture)
        }
    }
    
    
    
    @IBOutlet var viewNoAce : SpringView!
    
    @IBOutlet var btnAvailable: UIButton!
    
    /// Pagvarew to show scrollable features
    @IBOutlet var pagerViewBlog: FSPagerView! {
        didSet {
            let nib1 = UINib(nibName: "cellBlogCarousel", bundle: nil)
            self.pagerViewBlog.register(nib1, forCellWithReuseIdentifier: "cellBlogCarouselID")
            self.pagerViewBlog.delegate = self
            self.pagerViewBlog.dataSource = self
            self.pagerViewBlog.interitemSpacing = 10
            // self.pagerViewBlog.transformer = FSPagerViewTransformer(type: .linear)
        }
    }
    
    /// Pagvarew to show scrollable features
    @IBOutlet var pagerViewAce: FSPagerView! {
        didSet {
            let nib2 = UINib(nibName: "cellAceCarousel", bundle: nil)
            self.pagerViewAce.register(nib2, forCellWithReuseIdentifier: "cellAceCarouselID")
            self.pagerViewAce.transformer = FSPagerViewTransformer(type: .linear)
            self.pagerViewAce.delegate = self
            self.pagerViewAce.dataSource = self
        }
    }
    
    
    @IBOutlet var tblList: UITableView!
    @IBOutlet var btnSports: [UIButton]!
    @IBOutlet var viewSport: [UIView]!
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var arrAce : [Ace] = []
    fileprivate var gymCenter : (id:Int, name:String , address : String )!

    fileprivate var arrBlog : [Blog] = []
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
               
        refresh.attributedTitle = "Oppdater".changeColor(#colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        refresh.tintColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        refresh.addTarget(self, action: #selector(getAces) , for: UIControl.Event.valueChanged)
        //tblAce.addSubview(refresh)
        
        ChatManager.shared.unreadDelegate = self
        
        IK_NOTIFICATION.addObserver(self,
                                    selector: #selector(getBadgeCount),
                                    name: NOTIFICATION_BADGE,
                                    object: nil)
        
        
        if let a = (arrAllGyms.filter { $0.id == IK_USER.gym}).first {
            self.gymCenter = a
        }
        
        //self.btnAvailable.dropShadow(0.2, radius: 5, offset: CGSize(width: 0, height: 2), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        
        getAces(showProgess: true)
        
        loadBlogs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard isMovingToParent else {return}
        
        var nameStr = ""
        if let a = (arrAllGyms.filter { $0.id == IK_USER.gym}).first {
            nameStr = a.name
        }
        
        let sessionParam = ["interest" : IK_USER.sportId.map(String.init).joined(separator: ","),
                            "age_group" : arrAge[IK_USER.ageGroup],
                            "fitness_level" : arrFitness[IK_USER.fitness],
                            "gym" : nameStr,
                            "is_trainer" : IK_USER.userType == .ace ? true : false] as [String:Any]
        
        //Utils.shared.logFlurrySession(sessionParam: sessionParam)
        Utils.shared.logEventWithFlurry(EventName: "home_tab", EventParam: sessionParam)
        
        
        //Utils.shared.logEventWithFlurry(EventName: "home_tab", EventParam: [:])
        //Utils.shared.startEventDurationForEvents(eventName: "home_tab", eventParam: [:])
        
        conMenuLeading.constant = -view.bounds.width
        self.view.layoutIfNeeded()
        
        getAces(showProgess: false)

        //btnSelectGym.setTitle(self.gymCenter.name, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadUI()
        guard isMovingToParent   else { return  } // || filter.interestId != check
        
        conMenuLeading.constant = -view.bounds.width

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //Utils.shared.closeEventDurationForEvents(eventName: "home_tab", eventParam: [:])
    }
    
    override func viewDidLayoutSubviews() {
       // tblList.setTableViewHeight(lastObejct: btnAvailable)
    }
    
    
    func loadUI() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)   {
            self.pagerViewAce.itemSize =  self.pagerViewAce.frame.size.applying(CGAffineTransform(scaleX:  0.55, y: 0.95))
            self.pagerViewBlog.itemSize =  self.pagerViewBlog.frame.size.applying(CGAffineTransform(scaleX:  0.75, y: 1))
            self.pagerViewBlog.interitemSpacing = 15
        }
        
        btnSelectGym.setTitle(self.gymCenter.name, for: .normal)
        
        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        
        imgUser.kf.indicatorType = .activity
        imgUser.kf.setImage(with: URL(string: IK_USER.profile_image),
                            placeholder: #imageLiteral(resourceName: "thumb_menu"),
                            options: nil,
                            progressBlock: nil,
                            completionHandler: nil)
        lblUserName.text = IK_USER.first_name + " " + IK_USER.last_name
        getBadgeCount()
        
        self.pagerViewAce.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // jitendra
    func loadBlogs()  {

        self.arrBlog = arrBlogs
        self.pagerViewBlog.reloadData()
        if self.arrBlog.count > 2 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)   {
                self.pagerViewBlog.scrollToItem(at: 1, animated: false)
            }
        }
        
        WebConnect.requestBlogs(completion: { (status, arrBlog) in
            if status {
                if arrBlog.count > 0 {
                    self.arrBlog = arrBlog
                }
                else {
                    self.arrBlog = [Blog]()
                }
            } else {
                self.arrBlog = [Blog]()
            }
            
            DispatchQueue.main.async {
                self.pagerViewBlog.reloadData()
                if self.arrBlog.count > 2 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)   {
                        self.pagerViewBlog.scrollToItem(at: 1, animated: false)
                    }
                }
            }
        })
        
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickMenu(_ sender: UIButton) {
        conMenuLeading.constant =  0
        
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func clickChat(_ sender: UIButton) {
        let message = self.storyboard?.instantiateViewController(withIdentifier: "MessageList") as! MessageList
        self.navigationController?.pushViewController(message, animated: true)
    }
    
    @IBAction func clickSport(_ sender: UIButton) {
      //  filter.selectedTab = sender.tag
        pagerViewAce.reloadData()
        self.pagerViewBlog.reloadData()

        btnSports.forEach {
            $0.backgroundColor = $0.tag == sender.tag ?   #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1) : #colorLiteral(red: 0.9019607843, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
            viewSport[$0.tag].dropShadow(color: $0.tag == sender.tag ? #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1) : .clear)
        }
    }
    
    @IBAction func clickSeeAvailable() {
        WebConnect.requestStatus(completion: { (status, arrAce , message) in
            self.clearBadge()
            if status {
                if arrAce.count > 0 {
                    let list = self.storyboard?.instantiateViewController(withIdentifier: "AvailableList") as! AvailableList
                    list.arrAce = arrAce
                    self.navigationController?.pushViewController(list, animated: true)
                }
                else {
                    let wait = self.storyboard?.instantiateViewController(withIdentifier: "Waiting") as! Waiting
                    wait.message = message
                    self.navigationController?.pushViewController(wait, animated: true)
                }
            } else {
                
                let search = self.storyboard?.instantiateViewController(withIdentifier: "SearchAvailableAce") as! SearchAvailableAce
                search.gymCenter = self.gymCenter
                self.navigationController?.pushViewController(search, animated: true)
            }
        })
    }
    
    @IBAction func clickLocation(_ sender: UIButton) {
    
        let gym = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GymList") as! GymList
        
        // jitendra
        for i in 0..<arrGyms.count {
            if self.gymCenter.name == arrGyms[i].name {
                gym.selectedAddrss = i
                gym.selectedGymId = arrGyms[i].id
                break
            }
        }
        
        gym.isHandler = true
        gym.onSelect = { obj in
            self.btnSelectGym.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
            self.gymCenter = obj
            self.getAces(showProgess: false)
        }
        self.navigationController?.pushViewController(gym , animated: true)
    }
    
    @IBAction func clickWriteSuggestion(_ sender: UIButton) {
        let help = self.storyboard?.instantiateViewController(withIdentifier: "Help") as! Help
        self.navigationController?.pushViewController(help, animated: true)
    }
    
    @IBAction func clickBanner(_ sender: UIButton) {
        
        // jitendra
        if arrBlog.count > 0 {
            let blogListVC = self.storyboard?.instantiateViewController(withIdentifier: "BlogList") as! BlogList
            blogListVC.arrBlog = arrBlog
            navigationController?.pushViewController(blogListVC, animated: true)
        }
        
        /*
        if let package = IK_DEFAULTS.object(forKey: MARATHON_PLAN) as? Int {
            if package == 1 {
                let running = self.storyboard?.instantiateViewController(withIdentifier: "RunningType") as! RunningType
                navigationController?.pushViewController(running, animated: true)
            }
            else {
                let schedule = self.storyboard?.instantiateViewController(withIdentifier: "Schedule") as! Schedule
                self.navigationController?.pushViewController(schedule, animated: true)
            }
        }
        else {
            let running = self.storyboard?.instantiateViewController(withIdentifier: "RunningType") as! RunningType
            navigationController?.pushViewController(running, animated: true)
       }*/
        
    }
    
    //Menu
    
    @IBAction func clickHideMenu(_ sender: UIButton) {
        hideSideMenu()
    }
    
    @IBAction func clickProfile(_ sender: UIButton) {
        let profile = self.storyboard?.instantiateViewController(withIdentifier: "UserProfile") as! UserProfile
        navigationController?.pushViewController(profile, animated: true)
        // hideSideMenu()
    }
    
    @IBAction func clickHistory(_ sender: UIButton) {
        let workout = self.storyboard?.instantiateViewController(withIdentifier: "Workout") as! Workout
        workout.canGoBack = true
        navigationController?.pushViewController(workout, animated: true)
    }
    
    @IBAction func clickNotification(_ sender: UIButton) {
        let notification = self.storyboard?.instantiateViewController(withIdentifier: "Notifications") as! Notifications
        self.navigationController?.pushViewController(notification, animated: true)
    }
    
    @IBAction func clickPayment(_ sender: UIButton) {
        let cardDetail = self.storyboard?.instantiateViewController(withIdentifier: "CardList") as! CardList
        self.navigationController?.pushViewController(cardDetail, animated: true)
        //hideSideMenu()
    }
    
    @IBAction func clickHelp(_ sender: UIButton) {
        let help = self.storyboard?.instantiateViewController(withIdentifier: "Help") as! Help
        self.navigationController?.pushViewController(help, animated: true)
        // hideSideMenu()
    }
    
    @IBAction func clickSetting(_ sender: UIButton) {
        let setting = self.storyboard?.instantiateViewController(withIdentifier: "SettingCust") as! SettingCust
        self.navigationController?.pushViewController(setting, animated: true)
        // hideSideMenu()
    }
    
    @IBAction func clickBecomeAce(_ sender: UIButton) {
        
        guard IK_USER.userType != .requestedForAcer else {
            Utils.shared.alert(on: self, message: ALERT_ALREADY_APPLIED, affirmButton: "Ok", cancelButton: nil)
            return
        }
        
        WebConnect.checkCanAce { (status, message) in
            guard status else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                return
            }
            
            let welcome = self.storyboard?.instantiateViewController(withIdentifier: "Welcome") as! Welcome
            self.navigationController?.pushViewController(welcome, animated: true)
            self.hideSideMenu()
        }
    }
    
    @IBAction func sideMenuSwipped() {
        conMenuLeading.constant =  -view.bounds.width
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK:- Other
    //MARK:-
    
    fileprivate func hideSideMenu() {
        conMenuLeading.constant = -view.bounds.width
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc fileprivate func getAces(showProgess : Bool) {
        
        guard gymCenter != nil  else { return }
        
        if showProgess {
            FUProgressView.showProgress()
        }
        
        WebConnect.getAceList(centerId: gymCenter.id) { (status, aces , message, event)  in
            
            FUProgressView.hideProgress()
            self.refresh.endRefreshing()
          
            if event.active && event.packageId != 0 {
                IK_DEFAULTS.set(event.packageId, forKey: MARATHON_PLAN)
                IK_DEFAULTS.synchronize()
            } else {
                IK_DEFAULTS.removeObject(forKey: MARATHON_PLAN)
                IK_DEFAULTS.synchronize()
            }
            
            UIView.animate(withDuration: 0.35) {
                self.view.layoutIfNeeded()
            }
            
            if status {
                self.arrAce = aces
                
                UIView.animate(withDuration: 0.15, animations: {
                    self.viewNoAce.alpha = 0
                })
                
            } else {

                self.arrAce = []
                UIView.animate(withDuration: 0.15, animations: {
                    self.viewNoAce.alpha = 1
                })
            }
            
            DispatchQueue.main.async {
                self.pagerViewAce.reloadData()
                if showProgess {
                    if self.arrAce.count > 2 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)   {
                            self.pagerViewAce.scrollToItem(at: 1, animated: false)
                        }
                    }
                }
            }
        }
    }
    
    @objc func getBadgeCount() {
        WebConnect.avilabilityBadge { (status, count) in
            self.lblAvailableBadge.text = count > 0 ? "\(count)" : ""
            self.view.layoutIfNeeded()
            self.lblAvailableBadge.setBorder(cornerRadius: self.lblAvailableBadge.bounds.height/2, borderWidth: 0)
            self.lblAvailableBadge.isHidden = count == 0
            self.chekBookingStatus()
        }
    }
    
    func clearBadge() {
        WebConnect.clearBadge { (status) in
            self.lblAvailableBadge.isHidden = true
        }
    }
    
    func chekBookingStatus() {
        WebConnect.checkStatus(completion: { (status, arrCompleted, isAce) in
//            if isAce {
//                if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
//                    let newDic = NSMutableDictionary(dictionary: dict)
//                    newDic.setValue("2", forKey: "role")
//                    
//                    IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
//                    IK_DEFAULTS.synchronize()
//                    
//                    let bank = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddBankDetail") as! AddBankDetail
//                    bank.isForword = true
//                    self.navigationController?.pushViewController(bank, animated: true)
//                }
//            }
//            else
            
                if arrCompleted.count > 0 {
                let rating = self.storyboard?.instantiateViewController(withIdentifier: "Rating") as! Rating
                rating.arrRating = arrCompleted
                self.present(rating, animated: true, completion: nil)
            }
        })
    }
}

extension AceList : UnreadMessageDelegate {
    func unreadCountUpdate(count: Int) {
        icnUnread.isHidden =  ChatManager.unreadCount == 0
    }
}

extension AceList : InterestDelegate {
    
    func didSelectInterest(interest: [Int]) {
        /*
        filter.interestId = interest
        btnSports.forEach { btn in
            guard filter.interestId.count > btn.tag else {
                //btn.isHidden = true
                viewSport[btn.tag].isHidden = true
                return
            }
            
            viewSport[btn.tag].isHidden = false
            let name =  arrSports.first(where: { (sp) -> Bool in
                return sp.id == filter.interestId[btn.tag]
            })?.name ?? "Ernæring"
            btn.setTitle(name, for: .normal)
        }
         */
    }
}


//MARK:- FSPager Delegate
//MARK:-

extension AceList : FSPagerViewDelegate , FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        
        if pagerView == pagerViewAce {
            return arrAce.count
        }
        else if pagerView == pagerViewBlog {
            return arrBlog.count
        }
        else {
            return 0
        }
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        if pagerView == pagerViewAce {
            let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cellAceCarouselID", at: index) as! cellAceCarousel
            
            let obj = arrAce[index]
            cell.lblName.text = obj.firstName + " " + String(describing:obj.lastName.first ?? " ") + "."
            
            cell.imgVerified.isHidden = false //!obj.adminVerify
            
            cell.imgPerson.kf.indicatorType = .activity
            cell.imgPerson.kf.setImage(with: URL(string: obj.profileImage),
                                       placeholder: #imageLiteral(resourceName: "thumb_menu"),
                                       options: nil,
                                       progressBlock: nil,
                                       completionHandler: nil)
            
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                cell.imgPerson.layer.cornerRadius = cell.imgPerson.frame.width / 2
//            }
            return cell
        }
        else if pagerView == pagerViewBlog {

            let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cellBlogCarouselID", at: index) as! cellBlogCarousel
            if arrBlog.count < index {
                return cell
            }
            let obj = arrBlog[index]
            cell.lblTitle.text = obj.title
            //cell.lblSubTitle.text = obj.leadIn
            cell.lblSubTitle.text = obj.leadIn.htmlToString
            
            cell.imgBlog.kf.indicatorType = .activity
            cell.imgBlog.kf.setImage(with: URL(string: obj.thumb),
                                       placeholder: #imageLiteral(resourceName: "no_image"),
                                       options: nil,
                                       progressBlock: nil,
                                       completionHandler: nil)
            return cell
        }
        else {
            return FSPagerViewCell()
        }
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        if pagerView == pagerViewBlog {
            let blogDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "BlogDetail") as! BlogDetail
            blogDetailVC.blogObj = arrBlog[index]
            navigationController?.pushViewController(blogDetailVC, animated: true)
        }
        else {
            let detail = self.storyboard?.instantiateViewController(withIdentifier: "AceDetail") as! AceDetail
            detail.objAce = arrAce[index]
            navigationController?.pushViewController(detail, animated: true)
        }
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        //pageControl.currentPage =  pagerView.currentIndex
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


//MARK:- Tableview Cell
//MARK:-

class CellAces : UITableViewCell {
    
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var star: [UIImageView]!
    @IBOutlet var imgVerified: UIImageView!
    
    override func awakeFromNib() {
        imgProfile.setBorder(cornerRadius: imgProfile.bounds.height/2, borderWidth: 0, borderColor: .clear)
    }
}
