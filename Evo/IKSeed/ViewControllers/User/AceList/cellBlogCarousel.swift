//
//  cellBlogCarousel.swift
//  Ikseed
//
//  Created by Fahad Mohammed Firoz Khan on 08/01/19.
//  Copyright © 2019 Chanchal Warde. All rights reserved.
//

import UIKit
//import FSPagerView


class cellBlogCarousel: FSPagerViewCell {

    @IBOutlet var viewCard: UIView!
    @IBOutlet var imgBlog: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()


        //self.viewCard.dropShadow(0.2, radius: 3, offset: CGSize(width: 0, height: 1), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
    }

}
