//
//  SuccessMsg.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/23/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit

class SuccessMsg: UIViewController {
    
    @IBOutlet var imgMundi: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "successToHistorySegueId" {
            if let toViewController = segue.destination as? Workout {
                toViewController.isBack = false
                toViewController.canGoBack = true
            }
        }
    }
    
}
