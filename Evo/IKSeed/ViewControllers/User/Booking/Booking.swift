//
//  Booking.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/21/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import CoreLocation
import Spring
import Mixpanel
//import DropDown

class Booking: UIViewController {
    //MARK:- Constant
    //MARK:-
    
    enum PickerType {
        case date
        case time
    }
    
    enum PickerTag : Int {
        case startTime = 200
        case endTime
    }
    
    var arrHelpList = ["Teknikk","Program","Veiledning","Styrke","Skadeforebygging","Annet"]
    
    let arrTime = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]
    
    let SHOW_PICKER : CGFloat = 0
    let HIDE_PICKER : CGFloat = -265
    let person =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectPersons") as! SelectPersons
    fileprivate let refresh = UIRefreshControl()
    
    //MARK:- IBOutlet
    //MARK:-
    @IBOutlet var lblHelp: UILabel!
    @IBOutlet var btnHelp: UIButton!
    
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var btnDate: UIButton!
    @IBOutlet var btnTime: UIButton!
    @IBOutlet var btnLocation: UIButton!
    @IBOutlet var btnBook: UIButton! {
        didSet {
            //btnBook.dropShadow()
        }
    }
    @IBOutlet var btnPrice: UIButton!
    @IBOutlet var btnPeople: UIButton!
    
    //--- Alerts
    @IBOutlet var alertDate: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertDate,0.0)
        }
    }
    @IBOutlet var alertTime: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertTime,0.0)
        }
    }
    @IBOutlet var alertPeople: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertPeople,0.0)
        }
    }
    @IBOutlet var alertLocation: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertLocation,0.0)
        }
    }
    @IBOutlet var alertHelp: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertHelp,0.0)
        }
    }

    //--- Date Time Picker
    
    @IBOutlet var conPickerBottom: NSLayoutConstraint!
    @IBOutlet var viewPicker: SpringView! {
        didSet {
            viewPicker.animation =  Spring.AnimationPreset.FadeOut.rawValue
            viewPicker.curve = Spring.AnimationCurve.Linear.rawValue
            viewPicker.duration = 0
            viewPicker.animate()
        }
    }
    @IBOutlet var viewTime: UIView!
    @IBOutlet var viewDate: UIView!
    
    var dropDownHelp = DropDown()
    
    @IBOutlet var pickerStartTime: UIPickerView! {
        didSet {
            pickerStartTime.tag = PickerTag.startTime.rawValue
        }
    }
    @IBOutlet var pickerEndTime: UIPickerView!{
        didSet {
            pickerEndTime.tag = PickerTag.endTime.rawValue
        }
    }
    @IBOutlet var pickerDate: UIDatePicker!
    
    //MARK:- Variable
    //MARK:-
    
    var aceDetail : Ace!
    fileprivate var bookingInfo = (date:"",
                                   startTime:"22:00",
                                   endTime:"23:00",
                                   people:1,
                                   amount:"5",
                                   acePrice:"5",
                                   center : (id:0, name:"" , address : "" ) ,
                                   meetingPoint:"ab",
                                   hours:0)
    
    fileprivate var pickerSelected : PickerType!
    fileprivate var isCardForword = false
    fileprivate var cardCount = 0
    fileprivate var time = (sHour:0, eHour:0)
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerDate.minimumDate = Date()
        
        let hour = Date().getComponent(request: .hour) + 1
        time.sHour = hour
        time.eHour = hour + 1
        pickerStartTime.selectRow( hour, inComponent: 0, animated: true)
        pickerEndTime.selectRow( hour+1 , inComponent: 0, animated: true)
        
        if let a = (arrAllGyms.filter { $0.id == aceDetail.gymId}).first {
            btnLocation.setTitle(a.name, for: .normal)
            btnLocation.tag = a.id
            self.bookingInfo.center = a
        }
        
        lblHelp.text = arrHelpList.first
        bookingInfo.startTime = ""
        bookingInfo.endTime = ""
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        WebConnect.getCardList(isBackground: true) { (_, arrCard, _) in
//            self.cardCount = arrCard.count
//            if self.isCardForword {
//                self.clickBook(self.btnBook)
//            }
//        }
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        /*
        Mixpanel.sharedInstance()?.track("Adds booking information", properties:["session":track.start.timeIntervalSince1970,
                //    "amount": self.bookingInfo.amount,
                    "people": self.bookingInfo.people,
                    "ace_id":self.aceDetail.profileId,
                    "ace_name":self.aceDetail.firstName + " " + self.aceDetail.lastName,
            //    "sport":arrSports.filter({$0.id == self.aceDetail.sportId}).first?.name ?? "",
                    "loaction":self.bookingInfo.center.name
            ])
        */
        
        /*
        let eventParams = ["session":track.start.timeIntervalSince1970,
                           "people": self.bookingInfo.people,
                           "ace_id":self.aceDetail.profileId,
                           "ace_name":self.aceDetail.firstName + " " + self.aceDetail.lastName,
                           "loaction":self.bookingInfo.center.name
                          ] as [String : Any]
        Utils.shared.logEventWithFlurry(EventName: "adds_booking_information", EventParam: eventParams)
        */
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickBook(_ sender: UIButton) {
        guard validation() else {
            return
        }
        
//        guard self.cardCount > 0 else {
//            if self.isCardForword {
//                self.isCardForword = false
//                Utils.shared.alert(on: self, message: ERROR_NO_CARD, affirmButton: "Ok", cancelButton: nil)
//            }
//            else {
//                self.isCardForword = true
//                let card = self.storyboard?.instantiateViewController(withIdentifier: "AddCard") as! AddCard
//                self.navigationController?.pushViewController(card, animated: true)
//            }
//            return
//        }
        
        //bookingInfo.date = Date().string(format: "yyyy-MM-dd")

        WebConnect.bookAce(aceId: aceDetail.profileId,
                           center : bookingInfo.center.id ,
                          // sportId: aceDetail.sportId,
                         //  nutrition: "N",
                        //   locationId: bookingInfo.location,
                           date: bookingInfo.date,
                           startTime: bookingInfo.startTime,
                           endTime: bookingInfo.endTime,
                           person: bookingInfo.people,
                           amount:  bookingInfo.amount,
                           acePrice:  bookingInfo.acePrice,
                         //  coordinate: bookingInfo.coordinate,
                         //  address: bookingInfo.address,
                           meetingPoint: bookingInfo.meetingPoint, helpText: lblHelp.text!.trimString) { (status, message, isNoCard) in
                            
                            guard status else {
                                guard isNoCard else {
                                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                    return
                                }
                                return
                            }
                            
                            track.end = Date()
                            
                            /*
                            Mixpanel.sharedInstance()?.track("Books a session", properties:["session":track.start.timeIntervalSince1970,
                                "amount": self.bookingInfo.amount,
                                "people": self.bookingInfo.people,
                                "ace_id":self.aceDetail.profileId,
                                "ace_name":self.aceDetail.firstName + " " + self.aceDetail.lastName,
                                "sport":arrSports.filter({$0.id == self.aceDetail.sportId}).first?.name ?? "",
                                "location":self.bookingInfo.center.name])
                            */
                            
                           
                            
                            let eventParam = [//"profile_id":IK_USER.profile_id ?? "",
                                              "ace_id":self.aceDetail.profileId,
                                              "gym_id":self.aceDetail.gymId,
                                              "people":self.bookingInfo.people,
                                              "sport":arrSports.filter({$0.id == self.aceDetail.sportId }).first?.name ?? "",
                                              "amount":self.bookingInfo.amount] as [String:Any]
                            Utils.shared.logEventWithFlurry(EventName: "ace_booking", EventParam: eventParam)
                            
                            track.start = Date()
                            
                            let success = self.storyboard?.instantiateViewController(withIdentifier: "SuccessMsg") as! SuccessMsg
                            self.navigationController?.pushViewController(success, animated: true)
        }
    }
    
    @IBAction func clickHelp(_ sender: UIButton) {
        self.dropDownHelp = DropDown()
        self.dropDownHelp.anchorView = sender.superview
        //self.dropDownHelp.textFont = UIFont.init(name: Font.poppins_Regular, size: 15)!
        self.dropDownHelp.direction = .bottom
        self.dropDownHelp.bottomOffset = CGPoint(x: 0, y: (self.dropDownHelp.anchorView?.plainView.bounds.height)! + 10)
        self.dropDownHelp.dataSource = arrHelpList
        self.dropDownHelp.selectionAction =
            { [unowned self] (index: Int, item: String) in
                self.lblHelp.text = item
        }
        self.dropDownHelp.show()
    }

    @IBAction func clickDate(_ sender: UIButton) {
        pickerSelected = .date
        showPicker(type:.date)
    }
    
    @IBAction func clickTime(_ sender: UIButton) {
        pickerSelected = .time
        showPicker(type:.time)
    }
    
    @IBAction func clickLocation(_ sender: UIButton) {
        
        return
        
        let gym = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GymList") as! GymList
        gym.isHandler = true
        gym.onSelect = { obj in
            sender.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
            self.bookingInfo.center = obj
            
            Utils.shared.animateFadeOut(self.alertLocation, 0)
        }
        self.navigationController?.pushViewController(gym , animated: true)
    }
    
    @IBAction func clickPeople(_ sender: UIButton) {
        person.onSelectItem = { number , strValue in
            Utils.shared.animateFadeOut(self.alertPeople, 0)
            
            self.bookingInfo.people = number
            
            sender.setTitle(String(describing:number), for: .normal)
            
            WebConnect.rateCalculation(sportId: 210, person: number,hour: self.bookingInfo.hours) { (status, amount, aceAmount) in
                guard status else {return}
                self.bookingInfo.amount = amount
                self.bookingInfo.acePrice = aceAmount
                self.btnPrice.setTitle("\(amount) NOK", for: .normal)
            }
        }
        navigationController?.pushViewController(person, animated: true)
    }
    
    @IBAction func clickDone(_ sender: UIButton) {
        hidePicker()
        // if pickerSelec
    }
    
    //MARK:- Other
    //MARK:-
    
    fileprivate func showPicker(type:PickerType) {
        
        viewDate.isHidden = type == .time
        viewTime.isHidden = type == .date
        
        viewPicker.animation =  Spring.AnimationPreset.FadeIn.rawValue
        viewPicker.curve = Spring.AnimationCurve.Linear.rawValue
        viewPicker.duration = 0.3
        viewPicker.animateNext {
            self.conPickerBottom.constant = self.SHOW_PICKER
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    fileprivate func hidePicker() {
        conPickerBottom.constant = HIDE_PICKER
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.viewPicker.animation =  Spring.AnimationPreset.FadeOut.rawValue
            self.viewPicker.curve = Spring.AnimationCurve.Linear.rawValue
            self.viewPicker.duration = 0.3
            self.viewPicker.animate()
        }
        
        if pickerSelected == .date {
            Utils.shared.animateFadeOut(alertDate, 0)
            bookingInfo.date = pickerDate.date.string(format: "yyyy-MM-dd")
            btnDate.setTitle(pickerDate.date.string(format: DATE_FORMAT), for: .normal)
        }
        else {
            Utils.shared.animateFadeOut(alertTime, 0)
            bookingInfo.startTime = arrTime[pickerStartTime.selectedRow(inComponent: 0)] + ":00"
            bookingInfo.endTime =  arrTime[pickerEndTime.selectedRow(inComponent: 0)] + ":00"
            btnTime.setTitle(bookingInfo.startTime + " - " + bookingInfo.endTime, for: .normal)
            
            /*
            self.bookingInfo.hours = pickerEndTime.selectedRow(inComponent: 0) - pickerStartTime.selectedRow(inComponent: 0)
            
            WebConnect.rateCalculation(sportId: 210,
                                       person: self.bookingInfo.people,
                                       hour: self.bookingInfo.hours) { (status, amount, aceAmount) in
                                        guard status else {return}
                                        self.bookingInfo.amount = amount
                                        self.bookingInfo.acePrice = aceAmount
                                        self.btnPrice.setTitle("\(amount) NOK", for: .normal)
            }
            */
            
        }
    }
    
    fileprivate func validation() -> Bool {

        var isValid = true

        if  lblHelp.text!.isEmpty {
            Utils.shared.animatePop(alertDate)
            isValid = false
        }

        if  bookingInfo.date.isEmpty() {
            Utils.shared.animatePop(alertDate)
            isValid = false
        }

        if  bookingInfo.startTime.isEmpty() {
            Utils.shared.animatePop(alertTime)
            isValid = false
        }
        
        if  bookingInfo.endTime.isEmpty() {
            Utils.shared.animatePop(alertTime)
            isValid = false
        }
        
        return isValid
    }
}

//MARK:- Picker Datasource || Delegate
//MARK:-

extension Booking : UIPickerViewDataSource , UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return  arrTime.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return   arrTime[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == PickerTag.startTime.rawValue {
            time.sHour = row
            if time.sHour == 23 {
                pickerStartTime.selectRow(22 , inComponent: 0, animated: true)
                pickerEndTime.selectRow(23 , inComponent: 0, animated: true)
            } else if time.sHour >= time.eHour  {
                pickerEndTime.selectRow(time.sHour == 23 ? 0 : time.sHour+1 , inComponent: 0, animated: true)
            }
            
        } else {
            time.eHour = row
            if time.eHour == 0 {
                pickerStartTime.selectRow(0 , inComponent: 0, animated: true)
                pickerEndTime.selectRow(1 , inComponent: 0, animated: true)
            } else if time.eHour <= time.sHour {
                pickerStartTime.selectRow(time.eHour == 0 ? 0 : time.eHour-1 , inComponent: 0, animated: true)
            }
        }
    }
    
}


