//
//  SelectPersons.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/22/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit

class SelectPersons: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    
    //MARK:- IBOutlet
    //MARK:-
    
    //MARK:- Variable
    //MARK:-
    
    public var onSelectItem: (_ person:Int, _ strValue : String) -> () = { _,_  in }
    fileprivate var arrData = ["Privattime","Med 1 venn","Med 2 venner","Med 3 venner","Med 4 venner","Med 5 venner","Med 6 venner","Med 7 venner"]
    fileprivate var selected = -1
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction Method
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ =  navigationController?.popViewController(animated: true)
    }
}

//MARK:- Tableview Delegate || Datasource
//MARK:-

extension SelectPersons: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPeople", for: indexPath) as! CellPeople
        cell.lblValue.text = arrData[indexPath.row]
        cell.btnTick.tintColor = selected == indexPath.row  ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: tableView.bounds.width, height: 5)))
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onSelectItem(indexPath.row + 1, arrData[indexPath.row])
        selected = indexPath.row
        tableView.reloadData()
        _ = navigationController?.popViewController(animated: true)
    }
}

//MARK:- Cell
//MARK:-

class CellPeople: UITableViewCell {
    @IBOutlet var lblValue : UILabel!
    @IBOutlet var btnTick : UIButton!
    
    override func awakeFromNib() {
        
    }
}
