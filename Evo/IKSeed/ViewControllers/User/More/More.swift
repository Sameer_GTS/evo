//
//  More.swift
//  Ikseed
//
//  Created by Fahad Mohammed Firoz Khan on 09/01/19.
//  Copyright © 2019 Chanchal Warde. All rights reserved.
//

import UIKit

class More: UIViewController {

    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var btnBecomeAce: UIButton!
    @IBOutlet var icnLineBeforeButton: UIImageView!

    @IBOutlet var viwNotification: UIView!

    @IBOutlet var viwSepBlack: UIView!
    @IBOutlet var viwTrainingProgram: UIView!

    @IBOutlet var consStackPinToLine: NSLayoutConstraint!
    @IBOutlet var consStackPinToBottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;

        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 2)
        imgUser.kf.indicatorType = .activity
        imgUser.kf.setImage(with: URL(string: IK_USER.profile_image),
                            placeholder: #imageLiteral(resourceName: "thumb_menu"),
                            options: nil,
                            progressBlock: nil,
                            completionHandler: nil)
        lblUserName.text = IK_USER.first_name + " " + IK_USER.last_name
        
        if IK_USER.userType == .ace {
            Utils.shared.logEventWithFlurry(EventName: "ace_more_tab", EventParam: [:])
            
            btnBecomeAce.isHidden = true
            icnLineBeforeButton.isHidden = true
            
            consStackPinToLine.isActive = false
            consStackPinToBottom.isActive = true
            viwNotification.isHidden = false
            
            viwTrainingProgram.isHidden = true
            viwSepBlack.isHidden = true
        }
        else {
            Utils.shared.logEventWithFlurry(EventName: "more_tab", EventParam: [:])
            
            viwNotification.isHidden = true
            
            viwTrainingProgram.isHidden = false
            viwSepBlack.isHidden = false
            
            btnBecomeAce.isHidden = false
            icnLineBeforeButton.isHidden = false
            consStackPinToLine.isActive = true
            consStackPinToBottom.isActive = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 2)
    }

}

extension More
{
    @IBAction func clickProfile(_ sender: UIButton) {
        if IK_USER.userType == .ace {
            let profile = self.storyboard?.instantiateViewController(withIdentifier: "AceProfile") as! AceProfile
            _ = navigationController?.pushViewController(profile,animated:true)
        }
        else {
            let profile = self.storyboard?.instantiateViewController(withIdentifier: "UserProfile") as! UserProfile
            navigationController?.pushViewController(profile, animated: true)
        }
        
        // hideSideMenu()
    }
    
    @IBAction func clickHistory(_ sender: UIButton) {
        
        if IK_USER.userType == .ace {
            
        }
        else {
            let workout = self.storyboard?.instantiateViewController(withIdentifier: "Workout") as! Workout
            workout.canGoBack = true
            navigationController?.pushViewController(workout, animated: true)
        }
        
    }
    
    @IBAction func clickNotification(_ sender: UIButton) {
        let notification = self.storyboard?.instantiateViewController(withIdentifier: "Notifications") as! Notifications
        self.navigationController?.pushViewController(notification, animated: true)
    }
    
    @IBAction func clickPayment(_ sender: UIButton) {
        let cardDetail = self.storyboard?.instantiateViewController(withIdentifier: "CardList") as! CardList
        self.navigationController?.pushViewController(cardDetail, animated: true)
        //hideSideMenu()
    }
    
    @IBAction func clickMinEvo(_ sender: UIButton) {
        let term = self.storyboard?.instantiateViewController(withIdentifier: "Terms") as! Terms
        term.url = WebConnect.MinEvo
        term.strTitle = "Min EVO"
        navigationController?.present(term, animated: true, completion: {
        })
    }
    
    @IBAction func clickHelp(_ sender: UIButton) {
        let help = self.storyboard?.instantiateViewController(withIdentifier: "Help") as! Help
        self.navigationController?.pushViewController(help, animated: true)
        // hideSideMenu()
    }
    
    @IBAction func clickSetting(_ sender: UIButton) {
        let setting = self.storyboard?.instantiateViewController(withIdentifier: "SettingCust") as! SettingCust
        if IK_USER.userType == .ace {
            setting.isAce = true
        }
        self.navigationController?.pushViewController(setting, animated: true)
        // hideSideMenu()
    }
    
    @IBAction func clickBecomeAce(_ sender: UIButton) {
        
        guard IK_USER.userType != .requestedForAcer else {
            Utils.shared.alert(on: self, message: ALERT_ALREADY_APPLIED, affirmButton: "Ok", cancelButton: nil)
            return
        }
        
        WebConnect.checkCanAce { (status, message) in
            guard status else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                return
            }
            let welcome = self.storyboard?.instantiateViewController(withIdentifier: "Welcome") as! Welcome
            self.navigationController?.pushViewController(welcome, animated: true)
        }
    }
    
    @IBAction func clickToTraingProgram(_ sender: UIButton) {
        
        WebConnect.getTraningProgramUrl { (status, urlPath ,message) in
            guard status else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                return
            }
            DispatchQueue.main.async {
                self.navigateOnTraingProgram(urlPath: urlPath)
            }
        }
    }
    
    func navigateOnTraingProgram(urlPath : String)
    {
        //let eventParam = ["profile_id":IK_USER.profile_id] as [String:String]
        Utils.shared.logEventWithFlurry(EventName: "training_program", EventParam: [:])
        
        let policy = self.storyboard?.instantiateViewController(withIdentifier: "Terms") as! Terms
        policy.url = urlPath
        policy.strTitle = "Treningsprogram"
        policy.isHideTabBar = true
        navigationController?.present(policy, animated: true, completion: {
        })
//        navigationController?.pushViewController(policy, animated: true)

    }
}
