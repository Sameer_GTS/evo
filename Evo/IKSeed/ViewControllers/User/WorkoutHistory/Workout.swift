//
//  Workout.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/23/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Spring

class Workout: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    fileprivate let refresh = UIRefreshControl()
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var viewToggle: UIView!
    @IBOutlet var tblWorkout: UITableView!
    @IBOutlet var conBackHeight: NSLayoutConstraint!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var viewError: SpringView!
    
    @IBOutlet var btnBack: UIButton!
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var btnToggle: TTSegmentedControl!
    fileprivate var arrBooking : [Bookings] = []
    fileprivate var arrComplete : [Bookings] = []
    var isBack : Bool = false
    var canGoBack : Bool = false

    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refresh.attributedTitle = "Oppdater".changeColor(#colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        refresh.tintColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        refresh.addTarget(self, action: #selector(reloadData) , for: UIControl.Event.valueChanged)
        tblWorkout.addSubview(refresh)
        
        IK_NOTIFICATION.addObserver(self,
                                    selector: #selector(reloadData),
                                    name: NOTIFICATION_WORKOUT,
                                    object: nil)
        
        if #available(iOS 11.0, *) {
            var top = UIApplication.shared.keyWindow?.safeAreaInsets.top
            conBackHeight.constant = conBackHeight.constant + top!
            var frame = viewHeader.frame
            frame.size.height += top!
            viewHeader.frame = frame
        }
        else {
            let top = UIApplication.shared.statusBarFrame.size.height
            // tblWorkout.contentInset = UIEdgeInsets(top: CGFloat(-Float(top)), left: 0, bottom: 0, right: 0)
        }
        
        addToggle()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btnBack.isHidden = !canGoBack
        reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        if isBack {
            navigationController?.popViewController(animated: true)
        } else {
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    //MARK:- Other
    //MARK:-
    
    @IBAction func reloadData() {
        WebConnect.getUserSchedule { (status, arrBooking, arrComplete, message) in
            self.refresh.endRefreshing()
            if status {
                self.arrBooking = arrBooking
                self.arrComplete = arrComplete
                self.tblWorkout.reloadData()
            }
            else {
                self.arrBooking = []
                self.arrComplete = []
                self.tblWorkout.reloadData()
            }
            self.setPlaceholder ()
        }
    }
    
    fileprivate func setPlaceholder () {
        let isShow = btnToggle.currentIndex == 0 ? self.arrBooking.count == 0 : self.arrComplete.count == 0
        
        UIView.animate(withDuration: 0.25) {
            self.viewError.alpha = isShow ? 1 : 0
        }
    }
    
    /// Add toggle button
    fileprivate func addToggle() {
        btnToggle = TTSegmentedControl(frame: CGRect(origin: .zero, size: viewToggle.bounds.size))
        btnToggle.defaultTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnToggle.selectedTextColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        btnToggle.thumbColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnToggle.useShadow = true
        btnToggle.useGradient = false
        btnToggle.layer.borderWidth = 2
        btnToggle.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        btnToggle.layer.cornerRadius = 0 //viewToggle.bounds.size.height / 2
        btnToggle.itemTitles = ["Kommende","Fullførte"]
        btnToggle.defaultTextFont = UIFont(name: "HelveticaNeue", size: 17)!
        btnToggle.selectedTextFont = UIFont(name: "HelveticaNeue", size: 17)!
        btnToggle.containerBackgroundColor = .clear
        btnToggle.didSelectItemWith = { (index, title) -> () in
            self.tblWorkout.reloadData()
            self.setPlaceholder ()
        }
        viewToggle.addSubview(btnToggle)
    }
    
    @IBAction func clickCancelWorkout(_ sender: UIButton) {
        Utils.shared.alert(on: self, message: CONFIRM_CANCEL_WORKOUT, affirmButton: "Ja", cancelButton: "Avbryt") { (index) in
            guard index == 0 else {return}
            let  obj =  self.arrBooking[sender.tag]
            WebConnect.cancelWorkout(bookingId:obj.bookingId,completion: { status,message in
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                guard status else {return}
                self.reloadData()
            })
        }
    }
    
}

extension Workout : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return btnToggle.currentIndex == 0 ? arrBooking.count : arrComplete.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellWorkout", for: indexPath) as! CellWorkout
        
        let obj = btnToggle.currentIndex == 0 ? arrBooking[indexPath.row] : arrComplete[indexPath.row]
        
        cell.imgUser.kf.indicatorType = .activity
        cell.imgUser.kf.setImage(with: URL(string: obj.image),
                                 placeholder: #imageLiteral(resourceName: "thumb_menu"),
                                 options: nil,
                                 progressBlock: nil,
                                 completionHandler: nil)
        
        //cell.lblDate.text = obj.date
        cell.lblName.text = obj.firstName + " " + obj.lastName
        cell.lblSport.text = obj.gymName
        
        cell.lblDate.text = obj.date + "\n" + obj.startTime.string(format: "HH:mm") + " - " + obj.endTime.string(format: "HH:mm")
        cell.lblDuration.text = Utils.shared.secondsToHoursMinutesSeconds(seconds: Int(obj.endTime.timeIntervalSince(obj.startTime))) + "H"
        
        cell.btnCancel.isHidden = btnToggle.currentIndex != 0
        cell.btnCancel.tag = indexPath.row
        cell.btnCancel.addTarget(self, action: #selector(clickCancelWorkout), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = btnToggle.currentIndex == 0 ? arrBooking[indexPath.row] : arrComplete[indexPath.row]
        
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "AceDetail") as! AceDetail
        detail.objAce = obj.aceDetail
        navigationController?.pushViewController(detail, animated: true)
        
        //      let rate = self.storyboard?.instantiateViewController(withIdentifier: "Rating") as! Rating
        //    navigationController?.pushViewController(rate, animated: true)
    }
}

//MARK:- Tableview Cell
//MARK:-

class CellWorkout : UITableViewCell {
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var imgSport: UIImageView!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblSport: UILabel!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var lblTime : UILabel!
    
    override func awakeFromNib() {
        self.dropShadow(0.3, radius: 3, color: .black)
        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 1, borderColor: .clear)
        //viewContainer.setBorder(cornerRadius: 5, borderWidth: 0, borderColor: .clear)
    }
}
