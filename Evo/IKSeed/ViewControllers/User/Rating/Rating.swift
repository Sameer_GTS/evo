//
//  Rating.swift
//  IKSeed
//
//  Created by Chanchal Warde on 4/13/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.


import UIKit
import Spring

class Rating: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var lblAceName: UILabel!
    @IBOutlet var imgAce: UIImageView!
    @IBOutlet var lblBookingDate: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblRating: UILabel!
    
    @IBOutlet var btnStar1: UIButton! {
        didSet {
            btnStar1.tag = 1
        }
    }
    @IBOutlet var btnStar2: UIButton!{
        didSet {
            btnStar2.tag = 2
        }
    }
    @IBOutlet var btnStar3: UIButton!{
        didSet {
            btnStar3.tag = 3
        }
    }
    @IBOutlet var btnStar4: UIButton!{
        didSet {
            btnStar4.tag = 4
        }
    }
    @IBOutlet var btnStar5: UIButton!{
        didSet {
            btnStar5.tag = 5
        }
    }
    
    @IBOutlet var viewTitle: UIView!
    @IBOutlet var lblError: SpringLabel! {
        didSet {
            lblError.animation = Spring.AnimationPreset.FadeOut.rawValue
            lblError.curve = Spring.AnimationCurve.Linear.rawValue
            lblError.duration = 0
            lblError.animate()
        }
    }
    
    @IBOutlet var tvFeedback: UITextView!
    @IBOutlet var btnSubmit: UIButton!{
        didSet {
            btnSubmit.dropShadow()
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    var rating = 0
    var arrRating : [RatingDetails] = []
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //viewTitle.setBorder(cornerRadius: viewTitle.bounds.height/2, borderWidth: 0)
        imgAce.setBorder(cornerRadius: imgAce.bounds.height/2, borderWidth: 1)
    }
    override func viewDidAppear(_ animated: Bool) {
        //viewTitle.setBorder(cornerRadius: viewTitle.bounds.height/2, borderWidth: 0)
        imgAce.setBorder(cornerRadius: imgAce.bounds.height/2, borderWidth: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickStar(_ sender: UIButton) {
        rating = sender.tag
        
        UIView.animate(withDuration: 0.30) {
            self.lblRating.text = "\(self.rating).0"
            self.btnStar1.tintColor = sender.tag >= 1 ?  #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1) : #colorLiteral(red: 0.9246864915, green: 0.9253881574, blue: 0.9247950912, alpha: 1)
            self.btnStar2.tintColor = sender.tag >= 2 ?  #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1) : #colorLiteral(red: 0.9246864915, green: 0.9253881574, blue: 0.9247950912, alpha: 1)
            self.btnStar3.tintColor = sender.tag >= 3 ?  #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1) : #colorLiteral(red: 0.9246864915, green: 0.9253881574, blue: 0.9247950912, alpha: 1)
            self.btnStar4.tintColor = sender.tag >= 4 ?  #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1) : #colorLiteral(red: 0.9246864915, green: 0.9253881574, blue: 0.9247950912, alpha: 1)
            self.btnStar5.tintColor = sender.tag == 5 ?  #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1) : #colorLiteral(red: 0.9246864915, green: 0.9253881574, blue: 0.9247950912, alpha: 1)
        }
    }
    
    @IBAction func clickSubmit(_ sender: UIButton) {
        
        guard validation() else {      return  }
        self.view.endEditing(true)
        
        let obj = arrRating.first!
        
        WebConnect.rateAceSession(aceId: obj.profileId,
                                  bookingId: obj.bookingId,
                                  rating:rating ,
                                  feedback: tvFeedback.text.trimString) { (status, message) in
                                    
                                    guard status else {
                                        Utils.shared.alert(on: self , message: message!, affirmButton: "Ok", cancelButton: nil)
                                        return
                                    }
                                    
                                    self.arrRating.removeFirst()
                                    guard self.arrRating.first != nil else {
                                        self.dismiss(animated: true, completion: nil) ; return
                                    }
                                    
                                    self.setData()
        }
        
    }
    
    //MARK:- Other
    //MARK:-
    
    fileprivate func setData() {
        
        guard arrRating.first != nil else { return }
        
        let obj = arrRating.first!
        
        lblAceName.text = obj.name
        lblAmount.text = obj.amount + " NOK"
        lblBookingDate.text = obj.date
        
        imgAce.kf.indicatorType = .activity
        imgAce.kf.setImage(with: URL(string: obj.image),
                           placeholder: #imageLiteral(resourceName: "thumb_menu"),
                           options: nil,
                           progressBlock: nil,
                           completionHandler: nil)
        
        UIView.animate(withDuration: 0.35) {
            self.tvFeedback.text = ""
            self.rating = 0
            self.lblRating.text = "\(self.rating).0"
            self.btnStar1.tintColor =  #colorLiteral(red: 0.9246864915, green: 0.9253881574, blue: 0.9247950912, alpha: 1)
            self.btnStar2.tintColor =  #colorLiteral(red: 0.9246864915, green: 0.9253881574, blue: 0.9247950912, alpha: 1)
            self.btnStar3.tintColor =  #colorLiteral(red: 0.9246864915, green: 0.9253881574, blue: 0.9247950912, alpha: 1)
            self.btnStar4.tintColor =  #colorLiteral(red: 0.9246864915, green: 0.9253881574, blue: 0.9247950912, alpha: 1)
            self.btnStar5.tintColor =  #colorLiteral(red: 0.9246864915, green: 0.9253881574, blue: 0.9247950912, alpha: 1)
        }
    }
    
    fileprivate func validation() -> Bool {
        
        var isValid = true
        
        if tvFeedback.text!.isEmpty() {
            lblError.animation = Spring.AnimationPreset.FadeIn.rawValue
            lblError.curve = Spring.AnimationCurve.Linear.rawValue
            lblError.duration = 0.35
            lblError.animate()
            isValid = false
        }
        
        if rating == 0 {
            isValid = false
        }
        
        return isValid
    }
}

extension Rating : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblError.animation = Spring.AnimationPreset.FadeOut.rawValue
        lblError.curve = Spring.AnimationCurve.Linear.rawValue
        lblError.duration = 0
        lblError.animate()
    }
}
