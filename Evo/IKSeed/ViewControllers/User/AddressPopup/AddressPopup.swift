//
//  AddressPopup.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/29/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Google
import GooglePlaces
import Spring

class AddressPopup: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    
    let TAG_MEETING = 451
    let TAG_ADDRESS = 452
    
    fileprivate static let picker = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressPopup") as! AddressPopup
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var lblError: SpringLabel! {
        didSet {
            lblError.animation = Spring.AnimationPreset.FadeOut.rawValue
            lblError.curve = Spring.AnimationCurve.Linear.rawValue
            lblError.duration = 0
            lblError.animate()
        }
    }
    @IBOutlet var viewContainer: SpringView!
    @IBOutlet var conTableHeight: NSLayoutConstraint!
    @IBOutlet var tfAddress: UITextField!{
        didSet {
            tfAddress.tag = TAG_ADDRESS
        }
    }
    @IBOutlet var tfMeeting: UITextField! {
        didSet {
            tfMeeting.tag = TAG_MEETING
        }
    }
    @IBOutlet var btnSave: UIButton! {
        didSet {
            btnSave.dropShadow()
        }
    }
    
    @IBOutlet var tblList: UITableView! {
        didSet {
            //  tblList.dropShadow(0.4, radius: 10, color: .black)
        }
    }
    
    @IBOutlet var alertAddress: SpringImageView!{
        didSet {
            Utils.shared.animateFadeOut(alertAddress, 0.0)
        }
    }
    @IBOutlet var alertMeetingPoint: SpringImageView!{
        didSet {
            Utils.shared.animateFadeOut(alertMeetingPoint, 0.0)
        }
    }
    //MARK:- Variable
    //MARK:-
    
    public var onSaveAddress: ((address:String,
        city:String,
        country:String,
        coordinate:CLLocationCoordinate2D,
        meeting:String)) -> () = { _ in }
    
    fileprivate var arrPlaces : [(placeId:String , name : NSAttributedString)] = []
    fileprivate var tblHeight : CGFloat {
        return viewContainer.bounds.height - tblList.frame.origin.y - 20
    }
    
    fileprivate var selectedLocation = (address:"",city:"",country:"",coordinate:CLLocationCoordinate2D(latitude: 0, longitude: 0),meeting:"")
    
    fileprivate var isValid = false
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    @IBAction func clickCancel(_ sender: UIButton) {
        hidePicker()
    }
    
    func clickSave() {
        
        guard isValid else {
            return
        }
        
        view.endEditing(true)
        selectedLocation.meeting = tfMeeting.text!.trimString
        onSaveAddress(selectedLocation)
        hidePicker()
    }
    
    //MARK:- Other Method
    //MARK:-
    
    /// Show picker on view controller
    ///
    /// - Parameters:
    ///   - on: View controller on which picker will be shown
    ///   - title: Title of picker
    ///   - arrList: Array to show list
    ///   - multipleSelection: Is picker allow multi selection
    ///   - arrSelected: Already selected options
    /// - Returns: Object of picker class
    static func showPicker(on:UIViewController ) ->  AddressPopup {
        
        on.present(picker, animated: true, completion: {
            UIView.transition(with:picker.view,
                              duration: 0.20,
                              options: [.transitionCrossDissolve],
                              animations: {
            }, completion:nil)
        })
        
        return picker
    }
    
    /// Function to hide picker from superview
    fileprivate func hidePicker() {
        
        Utils.shared.animateFadeOut(alertMeetingPoint, 0)
        Utils.shared.animateFadeOut(alertAddress, 0)
        
        lblError.animation = Spring.AnimationPreset.FadeOut.rawValue
        lblError.curve = Spring.AnimationCurve.Linear.rawValue
        lblError.duration = 0
        lblError.animate()
        
        tfAddress.text = ""
        tfMeeting.text = ""
        
        arrPlaces = []
        // setTableview()
        tblList.reloadData()
        AddressPopup.picker.dismiss(animated: true, completion: {
            //ListPicker.picker.btnDismiss.alpha = 0
        })
    }
    
    func placeAutocomplete(search:String) {
        let filter = GMSAutocompleteFilter()
      //  filter.type = .establishment
        filter.country = "NO"
        filter.accessibilityLanguage = "no"
        
//        let bounds =  GMSCoordinateBounds(coordinate:CLLocationCoordinate2D(latitude: 59.1338296, longitude:11.458435),
//                                          coordinate: CLLocationCoordinate2D(latitude: 69.5912269, longitude: 30.1012627))
        
        GMSPlacesClient.shared().autocompleteQuery(search, bounds: nil, filter: filter, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            
            self.arrPlaces = []
            if let results = results {
                for result in results {
//                     print("-------------------------------")
//                     print("Result \(result.description)")
                    self.arrPlaces.append((placeId:result.placeID , name : result.attributedFullText))
                }
            }
            //  self.setTableview()
            self.tblList.reloadData()
        })
    }
    
    func placeDetail(placeId:String)  {
        
        FUProgressView.showProgress()
        
        GMSPlacesClient.shared().lookUpPlaceID(placeId, callback: { (place, error) -> Void in
            
            FUProgressView.hideProgress()
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                //print("No place details for \(placeId)")
                return
            }
            // place.coordinate
            
            for pl in place.addressComponents! {
                 //print("Key--- \(pl.type) ,  Value--- \(pl.name)")
                if pl.type == "postal_town" {
                    self.selectedLocation.city = pl.name
                }
                else if pl.type == "locality" && self.selectedLocation.city.isEmpty {
                    self.selectedLocation.city = pl.name
                }
                else if pl.type == "administrative_area_level_1" && self.selectedLocation.city.isEmpty  {
                    self.selectedLocation.city = pl.name
                }
                else if pl.type == "administrative_area_level_2" && self.selectedLocation.city.isEmpty  {
                    self.selectedLocation.city = pl.name
                }
                else if pl.type == "country" {
                    self.selectedLocation.country = pl.name
                }
            }
            
            self.selectedLocation.coordinate = place.coordinate
            //print("Place name \(place.coordinate)")
            DispatchQueue.main.async {
                self.validateAddress()
                self.clickSave()
            }
        })
    }
    
    //
    //    fileprivate func setTableview() {
    //        let height = CGFloat(arrPlaces.count * 44)
    //
    //        conTableHeight.constant = height < tblHeight ? height : tblHeight
    //        UIView.animate(withDuration: 0.25) {
    //            self.view.layoutIfNeeded()
    //        }
    //    }
    
    fileprivate func validate() -> Bool {
        
        var isValid = true
        
        if tfAddress.text!.isEmpty() || !isValid {
            Utils.shared.animatePop(alertAddress)
            isValid = false
        }
        
        if tfMeeting.text!.isEmpty() {
            Utils.shared.animatePop(alertMeetingPoint)
            isValid = false
        }
        
        return isValid
    }
    
    fileprivate func validateAddress() {
        
        guard selectedLocation.country.lowercased().contains("norway") || selectedLocation.country.lowercased().contains("norge")   else {
            isValid = false
            lblError.animation = Spring.AnimationPreset.FadeIn.rawValue
            lblError.curve = Spring.AnimationCurve.Linear.rawValue
            lblError.duration = 0.35
            lblError.animate()
            return
        }
        
        isValid = true
        return
    }
}

//MARK:- TextField Delegate
//MARK:-

extension AddressPopup : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard textField.tag == TAG_ADDRESS else {
            return true
        }
        
        lblError.animation = Spring.AnimationPreset.FadeOut.rawValue
        lblError.curve = Spring.AnimationCurve.Linear.rawValue
        lblError.duration = 0
        lblError.animate()
        
        guard let rangeT = Range(range, in: textField.text!) else { return true }
        let searchText: String = textField.text!.replacingCharacters(in: rangeT, with: string)
        placeAutocomplete(search: searchText)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == TAG_ADDRESS {
            tfMeeting.becomeFirstResponder()
        }
        else if textField.tag == TAG_MEETING {
            tfMeeting.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == TAG_ADDRESS {
            Utils.shared.animateFadeOut(alertAddress, 0)
        }
        else if textField.tag == TAG_MEETING {
            Utils.shared.animateFadeOut(alertMeetingPoint, 0)
        }
    }
}

//MARK:- Tableview Delegate || Datasource
//MARK:-

extension AddressPopup : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPlaces.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellAddress", for: indexPath) as! CellListPicker
        cell.lblOption.attributedText = arrPlaces[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedLocation.address = arrPlaces[indexPath.row].name.string
        
        placeDetail(placeId: arrPlaces[indexPath.row].placeId)
        tfAddress.text = selectedLocation.address
        arrPlaces = []
        tblList.reloadData()
        //  setTableview()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.bounds.width, height: 5)))
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
}


