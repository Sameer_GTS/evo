//
//  Login.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/9/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import IQKeyboardManager
import Spring
import FBSDKLoginKit
import GoogleSignIn
import Mixpanel

class Login: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var viewCircle: IQPreviousNextView!
    @IBOutlet var conTop: NSLayoutConstraint!
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!
    
    @IBOutlet var viewShadow: UIView! {
        didSet {
            viewShadow.dropShadow(radius: 10, color: .black)
        }
    }
    @IBOutlet var btnFB: UIButton!{
        didSet {
            btnFB.dropShadow(radius: 5)
            btnFB.setBorder(cornerRadius: btnFB.bounds.height/2, borderWidth: 0, borderColor: .clear)
        }
    }
    @IBOutlet var btnGoogle: UIButton!{
        didSet {
            btnGoogle.dropShadow(radius: 5)
            btnGoogle.setBorder(cornerRadius: btnGoogle.bounds.height/2, borderWidth: 0, borderColor: .clear)
        }
    }
    @IBOutlet var btnSignIn: UIButton!{
        didSet {
            //btnSignIn.setBorder(cornerRadius: btnSignIn.bounds.height/2, borderWidth: 0, borderColor: .clear)
        }
    }
    
    @IBOutlet var alertEmail: SpringImageView! {
        didSet {
            Utils.shared.animateFadeOut(alertEmail, 0.0)
        }
    }
    @IBOutlet var alertPassword: SpringImageView! {
        didSet {
            Utils.shared.animateFadeOut(alertPassword, 0.0)
        }
    }
    
    ///---- Get email
    
    @IBOutlet var viewEmail: SpringView! {
        didSet {
            viewEmail.animation = Spring.AnimationPreset.FadeOut.rawValue
            viewEmail.duration = 0
            viewEmail.curve = Spring.AnimationCurve.Linear.rawValue
            viewEmail.animate()
        }
    }
    @IBOutlet var viewEmailContainter: SpringView! {
        didSet {
            viewEmailContainter.setBorder(cornerRadius: 10, borderWidth: 0, borderColor: .clear)
            viewEmailContainter.animation = Spring.AnimationPreset.FadeOut.rawValue
            viewEmailContainter.duration = 0
            viewEmailContainter.curve = Spring.AnimationCurve.Linear.rawValue
            viewEmailContainter.animate()
        }
    }
    @IBOutlet var tfAddEmail: UITextField!
    @IBOutlet var btnSubmit: UIButton! {
        didSet {
            btnSubmit.dropShadow(radius: 10)
        }
    }
    @IBOutlet var alertEmailAdd: SpringImageView! {
        didSet {
            Utils.shared.animateFadeOut(alertEmailAdd, 0.0)
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //------- Enable Google to handel login with google.
        GIDSignIn.sharedInstance().clientID = KEY_GOOGLE
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btnFB.setBorder(cornerRadius: btnFB.bounds.height/2, borderWidth: 0, borderColor: .clear)
        btnGoogle.setBorder(cornerRadius: btnGoogle.bounds.height/2, borderWidth: 0, borderColor: .clear)
        //btnSignIn.setBorder(cornerRadius: btnSignIn.bounds.height/2, borderWidth: 0, borderColor: .clear)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        btnFB.setBorder(cornerRadius: btnFB.bounds.height/2, borderWidth: 0, borderColor: .clear)
        btnGoogle.setBorder(cornerRadius: btnGoogle.bounds.height/2, borderWidth: 0, borderColor: .clear)
        //btnSignIn.setBorder(cornerRadius: btnSignIn.bounds.height/2, borderWidth: 0, borderColor: .clear)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        tfPassword.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func clickFB(_ sender: UIButton) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile"], from: self) { (result, error) in
            if (AccessToken.current != nil) {
                
                GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, response, error) -> Void in
                    
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05, execute: {
                        if (error == nil){
                            let dict = response as! NSDictionary
                            let name = dict["name"] as? String ?? ""
                            let socialId =  String(describing: dict["id"]!)
                            let image =  ((dict["picture"] as! NSDictionary )["data"] as! NSDictionary)["url"] as? String ?? ""
                            let email = dict["email"] as? String ?? ""
                            
                            WebConnect.loginWithFB(email: email, name: name, profileId: socialId, imageURL: image, completion: { (status, message) in
                                if status {
                                    
                                    if IK_USER.email.isEmpty() {
                                        self.showEmail()
                                    }
                                    else {
                                        /*
                                        Mixpanel.sharedInstance()?.createAlias(IK_USER.profile_id,
                                                forDistinctID: Mixpanel.sharedInstance()!.distinctId,
                                                usePeople: true)
                                        */
                                        
                                        self.forwordView()
                                    }
                                }
                                else {
                                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                }
                            })
                        }
                    })
                })
            }
        }
    }
    
    @IBAction func clickGoogle(_ sender: UIButton) {
        
        if GIDSignIn.sharedInstance().currentUser == nil  {
            if GIDSignIn.sharedInstance().hasAuthInKeychain() {
                GIDSignIn.sharedInstance().signInSilently()
            } else {
                GIDSignIn.sharedInstance().signIn()
            }
        } else {
            
            let id = GIDSignIn.sharedInstance().currentUser.userID ?? ""
            let name = GIDSignIn.sharedInstance().currentUser.profile.name ?? ""
            let email = GIDSignIn.sharedInstance().currentUser.profile.email ?? ""
            let picture = GIDSignIn.sharedInstance().currentUser.profile.imageURL(withDimension: 32).absoluteString
            
            WebConnect.loginWithGoogle(email: email, name: name, profileId: id, imageURL: picture) { (status, message) in
                if status {
                    if IK_USER.email.isEmpty() {
                        self.showEmail()
                    }
                    else {
                        /*
                        Mixpanel.sharedInstance()?.createAlias(IK_USER.profile_id,
                            forDistinctID: Mixpanel.sharedInstance()!.distinctId,
                                usePeople: true)
                        */
                        self.forwordView()
                    }
                }
                else {
                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                }
            }
            
        }
    }
    
    @IBAction func clickSignin(_ sender: UIButton) {
        guard validate() else {
            return
        }
        self.view.endEditing(true)
        
        WebConnect.login(email: tfEmail.text!.trimString,
                         password: tfPassword.text!.trimString) { (status, message)  in
                            if status {
                                self.forwordView()
                                
                                if IK_USER.userType == .ace {
//                                    let eventParam = ["profile_id":IK_USER.profile_id,
//                                                      "interest":IK_USER.sportId.map(String.init).joined(separator: ","),
//                                                      "fitness_level":arrFitness[IK_USER.fitness],
//                                                      "age_group":arrAge[IK_USER.ageGroup]] as [String : String]
                                    
                                    Utils.shared.logEventWithFlurry(EventName: "ace_login", EventParam: [:])
                                }else {
//                                    let eventParam = ["profile_id":IK_USER.profile_id,
//                                                      "interest":IK_USER.sportId.map(String.init).joined(separator: ","),
//                                                      "fitness_level":arrFitness[IK_USER.fitness],
//                                                      "age_group":arrAge[IK_USER.ageGroup]] as [String : String]
                                    
                                    Utils.shared.logEventWithFlurry(EventName: "user_login", EventParam: [:])
                                }
                                
                            }
                            else {
                                
                                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                

                                /*
                                if message.lowercased() == "invalid email" {
                                    Utils.shared.animatePop(self.alertEmail)
                                }
                                else if message.lowercased() == "wrong password" {
                                    Utils.shared.animatePop(self.alertPassword)
                                }*/
                            }
        }
    }
    
    @IBAction func clickSubmitEmail(_ sender: UIButton) {
        
        guard tfAddEmail.text!.isEmail() else  {
            Utils.shared.animatePop(alertEmailAdd)
            return
        }
        
        self.view.endEditing(true)
        
        WebConnect.updateEmail(email: tfAddEmail.text!.trimString) { (status, message) in
            if status {
                self.forwordView()
                self.hideMail()
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
        
    }
    
    @IBAction func clickCancelEmail(_ sender: UIButton) {
        IK_DEFAULTS.removeObject(forKey: USER_DETAIL)
        IK_DEFAULTS.synchronize()
        hideMail()
    }

    //MARK:- Other
    //MARK:-
    
    fileprivate func forwordView() {
        
        if IK_USER.consentAgreed == consentAgree.consentAgreed.rawValue {
            
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "LoginPopUpVC") as! LoginPopUpVC
            
            vc.onDismiss = { (action) in
                /*
                if action {
                    Defaults.set(true, forKey: trackEvent)
                }else {
                    Defaults.set(false, forKey: trackEvent)
                }
                */
                
                self.navigateInApp()
            }
            self.navigationController?.present(vc, animated: true, completion: nil)
            
        }else {
            self.navigateInApp()
        }
        
    }
    
    func navigateInApp() {
        
        Utils.shared.logUserIdOnLogin()
        
        if IK_USER.userType == .customer || IK_USER.userType == .requestedForAcer {
            if IK_USER.sportId.count == 0 && IK_USER.nutritionist == "N"  {
                let sport = self.storyboard?.instantiateViewController(withIdentifier: "Sports") as! Sports
                self.navigationController?.pushViewController(sport, animated: true)
            }
            else if IK_USER.gym == 0 {
                let gym = self.storyboard?.instantiateViewController(withIdentifier: "GymList") as! GymList
                self.navigationController?.pushViewController(gym, animated: true)
            }
            else if IK_USER.fitness == 0 {
                let fitness = self.storyboard?.instantiateViewController(withIdentifier: "FitnessLevel") as! FitnessLevel
                self.navigationController?.pushViewController(fitness, animated: true)
            }
            else if IK_USER.ageGroup == 0 {
                let age = self.storyboard?.instantiateViewController(withIdentifier: "AgeGroup") as! AgeGroup
                self.navigationController?.pushViewController(age, animated: true)
            }
            else if IK_USER.profile_image.isEmpty() {
                let picture = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePicture") as! ProfilePicture
                self.navigationController?.pushViewController(picture, animated: true)
            }
            else {
                
                /*
                 Mixpanel.sharedInstance()?.identify(IK_USER.profile_id, usePeople: true)
                 Mixpanel.sharedInstance()?.people.set([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
                 "First Name":IK_USER.first_name,
                 "Last Name":IK_USER.last_name,*/
                 "Profile":IK_USER.profile_image,
                 /*"Email":IK_USER.email,
                 "$email":IK_USER.email,*/
                 "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                 "Fitness Level":arrFitness[IK_USER.fitness],
                 "Age Group":arrAge[IK_USER.ageGroup]])
                 
                 Mixpanel.sharedInstance()?.registerSuperProperties([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
                 "First Name":IK_USER.first_name,
                 "Last Name":IK_USER.last_name,
                 "Email":IK_USER.email,*/
                 "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                 "Fitness Level":arrFitness[IK_USER.fitness],
                 "Age Group":arrAge[IK_USER.ageGroup]])
                 */
                
                ChatManager.shared.connect(userId:IK_USER.profile_id, accessToken: IK_USER.sendbirdToken)
                
                IK_DELEGATE.TabBarConfiguration(true)
                
                /*
                 let home = self.storyboard?.instantiateViewController(withIdentifier: "AceList") as! AceList
                 let navigation = UINavigationController(rootViewController:home )
                 navigation.isNavigationBarHidden = true
                 
                 UIView.transition(from: IK_DELEGATE.window!.rootViewController!.view,
                 to: navigation.view,
                 duration: 0.45,
                 options: .transitionCrossDissolve,
                 completion: { (_) in
                 IK_DELEGATE.window!.rootViewController = navigation
                 })*/
            }
        }
        else if IK_USER.userType == .ace {
            /*
             Mixpanel.sharedInstance()?.registerSuperProperties([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
             "First Name":IK_USER.first_name,
             "Last Name":IK_USER.last_name,
             "Email":IK_USER.email,*/
             "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
             "Fitness Level":arrFitness[IK_USER.fitness],
             "Age Group":arrAge[IK_USER.ageGroup]])
             */
            
            ChatManager.shared.connect(userId:IK_USER.profile_id, accessToken: IK_USER.sendbirdToken)
            IK_DELEGATE.TabBarConfiguration(true)
        }

    }
    
    fileprivate func hideMail() {
        
        viewEmailContainter.setBorder(cornerRadius: 10, borderWidth: 0, borderColor: .clear)
        viewEmailContainter.animation = Spring.AnimationPreset.FadeOut.rawValue
        viewEmailContainter.duration = 0.3
        viewEmailContainter.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmailContainter.animate()
        
        viewEmail.animation = Spring.AnimationPreset.FadeOut.rawValue
        viewEmail.duration = 0.3
        viewEmail.delay = 0.15
        viewEmail.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmail.animate()
    }
    
    fileprivate func showEmail() {
        
        viewEmail.animation = Spring.AnimationPreset.FadeIn.rawValue
        viewEmail.duration = 0.3
        viewEmail.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmail.animate()
        
        viewEmailContainter.animation = Spring.AnimationPreset.FadeIn.rawValue
        viewEmailContainter.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmailContainter.duration = 0.3
        viewEmailContainter.delay = 0.25
        viewEmailContainter.scaleX = 0.5
        viewEmailContainter.scaleY = 0.5
        viewEmailContainter.animate()
    }
    
    fileprivate func validate() -> Bool {
        var isValid = true
        
        if tfEmail.text!.isEmpty() || !tfEmail.text!.isEmail() {
            Utils.shared.animatePop(alertEmail)
            isValid = false
        }
        
        if tfPassword.text!.isEmpty() || tfPassword.text!.length < 6 {
            Utils.shared.animatePop(alertPassword)
            isValid = false
        }
        
        return isValid
    }
    
}

extension Login : GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        let id = user.userID ?? ""
        let name = user.profile.name ?? ""
        let picture = user.profile.imageURL(withDimension: 32).absoluteString
        let email = user.profile.email ?? ""
        
        WebConnect.loginWithGoogle(email: email, name: name, profileId: id, imageURL: picture) { (status, message) in
            if status {
                if IK_USER.email.isEmpty() {
                    self.showEmail()
                }
                else {
                    /*
                    Mixpanel.sharedInstance()?.createAlias(IK_USER.profile_id,
                                                           forDistinctID: Mixpanel.sharedInstance()!.distinctId,
                                                           usePeople: true)
                    */
                    
                    
                    self.forwordView()
                }
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
    }
    
    // MARK: - Signin UI Delegate
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        viewController.dismiss(animated: true, completion: nil)
    }
}

extension Login : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfEmail {
            tfPassword.becomeFirstResponder()
        }
        else if textField == tfPassword {
            tfPassword.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfEmail {
            Utils.shared.animateFadeOut(alertEmail)
        }
        else if textField == tfPassword {
            Utils.shared.animateFadeOut(alertPassword)
        }
        else {
            Utils.shared.animateFadeOut(alertEmailAdd)
        }
    }
}


