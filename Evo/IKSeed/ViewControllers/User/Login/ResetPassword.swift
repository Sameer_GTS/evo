//
//  ResetPassword.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/9/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.


import UIKit
import Spring

class ResetPassword: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var btnSubmit: UIButton! {
        didSet {
            btnSubmit.dropShadow(radius: 5)
        }
    }
    @IBOutlet var alertEmail: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertEmail,0.0)
        }
    }
    @IBOutlet var tfEmail: UITextField!
    
    //MARK:- Variable
    //MARK:-
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ =  navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickSubmit(_ sender: UIButton) {
        if tfEmail.text!.isEmpty() || !tfEmail.text!.isEmail() {
            Utils.shared.animatePop(alertEmail)
            return
        }
        self.view.endEditing(true)
        
        WebConnect.requestOTP(email: tfEmail.text!.trimString) { (status, message) in
            Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil,dismissWith: { (_) in
                guard status else {return}
                
                let reset = self.storyboard?.instantiateViewController(withIdentifier: "SetPassword") as! SetPassword
                self.navigationController?.pushViewController(reset, animated: true)
            })
        }
    }
    
    //MARK:- Other
    //MARK:-
}

//MARK:- Textfield Delegate
//MARK:-

extension ResetPassword : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfEmail.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        Utils.shared.animateFadeOut(alertEmail)
    }
}
