//
//  LoginPopUpVC.swift
//  Ikseed
//
//  Created by Sameer's MACmini on 03/09/19.
//  Copyright © 2019 Chanchal Warde. All rights reserved.
//

import UIKit
import Nantes

class LoginPopUpVC: UIViewController {
    
    var onDismiss: (_ value: Bool) -> Void = { _ in }
    
    @IBOutlet weak var lblTitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTermPrivacyTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnJaTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTerms: NantesLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        if UIScreen.main.bounds.height >= 800 {
            lblTitleTopConstraint.constant = 150
            lblTermPrivacyTopConstraint.constant = 80
            btnJaTopConstraint.constant = 80
        }

        self.addClickableText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func addClickableText() {
        
        let terms = "Jeg har lest og godtar \nbrukerbetingelsene og personvernpolicy"
        
        lblTerms.text = terms
        lblTerms.delegate = self
        
        let linkFormatAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.init(name: "Helvetica Neue", size: 18.0) ?? "",
            .foregroundColor: UIColor.white,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        lblTerms.linkAttributes = linkFormatAttributes
        
        let nsRangeTerms = (terms as NSString).range(of: "brukerbetingelsene")
        lblTerms.addLink(to: URL.init(string: WebConnect.TermsNCondition)!, withRange: nsRangeTerms)
        
        let nsRangePrivacy = (terms as NSString).range(of: "personvernpolicy")
        lblTerms.addLink(to: URL.init(string: WebConnect.PrivacyPolicy)!, withRange: nsRangePrivacy)
    }
    
    //MARK: IBAction
    @IBAction func brukerbetingelseneBtnClicked(_ sender: UIButton) {
        let term = self.storyboard?.instantiateViewController(withIdentifier: "Terms") as! Terms
        term.url = WebConnect.TermsNCondition
        term.strTitle = "Brukerbetingelsene"
        
        self.present(term, animated: true, completion: {
        })
    }
    
    @IBAction func personvernpolicyBtnClicked(_ sender: UIButton) {
        let policy = self.storyboard?.instantiateViewController(withIdentifier: "Terms") as! Terms
        policy.url = WebConnect.PrivacyPolicy
        policy.strTitle = "Personvernpolicy"
        self.present(policy, animated: true, completion: {
        })
    }
    
    @IBAction func jaBtnClicked(_ sender: UIButton) {
        
        self.updateConsent(consent: 1)
    }
    
    @IBAction func neiBtnClicked(_ sender: UIButton) {
        
        self.updateConsent(consent: 2)
    }
    
    func updateConsent(consent:Int) {
        WebConnect.updateUserConsent(consentAgreed: consent) { (status, message) in
            guard status else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                return
            }
            
            self.dismiss(animated: true) {
                self.onDismiss(status)
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}


//MARK:- NantesLabel Delegate
//MARK:-

extension LoginPopUpVC : NantesLabelDelegate {
    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        
        if link == URL.init(string: WebConnect.TermsNCondition) {
            let term = self.storyboard?.instantiateViewController(withIdentifier: "Terms") as! Terms
            term.url = WebConnect.TermsNCondition
            term.strTitle = "Brukerbetingelsene"
            self.present(term, animated: true, completion: {
            })
        }
        else if link == URL.init(string: WebConnect.PrivacyPolicy) {
            let policy = self.storyboard?.instantiateViewController(withIdentifier: "Terms") as! Terms
            policy.url = WebConnect.PrivacyPolicy
            policy.strTitle = "Personvernpolicy"
            self.present(policy, animated: true, completion: {
            })
        }
    }
    
}
