//
//  SetPassword.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/9/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Spring

class SetPassword: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var btnSave: UIButton! {
        didSet {
            btnSave.dropShadow(radius: 5)
        }
    }
    @IBOutlet var alertOtp: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertOtp,0.0)
        }
    }
    @IBOutlet var alertPassword: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertPassword,0.0)
        }
    }
    @IBOutlet var alertConfirmPassword: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertConfirmPassword,0.0)
        }
    }
    @IBOutlet var tfOtp: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var tfConfirmPassword: UITextField!
    
    //MARK:- Variable
    //MARK:-
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction Method
    //MARK:-
    
    @IBAction func clickSave(_ sender: UIButton) {
        guard validation() else {
            return
        }
        self.view.endEditing(true)
        
        WebConnect.resetPassword(otp: tfOtp.text!.trimString, password: tfPassword.text!.trimString) { (status, message) in
            Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil, dismissWith: { (_) in
                guard status else { return }
                if let vc = self.navigationController?.viewControllers[1] as? Login {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            })
        }
    }
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickVisibilityOtp(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        tfOtp.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func clickVisibilityPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        tfPassword.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func clickVisibilityConfirmPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        tfConfirmPassword.isSecureTextEntry = !sender.isSelected
    }
    
    //MARK:- Other Method
    //MARK:-
    
    fileprivate func validation() -> Bool {
        var isValid = true
        
        if tfOtp.text!.isEmpty() {
            Utils.shared.animatePop(alertOtp)
            isValid = false
        }
        
        if tfPassword.text!.isEmpty() || tfPassword.text!.length < 6 {
            Utils.shared.animatePop(alertPassword)
            isValid = false
        }
        
        if tfConfirmPassword.text!.isEmpty() || tfConfirmPassword.text != tfPassword.text {
            Utils.shared.animatePop(alertConfirmPassword)
            isValid = false
        }
        
        return isValid
    }
}

extension SetPassword : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfOtp {
            tfPassword.becomeFirstResponder()
        }
        else if textField == tfPassword {
            tfConfirmPassword.becomeFirstResponder()
        }
        else {
            tfConfirmPassword.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfOtp {
            Utils.shared.animateFadeOut(alertOtp)
        }
        else if textField == tfPassword {
            Utils.shared.animateFadeOut(alertPassword)
        }
        else if textField == tfConfirmPassword {
            Utils.shared.animateFadeOut(alertConfirmPassword)
        }
    }
    
}


