//
//  AceDetail.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/21/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Mixpanel
import AMTagListView
//Fahad
import TagListView
import Flurry_iOS_SDK

class AceDetail: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var tblAce: UITableView!
    @IBOutlet var viewContainer : UIView!
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgBanner: UIImageView!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var star1: UIImageView!
    @IBOutlet var star2: UIImageView!
    @IBOutlet var star3: UIImageView!
    @IBOutlet var star4: UIImageView!
    @IBOutlet var star5: UIImageView!
    @IBOutlet var lblRating: UILabel!
    @IBOutlet var lblVerified: UILabel!
    @IBOutlet var lblPTLevel: UILabel!
    
    @IBOutlet var viewTag: TagListView! //UIView!
    //Fahad
    @IBOutlet var viewTagHeight: NSLayoutConstraint!
    //
    @IBOutlet var lblWhy: UILabel!
    @IBOutlet var lblMember: UILabel!
    @IBOutlet var btnChat: UIButton! {
        didSet {
            btnChat.dropShadow()
        }
    }
    @IBOutlet var btnBook: UIButton! {
        didSet {
            btnBook.dropShadow(color:  #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        }
    }
    @IBOutlet var conNavHeight: NSLayoutConstraint!
    
    //MARK:- Variable
    //MARK:-
    
    var objAce : Ace!
    var isBack : Bool = false
    
    fileprivate var arrHelp : [String] = []
    /// Get updated view height whenever layover list updated and height increase for table.
    fileprivate var viewHeight : CGFloat {
        return btnBook.superview!.frame.origin.y + btnBook.superview!.frame.height + 20
    }
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setAceDetail()
        if #available(iOS 11.0, *) {
            var top = UIApplication.shared.keyWindow?.safeAreaInsets.top
            conNavHeight.constant = conNavHeight.constant + top!
            //  tblAce.contentInset = UIEdgeInsets(top: CGFloat(Float(top!)), left: 0, bottom: 0, right: 0)
        }
        else {
            let top = UIApplication.shared.statusBarFrame.size.height
            tblAce.contentInset = UIEdgeInsets(top: CGFloat(-Float(top)), left: 0, bottom: 0, right: 0)
        }
        
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.extendedLayoutIncludesOpaqueBars = false
        self.automaticallyAdjustsScrollViewInsets = false
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateViewSize()
        
        /*
        Mixpanel.sharedInstance()?.track("Browse ace", properties:["session":track.start.timeIntervalSince1970,
                                                                   "ace_id":self.objAce.profileId,
                                                                   "ace_name":self.objAce.firstName + " " + self.objAce.lastName])
        */
        
        
        let eventParams = [//"session"   : track.start.timeIntervalSince1970,
                           "ace_id"    : self.objAce.profileId,
                           //"ace_name"  : self.objAce.firstName + " " + self.objAce.lastName
                          ] as [String : Any]
        Utils.shared.logEventWithFlurry(EventName: "browse_ace", EventParam: eventParams)
        //Utils.shared.startEventDurationForEvents(eventName: "browse_ace", eventParam: eventParams)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //let eventParams = ["ace_id"    : self.objAce.profileId] as [String : Any]
        //Utils.shared.closeEventDurationForEvents(eventName: "browse_ace", eventParam: eventParams)
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ =  navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Other
    //MARK:-
    
    /// Function to updated view size and table view content size whenever new layover added or deleted.
    fileprivate func updateViewSize() {
        var frame = self.viewContainer.frame
        frame.size.height = self.viewHeight
        self.viewContainer.frame = frame
        self.tblAce.contentSize = frame.size
        self.tblAce.setNeedsDisplay()
        self.viewContainer.setNeedsDisplay()
    }
    
    fileprivate func setAceDetail() {
        
        guard  (objAce != nil) else { return }
        
        btnBook.isHidden = !objAce.isBookEnabled
        btnChat.isHidden = !objAce.isChatEnabled
        
        WebConnect.trackActivity(aceID: objAce.profileId)
        
        lblName.text = objAce.firstName + " " + objAce.lastName
        lblRating.text = String(describing: objAce.rating)
        lblWhy.text = objAce.aboutExp
        lblMember.text = objAce.memberof
      //  lblVerified.text = objAce.adminVerify == 1 ? "Verifisert" : "Unverified Ace"
      //  lblVerified.textColor = objAce.adminVerify == 1 ? #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1) : #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        arrHelp = objAce.helpInputs.split(separator: ",").map(String.init)
        
        lblPTLevel.text = objAce.ptlevel
        
        star1.tintColor = objAce.rating >= 1 ? #colorLiteral(red: 1, green: 0.8235294118, blue: 0, alpha: 1) : #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        star2.tintColor = objAce.rating >= 2 ? #colorLiteral(red: 1, green: 0.8235294118, blue: 0, alpha: 1) : #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        star3.tintColor = objAce.rating >= 3 ? #colorLiteral(red: 1, green: 0.8235294118, blue: 0, alpha: 1) : #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        star4.tintColor = objAce.rating >= 4 ? #colorLiteral(red: 1, green: 0.8235294118, blue: 0, alpha: 1) : #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        star5.tintColor = objAce.rating >= 5 ? #colorLiteral(red: 1, green: 0.8235294118, blue: 0, alpha: 1) : #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        
        imgProfile.setBorder(cornerRadius: imgProfile.bounds.height/2, borderWidth: 1.5, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        imgProfile.kf.indicatorType = .activity
        imgProfile.kf.setImage(with: URL(string: objAce.profileImage),
                               placeholder: #imageLiteral(resourceName: "thumb_menu"),
                               options: nil,
                               progressBlock: nil,
                               completionHandler: nil)
        
        imgBanner.kf.indicatorType = .activity
        imgBanner.kf.setImage(with: URL(string: objAce.backgroundImage),
                              placeholder: #imageLiteral(resourceName: "no_image"),
                              options: nil,
                              progressBlock: nil,
                              completionHandler: { (image, error, _, nil) in
                                guard image != nil else { return}
                                self.imgBanner.contentMode = .scaleAspectFill
                                self.imgBanner.clipsToBounds = true
        })

        //Fahad
        //*********************************************************
//        AMTagView.appearance().radius = 10
//        AMTagView.appearance().tagLength = 0
//        AMTagView.appearance().tagColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
//        AMTagView.appearance().innerTagColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
//        AMTagView.appearance().textPadding = CGPoint(x: 5, y: 20)
//
//        let tagList = AMTagListView(frame: viewTag.bounds)
//        tagList.addTags(arrHelp)
//        tagList.rearrangeTags()
//        viewTag.addSubview(tagList)
//        viewTagHeight.constant = tagList.frame.size.height + 20
        viewTag.alignment = .left
        viewTag.tagBackgroundColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        viewTag.paddingX = 8
        viewTag.paddingY = 10
        viewTag.tagLineBreakMode = .byWordWrapping
        viewTag.textFont = UIFont(name: "Helvetica", size: 14) ?? UIFont.systemFont(ofSize: 14)
        viewTag.addTags(arrHelp)
        viewTagHeight.constant = viewTag.intrinsicContentSize.height
        viewTag.cornerRadius = 0 //(viewTag.tagViews.first?.frame.size.height ?? 20) / 2
        //*********************************************************
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "segueChat" &&  isBack {
            _ =  navigationController?.popViewController(animated: true)
            return false
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "segueBook" {
            let destination = segue.destination as? Booking
            destination?.aceDetail = objAce
        }
        else if segue.identifier == "segueChat" {
            
            /*
            Mixpanel.sharedInstance()?.track("Chat with an Ace", properties:["session":track.start.timeIntervalSince1970,
                                                                             "ace_id":self.objAce.profileId,
                                                                             "ace_name":self.objAce.firstName + " " + self.objAce.lastName])
            */
            
            let eventParams = [//"session"  : track.start.timeIntervalSince1970,
                               "ace_id"   : self.objAce.profileId] as [String : Any]
            Utils.shared.logEventWithFlurry(EventName: "chat_with_ace", EventParam: eventParams)
            //Utils.shared.startEventDurationForEvents(eventName: "chat_with_ace", eventParam: eventParams)
            
            
            let destination = segue.destination as? Chat
            destination?.otherUser =  (id:objAce.profileId,
                                       name:objAce.firstName + " " + objAce.lastName,
                                       image: objAce.profileImage)
        }
    }
    
}


