//
//  ExerciseSchedule.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/9/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Mixpanel

class FitnessLevel: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    enum TagsLevel : Int {
        case outOfShape = 1
        case average
        case athletic
        case elite
    }
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var btnNext: UIButton! {
        didSet{
            btnNext.dropShadow(radius: 5)
        }
    }
    @IBOutlet var btnOutOfShape: UIButton! {
        didSet{
            btnOutOfShape.tag = TagsLevel.outOfShape.rawValue
        }
    }
    @IBOutlet var btnAverage: UIButton! {
        didSet{
            btnAverage.tag = TagsLevel.average.rawValue
        }
    }
    @IBOutlet var btnAthletic: UIButton! {
        didSet{
            btnAthletic.tag = TagsLevel.athletic.rawValue
        }
    }
    @IBOutlet var btnElite: UIButton! {
        didSet{
            btnElite.tag = TagsLevel.elite.rawValue
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var selected = 0
    var isBack = false
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // jitendra
        if IK_USER.fitness == TagsLevel.elite.rawValue {
            btnElite.isSelected = true
            btnElite.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        if IK_USER.fitness == TagsLevel.outOfShape.rawValue {
            btnOutOfShape.isSelected = true
            btnOutOfShape.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        if IK_USER.fitness == TagsLevel.average.rawValue {
            btnAverage.isSelected =  true
            btnAverage.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        if IK_USER.fitness == TagsLevel.athletic.rawValue {
            btnAthletic.isSelected =  true
            btnAthletic.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        
        // ends
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction Method
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickLevel(_ sender: UIButton) {
        
        selected = sender.tag
        
        btnOutOfShape.isSelected = sender.tag == TagsLevel.outOfShape.rawValue ? true : false
        btnAverage.isSelected = sender.tag == TagsLevel.average.rawValue ? true : false
        btnAthletic.isSelected = sender.tag == TagsLevel.athletic.rawValue ? true : false
        btnElite.isSelected = sender.tag == TagsLevel.elite.rawValue ? true : false
        
        btnOutOfShape.tintColor = sender.tag == TagsLevel.outOfShape.rawValue ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.7333333333, green: 0.9019607843, blue: 0.7960784314, alpha: 0)
        btnAverage.tintColor = sender.tag == TagsLevel.average.rawValue ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.7333333333, green: 0.9019607843, blue: 0.7960784314, alpha: 0)
        btnAthletic.tintColor = sender.tag == TagsLevel.athletic.rawValue ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.7333333333, green: 0.9019607843, blue: 0.7960784314, alpha: 0)
        btnElite.tintColor = sender.tag == TagsLevel.elite.rawValue ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.7333333333, green: 0.9019607843, blue: 0.7960784314, alpha: 0)
        
        WebConnect.updateFitnessLevel(levelID: selected) { (status,message) in
            if status {
                if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                    
                    let newDic = NSMutableDictionary(dictionary: dict)
                    newDic.setValue(self.selected, forKey: "fitness_level")
                    
                    IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                    
                    if self.isBack {
                        _ = self.navigationController?.popViewController(animated: true)
                    } else {
                        /*
                        Mixpanel.sharedInstance()?.track("Fitness level", properties:[/*"Name":IK_USER.first_name + " " + IK_USER.last_name ,
                                "Email":IK_USER.email,*/
                                "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                "Fitness Level":arrFitness[IK_USER.fitness]])
                        */
                        
//                        let eventParams = ["profile_id"    : IK_USER.profile_id,
//                                           "interest"      : IK_USER.sportId.map(String.init).joined(separator: ","),
//                                           "fitness_level" : arrFitness[IK_USER.fitness]] as [String : String]
                        
                        Utils.shared.logEventWithFlurry(EventName: "fitness_level", EventParam: [:])
                        
                        
                        let age = self.storyboard?.instantiateViewController(withIdentifier: "AgeGroup") as! AgeGroup
                        self.navigationController?.pushViewController(age, animated: true)
                    }
                }
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
    }
    
    @IBAction func clickNext(_ sender: UIButton) {
        
        guard selected > 0 else {
            Utils.shared.alert(on: self, message: ERROR_FITNESS , affirmButton: "Ok", cancelButton: nil)
            return
        }
        
        WebConnect.updateFitnessLevel(levelID: selected) { (status,message) in
            if status {
                if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                    
                    let newDic = NSMutableDictionary(dictionary: dict)
                    newDic.setValue(self.selected, forKey: "fitness_level")
                    
                    IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                    
                    /*
                    Mixpanel.sharedInstance()?.track("Fitness level", properties:[/*"Name":IK_USER.first_name + " " + IK_USER.last_name,
                            "Email":IK_USER.email,*/
                            "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                            "Fitness Level":arrFitness[IK_USER.fitness]])
                    */
                    
//                    let eventParams = ["interest":IK_USER.sportId.map(String.init).joined(separator: ","),
//                                       "fitness_level":arrFitness[IK_USER.fitness]] as [String : Any]
                    Utils.shared.logEventWithFlurry(EventName: "fitness_level", EventParam: [:])
                    
                    
                    let age = self.storyboard?.instantiateViewController(withIdentifier: "AgeGroup") as! AgeGroup
                    self.navigationController?.pushViewController(age, animated: true)
                }
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
    }
    
    @IBAction func clickSkip(_ sender: UIButton) {
        
    }
    
}
