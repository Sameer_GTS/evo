//
//  ProfilePicture.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/16/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Mixpanel


class ProfilePicture: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    let picker = MediaPicker()
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var btnUpload: UIButton!
    @IBOutlet var btnNext: UIButton! {
        didSet {
            btnNext.dropShadow()
        }
    }
    @IBOutlet var btnSkip: UIButton! {
        didSet {
            btnSkip.dropShadow( radius: 10, color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 1, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        btnUpload.setBorder(cornerRadius: btnUpload.bounds.height/2, borderWidth: 1, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 1, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        btnUpload.setBorder(cornerRadius: btnUpload.bounds.height/2, borderWidth: 1, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickUpload(_ sender: UIButton) {
        //Fahad
        picker.isSquared = true 
        picker.targetVC = self
        picker.showPicker(vc: self)
        picker.delegate = self
    }
    
    @IBAction func clickNext(_ sender: UIButton) {
        guard imgUser.tag == 123  else {
            Utils.shared.alert(on: self, message: ERROR_IMAGE, affirmButton: "Ok", cancelButton: nil)
            return
        }
        
        WebConnect.updateImage(image: imgUser.image!) { (status, message, imageurl ) in
            guard status else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                return
            }
            
            if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                
                let newDic = NSMutableDictionary(dictionary: dict)
                newDic.setValue(imageurl, forKey: "profile_image")
                
                IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                IK_DEFAULTS.synchronize()
                
                self.setMixpannel()
                /*
                Mixpanel.sharedInstance()?.track("Photo uploadeda", properties:[/*"Name":IK_USER.first_name + " " + IK_USER.last_name,
                    "Email":IK_USER.email,*/
                    "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                    "Fitness Level":arrFitness[IK_USER.fitness],
                    "Age Group":arrAge[IK_USER.ageGroup]])
                */
                
                //let eventParams = [//"interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                    //"fitness_level":arrFitness[IK_USER.fitness],
                                    //"age_group":arrAge[IK_USER.ageGroup]] as [String : Any]
                    Utils.shared.logEventWithFlurry(EventName: "photo_uploaded", EventParam: [:])
                
                
                ChatManager.shared.connect(userId:IK_USER.profile_id, accessToken: IK_USER.sendbirdToken)
                
                IK_DELEGATE.TabBarConfiguration(true)

                /*
                let home = self.storyboard?.instantiateViewController(withIdentifier: "AceList") as! AceList
                
                let navigation = UINavigationController(rootViewController:home )
                navigation.isNavigationBarHidden = true
                
                UIView.transition(from: IK_DELEGATE.window!.rootViewController!.view,
                                  to: navigation.view,
                                  duration: 0.45,
                                  options: .transitionCrossDissolve,
                                  completion: { (_) in
                                    IK_DELEGATE.window!.rootViewController = navigation
                })*/
            }
        }
    }
    
    @IBAction func clickSkip(_ sender: UIButton) {
        self.setMixpannel()
        /*
        Mixpanel.sharedInstance()?.track("Photo upload skip", properties:[/*"Name":IK_USER.first_name + " " + IK_USER.last_name,
            "Email":IK_USER.email,*/
            "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
            "Fitness Level":arrFitness[IK_USER.fitness],
            "Age Group":arrAge[IK_USER.ageGroup]])
        */
        
//        let eventParams = ["interest":IK_USER.sportId.map(String.init).joined(separator: ","),
//                           "fitness_level":arrFitness[IK_USER.fitness],
//                           "age_group":arrAge[IK_USER.ageGroup]] as [String : Any]
        
        Utils.shared.logEventWithFlurry(EventName: "photo_upload_skipped", EventParam: [:])
        
        
        ChatManager.shared.connect(userId:IK_USER.profile_id, accessToken: IK_USER.sendbirdToken)
        
        IK_DELEGATE.TabBarConfiguration(true)

        /*
        let home = self.storyboard?.instantiateViewController(withIdentifier: "AceList") as! AceList
        let navigation = UINavigationController(rootViewController:home )
        navigation.isNavigationBarHidden = true
        
        UIView.transition(from: IK_DELEGATE.window!.rootViewController!.view,
                          to: navigation.view,
                          duration: 0.45,
                          options: .transitionCrossDissolve,
                          completion: { (_) in
                            IK_DELEGATE.window!.rootViewController = navigation
        })*/
    }
    
    //MARK:- Other
    //MARK:-
    
    
    fileprivate func setMixpannel() {
        /*
        Mixpanel.sharedInstance()?.registerSuperProperties([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
                                                             "First Name":IK_USER.first_name,
                                                             "Last Name":IK_USER.last_name,
                                                             "Email":IK_USER.email,*/
                                                             "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                                             "Fitness Level":arrFitness[IK_USER.fitness],
                                                             "Age Group":arrAge[IK_USER.ageGroup]])
        */
    }
}

//MARK:- ImagePicker delegate
//MARK:-

extension ProfilePicture : ImagePickerDelegate {
    func didSelect(image: UIImage) {
        imgUser.contentMode = .scaleAspectFill
        imgUser.image = image
        imgUser.isHighlighted = false
        imgUser.tag = 123
    }
}
