//
//  ExerciseSchedule.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/9/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Mixpanel

class AgeGroup: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    enum TagsAge : Int {
        case under25 = 1
        case age26 = 2
        case age46 = 3
        case age66 = 4
    }
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var btnNext: UIButton! {
        didSet{
            btnNext.dropShadow(radius: 5)
        }
    }
    @IBOutlet var btnUnder25: UIButton! {
        didSet {
            btnUnder25.tag = TagsAge.under25.rawValue
        }
    }
    @IBOutlet var btnAge26: UIButton! {
        didSet {
            btnAge26.tag = TagsAge.age26.rawValue
        }
    }
    @IBOutlet var btnAge46: UIButton! {
        didSet{
            btnAge46.tag = TagsAge.age46.rawValue
        }
    }
    @IBOutlet var btnAge66: UIButton! {
        didSet{
            btnAge66.tag = TagsAge.age66.rawValue
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var selected = 0
    var isBack = false
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // jitendra
        if IK_USER.ageGroup == TagsAge.under25.rawValue {
            btnUnder25.isSelected = true
            btnUnder25.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        if IK_USER.ageGroup == TagsAge.age26.rawValue {
            btnAge26.isSelected = true
            btnAge26.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        if IK_USER.ageGroup == TagsAge.age46.rawValue {
            btnAge46.isSelected = true
            btnAge46.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        if IK_USER.ageGroup == TagsAge.age66.rawValue {
            btnAge66.isSelected = true
            btnAge66.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        //ends
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction Method
    //MARK:-
    @IBAction func clickBack(_ sender: UIButton) {
        _ =  navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickAge(_ sender: UIButton) {
        
        selected = sender.tag
        
        btnUnder25.isSelected = sender.tag == TagsAge.under25.rawValue ? true : false
        btnAge26.isSelected = sender.tag == TagsAge.age26.rawValue ? true : false
        btnAge46.isSelected = sender.tag == TagsAge.age46.rawValue ? true : false
        btnAge66.isSelected = sender.tag == TagsAge.age66.rawValue ? true : false
        
        btnUnder25.tintColor = sender.tag == TagsAge.under25.rawValue ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.7333333333, green: 0.9019607843, blue: 0.7960784314, alpha: 0)
        btnAge26.tintColor = sender.tag == TagsAge.age26.rawValue ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.7333333333, green: 0.9019607843, blue: 0.7960784314, alpha: 0)
        btnAge46.tintColor = sender.tag == TagsAge.age46.rawValue ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.7333333333, green: 0.9019607843, blue: 0.7960784314, alpha: 0)
        btnAge66.tintColor = sender.tag == TagsAge.age66.rawValue ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.7333333333, green: 0.9019607843, blue: 0.7960784314, alpha: 0)
        
        WebConnect.updateAgeGroup(ageId: selected) { (status, message) in
            if status {
                if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                    
                    let newDic = NSMutableDictionary(dictionary: dict)
                    newDic.setValue(self.selected, forKey: "age_groups")
                    
                    IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                    
                    if self.isBack {
                        _ = self.navigationController?.popViewController(animated: true)
                    } else {
                        
                        /*
                        Mixpanel.sharedInstance()?.track("Age group", properties:[/*"First name":IK_USER.first_name,
                                "last name":IK_USER.last_name,
                                "email":IK_USER.email,*/
                                "interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                "fitness level":arrFitness[IK_USER.fitness],
                                "age group":arrAge[IK_USER.ageGroup]])
                        */
                        
                        
//                        let eventParams = ["profile_id":IK_USER.profile_id,
//                                           "interest":IK_USER.sportId.map(String.init).joined(separator: ","),
//                                           "fitness_level":arrFitness[IK_USER.fitness],
//                                           "age_group":arrAge[IK_USER.ageGroup]] as [String : String]
                        Utils.shared.logEventWithFlurry(EventName: "age_group", EventParam: [:])
                        
                        
                        let picture = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePicture") as! ProfilePicture
                        self.navigationController?.pushViewController(picture, animated: true)
                    }
                }
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
    }
    
    @IBAction func clickNext(_ sender: UIButton) {
        
        guard selected > 0 else {
            Utils.shared.alert(on: self, message: ERROR_AGE , affirmButton: "Ok", cancelButton: nil)
            return
        }
        
        WebConnect.updateAgeGroup(ageId: selected) { (status, message) in
            if status {
                if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                    
                    let newDic = NSMutableDictionary(dictionary: dict)
                    newDic.setValue(self.selected, forKey: "age_groups")
                    
                    IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                    
                    if self.isBack {
                        _ = self.navigationController?.popViewController(animated: true)
                    } else {
                        let picture = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePicture") as! ProfilePicture
                        self.navigationController?.pushViewController(picture, animated: true)
                    }
                }
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
    }
    
    @IBAction func clickSkip(_ sender: UIButton) {
        let level = self.storyboard?.instantiateViewController(withIdentifier: "FitnessLevel") as! FitnessLevel
        self.navigationController?.pushViewController(level, animated: true)
    }
    
}
