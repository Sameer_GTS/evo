//
//  GymList.swift
//  Ikseed
//
//  Created by Chanchal Warde on 11/22/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.


import UIKit

class GymList: UIViewController {

    
    //MARK:- Constants
    //MARK:-
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var arrGym : [(id:Int, name:String , address : String )] = []
    var selectedAddrss = -1
    var isBack = false
    var isHandler = false
    var showAll = false
    
    var selectedGymId = -1

    
    var onSelect : ((id:Int, name:String , address : String)) -> Void = { _ in }

    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var tblAddress: UITableView!
    
    //MARK:- View Methods
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if showAll {
            WebConnect.getGymList(type: "A") { (status, arrList) in
                self.arrGym = arrList
                self.setSelectedGym()
            }
        } else {
            arrGym =  arrGyms
            tblAddress.reloadData()
            WebConnect.getGymList(type: "C") { (status, arrList) in
                self.arrGym = arrList
                self.setSelectedGym()
            }
        }
        
        self.setSelectedGym()
    }
    
    // jitendra
    func setSelectedGym() {
        if selectedAddrss == -1 {
            for i in 0..<arrGym.count {
                if arrGym[i].id == IK_USER.gym {
                    selectedAddrss = i
                    break
                }
            }
        }
        
        tblAddress.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectNext(_ sender: UIButton) {
        
        guard !isHandler else {
            onSelect(arrGym[selectedAddrss]);
            _ = self.navigationController?.popViewController(animated: true)
            return
        }
        
        guard selectedAddrss >= 0 else {
            return
        }
        
        WebConnect.updateGym(gymId: arrGym[selectedAddrss].id) { (status,message) in
            if status {
                if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                    
                    let newDic = NSMutableDictionary(dictionary: dict)
                    newDic.setValue(self.arrGym[self.selectedAddrss].id, forKey: "gym_id")
                    
                    IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                    
                    var nameStr = ""
                    if let a = (arrAllGyms.filter { $0.id == IK_USER.gym}).first {
                        nameStr = a.name
                    }
                    
                    let eventParams = [//"profile_id" : IK_USER.profile_id ?? "",
                                       //"interest"   : IK_USER.sportId.map(String.init).joined(separator: ","),
                                        "gym"        : nameStr] as [String : Any]
                    Utils.shared.logEventWithFlurry(EventName: "gym", EventParam: eventParams)
                    
                    
                    if self.isBack {
                        _ = self.navigationController?.popViewController(animated: true)
                    } else {
                        let level = self.storyboard?.instantiateViewController(withIdentifier: "FitnessLevel") as! FitnessLevel
                        self.navigationController?.pushViewController(level, animated: true)
                    }
                }
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
    }
}

extension GymList : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrGym.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellGym") as! CellGym
        
        cell.lblName.text = arrGym[indexPath.row].name
        cell.lblAddress.text = arrGym[indexPath.row].address
        //cell.iconSelected.isHidden = selectedAddrss != indexPath.row
        cell.iconSelected.isHidden = selectedGymId != arrGym[indexPath.row].id
        
        cell.iconSelected.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedAddrss = indexPath.row
        selectedGymId = arrGym[indexPath.row].id
        tableView.reloadData()
        selectNext(btnNext)
    }
}

class CellGym : UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var iconSelected: UIImageView!
    
    override func awakeFromNib() {
        
    }
}
