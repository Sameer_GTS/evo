//
//  Categories.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/9/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Mixpanel

protocol InterestDelegate : class  {
    func didSelectInterest(interest:[Int])
}

class Sports: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    let HEIGHT_FOOTER : CGFloat = 0
    
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var collCategories: UICollectionView!
    @IBOutlet var btnDone: UIButton! {
        didSet {
            btnDone.dropShadow(radius: 5)
        }
    }
    @IBOutlet var viewContainer: UIView! {
        didSet {
            //viewContainer.setBorder(cornerRadius: 10, borderWidth: 0, borderColor: .clear)
        }
    }
    @IBOutlet var viewShadow: UIView! {
        didSet {
            viewShadow.dropShadow(radius: 18,color : .black)
        }
    }
    @IBOutlet var conBackHeight: NSLayoutConstraint!
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var arrSports : [Sport] = []
    var selectedInterest : [Int] = []
    var isBack = false
    var isShowBack = false
    var isMultiSelection = true
    
    weak var delegate  : SportDelegate?
    weak var interestDelegate : InterestDelegate?
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        conBackHeight.constant = isShowBack ? 35 : 0
        self.view.layoutIfNeeded()
        btnDone.setTitle(isBack || isShowBack ? "Ferdig" : "Neste", for: .normal)
        btnDone.alpha = isMultiSelection ? 1 : 0
        
        WebConnect.getSport(type: "C") { (status, arr) in
            self.arrSports = arr
            
            if self.isBack {
                for i in 0..<self.arrSports.count {
                    self.arrSports[i].isSelected = self.selectedInterest.contains(self.arrSports[i].id)
                }
            }
            self.collCategories.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard (IK_USER.sportId != nil) else {return}
        // selectedId = IK_USER.sportId
    }
    
    override func viewDidAppear(_ animated: Bool) {
        conBackHeight.constant = isShowBack ? 35 : 0
        self.view.layoutIfNeeded()

    }
    
    //MARK:- IBAction Method
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickDone(_ sender: UIButton) {
        
        guard  selectedInterest.count > 0  else {
            Utils.shared.alert(on: self, message: ERROR_INTEREST, affirmButton: "Ok", cancelButton: nil)
            return
        }
        
        guard !isBack else {
            interestDelegate?.didSelectInterest(interest: self.selectedInterest)
            _ = navigationController?.popViewController(animated: true)
            return
        }
        
        let isNutrition = selectedInterest.contains(NUTRITION)
        if isNutrition {
            selectedInterest.remove(at: selectedInterest.index(of: NUTRITION)!)
        }
        
        WebConnect.saveInterest(sportId: selectedInterest, nutrition: isNutrition) { (status, message) in
            if status {
                if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                    
                    let newDic = NSMutableDictionary(dictionary: dict)
                    newDic.setValue(self.selectedInterest, forKey: "categories")
                    newDic.setValue(isNutrition ? "Y" : "N", forKey: "i_need_nutritionist")
                    
                    IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                    
                    if self.isShowBack {
                        _ = self.navigationController?.popViewController(animated: true)
                    } else {
                        /*
                        Mixpanel.sharedInstance()?.track("Interest", properties:[/*"Name":IK_USER.first_name + " " + IK_USER.last_name,
                            "Email":IK_USER.email,*/
                            "Interest":IK_USER.sportId.map(String.init).joined(separator: ",")])
                        */
                        
                        //let eventParams = ["interest":IK_USER.sportId.map(String.init).joined(separator: ",")] as [String : Any]
                        Utils.shared.logEventWithFlurry(EventName: "interest", EventParam: [:])
                        
                        
                        let list = self.storyboard?.instantiateViewController(withIdentifier: "GymList") as! GymList
                        self.navigationController?.pushViewController(list, animated: true)
                    }
                }
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
    }
    
    @IBAction func clickNutrition(_ sender: UIButton) {
        
        guard isMultiSelection else {
            delegate?.didSelectNutrition()
            navigationController?.popViewController(animated: true)
            return
        }
        
        if selectedInterest.contains(NUTRITION) {
            if let index = selectedInterest.index(of: NUTRITION) {
                selectedInterest.remove(at: index)
            }
        } else {
            guard selectedInterest.count < 3 else {
                Utils.shared.alert(on: self, message: ERROR_MAX_SPORT, affirmButton: "Ok", cancelButton: nil)  ; return
            }
            selectedInterest.append(NUTRITION)
        }
        
        collCategories.reloadData()
    }
    
    //MARK:- Other Method
    //MARK:-
}

extension Sports : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSports.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellInterest", for: indexPath) as! CellCategory
     //   cell.imgIcon.image = arrSports[indexPath.row].image
        cell.lblName.text = arrSports[indexPath.row].name
        cell.lblName.textColor = arrSports[indexPath.row].isSelected ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        cell.viewBorder.backgroundColor = arrSports[indexPath.row].isSelected ? #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width ) / 2,
                      height: (collectionView.bounds.height) / 8)
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard isMultiSelection else {
            delegate?.didSelect(sport: arrSports[indexPath.row])
            navigationController?.popViewController(animated: true)
            return
        }
        
        if selectedInterest.contains(arrSports[indexPath.row].id) {
            arrSports[indexPath.row].isSelected = false
            if let index = selectedInterest.index(of: arrSports[indexPath.row].id) {
                selectedInterest.remove(at: index)
            }
        }
        else {
            guard selectedInterest.count < 3 else {
                Utils.shared.alert(on: self, message: ERROR_MAX_SPORT, affirmButton: "Ok", cancelButton: nil)  ; return
            }
            arrSports[indexPath.row].isSelected = true
            selectedInterest.append(arrSports[indexPath.row].id)
        }
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionFooter:
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                         withReuseIdentifier: "FooterNutrition",
                                                                         for: indexPath) as! FooterNutrition
            
            footer.btnNutrition.addTarget(self, action: #selector(clickNutrition(_:)), for: .touchUpInside)
            footer.lblNutrition.textColor = selectedInterest.contains(NUTRITION) ?  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)  :  #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
            footer.lblNutrition.backgroundColor = selectedInterest.contains(NUTRITION) ? #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1) :  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

            //5 cornarRadius
            footer.lblNutrition.setBorder(cornerRadius: 0, borderWidth: 1, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
            
            return footer
            
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: HEIGHT_FOOTER)
    }
    
}

class FooterNutrition : UICollectionReusableView {
    @IBOutlet var btnNutrition : UIButton!
    @IBOutlet var icnNutrition : UIImageView!
    @IBOutlet var lblNutrition : UILabel!
}
