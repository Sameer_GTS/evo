//
//  Signup.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/9/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Spring
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Mixpanel

class Signup: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfFirstName: UITextField!
    @IBOutlet var tfLastName: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var btnSignup: UIButton!
    
    @IBOutlet var alertEmail: SpringImageView! {
        didSet{
            Utils.shared.animateFadeOut(alertEmail,0.0)
        }
    }
    @IBOutlet var alertFirstName: SpringImageView! {
        didSet{
            Utils.shared.animateFadeOut(alertFirstName,0.0)
        }
    }
    @IBOutlet var alertLastName: SpringImageView! {
        didSet{
            Utils.shared.animateFadeOut(alertLastName,0.0)
        }
    }
    @IBOutlet var alertPassword: SpringImageView! {
        didSet{
            Utils.shared.animateFadeOut(alertPassword,0.0)
        }
    }
    
    @IBOutlet var viewLoginShadow: UIView!{
        didSet {
            viewLoginShadow.dropShadow(radius: 10)
        }
    }
    @IBOutlet var viewShadow: UIView! {
        didSet {
            viewShadow.dropShadow(radius: 10)
        }
    }
    @IBOutlet var btnFB: UIButton!{
        didSet {
            btnFB.setBorder(cornerRadius: btnFB.bounds.height/2, borderWidth: 0, borderColor: .clear)
        }
    }
    @IBOutlet var btnGoogle: UIButton!{
        didSet {
            btnGoogle.setBorder(cornerRadius: btnGoogle.bounds.height/2, borderWidth: 0, borderColor: .clear)
        }
    }
    
    ///---- Get email
    
    @IBOutlet var viewEmail: SpringView! {
        didSet {
            viewEmail.animation = Spring.AnimationPreset.FadeOut.rawValue
            viewEmail.duration = 0
            viewEmail.curve = Spring.AnimationCurve.Linear.rawValue
            viewEmail.animate()
        }
    }
    @IBOutlet var viewEmailContainter: SpringView!{
        didSet {
            viewEmailContainter.setBorder(cornerRadius: 10, borderWidth: 0, borderColor: .clear)
            viewEmailContainter.animation = Spring.AnimationPreset.FadeOut.rawValue
            viewEmailContainter.duration = 0
            viewEmailContainter.curve = Spring.AnimationCurve.Linear.rawValue
            viewEmailContainter.animate()
        }
    }
    @IBOutlet var tfAddEmail: UITextField!
    @IBOutlet var btnSubmit: UIButton! {
        didSet {
            btnSubmit.dropShadow(radius: 10)
        }
    }
    
    @IBOutlet var alertEmailAdd: SpringImageView! {
        didSet {
            Utils.shared.animateFadeOut(alertEmailAdd, 0.0)
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().clientID = KEY_GOOGLE
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btnSignup.setBorder(cornerRadius: btnSignup.bounds.height/2, borderWidth: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        btnSignup.setBorder(cornerRadius: btnSignup.bounds.height/2, borderWidth: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction Method
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickFacebook(_ sender: UIButton) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile"], from: self) { (result, error) in
            if (AccessToken.current != nil) {
                GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, response, error) -> Void in
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05, execute: {
                        if (error == nil){
                            let dict = response as! NSDictionary
                            let name = dict["name"] as? String ?? ""
                            let socialId =  String(describing: dict["id"]!)
                            let image =  ((dict["picture"] as! NSDictionary )["data"] as! NSDictionary)["url"] as? String ?? ""
                            let email = dict["email"] as? String ?? ""
                            
                            WebConnect.loginWithFB(email: email, name: name, profileId: socialId, imageURL: image, completion: { (status, message) in
                                if status {
                                    
                                    if IK_USER.email.isEmpty() {
                                        self.showEmail()
                                    }
                                    else {
                                        self.forwordView()
                                    }
                                }
                                else {
                                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                }
                            })
                        }
                    })
                })
            }
        }
    }
    
    @IBAction func clickGoogle(_ sender: UIButton) {
        
        if GIDSignIn.sharedInstance().currentUser == nil  {
            if GIDSignIn.sharedInstance().hasAuthInKeychain() {
                GIDSignIn.sharedInstance().signInSilently()
            } else {
                GIDSignIn.sharedInstance().signIn()
            }
        } else {
            
            let id = GIDSignIn.sharedInstance().currentUser.userID ?? ""
            let name = GIDSignIn.sharedInstance().currentUser.profile.name ?? ""
            let email = GIDSignIn.sharedInstance().currentUser.profile.email ?? ""
            let picture = GIDSignIn.sharedInstance().currentUser.profile.imageURL(withDimension: 32).absoluteString
            
            WebConnect.loginWithGoogle(email: email, name: name, profileId: id, imageURL: picture) { (status, message) in
                if status {
                    if IK_USER.email.isEmpty() {
                        self.showEmail()
                    }
                    else {
                        self.forwordView()
                    }
                }
                else {
                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                }
            }
        }
    }
    
    @IBAction func clickSignup(_ sender: UIButton) {
        guard validation() else {
            return
        }
        self.view.endEditing(true)
        
        WebConnect.register(firstName: tfFirstName.text!.trimString,
                            lastName: tfLastName.text!.trimString,
                            email: tfEmail.text!.trimString,
                            password: tfPassword.text!.trimString) { (status, message) in
                                if status {
                                    /*
                                    Mixpanel.sharedInstance()?.track("Sign up page", properties:[ /*"First name":IK_USER.first_name,
                                        "last name":IK_USER.last_name,
                                        "email":IK_USER.email,*/
                                        "registration":"Regular"])
                                    */
                                    
                                    let eventParams = ["registration":"Regular"] as [String : Any]
                                    Utils.shared.logEventWithFlurry(EventName: "Sign_up_page", EventParam: eventParams)
                                    
                                    self.forwordView()
                                }
                                else {
                                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                }
        }
        
    }
    
    @IBAction func clickTerms(_ sender: UIButton) {
        let term = self.storyboard?.instantiateViewController(withIdentifier: "Terms") as! Terms
        term.url = WebConnect.TermsNCondition
        term.strTitle = "Brukerbetingelsene"
        navigationController?.present(term, animated: true, completion: {
        })

        //navigationController?.pushViewController(term, animated: true)
    }
    
    @IBAction func clickPrivacy(_ sender: UIButton) {
        let policy = self.storyboard?.instantiateViewController(withIdentifier: "Terms") as! Terms
        policy.url = WebConnect.PrivacyPolicy
        policy.strTitle = "Personvernpolicy"
        navigationController?.present(policy, animated: true, completion: {
        })
        //navigationController?.pushViewController(policy, animated: true)
    }
    
    @IBAction func clickShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        tfPassword.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func clickSubmitEmail(_ sender: UIButton) {
        guard tfAddEmail.text!.isEmail() else  {
            Utils.shared.animatePop(alertEmailAdd)
            return
        }
        
        WebConnect.updateEmail(email: tfAddEmail.text!.trimString) { (status, message) in
            if status {
                self.forwordView()
                self.hideMail()
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
    }
    
    @IBAction func clickCancelEmail(_ sender: UIButton) {
        IK_DEFAULTS.removeObject(forKey: USER_DETAIL)
        IK_DEFAULTS.synchronize()
        hideMail()
    }
    
    //MARK:- Other Method
    //MARK:-
    
    fileprivate func forwordView() {
        
        if IK_USER.userType == .customer || IK_USER.userType == .requestedForAcer {
            if IK_USER.sportId.count == 0 && IK_USER.nutritionist == "N"  {
                let sport = self.storyboard?.instantiateViewController(withIdentifier: "Sports") as! Sports
                self.navigationController?.pushViewController(sport, animated: true)
            }
            else if IK_USER.gym == 0 {
                let gym = self.storyboard?.instantiateViewController(withIdentifier: "GymList") as! GymList
                self.navigationController?.pushViewController(gym, animated: true)
            }
            else if IK_USER.fitness == 0 {
                let fitness = self.storyboard?.instantiateViewController(withIdentifier: "FitnessLevel") as! FitnessLevel
                self.navigationController?.pushViewController(fitness, animated: true)
            }
            else if IK_USER.ageGroup == 0 {
                let age = self.storyboard?.instantiateViewController(withIdentifier: "AgeGroup") as! AgeGroup
                self.navigationController?.pushViewController(age, animated: true)
            }
            else if IK_USER.profile_image.isEmpty() {
                let picture = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePicture") as! ProfilePicture
                self.navigationController?.pushViewController(picture, animated: true)
            }
            else {
                
                /*
                Mixpanel.sharedInstance()?.createAlias(IK_USER.profile_id,
                                                       forDistinctID: Mixpanel.sharedInstance()!.distinctId,
                                                       usePeople: true)
                Mixpanel.sharedInstance()?.identify(IK_USER.profile_id, usePeople: true)
                
                Mixpanel.sharedInstance()?.people.set([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
                                                        "First name":IK_USER.first_name,
                                                        "last name":IK_USER.last_name,*/
                                                        "Profile":IK_USER.profile_image,
                                                        /*"Email":IK_USER.email,
                                                        "$email":IK_USER.email,*/
                                                        "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                                        "Fitness Level":arrFitness[IK_USER.fitness],
                                                        "Age Group":arrAge[IK_USER.ageGroup]])
                
                Mixpanel.sharedInstance()?.registerSuperProperties([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
                                                                     "First Name":IK_USER.first_name,
                                                                     "Last Name":IK_USER.last_name,
                                                                     "Email":IK_USER.email,*/
                                                                 "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                                                     "Fitness Level":arrFitness[IK_USER.fitness],
                                                                     "Age Group":arrAge[IK_USER.ageGroup]])
                */
                
                ChatManager.shared.connect(userId:IK_USER.profile_id, accessToken: IK_USER.sendbirdToken)
                
                IK_DELEGATE.TabBarConfiguration(true)
                /*
                let home = self.storyboard?.instantiateViewController(withIdentifier: "AceList") as! AceList
                let navigation = UINavigationController(rootViewController:home )
                navigation.isNavigationBarHidden = true
                
                UIView.transition(from: IK_DELEGATE.window!.rootViewController!.view,
                                  to: navigation.view,
                                  duration: 0.45,
                                  options: .transitionCrossDissolve,
                                  completion: { (_) in
                                    IK_DELEGATE.window!.rootViewController = navigation
                })*/
            }
        }
        else if IK_USER.userType == .ace {
            
            /*
            Mixpanel.sharedInstance()?.identify(IK_USER.profile_id, usePeople: true)
            Mixpanel.sharedInstance()?.people.set([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
                                                    "First name":IK_USER.first_name,
                                                    "last name":IK_USER.last_name,*/
                                                    "Profile":IK_USER.profile_image,
                                                    /*"Email":IK_USER.email,
                                                    "$email":IK_USER.email,*/
                                                    "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                                    "Fitness Level":arrFitness[IK_USER.fitness],
                                                    "Age Group":arrAge[IK_USER.ageGroup]])
            
            Mixpanel.sharedInstance()?.registerSuperProperties([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
                                                                 "First Name":IK_USER.first_name,
                                                                 "Last Name":IK_USER.last_name,
                                                                 "Email":IK_USER.email,*/
                                                              "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                                                 "Fitness Level":arrFitness[IK_USER.fitness],
                                                                 "Age Group":arrAge[IK_USER.ageGroup]])
            */
            
            ChatManager.shared.connect(userId:IK_USER.profile_id, accessToken: IK_USER.sendbirdToken)
            
            IK_DELEGATE.TabBarConfiguration(true)

            /*
            let home = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! Home
            let navigation = UINavigationController(rootViewController:home )
            navigation.isNavigationBarHidden = true
            
            UIView.transition(from: IK_DELEGATE.window!.rootViewController!.view,
                              to: navigation.view,
                              duration: 0.45,
                              options: .transitionCrossDissolve,
                              completion: { (_) in
                                IK_DELEGATE.window!.rootViewController = navigation
            })*/
        }
    }
    
    fileprivate func hideMail() {
        
        viewEmailContainter.setBorder(cornerRadius: 10, borderWidth: 0, borderColor: .clear)
        viewEmailContainter.animation = Spring.AnimationPreset.FadeOut.rawValue
        viewEmailContainter.duration = 0.3
        viewEmailContainter.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmailContainter.animate()
        
        viewEmail.animation = Spring.AnimationPreset.FadeOut.rawValue
        viewEmail.duration = 0.3
        viewEmail.delay = 0.15
        viewEmail.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmail.animate()
    }
    
    fileprivate func showEmail() {
        
        viewEmail.animation = Spring.AnimationPreset.FadeIn.rawValue
        viewEmail.duration = 0.3
        viewEmail.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmail.animate()
        
        viewEmailContainter.animation = Spring.AnimationPreset.FadeIn.rawValue
        viewEmailContainter.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmailContainter.duration = 0.3
        viewEmailContainter.delay = 0.25
        viewEmailContainter.scaleX = 0.5
        viewEmailContainter.scaleY = 0.5
        viewEmailContainter.animate()
    }
    
    fileprivate func validation() -> Bool {
        var isValid = true
        
        if tfEmail.text!.isEmpty() || !tfEmail.text!.isEmail() {
            Utils.shared.animatePop(alertEmail)
            isValid = false
        }
        
        if tfFirstName.text!.isEmpty() {
            Utils.shared.animatePop(alertFirstName)
            isValid = false
        }
        
        if tfLastName.text!.isEmpty() {
            Utils.shared.animatePop(alertLastName)
            isValid = false
        }
        
        if tfPassword.text!.isEmpty() || tfPassword.text!.length < 6 {
            Utils.shared.animatePop(alertPassword)
            isValid = false
        }
        
        return isValid
    }
}

extension Signup : GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        let id = user.userID ?? ""
        let name = user.profile.name ?? ""
        let picture = user.profile.imageURL(withDimension: 32).absoluteString
        let email = user.profile.email ?? ""
        
        WebConnect.loginWithGoogle(email: email, name: name, profileId: id, imageURL: picture) { (status, message) in
            if status {
                if IK_USER.email.isEmpty() {
                    self.showEmail()
                }
                else {
                    self.forwordView()
                }
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
    }
    
    // MARK: - Signin UI Delegate
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- Textfield Delegate
//MARK:-

extension Signup : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfEmail {
            tfFirstName.becomeFirstResponder()
        }
        else if textField == tfFirstName {
            tfLastName.becomeFirstResponder()
        }
        else if textField == tfLastName {
            tfPassword.becomeFirstResponder()
        }
        else if textField == tfPassword {
            tfPassword.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfEmail {
            Utils.shared.animateFadeOut(alertEmail)
        }
        else if textField == tfFirstName {
            Utils.shared.animateFadeOut(alertFirstName)
        }
        else if textField == tfLastName {
            Utils.shared.animateFadeOut(alertLastName)
        }
        else if textField == tfPassword {
            Utils.shared.animateFadeOut(alertPassword)
        }
        else {
            Utils.shared.animateFadeOut(alertEmailAdd)
        }
    }
    
}

