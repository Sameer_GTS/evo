//
//  LockGym_List.swift
//  Ikseed
//
//  Created by Chanchal Warde on 11/22/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.


import UIKit

class LockGym_List: UIViewController {

    
    //MARK:- Constants
    //MARK:-
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var arrLockGymLocal : [LockGymModel] = []
    var selectedIndex = -1
    var isBack = false
    var isHandler = false
    var showAll = false
    
    var selectedGymId = -1

    
    var onSelect : (LockGymModel) -> Void = { _ in }

    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var tblAddress: UITableView!
    
    //MARK:- View Methods
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()

        arrLockGymLocal =  arrLockGyms
        tblAddress.reloadData()
        
        WebConnect.getLockGymsList() { (status, arrList, message) in
            self.arrLockGymLocal = arrList
            self.setSelectedGym()
        }
        
        self.setSelectedGym()
    }
    
    // jitendra
    func setSelectedGym() {
        
        tblAddress.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectNext(_ sender: UIButton) {
        
        guard !isHandler else {
            onSelect(arrLockGymLocal[selectedIndex]);
            _ = self.navigationController?.popViewController(animated: true)
            return
        }
        
        guard selectedIndex >= 0 else {
            return
        }
        
    }
}

extension LockGym_List : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLockGymLocal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellGym") as! CellGym
        
        let tmpObj = arrLockGymLocal[indexPath.row]
        cell.lblName.text = tmpObj.name
        cell.lblAddress.text = tmpObj.address
        //cell.iconSelected.isHidden = selectedAddrss != indexPath.row
        cell.iconSelected.isHidden = selectedGymId != tmpObj.id
        
        cell.iconSelected.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        selectedGymId = arrLockGymLocal[indexPath.row].id
        
        tableView.reloadData()
        selectNext(btnNext)
    }
}

