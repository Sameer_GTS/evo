//
//  AddCard.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/27/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.


import UIKit
import Stripe
import Mixpanel

class AddCard: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var tfCardDetail: STPPaymentCardTextField!
    
    //MARK:- Variable
    //MARK:-
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickSubmit(_ sender: UIButton) {
        
        guard tfCardDetail.isValid else {return}
        self.view.endEditing(true)
        
        let cardParams = STPCardParams()
        cardParams.number = tfCardDetail.cardNumber
        cardParams.expMonth = tfCardDetail.expirationMonth
        cardParams.expYear = tfCardDetail.expirationYear
        cardParams.cvc = tfCardDetail.cvc
        
        FUProgressView.showProgress()
        STPAPIClient.shared().publishableKey = KEY_STRIPE
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            FUProgressView.hideProgress()
            guard let token = token, error == nil else {
                Utils.shared.alert(on: self, message: ERROR_INVALID_CARD, affirmButton: "Ok", cancelButton: nil)  ; return
                // Present error to user...
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                WebConnect.addCardDetail(token: token.tokenId, completion: { (status, message) in
                    guard status else {
                        Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil); return
                    }
                    
                    //Mixpanel.sharedInstance()?.track("Adds payment information", properties:["session":track.start.timeIntervalSince1970])
                    
                    //let eventParams = ["session":track.start.timeIntervalSince1970] as [String : Any]
                    Utils.shared.logEventWithFlurry(EventName: "adds_payment_information", EventParam: [:])
                    
                    self.navigationController?.popViewController(animated: true)
                })
            })
        }
    }
    
    //MARK:- Other Method
    //MARK:-
    
}


