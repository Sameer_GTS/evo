//
//  CardDetail.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/27/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit

class CardDetail: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var lblNumber: UILabel!
    @IBOutlet var lblExpiry: UILabel!
    @IBOutlet var btnPrimary: UIButton!
    
    //MARK:- Variable
    //MARK:-
    var card : Cards!
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showData()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickPrimary(_ sender: UIButton) {
        WebConnect.makePrimary(card_token: card.stToken) { (status, message) in
            guard status else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                return
            }
            
            self.btnPrimary.isSelected = true
            self.btnPrimary.setTitle("Primærkort", for: .normal)
            self.btnPrimary.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func clickRemoveCard(_ sender: UIButton) {
        
        Utils.shared.alert(on: self, title: "Bekrefte", message: CONFIRM_DELETE_CARD, affirmButton: "Ok", cancelButton: "Avbryt") { (index) in
            guard index == 0 else { return }
            
            WebConnect.removeCard(card_token: self.card.stToken, completion: { (status, message) in
                guard status else {
                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                    return
                }
                self.navigationController?.popViewController(animated: true)
            })
        }
        
    }
    @IBAction func clickBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Other
    //MARK:-
    
    fileprivate func showData() {
        lblNumber.text = card.number + " (" + card.country + ")"
        lblExpiry.text = card.expMonth + "/" + card.expYear
        
        guard card.isPrimary else {return}
        btnPrimary.isSelected = card.isPrimary
        
        btnPrimary.setTitle("Primærkort", for: .normal)
        btnPrimary.isUserInteractionEnabled = false
    }
    
}
