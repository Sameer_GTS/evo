//
//  CardList.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/27/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit

class CardList: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var tblPayment: UITableView!
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var arrCard : [Cards] = []
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.arrCard = []
        WebConnect.getCardList { (status, arr, message) in
            guard status else {
                self.arrCard.append(Cards())
                self.tblPayment.reloadData()
                return
            }
            
            self.arrCard = arr
            
            if arr.count < 2 {
                self.arrCard.append(Cards())
            }
            self.tblPayment.reloadData()
        }
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Other
    //MARK:-
}

//MARK:- Tableview Delegate || Datasource
//MARK:-

extension CardList : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCard.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellCard", for: indexPath) as! CellCard
        
        let obj = arrCard[indexPath.row]
        cell.cardDetail.text = obj.type == .none ?   obj.number  :  obj.number + " (" + obj.country + ") "
        cell.icnPrimary.isHidden = !obj.isPrimary
        cell.icnType.image = CardImages[obj.type.rawValue]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrCard[indexPath.row].type == .none {
            
            let add = self.storyboard?.instantiateViewController(withIdentifier: "AddCard") as! AddCard
            navigationController?.pushViewController(add, animated: true)
        }
        else {
            let detail = self.storyboard?.instantiateViewController(withIdentifier: "CardDetail") as! CardDetail
            detail.card = arrCard[indexPath.row]
            navigationController?.pushViewController(detail, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.bounds.width, height: 5)))
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
}

class CellCard : UITableViewCell {
    
    @IBOutlet var icnType: UIImageView!
    @IBOutlet var cardDetail: UILabel!
    @IBOutlet var icnPrimary: UIImageView!
    
    override func awakeFromNib() {
        
    }
}
