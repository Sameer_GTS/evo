//
//  DaysSelection.swift
//  Ikseed
//
//  Created by Chanchal Warde on 6/6/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Mixpanel

class DaysSelection: UIViewController {

    //MARK:- Constant
    //MARK:-
    
    let wait = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WaitForPackage") as! WaitForPackage
    
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var viewTwoDay: UIView!
    @IBOutlet var viewThreeDay: UIView!
    @IBOutlet var viewFiveDay: UIView!
    
    @IBOutlet var btnTwoDays: UIButton! {
        didSet {
            btnTwoDays.tag = 1
        }
    }
    @IBOutlet var btnThreeDays: UIButton!{
        didSet {
            btnThreeDays.tag = 1
        }
    }
    @IBOutlet var btnFiveDays: UIButton!{
        didSet {
            btnFiveDays.tag = 1
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var arrDays : [(id:Int, title:String)] = []

    //MARK:- View Method
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDays()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction Method
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickDays(_ sender: UIButton) {
        
        if arrDays.count > 2 {
            viewTwoDay.backgroundColor  = sender.tag == arrDays[0].id ? #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            viewThreeDay.backgroundColor  = sender.tag == arrDays[1].id ? #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            viewFiveDay.backgroundColor  = sender.tag == arrDays[2].id ? #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
            //Mixpanel.sharedInstance()?.track("Days in week selected", properties:["Days":arrDays[sender.tag-1].title])
            
            let eventParams = ["days":arrDays[sender.tag-1].title] as [String : Any]
            Utils.shared.logEventWithFlurry(EventName: "days_in_week_selected", EventParam: eventParams)
        }
        
        MarathonScheduleDetail.days = sender.tag
        
        self.present(wait, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                self.wait.dismiss(animated: true, completion: {
                    DispatchQueue.main.async {
                        let plan = self.storyboard?.instantiateViewController(withIdentifier: "Packages") as! Packages
                        self.navigationController?.pushViewController(plan, animated: true)
                    }
                })
            })
        }
    }
    
    //MARK:- Other Method
    //MARK:-
    
    
    fileprivate func getDays() {
        
        WebConnect.getWeekDays { (status, arrDays) in
            guard status else { self.getDays(); return }
            
            guard arrDays.count == 3 else {
                return
            }
            self.arrDays = arrDays
            self.btnTwoDays.tag = arrDays[0].id
            self.btnThreeDays.tag = arrDays[1].id
            self.btnFiveDays.tag = arrDays[2].id
        }
    }
}
