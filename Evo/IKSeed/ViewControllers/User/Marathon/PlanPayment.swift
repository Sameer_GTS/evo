//
//  PlanPayment.swift
//  Ikseed
//
//  Created by Chanchal Warde on 6/6/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.


import UIKit
import Mixpanel

class PlanPayment: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var btnPay: UIButton!
    @IBOutlet var tfCode: UITextField!
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var isCardForword = false
    fileprivate var cardCount = 0
    
    //MARK:- View Methods
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        WebConnect.getCardList(isBackground: true) { (_, arrCard, _) in
            self.cardCount = arrCard.count
            if self.isCardForword {
                self.clickPay(self.btnPay)
            }
        }
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func clickPay(_ sender: UIButton) {
        
        guard self.cardCount > 0 else {
            if self.isCardForword {
                self.isCardForword = false
                Utils.shared.alert(on: self, message: ERROR_NO_CARD, affirmButton: "Ok", cancelButton: nil)
            }
            else {
                self.isCardForword = true
                let card = self.storyboard?.instantiateViewController(withIdentifier: "AddCard") as! AddCard
                self.navigationController?.pushViewController(card, animated: true)
            }
            return
        }
        
        WebConnect.purchasePlan(type: MarathonScheduleDetail.type,
                                day: MarathonScheduleDetail.days,
                                plan: MarathonScheduleDetail.plan,
                                code: tfCode.text?.trimString) { (status, message) in
                                    
                                    guard status else {
                                        Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                        return
                                    }
                                    
                                    IK_DEFAULTS.set(MarathonScheduleDetail.plan, forKey: MARATHON_PLAN)
                                    IK_DEFAULTS.synchronize()
                                    
                                    
                                    //Mixpanel.sharedInstance()?.track("Program purchased", properties:nil)
                                    
                                    
                                    Utils.shared.logEventWithFlurry(EventName: "program_purchased", EventParam: [:])
                                    
                                    let schedule = self.storyboard?.instantiateViewController(withIdentifier: "Schedule") as! Schedule
                                    self.navigationController?.pushViewController(schedule, animated: true)
        }
    }
}
