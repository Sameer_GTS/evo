//
//  Schedule.swift
//  Ikseed
//
//  Created by Chanchal Warde on 6/6/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Mixpanel

class Schedule: UIViewController {

    //MARK:- Constant
    //MARK:-
    
    fileprivate let refresh = UIRefreshControl()

    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var tblSchedule: UITableView!
    @IBOutlet var btnTrainer: UIButton!
    @IBOutlet var lblTitle: UILabel!
    
    //MARK:- Variable
    //MARK:-
    
    var arrSchedule : [WeekSchedule] = [ ]
    
    //MARK:- View Methods
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSchedule()
        
        refresh.attributedTitle = "Oppdater".changeColor(#colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        refresh.tintColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        refresh.addTarget(self, action: #selector(getSchedule) , for: UIControl.Event.valueChanged)
        tblSchedule.addSubview(refresh)
        
        guard let package = IK_DEFAULTS.object(forKey: MARATHON_PLAN) as? Int else {return}
        btnTrainer.isHidden = package != 3
        
        //Mixpanel.sharedInstance()?.track("See weekely program", properties:nil)

        Utils.shared.logEventWithFlurry(EventName: "see_weekely_program", EventParam: [:])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func clickTrainer(_ sender: UIButton) {
        let list = self.storyboard?.instantiateViewController(withIdentifier: "AvailableList") as! AvailableList
        list.shouldLoadList = true
        list.isBack = true
        self.navigationController?.pushViewController(list, animated: true)
    }
    
    //MARK:- Cell Action
    //MARK:-
    
    @IBAction func clickExpand(_ sender: UIButton) {
        arrSchedule[sender.tag].expanded = !arrSchedule[sender.tag].expanded
        tblSchedule.reloadSections(IndexSet(integer: sender.tag), with:.none)
    }
    
    //MARK:- Other
    //MARK:-
    
    @objc fileprivate func getSchedule() {
        WebConnect.getWeeklySchedule { (status, arrSchedule) in
            self.refresh.endRefreshing()
            guard status else { return }
           self.arrSchedule = arrSchedule
            self.tblSchedule.reloadData()
            self.lblTitle.text = arrSchedule[0].title
        }
    }
    
}

//MARK:- Tableview Delegate || Datasource
//MARK:-

extension Schedule : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSchedule.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section == 0 else {
            return arrSchedule[section].expanded ? arrSchedule[section].days.count + 1 : 0
        }
        return arrSchedule[section].days.count + 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableCell(withIdentifier: "HeaderWeek") as! HeaderWeek
        view.btnAction.tag = section
        view.btnAction.isHidden = section == 0
        view.btnAction.isSelected = arrSchedule[section].expanded
        view.lblTitle.text = arrSchedule[section].title
        view.btnAction.addTarget(self, action: #selector(clickExpand), for: .touchUpInside)
        return view.contentView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != arrSchedule[indexPath.section].days.count  else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FooterComment", for: indexPath) as! FooterComment
            cell.lblComment.text = arrSchedule[indexPath.section].comment
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellSchedule") as! CellSchedule
        cell.viewDay.isHidden = arrSchedule[indexPath.section].days[indexPath.row].day == 0
        cell.lblDay.text = String(describing: arrSchedule[indexPath.section].days[indexPath.row].day)
        cell.lblDescription.text = arrSchedule[indexPath.section].days[indexPath.row].description
        cell.lblTitle.text = arrSchedule[indexPath.section].days[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  section == 0 ? 0 : 55
    }
}

//MARK:- Tableview Cells
//MARK:-

class HeaderWeek : UITableViewCell {
    
    @IBOutlet var btnAction: UIButton!
    @IBOutlet var lblTitle: UILabel!
    override func awakeFromNib() {
    }
}

class CellSchedule : UITableViewCell {
    @IBOutlet var lblDay: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var viewDay: UIView!
    
    override func awakeFromNib() {
    }
}

class FooterComment : UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblComment: UILabel!
    
    override func awakeFromNib() {
    }
}
