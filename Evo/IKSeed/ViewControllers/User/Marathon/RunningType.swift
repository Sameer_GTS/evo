//
//  RunningType.swift
//  Ikseed
//
//  Created by Chanchal Warde on 6/6/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Mixpanel

class RunningType: UIViewController {

    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var viewTen: UIView!
    @IBOutlet var viewHalfMarathon: UIView!
    @IBOutlet var viewMarathon: UIView!
    
    @IBOutlet var btnTen: UIButton! {
        didSet {
            btnTen.tag = 1
        }
    }
    @IBOutlet var btnHalfMarathon: UIButton!{
        didSet {
            btnHalfMarathon.tag = 2
        }
    }
    @IBOutlet var btnMarathon: UIButton!{
        didSet {
            btnMarathon.tag = 3
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var arrTypes : [(id:Int, title:String)] = []
    
    //MARK:- View Method
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getType()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickType(_ sender: UIButton) {
        if arrTypes.count > 2 {
            viewTen.backgroundColor  = sender.tag == arrTypes[0].id ? #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            viewHalfMarathon.backgroundColor  = sender.tag == arrTypes[1].id ? #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            viewMarathon.backgroundColor  = sender.tag == arrTypes[2].id ? #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }

        /*
        Mixpanel.sharedInstance()?.track("Marathon Type Selected", properties:["Selected Type":arrTypes[sender.tag-1].title])
        */
        
        let eventParams = ["Selected Type":arrTypes[sender.tag-1].title] as [String : Any]
        Utils.shared.logEventWithFlurry(EventName: "Marathon_Type_Selected", EventParam: eventParams)

        MarathonScheduleDetail.type = sender.tag
  
        let activity = self.storyboard?.instantiateViewController(withIdentifier: "CurrentActivity") as! CurrentActivity
        self.navigationController?.pushViewController(activity, animated: true)
        
    }
    
    //MARK:- Other
    //MARK:-
    
    
    fileprivate func getType() {
        
        WebConnect.getEventType { (status, arrType) in
            guard status else { self.getType(); return }
            
            guard arrType.count == 3 else {
                return
            }
            self.arrTypes = arrType
            self.btnTen.tag = arrType[0].id
            self.btnHalfMarathon.tag = arrType[1].id
            self.btnMarathon.tag = arrType[2].id
        }
    }
}
