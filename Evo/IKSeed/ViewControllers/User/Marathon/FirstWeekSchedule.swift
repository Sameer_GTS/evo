//
//  FirstWeekSchedule.swift
//  Ikseed
//
//  Created by Chanchal Warde on 6/6/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Mixpanel

class FirstWeekSchedule: UIViewController {

    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var viewShadow: UIView! {
        didSet {
            viewShadow.dropShadow(0.15, radius: 10 , color: #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1))
        }
    }
    @IBOutlet var btnForword: UIButton! {
        didSet {
            btnForword.dropShadow(0.15, radius: 10 , color:  #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        }
    }
    @IBOutlet var tblSchedule: UITableView!
    
    //MARK:- Variable
    //MARK:-
    
   fileprivate var arrSchedule : [WeekSchedule] = [ ]

    //MARK:- View Method
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDemoData()
        
        //Mixpanel.sharedInstance()?.track("Free first week schedule", properties:nil)
        
        Utils.shared.logEventWithFlurry(EventName: "free_first_week_schedule", EventParam: [:])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }

    //MARK:- Other
    //MARK:-
    
    fileprivate func getDemoData() {
        WebConnect.getDemoSchedule(type:MarathonScheduleDetail.type , weekInDay: MarathonScheduleDetail.days) { (status, arrSchedule) in
            guard status else {
                return
            }
            self.lblTitle.text = arrSchedule[0].title
            self.arrSchedule = arrSchedule
            self.tblSchedule.reloadData()
        }
    }

}

//MARK:- TableView Delegate || DataSource
//MARK:-

extension FirstWeekSchedule : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSchedule.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section == 0 else {
            return arrSchedule[section].expanded ? arrSchedule[section].days.count + 1 : 0
        }
        return arrSchedule[section].days.count + 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != arrSchedule[indexPath.section].days.count  else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FooterCommentFirst", for: indexPath) as! FooterComment
            cell.lblComment.text = arrSchedule[indexPath.section].comment
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellScheduleFirst") as! CellSchedule
        cell.viewDay.isHidden = arrSchedule[indexPath.section].days[indexPath.row].day == 0 
        cell.lblDay.text = String(describing: arrSchedule[indexPath.section].days[indexPath.row].day)
        cell.lblDescription.text = arrSchedule[indexPath.section].days[indexPath.row].description
        cell.lblTitle.text = arrSchedule[indexPath.section].days[indexPath.row].title
        return cell
    }

    
}

