//
//  CurrentActivity.swift
//  Ikseed
//
//  Created by Chanchal Warde on 6/6/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.


import UIKit
import Mixpanel

class CurrentActivity: UIViewController {

    //MARK:- IBoutlet
    //MARK:-
    
    fileprivate let ROW_COUNT : CGFloat = 70
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var tblActivity: UITableView!
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var arrActivity : [Acitvity] = []
    
    //MARK:- View Method
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getActivityList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction Method
    //MARK:-
    
    @IBAction func clickNext(_ sender: UIButton) {
        
        var arrData : [[String:Int]] = []
        
        for obj in arrActivity {
            arrData.append([obj.key:obj.total])
        }
        
        WebConnect.saveCurrentActivity(activity: arrData) { (status, message) in
            
            guard status else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil) ; return
            }
            
            //Mixpanel.sharedInstance()?.track("Current Schedule Selected", properties:nil)
            
            Utils.shared.logEventWithFlurry(EventName: "current_schedule_selected", EventParam: [:])
            
            let day = self.storyboard?.instantiateViewController(withIdentifier: "DaysSelection") as! DaysSelection
            self.navigationController?.pushViewController(day, animated: true)
        }
    }
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Cell Method
    //MARK:-
    
    @IBAction func clickIncrease(_ sender: UIButton) {
        guard arrActivity.count > sender.tag  else {return}
        arrActivity[sender.tag].total +=  1
        tblActivity.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
    }
    
    @IBAction func clickDecrease(_ sender: UIButton) {
        guard arrActivity.count > sender.tag &&  arrActivity[sender.tag].total > 0 else {return}
        arrActivity[sender.tag].total -=  1
        tblActivity.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
    }
    
    //MARK:- Other Method
    //MARK:-
    
    fileprivate func getActivityList() {
        WebConnect.getCurrentActivityList() { status , array in
            guard status else {
                return
            }
            self.arrActivity = array
            self.tblActivity.reloadData()
        }
    }
}

//MARK:- Tableview Delegate || Datasource
//MARK:-

extension CurrentActivity : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arrActivity.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellActivity", for: indexPath) as! CellActivity
        
        cell.btnPlus.tag = indexPath.row
        cell.btnMinus.tag = indexPath.row
        
        cell.btnPlus.addTarget(self, action: #selector(clickIncrease(_:)), for: .touchUpInside)
        cell.btnMinus.addTarget(self, action: #selector(clickDecrease(_:)), for: .touchUpInside)
        
        cell.lblTitle.text = arrActivity[indexPath.row].name
        cell.lblCount.text = "\(arrActivity[indexPath.row].total)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ROW_COUNT
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
}

//MARK:- Tableview Cell
//MARK:-

class CellActivity : UITableViewCell {
    
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblCount : UILabel!
    @IBOutlet var btnPlus : UIButton!
    @IBOutlet var btnMinus : UIButton!
    
    override func awakeFromNib() {
        
    }
}
