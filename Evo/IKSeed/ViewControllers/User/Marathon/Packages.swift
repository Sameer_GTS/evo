//
//  Packages.swift
//  Ikseed
//
//  Created by Chanchal Warde on 6/6/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Mixpanel
class Packages: UIViewController {

    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var lblPrice1: UILabel!
    @IBOutlet var lblPrice2: UILabel!
    @IBOutlet var lblPrice3: UILabel!
    
    //MARK:- Variable
    //MARK:-

    fileprivate var arrPlan :  [(id:Int, price:Int)] = []
    
    //MARK:- View Methods
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPlan()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickPersonalTrainer(_ sender: UIButton) {
        MarathonScheduleDetail.plan = arrPlan[0].id
        
        //Mixpanel.sharedInstance()?.track("Program Selected", properties:["Program":"Personlig Trener"])
        
        let eventParams = ["program":"Personlig Trener"] as [String : Any]
        Utils.shared.logEventWithFlurry(EventName: "program_selected", EventParam: eventParams)
        
        
        WebConnect.purchasePlan(type: MarathonScheduleDetail.type,
                                day: MarathonScheduleDetail.days,
                                plan: MarathonScheduleDetail.plan,
                                code: nil) { (status, message) in
                                    
                                    guard status else {
                                        Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                        return
                                    }
                                    
                                    //Mixpanel.sharedInstance()?.track("Program purchased", properties:nil)
                                    
                                    Utils.shared.logEventWithFlurry(EventName: "program_purchased", EventParam: [:])
                                    
                                    IK_DEFAULTS.set(MarathonScheduleDetail.plan, forKey: MARATHON_PLAN)
                                    IK_DEFAULTS.synchronize()
                                    
                                    let list = self.storyboard?.instantiateViewController(withIdentifier: "AvailableList") as! AvailableList
                                    list.shouldLoadList = true
                                    self.navigationController?.pushViewController(list, animated: true)
        }
    }
    
    @IBAction func clickDigital(_ sender: UIButton) {
        MarathonScheduleDetail.plan = arrPlan[1].id
        //Mixpanel.sharedInstance()?.track("Program Selected", properties:["Program":"Digilalt"])
        
        
        let eventParams = ["Program":"Digilalt"] as [String : Any]
        Utils.shared.logEventWithFlurry(EventName: "Program_Selected", EventParam: eventParams)
        
        let payment = self.storyboard?.instantiateViewController(withIdentifier: "PlanPayment") as! PlanPayment
        navigationController?.pushViewController(payment, animated: true)
    }
    
    @IBAction func clickDigitalAndPersonal(_ sender: UIButton) {
        MarathonScheduleDetail.plan = arrPlan[2].id
        //Mixpanel.sharedInstance()?.track("Program Selected", properties:["Program":"Digilalt + Personlig Trener"])
        
        let eventParams = ["program":"Digilalt + Personlig Trener"] as [String : Any]
        Utils.shared.logEventWithFlurry(EventName: "program_Selected", EventParam: eventParams)

        let payment = self.storyboard?.instantiateViewController(withIdentifier: "PlanPayment") as! PlanPayment
        navigationController?.pushViewController(payment, animated: true)
    }
    
    //MARK:- Other
    //MARK:-
    
    fileprivate func getPlan() {
        
        WebConnect.getPlans { (status, arrPlan) in
            guard status else { self.getPlan(); return }
            
            guard arrPlan.count == 3 else {
                return
            }
            self.arrPlan = arrPlan
         //   self.lblPrice1.text = "kr \(arrPlan[0].price)"
         //   self.lblPrice2.text = "kr \(arrPlan[1].price)"
           // self.lblPrice3.text = "fra kr \(arrPlan[2].price)"
        }
    }

}
