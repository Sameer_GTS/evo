//
//  BlogList.swift
//  Ikseed
//
//  Created by Fahad Mohammed Firoz Khan on 20/12/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit

class BlogList: UIViewController {
    
    @IBOutlet var tblList: UITableView!
    
    var arrBlog : [Blog] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func backTapped(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

extension BlogList : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arrBlog.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlogListCell", for: indexPath) as! BlogListCell
        let obj = arrBlog[indexPath.row]
        
        cell.imgBanner.kf.setImage(with:URL(string: obj.thumb),
                                   placeholder: #imageLiteral(resourceName: "no_image"),
                                   options: nil,
                                   progressBlock: nil,
                                   completionHandler: nil)
        cell.lblHeader.text = obj.title
        cell.lblDetail.text = ""
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240 //(self.view.frame.height - 70 - 20) / 2.5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let blogDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "BlogDetail") as! BlogDetail
        blogDetailVC.blogObj = arrBlog[indexPath.row]
        navigationController?.pushViewController(blogDetailVC, animated: true)        
    }
    
}



class BlogListCell : UITableViewCell {
    @IBOutlet var imgBanner: UIImageView!
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var lblDetail: UILabel!

}
