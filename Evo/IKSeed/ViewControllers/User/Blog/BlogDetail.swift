//
//  BlogDetail.swift
//  Ikseed
//
//  Created by Fahad Mohammed Firoz Khan on 20/12/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import SafariServices

class BlogDetail: UIViewController {
    
    
    //MARK:- IBOutlet
    //MARK:-
    
    let urlToLoad = "https://evolution.evofitness.no/"
    
    @IBOutlet var webview: UIWebView!
    @IBOutlet var imgBanner: UIImageView!

    @IBOutlet var consTopHeight: NSLayoutConstraint!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var viewContent: UIView!
    
    @IBOutlet var tblDetail: UITableView!
    
    @IBOutlet var consProgressBottomPin: NSLayoutConstraint!
    
    var blogObj : Blog!

    var customizedHtml = """
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700" rel="stylesheet">
    <link href="https://evolution.evofitness.no/static/dist/styles/evostyle.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="mainContent">
    <div class="contentwrap row">
        <div class="app app-blog">
            <div class="blog-detail">
                <!-- optionally output an img tag for the 'thumbnail' from the json feed here -->
                <!--<img src="##IMAGE_URL##"/>-->
                <!-- output 'title' from the json feed here -->
                <h1>##TITLE##</h1>
                           
                <!-- output 'lead_in' from the json feed here -->
                <div class="blog-lead">
                    ##LEAD_IN##
                </div>

                <!-- output 'author name' from the json feed here -->
                <div class="blog-author-info" style="min-height:10px;">
                    <div class="auth-name">
                        ##AUTHOR_NAME##
                    </div>
                </div>

                <!-- output the 'content_html' from the json feed here -->
                <div class="blog-content">
                    ##BLOG_CONTENT##
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

"""
    
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let eventParam = ["title":blogObj.title] as [String:String]
        Utils.shared.logEventWithFlurry(EventName: "blog", EventParam: eventParam)
        //Utils.shared.startEventDurationForEvents(eventName: "blog", eventParam: eventParam)
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436,2688,1792:
                let top = UIApplication.shared.statusBarFrame.size.height
                tblDetail.contentInset = UIEdgeInsets(top: CGFloat(-Float(top)), left: 0, bottom: 0, right: 0)
                consTopHeight.constant = 70 + top
                break
            default:
                print("Unknown")
            }
        }
        
        imgBanner.kf.setImage(with:URL(string: blogObj.thumb),
                                   placeholder: #imageLiteral(resourceName: "no_image"),
                                   options: nil,
                                   progressBlock: nil,
                                   completionHandler: nil)
        
        lblTitle.text = "" //blogObj.title
        
        var htmlData = blogObj.html
        
        //print(htmlData)
        
         htmlData = htmlData.replacingOccurrences(of: "width=\"[0-9px%]+", with: "width=\"100%", options: .regularExpression, range: nil)
         htmlData = htmlData.replacingOccurrences(of: "height=\"[0-9px%]+", with: "height=\"auto", options: .regularExpression, range: nil)
         htmlData = htmlData.replacingOccurrences(of: "href=\"", with: "href=\"https://evolution.evofitness.no")
        
        //print(htmlData)
        
        customizedHtml = customizedHtml.replacingOccurrences(of: "##TITLE##", with: blogObj.title)
        customizedHtml = customizedHtml.replacingOccurrences(of: " ##LEAD_IN##", with: blogObj.leadIn)
        customizedHtml = customizedHtml.replacingOccurrences(of: "##BLOG_CONTENT##", with: blogObj.html)
        customizedHtml = customizedHtml.replacingOccurrences(of: "##AUTHOR_NAME##", with: blogObj.authorName)

        
        webview.loadHTMLString(customizedHtml, baseURL: URL(string: urlToLoad))
        
        //webview.scalesPageToFit = true
        //webview.loadHTMLString(htmlData, baseURL: nil)
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.extendedLayoutIncludesOpaqueBars = false
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //let eventParam = ["title":blogObj.title] as [String:String]
        //Utils.shared.closeEventDurationForEvents(eventName: "blog", eventParam: eventParam)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        if webview.canGoBack {
            webview.goBack()
            return
        }
        _ = navigationController?.popViewController(animated: true)
    }
    
}

extension BlogDetail : UIWebViewDelegate ,SFSafariViewControllerDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        consProgressBottomPin.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        //FUProgressView.showProgress()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        consProgressBottomPin.constant = 100
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        //FUProgressView.hideProgress()
        /*webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(CGSize.zero)
        //print(webview.frame)
        DispatchQueue.main.async {
            self.tblDetail.setTableViewHeight(lastObejct: webView)
        }*/
        
        DispatchQueue.main.async {
            var frame = self.viewContent.frame
            frame.size.height = webView.frame.origin.y + webView.scrollView.contentSize.height //+ 180
            self.viewContent.frame = frame
            self.tblDetail.contentSize = frame.size
        }
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        //print(request)
        
        if request.url?.absoluteString != urlToLoad && navigationType == .linkClicked {
            self.showLinksClicked(url: (request.url?.absoluteString)!)
            
            /*let url = URL(string: (request.url?.absoluteString)!)

            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url!)
            }*/
            
            return false
        }
        
        return true
    }
    
    func showLinksClicked(url:String) {
        let safariVC = SFSafariViewController(url: NSURL(string: url)! as URL)
        if #available(iOS 10.0, *) {
            safariVC.preferredBarTintColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        } else {
        }
        
        safariVC.delegate = self
        self.present(safariVC, animated: true, completion: nil)
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension UITableView {
    
    /// To adjust table view hieght for view added to scroll
    ///
    /// - Parameter lastObejct: last view in table view container
    func setTableViewHeight(lastObejct : UIView) {
        var height : CGFloat = 0
        var top:CGFloat
        if #available(iOS 11.0, *) {
            top = UIApplication.shared.keyWindow!.safeAreaInsets.top
        }
        else {
            top = UIApplication.shared.statusBarFrame.size.height
        }
        height += top
        height =  height + lastObejct.frame.origin.y + lastObejct.frame.height + 10
        //self.contentSize.height = height
        self.contentSize = CGSize(width: self.contentSize.width, height: height)
    }
}


