//
//  SearchAvailableAce.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/20/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Spring
import CoreLocation
import Mixpanel

import Google
import GooglePlaces

class SearchAvailableAce: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    enum PickerType {
        case date
        case time
    }
    
    enum PickerTag : Int {
        case startTime = 200
        case endTime
    }
    
    let SHOW_PICKER : CGFloat = 0
    let HIDE_PICKER : CGFloat = -300
    let person =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectPersons") as! SelectPersons
    
    let arrTime = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var btnSport: UIButton!
    @IBOutlet var btnDate: UIButton!
    @IBOutlet var btnTime: UIButton!
    @IBOutlet var btnLocation: UIButton!
    @IBOutlet var tblHelp: UITableView!
    @IBOutlet var btnDone: UIButton! {
        didSet {
            btnDone.dropShadow()
        }
    }
    
    //--- Alerts
    @IBOutlet var alertDate: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertDate,0.0)
        }
    }
    @IBOutlet var alertTime: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertTime,0.0)
        }
    }
    @IBOutlet var alertLocation: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertLocation,0.0)
        }
    }
    @IBOutlet var alertHelp: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertHelp,0.0)
        }
    }
    @IBOutlet var alertSport: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertSport,0.0)
        }
    }
    @IBOutlet var alertPeople: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertPeople,0.0)
        }
    }
    
    //--- Date Time Picker
    
    @IBOutlet var conPickerBottom: NSLayoutConstraint!
    @IBOutlet var viewPicker: SpringView! {
        didSet {
            viewPicker.animation =  Spring.AnimationPreset.FadeOut.rawValue
            viewPicker.curve = Spring.AnimationCurve.Linear.rawValue
            viewPicker.duration = 0
            viewPicker.animate()
        }
    }
    @IBOutlet var viewTime: UIView!
    @IBOutlet var viewDate: UIView!
    
    @IBOutlet var pickerDate: UIDatePicker!
    
    //    @IBOutlet var pickerStartTime: UIDatePicker!
    //    @IBOutlet var pickerEndTime: UIDatePicker!
    
    @IBOutlet var pickerStartTime: UIPickerView! {
        didSet {
            pickerStartTime.tag = PickerTag.startTime.rawValue
        }
    }
    @IBOutlet var pickerEndTime: UIPickerView!{
        didSet {
            pickerEndTime.tag = PickerTag.endTime.rawValue
        }
    }
    
    @IBOutlet var lblLocation: UILabel!
    
    //MARK:- Variable
    //MARK:-
    
    //var interest : (id:Int,name:String,isNutrition:Bool)!
    var gymCenter : (id:Int, name:String , address : String )!
    
    fileprivate var arrHelp = ["Teknikk","Program","Veiledning","Styrke","Skadeforebygging","Annet"]
    
    fileprivate var search = (date:"",
                              person:1,
                              startTime:"",
                              endTime:"",
                              center:(id:0, name:"" , address : "" ),
                              interest:(id:0,name:"",isNutrition:false),
                              helpwith:"",
                              meetingPoint:"acv")
    
    fileprivate var time = (sHour:0, eHour:0)
    fileprivate var pickerSelected : PickerType!
    
    //Temporary
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        search.interest.id = interest.id
        //        search.interest.name = interest.name
        //        search.interest.isNutrition = interest.isNutrition
        search.center = gymCenter
        
        btnLocation.setTitle(gymCenter.name, for: .normal)
        //btnSport.setTitle(search.interest.isNutrition ? "Ernæring" : interest.name , for: .normal)
        
        //Date picker
        let calendar = Calendar.current
        
        pickerDate.minimumDate =  calendar.date(byAdding: .day, value: 1, to: Date())
        
        let hour = Date().getComponent(request: .hour) + 1
        time.sHour = hour
        time.eHour = hour + 1
        pickerStartTime.selectRow( hour, inComponent: 0, animated: true)
        pickerEndTime.selectRow( hour+1 , inComponent: 0, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ =  navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickSubmit(_ sender: UIButton) {
        guard validation() else { return }
        
        //        sportId: search.interest.id,
        //        nutrition: search.interest.isNutrition ? "Y" : "N",
        
        
        
        
        /*
        search.date = Date().string(format: "yyyy-MM-dd")
        search.startTime = Date().string(format: "hh") + ":00"
        search.endTime =  Date().string(format: "hh") + ":00"
        */
        
        
        WebConnect.requestAvailability(centerId: search.center.id,
                                       date: search.date,
                                       startTime: search.startTime,
                                       endTime: search.endTime,
                                       help : search.helpwith,
                                       person:search.person) { (status, message) in
                                        guard status else {
                                            Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                            return
                                        }
                                        
                                        /*
                                        Mixpanel.sharedInstance()?.track("Requests for an ace", properties:["session":track.start.timeIntervalSince1970,
                                                                                                    "sport":arrSports.filter({$0.id == self.search.interest.id}).first?.name ?? "",
                                                                                                            "location":self.search.center.name])
                                        */
                                        
                                        let eventParams = [//"session"  : track.start.timeIntervalSince1970,
                                                           "location" : self.search.center.name] as [String : Any]
                                        Utils.shared.logEventWithFlurry(EventName: "request_for_ace", EventParam: eventParams)
                                        
                                        
                                        let wait = self.storyboard?.instantiateViewController(withIdentifier: "Waiting") as! Waiting
                                        wait.message = message
                                        self.navigationController?.pushViewController(wait, animated: true)
        }
    }
    
    @IBAction func clickPeople(_ sender: UIButton) {
        
        return
            
            person.onSelectItem = { number , strValue in
                Utils.shared.animateFadeOut(self.alertPeople, 0)
                self.search.person = number
                sender.setTitle(String(describing:number), for: .normal)
        }
        navigationController?.pushViewController(person, animated: true)
    }
    
    @IBAction func clickDate(_ sender: UIButton) {
        pickerSelected = .date
        showPicker(type:.date)
    }
    
    @IBAction func clickTime(_ sender: UIButton) {
        pickerSelected = .time
        showPicker(type:.time)
    }
    
    @IBAction func clickLocation(_ sender: UIButton) {
        
        return
        
        let gym = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GymList") as! GymList
        
        // jitendra
        for i in 0..<arrGyms.count {
            if self.search.center.name == arrGyms[i].name {
                gym.selectedAddrss = i
                gym.selectedGymId = arrGyms[i].id
                break
            }
        }
        
        gym.isHandler = true
        gym.onSelect = { obj in
            sender.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
            
            self.search.center = obj
            Utils.shared.animateFadeOut(self.alertLocation, 0)
        }
        self.navigationController?.pushViewController(gym , animated: true)
        
        //        let autocompleteController = GMSAutocompleteViewController()
        //        autocompleteController.delegate = self
        //        self.present(autocompleteController, animated: true, completion: nil)
        
        //        let picker = AddressPopup.showPicker(on: self)
        //        picker.onSaveAddress = { selected in
        //
        //            self.search.location  = arrCities.filter({$0.name == selected.city }).first?.id ?? 0
        //            self.search.address = selected.address
        //            self.search.coordinate = selected.coordinate
        //            self.search.meetingPoint = selected.meeting
        //            self.lblLocation.text = selected.address
        //            Utils.shared.animateFadeOut(self.alertLocation, 0)
        //        }
        //
        //                let picker =  ListPicker.showPicker(on: self, arrList: arrCities)
        //                picker.onSelectItem = { obj in
        //                    Utils.shared.animateFadeOut(self.alertLocation, 0)
        //
        //                    self.search.location = obj
        //                    sender.setTitle(obj.name, for: .normal)
        //                }
    }
    
    @IBAction func clickDone(_ sender: UIButton) {
        hidePicker()
    }
    
    @IBAction func clickInterest(_ sender: UIButton) {
        let interest = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Sports") as! Sports
        interest.selectedInterest = search.interest.isNutrition ?   [NUTRITION] : [search.interest.id]
        interest.isBack = true
        interest.delegate = self
        interest.isMultiSelection = false
        
        navigationController?.pushViewController(interest, animated: true)
    }
    
    //MARK:- Other
    //MARK:-
    
    fileprivate func showPicker(type:PickerType) {
        
        viewDate.isHidden = type == .time
        viewTime.isHidden = type == .date
        
        viewPicker.animation =  Spring.AnimationPreset.FadeIn.rawValue
        viewPicker.curve = Spring.AnimationCurve.Linear.rawValue
        viewPicker.duration = 0.3
        viewPicker.animateNext {
            self.conPickerBottom.constant = self.SHOW_PICKER
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func convertStrToDate(strDt: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH"
        
        let date = formatter.date(from:strDt)!
        return date
    }
    
    fileprivate func hidePicker() {
        conPickerBottom.constant = HIDE_PICKER
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.viewPicker.animation =  Spring.AnimationPreset.FadeOut.rawValue
            self.viewPicker.curve = Spring.AnimationCurve.Linear.rawValue
            self.viewPicker.duration = 0.3
            self.viewPicker.animate()
        }
        
        if pickerSelected == .date {
            Utils.shared.animateFadeOut(alertDate, 0)
            search.date = pickerDate.date.string(format: "yyyy-MM-dd")
            btnDate.setTitle(pickerDate.date.string(format: DATE_FORMAT), for: .normal)
        }
        else  {
            Utils.shared.animateFadeOut(alertTime, 0)
            search.startTime = arrTime[pickerStartTime.selectedRow(inComponent: 0)] + ":00"
            search.endTime =  arrTime[pickerEndTime.selectedRow(inComponent: 0)] + ":00"
            btnTime.setTitle(search.startTime + " - " + search.endTime, for: .normal)
        }
    }
    
    
    fileprivate func validation() -> Bool {
        
        var isValid = true
        
        /*
         if  search.date.isEmpty() {
         Utils.shared.animatePop(alertDate)
         isValid = false
         }
         
         if  search.startTime.isEmpty() {
         Utils.shared.animatePop(alertTime)
         isValid = false
         }
         
         if  search.endTime.isEmpty() {
         Utils.shared.animatePop(alertTime)
         isValid = false
         }
         
         let sdate = (search.date + " " + search.startTime + ":00").date
         
         if Date().compareTo(date: sdate!) != .less {
         Utils.shared.animatePop(alertTime)
         isValid  = false
         }*/
        
        if  search.helpwith.isEmpty() {
            Utils.shared.animatePop(alertHelp)
            isValid = false
        }
        
        if  search.center.id == 0 {
            Utils.shared.animatePop(alertLocation)
            isValid = false
        }
        
        //        if  search.interest.id == 0 && !search.interest.isNutrition {
        //            Utils.shared.animatePop(alertSport)
        //            isValid = false
        //        }
        
        //        if  search.person == 0 {
        //            Utils.shared.animatePop(alertPeople)
        //            isValid = false
        //        }
        
        return isValid
    }
}

//MARK:- Sport Delegate
//MARK:-

extension SearchAvailableAce : SportDelegate {
    func didSelect(sport: Sport) {
        search.interest.id = sport.id
        search.interest.name = sport.name
        search.interest.isNutrition = false
        btnSport.setTitle(sport.name, for: .normal)
    }
    
    func didSelectNutrition() {
        search.interest.isNutrition = true
        btnSport.setTitle("Ernæring", for: .normal)
    }
}

//MARK:- Tableview Datasource || Delegate
//MARK:-

extension SearchAvailableAce : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHelp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellHelpWith", for: indexPath) as! CellHelpWith
        cell.lblHelp.text = arrHelp[indexPath.row]
        cell.lblHelp.textColor = arrHelp[indexPath.row] == search.helpwith ?  #colorLiteral(red: 0.937254902, green: 0.9803921569, blue: 0, alpha: 0.99)  : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        search.helpwith =  arrHelp[indexPath.row]
        tableView.reloadData()
        Utils.shared.animateFadeOut(self.alertHelp, 0)
    }
}

//MARK:- Picker Datasource || Delegate
//MARK:-


extension SearchAvailableAce : UIPickerViewDataSource , UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return  arrTime.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return   arrTime[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == PickerTag.startTime.rawValue {
            time.sHour = row
            if time.sHour == 23 {
                pickerStartTime.selectRow(22 , inComponent: 0, animated: true)
                pickerEndTime.selectRow(23 , inComponent: 0, animated: true)
            } else if time.sHour >= time.eHour  {
                pickerEndTime.selectRow(time.sHour == 23 ? 0 : time.sHour+1 , inComponent: 0, animated: true)
            }
            
        } else {
            time.eHour = row
            if time.eHour == 0 {
                pickerStartTime.selectRow(0 , inComponent: 0, animated: true)
                pickerEndTime.selectRow(1 , inComponent: 0, animated: true)
            } else if time.eHour <= time.sHour {
                pickerStartTime.selectRow(time.eHour == 0 ? 0 : time.eHour-1 , inComponent: 0, animated: true)
            }
        }
    }
    
}

extension SearchAvailableAce: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        print("Place name: \(place.name)")
//        print("Place address: \(place.formattedAddress)")
//        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

//MARK:- Tableview Cell
//MARK:-

class CellHelpWith : UITableViewCell {
    @IBOutlet var lblHelp: UILabel!
}
