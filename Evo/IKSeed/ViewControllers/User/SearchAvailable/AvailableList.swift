//
//  AvailableList.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/21/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.

import UIKit
import Mixpanel

class AvailableList: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    fileprivate let refresh = UIRefreshControl()
    fileprivate let ROW_HEIGHT : CGFloat = 130
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var tblList: UITableView!
    
    //MARK:- Variable
    //MARK:-
    
    var arrAce : [Ace] = []
    var shouldLoadList = false
    var isBack = false
    fileprivate var count = 1
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        guard shouldLoadList else {
            return
        }
        
        getAceList()
        
        //Mixpanel.sharedInstance()?.track("See personal trainer", properties:nil)
        
        Utils.shared.logEventWithFlurry(EventName: "see_personal_trainer", EventParam: [:])
        
        //let eventParam = ["profile_id":IK_USER.profile_id] as [String:String]
        Utils.shared.logEventWithFlurry(EventName: "see_availability", EventParam: [:])

        refresh.attributedTitle = "Oppdater".changeColor(#colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        refresh.tintColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        refresh.addTarget(self, action: #selector(getAceList) , for: UIControl.Event.valueChanged)
        tblList.addSubview(refresh)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblList.reloadData()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
       if isBack || shouldLoadList {
            navigationController?.popViewController(animated: true)
        } else {
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    //MARK:- Other
    //MARK:-
    
    @objc fileprivate func getAceList() {
        WebConnect.getPersonalTrainers { (status, arrList) in
            
            self.refresh.endRefreshing()
            guard status else {
                return
            }
            self.arrAce = arrList
            self.tblList.reloadData()
        }
    }
}

//MARK:- Collection View Datasource || Delegate
//MARK:-

extension AvailableList : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAce.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellAcesAvailable", for: indexPath) as! CellAces
        let obj = arrAce[indexPath.row]
        
        cell.imgProfile.kf.indicatorType = .activity
        cell.imgProfile.kf.setImage(with: URL(string: obj.profileImage),
                                    placeholder: #imageLiteral(resourceName: "thumb_menu"),
                                    options: nil,
                                    progressBlock: nil,
                                    completionHandler: nil)
        
        cell.lblName.text = obj.firstName + " " + String(describing:obj.lastName.first ?? " ") + "."
        //        cell.lblRating.text = String(describing:obj.rating)
        
        cell.star.forEach {
            //print("Rating : \(Int(obj.rating)) ------------ Tag: \($0.tag)")
            $0.tintColor = Int(obj.rating) >= $0.tag ? #colorLiteral(red: 1, green: 0.8235294118, blue: 0, alpha: 1) : #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ROW_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "AceDetail") as! AceDetail
        detail.objAce = arrAce[indexPath.row]
        navigationController?.pushViewController(detail, animated: true)
    }
}

