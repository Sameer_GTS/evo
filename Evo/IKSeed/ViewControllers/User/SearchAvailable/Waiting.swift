//
//  ViewController.swift
//  AirPlaneAnimation
//
//  Created by Fuad  on 19/02/18.
//  Copyright © 2018 Fuad. All rights reserved.
//

import UIKit

class Waiting: UIViewController {
    
    //MARK:- IBOutlet
    //MARK:-
    
    
    //let messageShow = "We have sent your request to 6 Aces at Oslo. We will show you who all are available in 23 hours."
    
    
    /// Array of bliped based
    @IBOutlet var blips: [UIView]!
    /// Height constraint for blips under flight
    @IBOutlet var blipsHeight: [NSLayoutConstraint]!
    @IBOutlet var lblMessage: UILabel!
    
    @IBOutlet var btnDone: UIButton! {
        didSet {
            btnDone.dropShadow()
        }
    }
    //MARK:- Variable
    //MARK:-
    
    /// Timer to move plane
    fileprivate var timer: Timer!
    /// Bips count
    fileprivate var ticks: Int = 0
    
    var message : String!
    var count  = "0"
    var hours = "0"
    
    //MARK:- View Method
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let eventParam = ["msg":message] as [String:String]
        Utils.shared.logEventWithFlurry(EventName: "available_ace", EventParam: [:])
        
        lblMessage.text = message
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //animatePlane()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.layoutIfNeeded()
        animateView()
    }
    
    //MARK:- Other Method
    //MARK:-
    
    /// Animate progress with plane movement
    fileprivate func animateView() {
        
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 0.03, repeats: true, block: { (timer) in
                self.blipView()
            })
        } else {
            timer = Timer.scheduledTimer(timeInterval: 0.03, target: self, selector: #selector(blipView), userInfo: nil, repeats: true)
            // Fallback on earlier versions
        }
    }
    
    @IBAction func clickBack(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func blipView() {
        self.blipsHeight.forEach {
            $0.constant = abs(CGFloat((self.ticks - (Int($0.identifier!) ?? 0)) % 150))
            ($0.firstItem as! UIView).alpha = (150 - $0.constant) / 150
            ($0.firstItem as! UIView).layer.cornerRadius = ($0.constant / 2)
        }
        
        self.ticks += 1
    }
}
