//
//  SideMenuCust.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/13/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit

class SideMenuCust: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    
    //MARK:- Variable
    //MARK:-
    
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickHistory(_ sender: UIButton) {
    }
    
    @IBAction func clickNotification(_ sender: UIButton) {
    }
    
    @IBAction func clickPayment(_ sender: UIButton) {
    }
    @IBAction func clickHelp(_ sender: UIButton) {
    }
    @IBAction func clickSetting(_ sender: UIButton) {
    }
    
    @IBAction func clickBecomeAce(_ sender: UIButton) {
    }
    //MARK:- Other
    //MARK:-
    
}
