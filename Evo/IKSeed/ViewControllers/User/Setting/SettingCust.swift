//
//  SettingCust.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/13/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Spring

class SettingCust: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    let EN  = 14521
    let NR  = 14522
    
    let SHOW_LANGUAGE : CGFloat = 62
    let HIDE_LANGUAGE : CGFloat = 0
    
    //MARK:- IBOutlet
    //MARK:-
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblJoining: UILabel!
    @IBOutlet var viewAce: UIView!
    
    
    @IBOutlet var btnLanguage: UIButton!
    @IBOutlet var lblLanguage: UILabel!
    @IBOutlet var stackLanguage: UIStackView!
    @IBOutlet var conHeight: NSLayoutConstraint!
    
    //MARK:- Variable
    //MARK:-
    var isAce = false
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewAce.isHidden = isAce
        
        Utils.shared.logEventWithFlurry(EventName: "settings_screen", EventParam: [:])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 1)
        imgUser.kf.indicatorType = .activity
        imgUser.kf.setImage(with: URL(string: IK_USER.profile_image),
                            placeholder: #imageLiteral(resourceName: "thumb_menu"),
                            options: nil,
                            progressBlock: nil,
                            completionHandler: nil)
        lblName.text = IK_USER.first_name + " " + IK_USER.last_name
        lblJoining.text = "Ble medlem " + (IK_USER.created_at.date?.string(format: DATE_FORMAT))!
        
        lblLanguage.text = settingLn == "EN" ? "English" : "Norwegian"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickLanguage(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        conHeight.constant = sender.isSelected ? SHOW_LANGUAGE : HIDE_LANGUAGE
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
            self.stackLanguage.alpha = sender.isSelected ?  1 : 0
        }
    }
    
    @IBAction func clickOption(_ sender: UIButton) {
        lblLanguage.text = sender.tag == EN ? "English" : "Norwegian"
        
        btnLanguage.isSelected = false
        conHeight.constant =  HIDE_LANGUAGE
        
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
            self.stackLanguage.alpha = 0
        }
        
        WebConnect.updateSetting(language: sender.tag == EN ? "EN" : "NR") { (status) in
            
        }
    }
    
    @IBAction func clickBecomeAce(_ sender: UIButton) {
        WebConnect.checkCanAce { (status, message) in
            guard status else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                return
            }
            
            let welcome = self.storyboard?.instantiateViewController(withIdentifier: "Welcome") as! Welcome
            self.navigationController?.pushViewController(welcome, animated: true)
        }
    }
    
    @IBAction func clickTerms(_ sender: UIButton) {
        let term = self.storyboard?.instantiateViewController(withIdentifier: "Terms") as! Terms
        term.url = WebConnect.TermsNCondition
        term.strTitle = "Terms of use"
        navigationController?.present(term, animated: true, completion: {
        })
        //navigationController?.pushViewController(term, animated: true)
    }
    
    @IBAction func clickPrivacy(_ sender: UIButton) {
        let policy = self.storyboard?.instantiateViewController(withIdentifier: "Terms") as! Terms
        policy.url = WebConnect.PrivacyPolicy
        policy.strTitle = "Privacy policy"
        navigationController?.present(policy, animated: true, completion: {
        })
        //navigationController?.pushViewController(policy, animated: true)
    }
    
    @IBAction func clickLogout(_ sender: UIButton) {
        
        Utils.shared.alert(on: self, message: CONFIRM_LOGOUT, affirmButton: "Logg ut", cancelButton: "Avbryt") { (index) in
            guard index == 0 else {
                return
            }
            
            WebConnect.logout(completion: { (_, _) in
                ChatManager.shared.disconnect()
              
                
                IK_DEFAULTS.removeObject(forKey: USER_DETAIL)
                IK_DEFAULTS.synchronize()
                
                let register = self.storyboard?.instantiateViewController(withIdentifier: "Landing") as! Landing
                let navigation = UINavigationController(rootViewController:register )
                navigation.isNavigationBarHidden = true
                
                UIView.transition(from: IK_DELEGATE.window!.rootViewController!.view,
                                  to: navigation.view,
                                  duration: 0.45,
                                  options: .transitionCrossDissolve,
                                  completion: { (_) in
                                    IK_DELEGATE.window!.rootViewController = navigation
                })
            })
        }
    }
}
