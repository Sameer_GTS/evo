//
//  UserProfile.swift
//  IKSeed
//
//  Created by Chanchal Warde on 4/2/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Spring

class UserProfile: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    let TAG_FIRST_NAME = 1234
    let TAG_LAST_NAME = 1235
    
    let picker = MediaPicker()
    
    let interest  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Sports") as! Sports
    let fitness = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FitnessLevel") as! FitnessLevel
    let age = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AgeGroup") as! AgeGroup
    let gym = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GymList") as! GymList

    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var tfFirstName: UITextField! {
        didSet {
            tfFirstName.tag = TAG_FIRST_NAME
            //        tfFirstName.isUserInteractionEnabled = false
            
        }
    }
    
    @IBOutlet var tfLastName: UITextField!  {
        didSet {
            tfLastName.tag = TAG_LAST_NAME
            //        tfLastName.isUserInteractionEnabled = false
            
        }
    }
    
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var btnInterest: UIButton!
    @IBOutlet var btnAge: UIButton!
    @IBOutlet var btnFitness: UIButton!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var btnImage: UIButton!
    @IBOutlet var btnCenter: UIButton!

    @IBOutlet var alertFName: SpringImageView!{
        didSet {
            Utils.shared.animateFadeOut(alertFName, 0.0)
        }
    }
    @IBOutlet var alertLName: SpringImageView!{
        didSet {
            Utils.shared.animateFadeOut(alertLName, 0.0)
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    var updates : (image:UIImage? , firstName:String?, lastName:String?)
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfFirstName.text = IK_USER.first_name
        tfLastName.text = IK_USER.last_name
        lblEmail.text = IK_USER.email
        
        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 0)
        imgUser.kf.indicatorType = .activity
        imgUser.kf.setImage(with: URL(string: IK_USER.profile_image),
                            placeholder: #imageLiteral(resourceName: "thumb_menu"),
                            options: nil,
                            progressBlock: nil,
                            completionHandler: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupdData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction Method
    //MARK:-
    
    @IBAction func clickSave(_ sender: UIButton) {
        if sender.isSelected {
            
            guard validation()  else {
                return
            }
            self.view.endEditing(true)
            
            sender.isSelected = !sender.isSelected
            btnImage.isUserInteractionEnabled = false
            tfFirstName.isUserInteractionEnabled = false
            tfLastName.isUserInteractionEnabled = false
            
            WebConnect.updateUserProfile(firstName: tfFirstName.text!.trimString, lastName: tfLastName.text!.trimString, profileImage: imgUser.tag == 123 ? imgUser.image : nil, completion: { (status, message ,  imageUrl  ) in
                
                guard status else {
                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                    return
                }
                
                if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                    
                    let newDic = NSMutableDictionary(dictionary: dict)
                    newDic.setValue(self.tfFirstName.text!.trimString, forKey: "first_name")
                    newDic.setValue(self.tfLastName.text!.trimString, forKey: "last_name")
                    if imageUrl != nil { newDic.setValue(imageUrl, forKey: "profile_image") }
                    
                    IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                    IK_DEFAULTS.synchronize()
                }
            })
        }
        else {
            sender.isSelected = !sender.isSelected
            btnImage.isUserInteractionEnabled = true
            tfFirstName.isUserInteractionEnabled = true
            tfLastName.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func clickBack(_ sender: UIButton) {
        
        guard updates.firstName != nil || updates.lastName != nil || updates.image != nil else {
            _ = navigationController?.popViewController(animated: true)
            return
        }
        
        Utils.shared.alert(on: self, message: CONFIRM_UPDATE, affirmButton: "Ja", cancelButton: "Nei") { (index) in
            guard index == 0 else {
                self.navigationController?.popViewController(animated: true)
                return
            }
            self.saveUpdate()
        }
    }
    
    @IBAction func clickInterest(_ sender: UIButton) {
        interest.isShowBack = true
        interest.isMultiSelection = true
        interest.selectedInterest = IK_USER.sportId
        if IK_USER.nutritionist == "Y" { interest.selectedInterest.append(NUTRITION) }
        self.navigationController?.pushViewController(interest , animated: true)
    }
    
    @IBAction func clickAge(_ sender: UIButton) {
        age.isBack = true
        self.navigationController?.pushViewController(age , animated: true)
    }
    
    @IBAction func clickFitness(_ sender: UIButton) {
        fitness.isBack = true
        self.navigationController?.pushViewController(fitness , animated: true)
    }
    
    @IBAction func clickCenter(_ sender: UIButton) {
        
        let gym = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GymList") as! GymList
        
        // jitendra
        for i in 0..<arrGyms.count {
            if self.btnCenter.titleLabel?.text == arrGyms[i].name {
                gym.selectedAddrss = i
                gym.selectedGymId = arrGyms[i].id
                break
            }
        }
        
        gym.isBack = true
        self.navigationController?.pushViewController(gym , animated: true)
    }
    
    @IBAction func clickImage(_ sender: UIButton) {
        //Fahad
        picker.isSquared = true
        picker.targetVC = self
        picker.showPicker(vc: self)
        picker.delegate = self
    }
    
    //MARK:- Other Method
    //MARK:-
    
    fileprivate func saveUpdate() {
        guard validation()  else {
            return
        }
        
        WebConnect.updateUserProfile(firstName: tfFirstName.text!.trimString, lastName: tfLastName.text!.trimString, profileImage: imgUser.tag == 123 ? imgUser.image : nil, completion: { (status, message ,  imageUrl  ) in
            
            guard status else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                return
            }
            
            if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                
                let newDic = NSMutableDictionary(dictionary: dict)
                newDic.setValue(self.tfFirstName.text!.trimString, forKey: "first_name")
                newDic.setValue(self.tfLastName.text!.trimString, forKey: "last_name")
                if imageUrl != nil { newDic.setValue(imageUrl, forKey: "profile_image") }
                
                IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                IK_DEFAULTS.synchronize()
            }
            Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton:nil) { _ in
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    fileprivate func validation() -> Bool {
        var isValid = true
        
        if tfFirstName.text!.isEmpty() {
            Utils.shared.animatePop(alertFName)
            isValid = false
        }
        
        if tfLastName.text!.isEmpty() {
            Utils.shared.animatePop(alertLName)
            isValid = false
        }
        return isValid
    }
    
    fileprivate func setupdData() {
        
        var interestName : [String] = []
        
        for i in IK_USER.sportId {
            let name = arrSports.first(where: { (sp) -> Bool in
                return sp.id == i
            })?.name ?? "Ernæring"
            
            interestName.append( name)
        }
        
        if IK_USER.nutritionist == "Y" { interestName.append("Ernæring") }
        
        btnInterest.setTitle(interestName.joined(separator: " , "), for: .normal)
        btnAge.setTitle(arrAge[IK_USER.ageGroup], for: .normal)
        btnFitness.setTitle(arrFitness[IK_USER.fitness], for: .normal)
        
        if let a = (arrAllGyms.filter { $0.id == IK_USER.gym}).first {
            btnCenter.setTitle(a.name, for: .normal)
        }
    }
}

//MARK:- Textfield delegate
//MARK:-

extension UserProfile : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == TAG_FIRST_NAME {
            updates.firstName = textField.text
            tfLastName.becomeFirstResponder()
        }
        else if textField.tag == TAG_LAST_NAME {
            updates.lastName = textField.text
            tfLastName.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == TAG_FIRST_NAME {
            updates.firstName = textField.text
        }
        else if textField.tag == TAG_LAST_NAME {
            updates.lastName = textField.text
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == TAG_FIRST_NAME {
            Utils.shared.animateFadeOut(alertFName)
        }
        else if textField.tag == TAG_LAST_NAME {
            Utils.shared.animateFadeOut(alertLName)
        }
    }
}

//MARK:- ImagePicker delegate
//MARK:-

extension UserProfile : ImagePickerDelegate {
    func didSelect(image: UIImage) {
        imgUser.contentMode = .scaleAspectFill
        imgUser.image = image
        imgUser.isHighlighted = false
        updates.image = image
        imgUser.tag = 123
    }
}
