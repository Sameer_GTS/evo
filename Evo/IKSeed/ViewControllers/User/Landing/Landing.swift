//
//  Landing.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/9/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Spring
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Mixpanel
import SafariServices

class Landing: UIViewController {
    
    //MRAK:- Constant
    //MARK:-
    
    //MRAK:- IBOutlet
    //MARK:-
    
    @IBOutlet var btnSignIn: UIButton!{
        didSet {
            //btnSignIn.dropShadow(radius: 5)
            btnSignIn.setBorder(cornerRadius: btnSignIn.bounds.height/2, borderWidth: 0, borderColor: .clear)
        }
    }
    
    @IBOutlet var btnRegister: UIButton!{
        didSet {
            btnRegister.dropShadow( radius: 5, color: #colorLiteral(red: 0.8901960784, green: 0.2705882353, blue: 0.4078431373, alpha: 1))
        }
    }
    
    @IBOutlet var btnFB: UIButton!{
        didSet {
            btnFB.setBorder(cornerRadius: btnFB.bounds.height/2, borderWidth: 0, borderColor: .clear)
        }
    }
    
    @IBOutlet var btnGoogle: UIButton!{
        didSet {
            btnGoogle.setBorder(cornerRadius: btnGoogle.bounds.height/2, borderWidth: 0, borderColor: .clear)
        }
    }
    
    ///---- Get email
    
    @IBOutlet var viewEmail: SpringView! {
        didSet {
            viewEmail.animation = Spring.AnimationPreset.FadeOut.rawValue
            viewEmail.duration = 0
            viewEmail.curve = Spring.AnimationCurve.Linear.rawValue
            viewEmail.animate()
        }
    }
    @IBOutlet var viewEmailContainter: SpringView!{
        didSet {
            viewEmailContainter.setBorder(cornerRadius: 10, borderWidth: 0, borderColor: .clear)
            viewEmailContainter.animation = Spring.AnimationPreset.FadeOut.rawValue
            viewEmailContainter.duration = 0
            viewEmailContainter.curve = Spring.AnimationCurve.Linear.rawValue
            viewEmailContainter.animate()
        }
    }
    @IBOutlet var tfAddEmail: UITextField!
    @IBOutlet var btnSubmit: UIButton! {
        didSet {
            btnSubmit.dropShadow(radius: 10)
        }
    }
    
    @IBOutlet var alertEmailAdd: SpringImageView! {
        didSet {
            Utils.shared.animateFadeOut(alertEmailAdd, 0.0)
        }
    }
    
    //MRAK:- Variable
    //MARK:-
    
    fileprivate var signupWay = ""
    
    //MRAK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utils.shared.logEventWithFlurry(EventName: "welcome_screen", EventParam: [:])
        
        GIDSignIn.sharedInstance().clientID = KEY_GOOGLE
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btnFB.setBorder(cornerRadius: btnFB.bounds.height/2, borderWidth: 0, borderColor: .clear)
        btnGoogle.setBorder(cornerRadius: btnGoogle.bounds.height/2, borderWidth: 0, borderColor: .clear)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        btnFB.setBorder(cornerRadius: btnFB.bounds.height/2, borderWidth: 0, borderColor: .clear)
        btnGoogle.setBorder(cornerRadius: btnGoogle.bounds.height/2, borderWidth: 0, borderColor: .clear)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MRAK:- IBAction
    //MARK:-
    
    @IBAction func clickFacebook(_ sender: UIButton) {
        let loginManager = LoginManager()
        
        loginManager.logIn(permissions: ["public_profile"], from: self) { (result, error) in
            if (AccessToken.current != nil) {
                FUProgressView.showProgress()
                GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, response, error) -> Void in
                    FUProgressView.hideProgress()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05, execute: {
                        if (error == nil){
                            
                            loginManager.logOut()
                            let dict = response as! NSDictionary
                            let name = dict["name"] as? String ?? ""
                            let socialId =  String(describing: dict["id"]!)
                            let image =  ((dict["picture"] as! NSDictionary )["data"] as! NSDictionary)["url"] as? String ?? ""
                            let email = dict["email"] as? String ?? ""
                            
                            //  print(response)
                            
                            WebConnect.loginWithFB(email: email, name: name, profileId: socialId, imageURL: image, completion: { (status, message) in
                                if status {
                                    
                                    if IK_USER.email.isEmpty() {
                                        self.showEmail()
                                        self.signupWay = "Facebook"
                                    }
                                    else {
                                        self.forwordView()
                                    }
                                }
                                else {
                                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                }
                            })
                        }
                    })
                })
            }
        }
    }
    
    @IBAction func clickGoogle(_ sender: UIButton) {
        
        if GIDSignIn.sharedInstance().currentUser == nil  {
            if GIDSignIn.sharedInstance().hasAuthInKeychain() {
                GIDSignIn.sharedInstance().signInSilently()
            } else {
                GIDSignIn.sharedInstance().signIn()
            }
        } else {
            
            let id = GIDSignIn.sharedInstance().currentUser.userID ?? ""
            let name = GIDSignIn.sharedInstance().currentUser.profile.name ?? ""
            let email = GIDSignIn.sharedInstance().currentUser.profile.email ?? ""
            let picture = GIDSignIn.sharedInstance().currentUser.profile.imageURL(withDimension: 32).absoluteString
            
            WebConnect.loginWithGoogle(email: email, name: name, profileId: id, imageURL: picture) { (status, message) in
                if status {
                    if IK_USER.email.isEmpty() {
                        self.showEmail()
                        self.signupWay = "Google"
                    }
                    else {
                        self.forwordView()
                    }
                }
                else {
                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                }
            }
        }
    }
    
    @IBAction func clickSubmitEmail(_ sender: UIButton) {
        guard tfAddEmail.text!.isEmail() else  {
            Utils.shared.animatePop(alertEmailAdd)
            return
        }
        
        WebConnect.updateEmail(email: tfAddEmail.text!.trimString) { (status, message) in
            if status {
                
                /*
                Mixpanel.sharedInstance()?.track("Sign up page", properties:[/*"Name":IK_USER.first_name + " " + IK_USER.last_name ,
                                                                             "Email":IK_USER.email,*/
                                                                             "Registration":self.signupWay])
                */
                
                let eventParams = ["Registration" : self.signupWay] as [String : Any]
                Utils.shared.logEventWithFlurry(EventName: "Sign_up_page", EventParam: eventParams)
                
                
                self.forwordView()
                self.hideMail()
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
    }
    
    @IBAction func clickCancelEmail(_ sender: UIButton) {
        IK_DEFAULTS.removeObject(forKey: USER_DETAIL)
        IK_DEFAULTS.synchronize()
        hideMail()
    }
    
    //MARK:- Other Method
    //MARK:-
    
    fileprivate func forwordView() {
        if IK_USER.userType == .customer || IK_USER.userType == .requestedForAcer {
            if  IK_USER.sportId.count == 0 && IK_USER.nutritionist == "N"  {
                let sport = self.storyboard?.instantiateViewController(withIdentifier: "Sports") as! Sports
                self.navigationController?.pushViewController(sport, animated: true)
            }
            else if IK_USER.gym == 0 {
                let gym = self.storyboard?.instantiateViewController(withIdentifier: "GymList") as! GymList
                self.navigationController?.pushViewController(gym, animated: true)
            }
            else if IK_USER.fitness == 0 {
                let fitness = self.storyboard?.instantiateViewController(withIdentifier: "FitnessLevel") as! FitnessLevel
                self.navigationController?.pushViewController(fitness, animated: true)
            }
            else if IK_USER.ageGroup == 0 {
                let age = self.storyboard?.instantiateViewController(withIdentifier: "AgeGroup") as! AgeGroup
                self.navigationController?.pushViewController(age, animated: true)
            }
            else if IK_USER.profile_image.isEmpty() {
                let picture = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePicture") as! ProfilePicture
                self.navigationController?.pushViewController(picture, animated: true)
            }
            else {
                
                /*
                Mixpanel.sharedInstance()?.identify(IK_USER.profile_id, usePeople: true)
                
                Mixpanel.sharedInstance()?.people.set([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
                                                        "First Name":IK_USER.first_name,
                                                        "Last Name":IK_USER.last_name,*/
                                                        "Profile":IK_USER.profile_image,
                                                        /*"Email":IK_USER.email,
                                                        "$email":IK_USER.email,*/
                                                        "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                                        "Fitness Level":arrFitness[IK_USER.fitness],
                                                        "Age Group":arrAge[IK_USER.ageGroup]])
                
                Mixpanel.sharedInstance()?.registerSuperProperties([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
                                                                     "First Name":IK_USER.first_name,
                                                                     "Last Name":IK_USER.last_name,
                                                                     "Email":IK_USER.email,*/
                                                               "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                                                     "Fitness Level":arrFitness[IK_USER.fitness],
                                                                     "Age Group":arrAge[IK_USER.ageGroup]])
                */
                
                ChatManager.shared.connect(userId:IK_USER.profile_id, accessToken: IK_USER.sendbirdToken)
                
                IK_DELEGATE.TabBarConfiguration(true)
            }
        }
        else if IK_USER.userType == .ace {
            
            ChatManager.shared.connect(userId:IK_USER.profile_id, accessToken: IK_USER.sendbirdToken)
            
            /*
            Mixpanel.sharedInstance()?.registerSuperProperties([ /*"Name": IK_USER.first_name + " " + IK_USER.last_name,
                                                                 "First Name":IK_USER.first_name,
                                                                 "Last Name":IK_USER.last_name,
                                                                 "Email":IK_USER.email,*/
                                                            "Interest":IK_USER.sportId.map(String.init).joined(separator: ","),
                                                                 "Fitness Level":arrFitness[IK_USER.fitness],
                                                                 "Age Group":arrAge[IK_USER.ageGroup]])
            */
            
            IK_DELEGATE.TabBarConfiguration(true)
        }
    }
    
    fileprivate func hideMail() {
        
        viewEmailContainter.setBorder(cornerRadius: 10, borderWidth: 0, borderColor: .clear)
        viewEmailContainter.animation = Spring.AnimationPreset.FadeOut.rawValue
        viewEmailContainter.duration = 0.3
        viewEmailContainter.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmailContainter.animate()
        
        viewEmail.animation = Spring.AnimationPreset.FadeOut.rawValue
        viewEmail.duration = 0.3
        viewEmail.delay = 0.15
        viewEmail.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmail.animate()
    }
    
    fileprivate func showEmail() {
        
        viewEmail.animation = Spring.AnimationPreset.FadeIn.rawValue
        viewEmail.duration = 0.3
        viewEmail.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmail.animate()
        
        viewEmailContainter.animation = Spring.AnimationPreset.FadeIn.rawValue
        viewEmailContainter.curve = Spring.AnimationCurve.Linear.rawValue
        viewEmailContainter.duration = 0.3
        viewEmailContainter.delay = 0.25
        viewEmailContainter.scaleX = 0.5
        viewEmailContainter.scaleY = 0.5
        viewEmailContainter.animate()
    }
    
}

extension Landing : GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        guard error == nil else {
            return
        }
        
        let id = user.userID ?? ""
        let name = user.profile.name ?? ""
        let picture = user.profile.imageURL(withDimension: 32).absoluteString
        let email = user.profile.email ?? ""
        
        WebConnect.loginWithGoogle(email: email, name: name, profileId: id, imageURL: picture) { (status, message) in
            if status {
                if IK_USER.email.isEmpty() {
                    self.showEmail()
                    self.signupWay = "Google"
                }
                else {
                    self.forwordView()
                }
            }
            else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
    }
    
    // MARK: - Signin UI Delegate
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
}

extension Landing : UIWebViewDelegate ,SFSafariViewControllerDelegate {
    
    @IBAction func clickSignUp(_ sender: UIButton) {
        showLinksClicked(url: "https://signup.evofitness.no/profile")
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {

    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        
        return true
    }
    
    func showLinksClicked(url:String) {
        let safariVC = SFSafariViewController(url: NSURL(string: url)! as URL)
        if #available(iOS 10.0, *) {
            safariVC.preferredBarTintColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        } else {
        }
        
        safariVC.delegate = self
        self.present(safariVC, animated: true, completion: nil)
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

