//
//  CustomizedTabBarController.swift
//  Qbitt
//
//  Created by Fahad Mohammed Firoz Khan on 03/09/18.
//  Copyright © 2018 GTS. All rights reserved.
//

import UIKit

class CustomizedTabBarController: UITabBarController,UITabBarControllerDelegate  {

    override func viewDidLoad() {
        super.viewDidLoad()       
        self.tabBar.barTintColor = UIColor.white
        self.delegate = self;
    }
        
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let vc = viewController as? UINavigationController {
            vc.popToRootViewController(animated: false)
        }
    }
    
    override func viewDidLayoutSubviews() {
        if IK_USER.userType == .ace {
            self.tabBar.items![1].imageInsets.top = -21
        }
        else
        {
            self.tabBar.items![2].imageInsets.top = -21
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
