//
//  ChangeDateTimeVC.swift
//  Ikseed
//
//  Created by Sameer's MACmini on 06/08/19.
//  Copyright © 2019 Chanchal Warde. All rights reserved.
//

import UIKit
import Spring

class ChangeDateTimeVC: UIViewController {
    //MARK:- Constant
    //MARK:-
    
    enum PickerType {
        case date
        case time
    }
    
    enum PickerTag : Int {
        case startTime = 200
        case endTime
    }
    
    let arrTime = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]
    
    let SHOW_PICKER : CGFloat = 0
    let HIDE_PICKER : CGFloat = -265
    let person =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectPersons") as! SelectPersons
    fileprivate let refresh = UIRefreshControl()
    
    //MARK:- IBOutlet
    //MARK:-
    @IBOutlet var btnDate: UIButton!
    @IBOutlet var btnTime: UIButton!
    @IBOutlet var btnBook: UIButton! {
        didSet {
            //btnBook.dropShadow()
            btnBook.setBorder(cornerRadius: 0, borderWidth: 1, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        }
    }
    
    //--- Alerts
    @IBOutlet var alertDate: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertDate,0.0)
        }
    }
    @IBOutlet var alertTime: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertTime,0.0)
        }
    }
    
    //--- Date Time Picker
    
    @IBOutlet var conPickerBottom: NSLayoutConstraint!
    @IBOutlet var viewPicker: SpringView! {
        didSet {
            viewPicker.animation =  Spring.AnimationPreset.FadeOut.rawValue
            viewPicker.curve = Spring.AnimationCurve.Linear.rawValue
            viewPicker.duration = 0
            viewPicker.animate()
        }
    }
    @IBOutlet var viewTime: UIView!
    @IBOutlet var viewDate: UIView!
    
    @IBOutlet var pickerStartTime: UIPickerView! {
        didSet {
            pickerStartTime.tag = PickerTag.startTime.rawValue
        }
    }
    @IBOutlet var pickerEndTime: UIPickerView!{
        didSet {
            pickerEndTime.tag = PickerTag.endTime.rawValue
        }
    }
    @IBOutlet var pickerDate: UIDatePicker!
    
    //MARK:- Variable
    //MARK:-
    
    var aceDetail : Ace!
    /*
    var bookingInfo = (date:"",
                       startTime:"22:00",
                       endTime:"23:00",
                       people:1,
                       amount:"5",
                       acePrice:"5",
                       requestId:"",
                       meetingPoint:"ab",
                       hours:0)
    */
    
    fileprivate var pickerSelected : PickerType!
    fileprivate var isCardForword = false
    fileprivate var cardCount = 0
    fileprivate var time = (sHour:0, eHour:0)
    fileprivate var selectedDate = ""
    
    var requestData : Request?
    
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        pickerDate.minimumDate = Date()
        
        let hour = Date().getComponent(request: .hour) + 1
        time.sHour = hour
        time.eHour = hour + 1
        pickerStartTime.selectRow( hour, inComponent: 0, animated: true)
        pickerEndTime.selectRow( hour+1 , inComponent: 0, animated: true)
        
//        if let a = (arrAllGyms.filter { $0.id == aceDetail.gymId}).first {
//            btnLocation.setTitle(a.name, for: .normal)
//            btnLocation.tag = a.id
//            self.bookingInfo.center = a
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        btnDate.setTitle(requestData?.date, for: .normal)
        btnTime.setTitle((requestData?.startTime.string(format: "HH:mm") ?? "") + " - " + (requestData?.endTime.string(format: "HH:mm") ?? ""), for: .normal)
        
        selectedDate = convertDateFormater(requestData?.date ?? "")
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickBook(_ sender: UIButton) {
        guard validation() else {
            return
        }
        
        WebConnect.updateAce(bookingId : requestData?.requestId ?? 0,
                             date: selectedDate,
                             startTime: requestData?.startTime.string(format: "HH:mm") ?? "",
                             endTime: requestData?.endTime.string(format: "HH:mm") ?? "") { (status, message, isNoCard) in
            
            guard status else {
                guard isNoCard else {
                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                    return
                }
                return
            }
            
            track.end = Date()
            
            //Flurry Analytics for eventLog
            let eventParam = [//"profile_id" : IK_USER.profile_id ?? "",
                              "request_id" : self.requestData?.requestId ?? ""] as [String:Any]
            Utils.shared.logEventWithFlurry(EventName: "booking_updated", EventParam: eventParam)
            
            track.start = Date()
            
//            let success = self.storyboard?.instantiateViewController(withIdentifier: "SuccessMsg") as! SuccessMsg
//            self.navigationController?.pushViewController(success, animated: true)
                              
                                Utils.shared.alert(on: self, message: message, affirmButton: "OK", cancelButton: nil){(ind) in
                                    self.navigationController?.popViewController(animated: true)
                                }
                    
            
        }
        
    }
    
    @IBAction func clickDate(_ sender: UIButton) {
        pickerSelected = .date
        showPicker(type:.date)
    }
    
    @IBAction func clickTime(_ sender: UIButton) {
        pickerSelected = .time
        showPicker(type:.time)
    }
 
    @IBAction func clickDone(_ sender: UIButton) {
        hidePicker()
    }
    
    func convertStrToDate(strDt: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH"
        
        let date = formatter.date(from:strDt)!
        return date
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
    }
    
    //MARK:- Other
    //MARK:-
    fileprivate func showPicker(type:PickerType) {
        
        viewDate.isHidden = type == .time
        viewTime.isHidden = type == .date
        
        viewPicker.animation =  Spring.AnimationPreset.FadeIn.rawValue
        viewPicker.curve = Spring.AnimationCurve.Linear.rawValue
        viewPicker.duration = 0.3
        viewPicker.animateNext {
            self.conPickerBottom.constant = self.SHOW_PICKER
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    fileprivate func hidePicker() {
        conPickerBottom.constant = HIDE_PICKER
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.viewPicker.animation =  Spring.AnimationPreset.FadeOut.rawValue
            self.viewPicker.curve = Spring.AnimationCurve.Linear.rawValue
            self.viewPicker.duration = 0.3
            self.viewPicker.animate()
        }
        
        if pickerSelected == .date {
            Utils.shared.animateFadeOut(alertDate, 0)
            //bookingInfo.date = pickerDate.date.string(format: "yyyy-MM-dd")
            selectedDate = pickerDate.date.string(format: "yyyy-MM-dd")
            btnDate.setTitle(pickerDate.date.string(format: DATE_FORMAT), for: .normal)
        }
        else {
            Utils.shared.animateFadeOut(alertTime, 0)
            //requestData?.startTime = arrTime[pickerStartTime.selectedRow(inComponent: 0)]  //+ ":00"
            //requestData?.endTime = arrTime[pickerEndTime.selectedRow(inComponent: 0)]  //+ ":00"
            //btnTime.setTitle(requestData?.startTime + " - " + requestData?.endTime, for: .normal)
            
            requestData?.startTime = convertStrToDate(strDt: arrTime[pickerStartTime.selectedRow(inComponent: 0)])
            requestData?.endTime = convertStrToDate(strDt: arrTime[pickerEndTime.selectedRow(inComponent: 0)])
            
            btnTime.setTitle((requestData?.startTime.string(format: "HH:mm") ?? "") + " - " + (requestData?.endTime.string(format: "HH:mm") ?? ""), for: .normal)
            
            
            //bookingInfo.startTime = arrTime[pickerStartTime.selectedRow(inComponent: 0)] + ":00"
            //bookingInfo.endTime =  arrTime[pickerEndTime.selectedRow(inComponent: 0)] + ":00"
            //btnTime.setTitle(bookingInfo.startTime + " - " + bookingInfo.endTime, for: .normal)
            //self.bookingInfo.hours = pickerEndTime.selectedRow(inComponent: 0) - pickerStartTime.selectedRow(inComponent: 0)
            
            /*
            let totalHour = pickerEndTime.selectedRow(inComponent: 0) - pickerStartTime.selectedRow(inComponent: 0)
            
            WebConnect.rateCalculation(sportId: 210,
                                       person: 1,
                                       hour: totalHour) { (status, amount, aceAmount) in
                                        guard status else {return}
                                        //self.bookingInfo.amount = amount
                                        //self.bookingInfo.acePrice = aceAmount
                                        //self.btnPrice.setTitle("\(amount) NOK", for: .normal)
            }
            */
        }
    }
    
    fileprivate func validation() -> Bool {
        
        var isValid = true
        
        return isValid
    }
    
}

//MARK:- Picker Datasource || Delegate
//MARK:-


extension ChangeDateTimeVC : UIPickerViewDataSource , UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return  arrTime.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return   arrTime[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == PickerTag.startTime.rawValue {
            time.sHour = row
            if time.sHour == 23 {
                pickerStartTime.selectRow(22 , inComponent: 0, animated: true)
                pickerEndTime.selectRow(23 , inComponent: 0, animated: true)
            } else if time.sHour >= time.eHour  {
                pickerEndTime.selectRow(time.sHour == 23 ? 0 : time.sHour+1 , inComponent: 0, animated: true)
            }
            
        } else {
            time.eHour = row
            if time.eHour == 0 {
                pickerStartTime.selectRow(0 , inComponent: 0, animated: true)
                pickerEndTime.selectRow(1 , inComponent: 0, animated: true)
            } else if time.eHour <= time.sHour {
                pickerStartTime.selectRow(time.eHour == 0 ? 0 : time.eHour-1 , inComponent: 0, animated: true)
            }
        }
    }
    
}

extension Date {
    /// Returns datecomponent for exact date
    var timeHMComponent : Int64
    {
        get {
            var component = Calendar.current.dateComponents([.hour, .minute, .month, .year, .day], from: self)
            component.day = 1
            component.month = 1
            component.year = 1970
            component.second = 0
            component.nanosecond = 0
            return Int64((Calendar.current.date(from: component)!.timeIntervalSince1970  * 1000))
        }
    }
    
    var formattedTimeWithOutAMPM: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: self)
    }
}
