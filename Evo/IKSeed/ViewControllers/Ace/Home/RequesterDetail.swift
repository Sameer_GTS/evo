//
//  RequesterDetail.swift
//  IKSeed
//
//  Created by Chanchal Warde on 4/4/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.

import UIKit

class RequesterDetail: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    //let arrAge = ["","Under 20 years old","21 - 40 years old","41 - 50 years old","51 years or older"]
    //let arrFitness = ["","Out of shape","About average","Athletic","Elite"]
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var viewSport: UIView!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblAge: UILabel!
    @IBOutlet var lblFitness: UILabel!
    @IBOutlet var lblCategoryName: UILabel!
    
    //MARK:- Variable
    //MARK:-
    
    var userDetail = (name:"",ageGroup:0, fitness:0,image:"" , category : "")
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let eventParam = ["profile_id":IK_USER.profile_id] as [String:String]
        Utils.shared.logEventWithFlurry(EventName: "user_profile_view", EventParam: [:])
        
        setData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBAction
    //MARK:-
    
    fileprivate func setData() {
        
        lblName.text = userDetail.name
        lblAge.text = arrAge[userDetail.ageGroup]
        lblFitness.text = arrFitness[userDetail.fitness]
        
        lblCategoryName.text = userDetail.category
        
        imgProfile.setBorder(cornerRadius: imgProfile.bounds.height/2, borderWidth: 1)
        imgProfile.kf.indicatorType = .activity
        imgProfile.kf.setImage(with: URL(string: userDetail.image),
                               placeholder: #imageLiteral(resourceName: "thumb_menu"),
                               options: nil,
                               progressBlock: nil,
                               completionHandler: nil)
    }
    
}
