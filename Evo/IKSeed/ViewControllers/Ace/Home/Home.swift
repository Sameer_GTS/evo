//
//  Home.swift
//  IKSeed
//
//  Created by Fuad  on 13/03/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.


import UIKit
import MapKit
//import FSPagerView
import Spring
import Mixpanel

class Home: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    fileprivate let refresh = UIRefreshControl()
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var tblBookings: UITableView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var imgHomeUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var consMenuLead: NSLayoutConstraint!
    @IBOutlet var viewToggle: UIView!
    @IBOutlet var imgUnread: UIImageView!
    @IBOutlet var viewError: SpringView!
    
    /// Pagvarew to show scrollable features
    @IBOutlet var pagerView: FSPagerView! {
        didSet {
            let nib = UINib(nibName: "CellRequestCard", bundle: nil)
            self.pagerView.register(nib, forCellWithReuseIdentifier: "cell")
            self.pagerView.transformer = FSPagerViewTransformer(type: .linear)
            self.pagerView.delegate = self
            self.pagerView.dataSource = self
            self.pagerView.alpha = 0
        }
    }
    
    @IBOutlet var viewSideMenu: UIView! {
        didSet {
            let gesture = UISwipeGestureRecognizer.init(target: self, action: #selector(sideMenuSwipped))
            gesture.direction = .left
            viewSideMenu.addGestureRecognizer(gesture)
        }
    }
    
    //MARK:- Variable
    //MARK:-
    
    var btnToggle: TTSegmentedControl!
    var transformWidth : CGFloat {
        if view.bounds.size.width <= 320 {
            return 0.79
        }
        else if view.bounds.size.width <= 375 {
            return 0.82
        }
        else {
            return 0.87
        }
    }
    
    var arrRequest : [Request] = []
    var arrBooking : [Request] = []
    var arrComplete : [Request] = []
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewError.alpha = 0
        
        refresh.attributedTitle = "Oppdater".changeColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        refresh.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        refresh.addTarget(self, action: #selector(reloadData) , for: UIControl.Event.valueChanged)
        tblBookings.addSubview(refresh)
        
        addToggle()
        
        imgUnread.isHidden = ChatManager.unreadCount == 0
        ChatManager.shared.unreadDelegate = self
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.extendedLayoutIncludesOpaqueBars = false
        self.automaticallyAdjustsScrollViewInsets = false
        
        IK_NOTIFICATION.addObserver(self,
                                    selector: #selector(reloadData),
                                    name: NOTIFICATION_HOME,
                                    object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 1)
        imgUser.kf.indicatorType = .activity
        imgUser.kf.setImage(with: URL(string: IK_USER.profile_image),
                            placeholder: #imageLiteral(resourceName: "thumb_menu"),
                            options: nil,
                            progressBlock: nil,
                            completionHandler: nil)
        
        imgHomeUser.setBorder(cornerRadius: imgHomeUser.bounds.height/2, borderWidth: 1)
        imgHomeUser.kf.indicatorType = .activity
        imgHomeUser.kf.setImage(with: URL(string: IK_USER.profile_image),
                                placeholder: #imageLiteral(resourceName: "thumb_menu"),
                                options: nil,
                                progressBlock: nil,
                                completionHandler: nil)
        
        lblUserName.text = IK_USER.first_name + " " + IK_USER.last_name
        reloadData()
        
        guard isMovingToParent else {return}
        
        consMenuLead.constant = -view.bounds.width
        self.view.layoutIfNeeded()
        
        //Mixpanel.sharedInstance()?.track("Home Page Ace", properties:[:])
        
        //Utils.shared.logEventWithFlurry(EventName: "ace_home_tab", EventParam: [:])
        
        var nameStr = ""
        if let a = (arrAllGyms.filter { $0.id == IK_USER.gym}).first {
            nameStr = a.name
        }
        
        let sessionParam = ["interest" : IK_USER.sportId.map(String.init).joined(separator: ","),
                            "age_group" : arrAge[IK_USER.ageGroup],
                            "fitness_level" : arrFitness[IK_USER.fitness],
                            "gym" : nameStr,
                            "is_trainer" : IK_USER.userType == .ace ? true : false] as [String:Any]
        
        //Utils.shared.logFlurrySession(sessionParam: sessionParam)
        Utils.shared.logEventWithFlurry(EventName: "ace_home_tab", EventParam: sessionParam)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)  //320//375//414
        self.pagerView.itemSize =  self.pagerView.frame.size.applying(CGAffineTransform(scaleX:  transformWidth, y: 0.98))
        
        UIView.animate(withDuration: 0.35) {
            self.pagerView.alpha = 1
        }
        
        guard isMovingToParent else {return}
        hideSideMenu()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickMenu(_ sender: UIButton) {
        consMenuLead.constant =  0
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func clickChat(_ sender: UIButton) {
        let message = self.storyboard?.instantiateViewController(withIdentifier: "MessageList") as! MessageList
        self.navigationController?.pushViewController(message, animated: true)
    }
    
    ///--- Side menu
    
    @IBAction func clickProfile(_ sender: UIButton) {
        let profile = self.storyboard?.instantiateViewController(withIdentifier: "AceProfile") as! AceProfile
        _ = navigationController?.pushViewController(profile,animated:true)
    }
    
    @IBAction func clickHideMenu(_ sender: UIButton) {
        hideSideMenu()
    }
    
    @IBAction func clickMessage(_ sender: UIButton) {
        
        let obj = self.arrRequest[sender.tag]
        
        let chat = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! Chat
        chat.otherUser =  (id:obj.profileId,
                           name:obj.name,
                           image: obj.image)
        self.navigationController?.pushViewController(chat, animated: true)
    }
    
    @IBAction func clickHistoryMessage(_ sender: UIButton) {
        
        let obj = btnToggle.currentIndex == 0 ? arrRequest[sender.tag] : ( btnToggle.currentIndex == 1 ? arrBooking[sender.tag] : arrComplete[sender.tag])
        
        let chat = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! Chat
        chat.otherUser =  (id:obj.profileId,
                           name:obj.name,
                           image: obj.image)
        self.navigationController?.pushViewController(chat, animated: true)
    }
    
    @IBAction func clickNotification(_ sender: UIButton) {
        let notification = self.storyboard?.instantiateViewController(withIdentifier: "Notifications") as! Notifications
        self.navigationController?.pushViewController(notification, animated: true)
    }
    
    @IBAction func clickPayment(_ sender: UIButton) {
        let payment = self.storyboard?.instantiateViewController(withIdentifier: "AddBankDetail") as! AddBankDetail
        self.navigationController?.pushViewController(payment, animated: true)
    }
    
    @IBAction func clickHelp(_ sender: UIButton) {
        let help = self.storyboard?.instantiateViewController(withIdentifier: "Help") as! Help
        self.navigationController?.pushViewController(help, animated: true)
    }
    
    @IBAction func clickSettings(_ sender: UIButton) {
        let setting = self.storyboard?.instantiateViewController(withIdentifier: "SettingCust") as! SettingCust
        setting.isAce = true
        navigationController?.pushViewController(setting, animated: true)
        //   hideSideMenu()
    }
    
    @IBAction func sideMenuSwipped() {
        consMenuLead.constant =  -view.bounds.width
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK:- Cell IBAction
    //MARK:-
    
    @IBAction func clickAccept(_ sender: UIButton) {
        
        let obj = self.arrRequest[sender.tag]
        
        Utils.shared.alert(on: self,
                           message: obj.isBooking ? ACCEPT_BOOKING :  ACCEPT_AVAIL,
                           affirmButton: "Ok",
                           cancelButton: "Avbryt") { (index) in
                            
                            guard index == 0 else {return}
                            
                            if obj.isBooking {
                                WebConnect.replyBooking(requestId: obj.requestId , isAccept: true) { (status, message) in
                                    guard status else {
                                        Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                        return
                                    }
                                    self.reloadData()
                                }
                            }  else {
                                WebConnect.replyAvailabilty(requestId: obj.requestId , isAccept: true) { (status, message) in
                                    guard status else {
                                        Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                        return
                                    }
                                    self.reloadData()
                                }
                            }
        }
    }
    
    @IBAction func clickReject(_ sender: UIButton) {
        let obj = self.arrRequest[sender.tag]
        
        Utils.shared.alert(on: self,
                           message: obj.isBooking ? REJECT_BOOKING : REJECT_AVAIL,
                           affirmButton: "Ok",
                           cancelButton: "Avbryt") { (index) in
                            
                            guard index == 0 else {return}
                            
                            if obj.isBooking {
                                WebConnect.replyBooking(requestId: obj.requestId , isAccept: false) { (status, message) in
                                    guard status else {
                                        Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                        return
                                    }
                                    self.reloadData()
                                }
                            } else {
                                WebConnect.replyAvailabilty(requestId: obj.requestId , isAccept: false) { (status, message) in
                                    guard status else {
                                        Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                                        return
                                    }
                                    self.reloadData()
                                }
                            }
        }
    }
    
    @IBAction func clickCancelWorkout(_ sender: UIButton) {
        Utils.shared.alert(on: self, message: CONFIRM_CANCEL_WORKOUT, affirmButton: "Ja", cancelButton: "Avbryt") { (index) in
            guard index == 0 else {return}
            let  obj =   self.arrBooking[sender.tag]
            WebConnect.cancelBooking(bookingId:obj.requestId,completion: { status,message in
                guard status else {
                    Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil) ; return
                }
                self.reloadData()
            })
        }
    }
    
    //MARK:- Other
    //MARK:-
    
    fileprivate func setPlaceholder () {
        
        if btnToggle.currentIndex == 0 {
            self.tblBookings.isHidden = true
            self.pagerView.isHidden = false
        }
        else {
            self.pagerView.isHidden = true
            self.tblBookings.isHidden = false
        }
        
        var isShow = false
        
        if  btnToggle.currentIndex == 0 {
            isShow = self.arrRequest.count == 0
        }
        else if  btnToggle.currentIndex == 1 {
            isShow = self.arrBooking.count == 0
        }
        else if  btnToggle.currentIndex == 2 {
            isShow = self.arrComplete.count == 0
        }
        
        UIView.animate(withDuration: 0.25) {
            self.viewError.alpha = isShow ? 1 : 0
        }
    }
    
    @IBAction func reloadData() {
        WebConnect.getRequest { (status, arr, arrBook , arrComplete ,message) in
            self.refresh.endRefreshing()
            self.arrRequest = arr
            self.arrBooking = arrBook
            self.arrComplete = arrComplete
            self.setPlaceholder()
            
            /*
            UIView.animate(withDuration: 0.35, animations: {
                if self.arrRequest.count == 0 {
                    self.tblBookings.isHidden = false
                    self.pagerView.isHidden = true
                }
                else {
                    self.tblBookings.isHidden = true
                    self.pagerView.isHidden = false
                }
            })*/
            
            self.viewToggle.isHidden = false
            
            self.pagerView.reloadData()
            self.tblBookings.reloadData()
        }
    }
    
    /// Add toggle button
    fileprivate func addToggle() {
        btnToggle = TTSegmentedControl(frame: CGRect(origin: .zero, size: viewToggle.bounds.size))
        btnToggle.defaultTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnToggle.selectedTextColor = #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        btnToggle.thumbColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnToggle.useShadow = true
        btnToggle.useGradient = false
        btnToggle.layer.borderWidth = 2
        btnToggle.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        btnToggle.layer.cornerRadius = 0 //viewToggle.bounds.size.height / 2
        btnToggle.itemTitles = ["Forespørsel","Bestilt","Fullført"]
        btnToggle.defaultTextFont = UIFont(name: "HelveticaNeue", size: 17)!
        btnToggle.selectedTextFont = UIFont(name: "HelveticaNeue", size: 17)!
        btnToggle.containerBackgroundColor = .clear
        btnToggle.didSelectItemWith = { (index, title) -> () in
            self.setPlaceholder ()
            self.tblBookings.reloadData()
        }
        viewToggle.addSubview(btnToggle)
    }
    
    fileprivate func hideSideMenu() {
        consMenuLead.constant = -view.bounds.width
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func showDialogue(sender: UIButton) {
        Utils.shared.alert(on: self, message: "Når du endrer dato og tid så aksepterer du bestillingsforespørselen automatisk.", affirmButton: "Avbryt", cancelButton: "OK"){ (ind) in
            if ind == 1 {
                //Navigate to changeDateTimeVC
                
                let ind = self.pagerView.currentIndex
                let story = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "ChangeDateTimeVC") as! ChangeDateTimeVC
                vc.requestData = self.arrRequest[ind]
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @objc func navigateToNextVC(sender : UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.tblBookings)
        let indPath = self.tblBookings.indexPathForRow(at: buttonPosition)
        
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ChangeDateTimeVC") as! ChangeDateTimeVC
        
        vc.requestData = btnToggle.currentIndex == 0 ? arrRequest[indPath?.row ?? 0] : (btnToggle.currentIndex == 1 ? arrBooking[indPath?.row ?? 0] : arrComplete[indPath?.row ?? 0])
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func sendData(ind:Int) {
        
    }
   
}

extension Home : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return btnToggle.currentIndex == 0 ? arrRequest.count : ( btnToggle.currentIndex == 1 ? arrBooking.count : arrComplete.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellBooking", for: indexPath) as! CellBooking
        
        let obj = btnToggle.currentIndex == 0 ? arrRequest[indexPath.row] : (btnToggle.currentIndex == 1 ? arrBooking[indexPath.row] : arrComplete[indexPath.row])
        
        cell.imgUser.kf.indicatorType = .activity
        cell.imgUser.kf.setImage(with: URL(string: obj.image),
                                 placeholder: #imageLiteral(resourceName: "thumb_menu"),
                                 options: nil,
                                 progressBlock: nil,
                                 completionHandler: nil)
        
     //   cell.lblAmount.text = obj.amount
      //  cell.lblDuration.text =  String(describing:obj.startTime.getDifference(to: obj.endTime)) + " h"
      //  cell.lblTime.text = obj.startTime.string(format: "HH:mm") + " - " + obj.endTime.string(format: "HH:mm")
        cell.lblName.text = obj.name
        cell.lblDate.text = obj.date
        cell.lblSport.text = obj.location
      //  cell.icnSport.image = SportSmImage[obj.categoryName]
        
        cell.lblDate.text = obj.date + "\n" + obj.startTime.string(format: "HH:mm") + " - " + obj.endTime.string(format: "HH:mm")
        cell.lblDuration.text = Utils.shared.secondsToHoursMinutesSeconds(seconds: Int(obj.endTime.timeIntervalSince(obj.startTime))) + "H"
        cell.btnChangeDataTime.addTarget(self, action: #selector(navigateToNextVC(sender:)), for: .touchUpInside)
        
        cell.btnChangeDataTime.isHidden = btnToggle.currentIndex == 2
        cell.btnCancel.isHidden = btnToggle.currentIndex != 1
        cell.btnCancel.tag = indexPath.row
        cell.btnCancel.addTarget(self, action: #selector(clickCancelWorkout), for: .touchUpInside)
        
        cell.btnChat.tag = indexPath.row
        cell.btnChat.addTarget(self, action: #selector(clickHistoryMessage(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = btnToggle.currentIndex == 0 ? arrRequest[indexPath.row] : (btnToggle.currentIndex == 1 ? arrBooking[indexPath.row] : arrComplete[indexPath.row])
        
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "RequesterDetail") as! RequesterDetail
        detail.userDetail = (name: obj.name,
                             ageGroup:obj.age,
                             fitness:obj.fitness,
                             image:obj.image,
                             category : obj.categoryName)
        self.navigationController?.pushViewController(detail, animated: true)
    }
}

//MARK:- FSPager Delegate
//MARK:-

extension Home : FSPagerViewDelegate , FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return arrRequest.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! CellRequestCard
        
        let obj = arrRequest[index]
        
        if obj.isBooking {
            cell.lblTitle.text = "Bestillingsforespørsel"
            cell.btnAccept.setTitle("Aksepter", for: .normal)
            cell.btnReject.setTitle("Avvis", for: .normal)
            //cell.lblTime.text = ""
            //cell.iconTime.isHidden = true
            cell.lblTime.text = obj.startTime.string(format: "HH:mm") + " - " + obj.endTime.string(format: "HH:mm")
            
            cell.backImgView.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.3450980392, blue: 0.003921568627, alpha: 1)
            cell.btnChangeDtTime.isHidden = false
            cell.lblChangeDateTime.isHidden = false
            
        } else {
            cell.lblTitle.text = "Er du tilgjengelig?"
            // cell.lblAmount.isHidden = true
            cell.btnAccept.setTitle("Ja", for: .normal)
            cell.btnReject.setTitle("Nei", for: .normal)
            //cell.lblTime.text = obj.startTime.string(format: TIME_FORMAT) + " - " + obj.endTime.string(format: TIME_FORMAT)
            //cell.iconTime.isHidden = false
            
            cell.backImgView.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.831372549, blue: 0.003921568627, alpha: 1)
            cell.btnChangeDtTime.isHidden = true
            cell.lblChangeDateTime.isHidden = true
            
        }
        
        cell.btnChangeDtTime.addTarget(self, action: #selector(showDialogue(sender:)), for: .touchUpInside)
        cell.lblDate.text = obj.date
        
       // cell.lblPersons.text =  String(describing: obj.noPeople)
        cell.lblAddress.text = obj.location
        cell.lblHelp.text = obj.helpWith
        cell.lblName.text = obj.name
        cell.lblName.sizeToFit()

        //cell.viewName.setBorder(cornerRadius: 0, borderWidth: 2, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
//        cell.viewName.setBorder(cornerRadius: cell.viewName.bounds.size.height/2, borderWidth: 2, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        
//        cell.lblHelp.isHidden = obj.isBooking
//        cell.lblHelpTitle.isHidden = obj.isBooking
        
        cell.btnReject.tag = index
        cell.btnAccept.tag = index
        cell.btnChat.tag = index
      //  cell.btnChat.isHidden = !obj.isBooking
        
        cell.btnReject.addTarget(self, action: #selector(clickReject(_:)), for: .touchUpInside)
        cell.btnAccept.addTarget(self, action: #selector(clickAccept(_:)), for: .touchUpInside)
        cell.btnChat.addTarget(self, action: #selector(clickMessage(_:)), for: .touchUpInside)
        
        cell.imgUser.kf.indicatorType = .activity
        cell.imgUser.kf.setImage(with: URL(string: obj.image),
                                 placeholder: #imageLiteral(resourceName: "thumb_menu"),
                                 options: nil,
                                 progressBlock: nil,
                                 completionHandler: nil)
        
        let cordinate = CLLocationCoordinate2D(
            latitude: obj.lat, //obj.lat,
            longitude: obj.long //obj.long   ,10.6632316
        )
        
        cell.viewMap.setCenter(cordinate, animated: false)
        cell.viewMap.setRegion(MKCoordinateRegion(center:cordinate, span: MKCoordinateSpan(latitudeDelta: 0.008, longitudeDelta: 0.008)), animated: true)
        
        cell.viewMap.isHidden = true
        
        //        let annotation = MKPointAnnotation()
        //        annotation.coordinate = CLLocationCoordinate2D(
        //            latitude: obj.lat, //obj.lat,
        //            longitude: obj.long //obj.long   ,10.6632316
        //        )
        //        cell.viewMap.delegate = self
        //        cell.viewMap.tag = index
        //        cell.viewMap.setCenter(CLLocationCoordinate2D(
        //            latitude: obj.lat, //obj.lat,
        //            longitude: obj.long //obj.long   ,10.6632316
        //        ), animated: true)
        
        //        cell.viewMap.removeAnnotations(cell.viewMap.annotations)
        //        cell.viewMap.addAnnotation(annotation)
        
        //  cell.layoutIfNeeded()
        
        //cell.scrlView.contentSize = CGSize.init(width: 300, height: 500)//CGSizeMake(400, 500)
        //cell.scrlViewHeight.constant = 500
        //cell.scrlViewHeight.constant = 500
        cell.layoutIfNeeded()
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        let obj = arrRequest[index]
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "RequesterDetail") as! RequesterDetail
        detail.userDetail = (name: obj.name,
                             ageGroup:obj.age,
                             fitness:obj.fitness,
                             image:obj.image,
                             category : obj.categoryName)
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        //pageControl.currentPage =  pagerView.currentIndex
    }
}

extension Home : UnreadMessageDelegate {
    func unreadCountUpdate(count: Int) {
        imgUnread.isHidden = count == 0
    }
}

//MARK:- Mapview Delegate
//MARK:-

extension Home: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annoView = mapView.dequeueReusableAnnotationView(withIdentifier: "userAnnotation")
        if annoView == nil {
            annoView = MKAnnotationView(annotation: annotation, reuseIdentifier: "userAnnotation")
        }
        
        for view in annoView!.subviews {
            view.removeFromSuperview()
        }
        
        let imgUser = UIImageView()
        imgUser.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        imgUser.center = annoView!.center
        imgUser.setBorder(cornerRadius: 30, borderWidth: 0)
        imgUser.kf.indicatorType = .activity
        imgUser.kf.setImage(with: URL(string: arrRequest[mapView.tag].image),
                            placeholder: #imageLiteral(resourceName: "thumb_menu"),
                            options: nil,
                            progressBlock: nil,
                            completionHandler: nil)
        
        let wave1 = UIView()
        wave1.backgroundColor = #colorLiteral(red: 0.6823529412, green: 0.003921568627, blue: 0.337254902, alpha: 0.2)
        wave1.frame.size = CGSize(width: 80, height: 80)
        wave1.setBorder(cornerRadius: 40, borderWidth: 0)
        
        let wave2 = UIView()
        wave2.backgroundColor = #colorLiteral(red: 0.6823529412, green: 0.003921568627, blue: 0.337254902, alpha: 0.1)
        wave2.frame.size = CGSize(width: 110, height: 110)
        wave2.setBorder(cornerRadius: 55, borderWidth: 0)
        
        let lblName = UILabel()
        lblName.font = UIFont(name: "HelveticaNeue", size: 17)!
        lblName.text = arrRequest[mapView.tag].name
        lblName.textAlignment = .center
        lblName.backgroundColor = .white
        lblName.center = imgUser.center
        lblName.frame.origin.y = imgUser.center.y + 35
        lblName.sizeToFit()
        lblName.frame.size.height = 30
        lblName.frame.size.width += 50
        lblName.setBorder(cornerRadius: 15, borderWidth: 2, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        
        annoView?.addSubview(wave1)
        annoView?.addSubview(wave2)
        annoView?.addSubview(imgUser)
        lblName.center.x = imgUser.center.x
        wave1.center = imgUser.center
        wave2.center = imgUser.center
        annoView?.addSubview(lblName)
        
        //        mapView.setCenter(CLLocationCoordinate2D(
        //            latitude: annotation.coordinate.latitude, //obj.lat,
        //            longitude: annotation.coordinate.latitude //obj.long   ,10.6632316
        //        ), animated: true)
        
        return annoView
    }
}

class CellBooking : UITableViewCell {
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var icnSport: UIImageView!
    @IBOutlet var lblSport: UILabel!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnChat: UIButton!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet weak var btnChangeDataTime: UIButton!
    
    override func awakeFromNib() {
        self.dropShadow(0.3, radius: 3, color: .black)
        imgUser.setBorder(cornerRadius: imgUser.bounds.height/2, borderWidth: 0, borderColor: .clear)
//        viewContainer.setBorder(cornerRadius: 5, borderWidth: 0, borderColor: .clear)
    }
}
