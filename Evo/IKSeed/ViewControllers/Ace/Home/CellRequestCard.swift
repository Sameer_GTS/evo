//
//  CellRequestCard.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/23/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
//import FSPagerView
import MapKit

class CellRequestCard: FSPagerViewCell {
    
    @IBOutlet var viewCard: UIView!
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblPersons: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblHelp: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblHelpTitle: UILabel!
    @IBOutlet var iconTime: UIImageView!
    @IBOutlet var backImgView: UIImageView!
    
    //@IBOutlet var scrlViewHeight: NSLayoutConstraint!
    @IBOutlet var scrlView: UIScrollView!
    
    @IBOutlet var viewMap: MKMapView!
    
    @IBOutlet var btnReject: UIButton!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var btnChat: UIButton!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var viewName: UIView!
    
    @IBOutlet weak var lblChangeDateTime: UILabel!
    @IBOutlet weak var btnChangeDtTime: UIButton!
    
    override func awakeFromNib() {
//        self.viewCard.dropShadow(0.2, radius: 5, offset: CGSize(width: 0, height: 1), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        self.scrlView.dropShadow(0.2, radius: 5, offset: CGSize(width: 0, height: 1), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        btnReject.setBorder(cornerRadius: 0, borderWidth: 1, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        btnChat.setBorder(cornerRadius: 0, borderWidth: 1, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        imgUser.setBorder(cornerRadius: (imgUser.frame.height / 2), borderWidth: 2)
        
//        btnReject.setBorder(cornerRadius: ( btnReject.frame.height / 2), borderWidth: 1, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
//        btnChat.setBorder(cornerRadius: 20, borderWidth: 1, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        //viewName.setBorder(cornerRadius: ( viewName.frame.height / 2), borderWidth: 0)
    }
}
