//
//  Welcome.swift
//  Ikseed
//
//  Created by Chanchal Warde on 5/18/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit

class Welcome: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

}
