 //
//  AceProfile.swift
//  IKSeed
//
//  Created by Chanchal Warde on 4/4/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Spring

class AceProfile: UIViewController {
    
    let PLACEHOLDER_EXP = "Skriv litt mer detaljert om din erfaring, og hvorfor en kunde bør bestille deg. Husk at dette viser vi kundene."
    let PLACEHOLDER_COMMENT  = "Jo mer du deler med oss, desto bedre..."
    
    let sportList = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Categories") as! Categories
    
    let ROW_HEIGHT : CGFloat = 50
    let TAG_HELP = 1111
    let TAG_LINK = 1121
    let TAG_WHY = 1131
    let TAG_EXPERIENCE = 1141
    let TAG_QUALIFICATION = 1151
    let TAG_MEMBER = 1161
    let TAG_FIRST_NAME = 1125
    let TAG_LAST_NAME = 1325
    
    let picker = MediaPicker()
    let TAG_COVER = 1451
    let TAG_IMAGE = 1452
    
    //Fahad
    let NUTRITIONIST_BTN_HEIGHT: CGFloat = 30
    let NUTRITIONIST_TXT_HEIGHT: CGFloat = 40
    let NUTRITIONIST_LBL_HEIGHT: CGFloat = 17
    let PTNIVA_TOP_DEFAULT: CGFloat = 136
    let PTNIVA_TOP_ADJUSTED: CGFloat = 10
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var imgSport: UIImageView!
    @IBOutlet var lblSport: UILabel!
    @IBOutlet var lblExp: UILabel! {
        didSet {
            lblExp.text = "0"
        }
    }
    
    @IBOutlet var tblHelp: UITableView!
    @IBOutlet var tblContainer: UITableView!
    @IBOutlet var conTblHeight: NSLayoutConstraint!
    
    @IBOutlet var tfMember: UITextField! {
        didSet {
            tfMember.tag = TAG_MEMBER
        }
    }
    @IBOutlet var tfLink: UITextField! {
        didSet {
            tfLink.tag = TAG_LINK
        }
    }
//    @IBOutlet var tfWhy: UITextView! {
//        didSet {
//            tfWhy.tag = TAG_WHY
//        }
//    }
    @IBOutlet var tfQualification: UITextField! {
        didSet {
            tfQualification.tag = TAG_QUALIFICATION
        }
    }
    @IBOutlet var tvExperience: UITextView! {
        didSet {
            tvExperience.tag = TAG_EXPERIENCE
        }
    }
    
    @IBOutlet var btnSubmit: UIButton! {
        didSet {
            btnSubmit.dropShadow()
        }
    }
    
    @IBOutlet var btnBack: UIButton! /*{
        didSet {
            btnBack.dropShadow(0.8, radius: 10.0, offset: CGSize(width: 0, height: 2), color:  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) )
        }
    }*/
    
    @IBOutlet var btnCity: UIButton!
    @IBOutlet var btnNutrition: UIButton!
    @IBOutlet var btnIncrement: UIButton!
    @IBOutlet var btnDecrement: UIButton!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnUpdateProfile: UIButton!
    
    //Fahad
    @IBOutlet var imgCover: UIImageView!
    @IBOutlet var lblNutritionist: UILabel!
    @IBOutlet var vuNutritionist: UIView!
    
    @IBOutlet var btnNutritionistHeight: NSLayoutConstraint!
    @IBOutlet var lblNutritionistHeight: NSLayoutConstraint!
    @IBOutlet var imgNutritionistHeight: NSLayoutConstraint!
    @IBOutlet var vuNutritionistHeight: NSLayoutConstraint!
    
    @IBOutlet var ptNivaTop: NSLayoutConstraint!
    //
    
    @IBOutlet var btnPT: UIButton!
    @IBOutlet var btnCenter: UIButton!
    
    @IBOutlet var tfFirstName: UITextField!{
        didSet {
            tfFirstName.tag = TAG_FIRST_NAME
        }
    }
    
    @IBOutlet var tfLastName: UITextField!{
        didSet {
            tfLastName.tag = TAG_LAST_NAME
        }
    }
    
    @IBOutlet var lblEmail: UILabel!
    
    //---Alerts
    
    @IBOutlet var alertPT: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertPT,0.0)
        }
    }
    
    @IBOutlet var alertSport: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertSport,0.0)
        }
    }
    @IBOutlet var alertQualification: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertQualification,0.0)
        }
    }
    @IBOutlet var alertExp: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertExp,0.0)
        }
    }
    @IBOutlet var alertHelp: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertHelp,0.0)
        }
    }
    @IBOutlet var alertLink: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertLink,0.0)
        }
    }
//    @IBOutlet var alertAce: SpringImageView!{
//        didSet{
//            Utils.shared.animateFadeOut(alertAce,0.0)
//        }
//    }
    @IBOutlet var alertCity: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertCity,0.0)
        }
    }
    @IBOutlet var alertMember: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertMember,0.0)
        }
    }
    @IBOutlet var alertFirstName: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertFirstName,0.0)
        }
    }
    @IBOutlet var alertLastName: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertLastName,0.0)
        }
    }
    @IBOutlet var alertExpDetail: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertExpDetail,0.0)
        }
    }
    @IBOutlet var conNavHeight: NSLayoutConstraint!
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var viewHeight : CGFloat {
        return btnSubmit.frame.origin.y + btnSubmit.frame.height + 50
    }
    
    fileprivate var arrHelp : [String] = [""]
    fileprivate var strHelp = ""
    fileprivate var experience = 0
    fileprivate var isEdited = false
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sportList.delegate = self
        
        Utils.shared.logEventWithFlurry(EventName: "ace_profile_view", EventParam: [:])
        
        if #available(iOS 11.0, *) {
            let top = UIApplication.shared.keyWindow?.safeAreaInsets.top
            conNavHeight.constant = conNavHeight.constant + top!
            //  tblAce.contentInset = UIEdgeInsets(top: CGFloat(Float(top!)), left: 0, bottom: 0, right: 0)
        }
        else {
            let top = UIApplication.shared.statusBarFrame.size.height
            tblContainer.contentInset = UIEdgeInsets(top: CGFloat(-Float(top)), left: 0, bottom: 0, right: 0)
        }
        
        WebConnect.getAceProfile { (data, status, message) in
            guard status else { return }
            
            self.setData(data!)
            self.editingMode(on : true)
        }
        picker.delegate = self
        //Fahad
        picker.targetVC = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickCoverImage(_ sender: UIButton) {
        picker.isSquared = false
        picker.showPicker(vc: self)
        picker.tag = TAG_COVER
    }
    
    @IBAction func clickProfilePicture(_ sender: UIButton) {
        picker.isSquared = true 
        picker.showPicker(vc: self)
        picker.tag = TAG_IMAGE
    }
    
    @IBAction func clickIncrease(_ sender: UIButton) {
        guard experience < 50 else {
            return
        }
        isEdited = true
        experience += 1
        lblExp.text = String(describing:experience)
    }
    
    @IBAction func clickDecrease(_ sender: UIButton) {
        guard experience > 0  else {
            return
        }
        isEdited = true
        experience -= 1
        lblExp.text = String(describing:experience)
    }
    
    @IBAction func clickBack(_ sender: UIButton) {
        
        guard isEdited else {
            navigationController?.popViewController(animated: true) ; return
        }
        
        Utils.shared.alert(on: self, message: CONFIRM_UPDATE, affirmButton: "Ja", cancelButton: "Nei") { (index) in
            guard index == 0 else {
                self.navigationController?.popViewController(animated: true) ;  return
            }
            self.saveDetails()
        }
    }
    
    @IBAction func clickSport(_ sender: UIButton) {
        navigationController?.pushViewController(sportList, animated: true)
    }
    
    @IBAction func clickNutrition(_ sender: UIButton) {
        isEdited = true
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func clickAddHelp(_ sender: UIButton) {
        
        if let cell = tblHelp.cellForRow(at: IndexPath(row: 0, section: 0)) as? CellHelp {
            
            guard !cell.tfComment.text!.isEmpty()  else {
                Utils.shared.alert(on: self, message: ERROR_HELP, affirmButton: "Ok", cancelButton: nil)
                return
            }
            
            guard !arrHelp.contains(cell.tfComment.text!) else {
                Utils.shared.alert(on: self, message: ERROR_HELP_EXISTS, affirmButton: "Ok", cancelButton: nil)
                return
            }
            
            guard arrHelp.count < 5 else {
                return
            }
            
            Utils.shared.animateFadeOut(alertHelp)
            arrHelp.append(cell.tfComment.text!)
            cell.tfComment.text = nil
            tblHelp.reloadData()
            isEdited = true

            self.conTblHeight.constant = CGFloat(arrHelp.count+1) * self.ROW_HEIGHT
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                self.updateViewSize()
            }
        }
    }
    
    @IBAction func clickDeleteHelp(_ sender: UIButton) {
        arrHelp.remove(at: sender.tag)
        self.conTblHeight.constant = CGFloat(arrHelp.count+1) * self.ROW_HEIGHT
        tblHelp.reloadData()
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.updateViewSize()
        }
        isEdited = true
    }
    
    @IBAction func clickCity(_ sender: UIButton) {
        let picker =  ListPicker.showPicker(on: self, arrList: arrCities)
        picker.onSelectItem = { obj in
            Utils.shared.animateFadeOut(self.alertCity)
            sender.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
            self.isEdited = true
        }
    }
    
    @IBAction func clickEdit(_ sender: UIButton) {
        if sender.isSelected {
            saveDetails()
        } else {
            sender.isSelected = true
            //editingMode(on: true)
        }
    }
    
    @IBAction func clickPtLevel(_ sender: UIButton) {
        let picker =  ListPicker.showPicker(on: self,title : "PT Nivå" , arrList: arrPTLevel)
        picker.onSelectItem = { obj in
            Utils.shared.animateFadeOut(self.alertPT)
            sender.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
            self.isEdited = true

        }
    }
    
    @IBAction func clickCenter(_ sender: UIButton) {
        let gym = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GymList") as! GymList
        
        // jitendra
        if sender.tag != 0 {
            gym.selectedGymId = sender.tag
        }
        
        gym.isHandler = true
        gym.showAll = true
        gym.onSelect = { obj in
            sender.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
            
            self.isEdited = true
        }
        self.navigationController?.pushViewController(gym , animated: true)
    }
    
    //MARK:- Other
    //MARK:-
    
    fileprivate func editingMode(on:Bool) {
        
//        tfFirstName.isUserInteractionEnabled = on
//        tfLastName.isUserInteractionEnabled = on
//        tfMember.isUserInteractionEnabled = on
//        tfWhy.isUserInteractionEnabled = on
//        tfLink.isUserInteractionEnabled = on
//        tvExperience.isUserInteractionEnabled = on
//        btnCity.isUserInteractionEnabled = on
//        tfQualification.isUserInteractionEnabled = on
//        btnNutrition.isUserInteractionEnabled = on
      //  btnUpdateProfile.isUserInteractionEnabled = on
        
        btnIncrement.isHidden = !on
        btnDecrement.isHidden = !on
//
//        if on {
//            if arrHelp.count < 5 {
//                arrHelp.insert(" ", at: 0)
//            }
//        }else {
//            if arrHelp.first == " " {
//                arrHelp.remove(at: 0)
//            }
//        }
        tblHelp.reloadData()
        self.conTblHeight.constant = CGFloat(arrHelp.count+1) * self.ROW_HEIGHT
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.updateViewSize()
        }
    }
    
    //Fahad
    fileprivate func nutrionistInfo(_ isNutrionist: Bool) {
        btnNutritionistHeight.constant = isNutrionist ? NUTRITIONIST_BTN_HEIGHT : 0
        lblNutritionistHeight.constant = isNutrionist ? NUTRITIONIST_LBL_HEIGHT : 0
        imgNutritionistHeight.constant = isNutrionist ? NUTRITIONIST_LBL_HEIGHT : 0
        vuNutritionistHeight.constant = isNutrionist ? NUTRITIONIST_TXT_HEIGHT : 0
        btnNutrition.alpha = isNutrionist ? 1 : 0
        lblNutritionist.alpha = isNutrionist ? 1 : 0
        alertQualification.alpha = isNutrionist ? 1 : 0
        vuNutritionist.alpha = isNutrionist ? 1 : 0
        
        ptNivaTop.constant = isNutrionist ? PTNIVA_TOP_DEFAULT : PTNIVA_TOP_ADJUSTED
        
        btnNutrition.isSelected = isNutrionist
//        self.view.layoutIfNeeded()
    }
    
    fileprivate func setData(_ result : NSDictionary) {
        
        tfFirstName.text = IK_USER.first_name
        tfLastName.text = IK_USER.last_name
        lblEmail.text = IK_USER.email
        tfLink.text =  String(describing:result["links"]!)
        experience =   result["experience"] as! Int //Int(String(describing:result["experience"]))!
        tvExperience.text = String(describing:result["about_experience_work"]!)
        lblExp.text = String(describing:result["experience"]!)
        arrHelp = String(describing:result["help_inputs"]!).split(separator: ",").map(String.init)
        
        tfMember.text  = String(describing:result["member_of"]!)

        btnPT.setTitle(String(describing:result["pt_level"]!) , for: .normal)
        btnCenter.setTitle(String(describing:result["gym"]!) , for: .normal)
        
        btnPT.tag = result["pt_level_id"] as! Int
        btnCenter.tag = result["gym_id"] as! Int
        
        tfQualification.text = String(describing:result["nutritionist_degree"]!)
        nutrionistInfo(String(describing:result["i_am_nutritionist"]!) == "Y")
        //btnNutrition.isSelected = String(describing:result["i_am_nutritionist"]!) == "Y"
        
        //arrHelp.count > 0 ? arrHelp.insert("", at: 0) : arrHelp.append("")
        //   lblSport.text = String(describing:result["cat_name"]!)
        // imgSport.image = SportImage[lblSport.text!]
        //  imgSport.tag =  result["cat_id"] as! Int
        //lblSport.tag = result["cat_id"] as! Int

       // btnCity.tag =  result["city_id"] as! Int
      //  let cityName = arrCities.filter({$0.id == btnCity.tag}).first?.name ?? ""
        
       // btnCity.setTitle(cityName, for: .normal)
        
        tblHelp.reloadData()
        self.conTblHeight.constant = CGFloat(arrHelp.count+1) * self.ROW_HEIGHT
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.updateViewSize()
        }
        
        imgProfile.setBorder(cornerRadius: imgProfile.bounds.height/2, borderWidth: 1.5, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
        imgProfile.kf.indicatorType = .activity
        imgProfile.kf.setImage(with: URL(string: IK_USER.profile_image), 
                               placeholder: #imageLiteral(resourceName: "thumb_menu"),
                               options: nil,
                               progressBlock: nil,
                               completionHandler: nil)
        
        imgCover.kf.indicatorType = .activity
        imgCover.kf.setImage(with: URL(string: String(describing:result["bg_img"] ?? "abc")),
                               placeholder: #imageLiteral(resourceName: "icn_no_image"),
                               options: nil,
                               progressBlock: nil,
                               completionHandler: { (image, error, _, nil) in
                                guard image != nil else { return}
                                self.imgCover.contentMode = .scaleAspectFill
                                self.imgCover.clipsToBounds = true
        })
        
        //        "about_experience_work" = "Great experience";
        //        "become_ace_reasons" = "Money earnings";
        //        "cat_id" = 656;
        //        "cat_name" = Golf;
        //        experience = 0;
        //        "first_name" = Chirag;
        //        "help_inputs" = "first,last";
        //        "how_far" = "Not avialable";
        //        "last_name" = jain;
        //        links = "www.google.com";
        //        location = "Not avialable";
        //        "location_name" = "";
        //        "profile_id" = 11521627842;
        //        "profile_image" = "";
        //        rating = 5;
        //        ssno = ssno;
        //        "user_email" = "chirag@gmail.com";
        //        "zip_code" = 1;
    }
    
    /// Function to updated view size and table view content size whenever new layover added or deleted.
    fileprivate func updateViewSize() {
        var frame = self.viewContainer.frame
        frame.size.height = self.viewHeight
        self.viewContainer.frame = frame
        self.tblContainer.contentSize = frame.size
        self.tblContainer.setNeedsDisplay()
        self.viewContainer.setNeedsDisplay()
    }
    
    fileprivate func validation() -> Bool {
        var isValid = true
        
        //        if btnSport.tag == 0 {
        //            Utils.shared.animatePop(alertSport)
        //            isValid = false
        //        }
        
        
        
        if btnPT.tag == 0 {
            Utils.shared.animatePop(alertPT)
            isValid = false
        }
        
//        if btnNutrition.isSelected && tfQualification.text!.isEmpty() {
//            Utils.shared.animatePop(alertQualification)
//            isValid = false
//        }
        
        if experience == 0 {
            Utils.shared.animatePop(alertExp)
            isValid = false
        }
        
        if tvExperience.text!.isEmpty() || tvExperience.text! == PLACEHOLDER_EXP {
            Utils.shared.animatePop(alertExpDetail)
            isValid = false
        }
        
        if arrHelp.count == 0 && strHelp.isEmpty() {
            Utils.shared.animatePop(alertHelp)
            isValid = false
        }
        
        if tfLink.text!.isEmpty() {
            Utils.shared.animatePop(alertLink)
            isValid = false
        }
        
//        if tfWhy.text!.isEmpty() || tfWhy.text! ==  PLACEHOLDER_COMMENT {
//            Utils.shared.animatePop(alertAce)
//            isValid = false
//        }
//
//        if btnCity.tag == 0 {
//            Utils.shared.animatePop(alertCity)
//            isValid = false
//        }
        
        if btnCenter.tag == 0 {
            Utils.shared.animatePop(alertCity)
            isValid = false
        }
        
        
        if tfMember.text!.isEmpty() {
            Utils.shared.animatePop(alertMember)
            isValid = false
        }
        
        return isValid
    }
    
    fileprivate func saveDetails() {
        
        guard validation() else {
            return
        }
        
        if arrHelp.count == 1 && arrHelp.first!.isEmpty() {
            arrHelp[0] = strHelp
        }
        else if !strHelp.isEmpty() {
            arrHelp.append(strHelp)
        }
        
        for a in  arrHelp {
            if a.isEmpty() {
                arrHelp.removeFirst()
            }
        }
        
        self.view.endEditing(true)
        
        let param : [String : Any] = [  "gym_id" : "\(btnCenter.tag)",
                                        "pt_level_id" : "\(btnPT.tag)",
                                        "nutritionist" : btnNutrition.isSelected ? "Y" : "N",
                                       // "city_id" : "\(btnCity.tag)",
                                        "nutritionist_degree": btnNutrition.isSelected ? tfQualification.text!.trimString: "",
                                        "zip_code": "1",
                                        "user_id": IK_USER.profile_id,
                                      //  "cat_id": "\(imgSport.tag)",
                                        "experience": "\(experience)" ,
                                        "about_experience_work": tvExperience.text!.trimString ,
                                        "help_inputs":arrHelp.joined(separator: ","),
                                        "ssno":"ssno",
                                        "links":tfLink.text!.trimString,
                                       // "become_ace_reasons":tfWhy.text!.trimString,
                                        "member_of":tfMember.text!.trimString,
                                        "first_name":tfFirstName.text!.trimString,
                                        "last_name":tfLastName.text!.trimString,
                                        "country":"Norway"]
        
        var uploading : [String:URL] = [:]
        
        if imgCover.tag == TAG_COVER {
//           uploading["bg_image"] = FileManager.default.saveToDocs(data:UIImageJPEGRepresentation(imgCover.image!, 0.6)!, name: "coverImage.png")
            uploading["bg_image"] = FileManager.default.saveToDocs(data:imgCover.image!.jpegData(compressionQuality: 0.6)!, name: "coverImage.png")
        }
        if imgProfile.tag == TAG_IMAGE {
//            uploading["profile_image"] = FileManager.default.saveToDocs(data:UIImageJPEGRepresentation(imgProfile.image!, 0.6)!, name: "profileImage.png")
            uploading["profile_image"] = FileManager.default.saveToDocs(data:imgProfile.image!.jpegData(compressionQuality: 0.6)!, name: "profileImage.png")
        }
         
        WebConnect.updateAceProfile(param: param, images: uploading) { (status, message,imageUrl) in
            
            guard status else { Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil) ; return }
            
            if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                
                let newDic = NSMutableDictionary(dictionary: dict)
                newDic.setValue(self.tfFirstName.text!.trimString, forKey: "first_name")
                newDic.setValue(self.tfLastName.text!.trimString, forKey: "last_name")
                if imageUrl != nil { newDic.setValue(imageUrl, forKey: "profile_image") }
                
                IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                IK_DEFAULTS.synchronize()
            }
            
            self.isEdited = false
            self.btnEdit.isSelected = false
            self.navigationController?.popViewController(animated: true)
           // self.editingMode(on: false)
        }
    }
}

extension AceProfile : SportDelegate {
    func didSelect(sport: Sport) {
        Utils.shared.animateFadeOut(alertSport)
        //   btnSport.tag = sport.id
        lblSport.text = sport.name
        // btnSport.setImage(sport.image, for: .normal)
    }
}

//MARK:- Tableview Delegate
//MARK:-

extension AceProfile : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : arrHelp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellHelp", for: indexPath) as! CellHelp
        
        guard indexPath.section == 1 else {
            cell.tfComment.isUserInteractionEnabled =  true
            cell.tfComment.delegate = self
            cell.tfComment.tag = TAG_HELP
            
            cell.btnAdd.isHidden = false
            cell.btnCancel.isHidden = true
            cell.btnAdd.addTarget(self, action: #selector(clickAddHelp), for: .touchUpInside)
            return cell
        }
        
        cell.btnAdd.isHidden = true
        cell.btnCancel.isHidden = false
        
        cell.btnCancel.tag = indexPath.row
        cell.btnCancel.addTarget(self, action: #selector(clickDeleteHelp), for: .touchUpInside)
        
        cell.tfComment.text = arrHelp[indexPath.row]
        cell.tfComment.isUserInteractionEnabled =  false

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ROW_HEIGHT
    }
}

//MARK:- Textfield Delegate
//MARK:-

extension AceProfile : UITextFieldDelegate, UITextViewDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == TAG_HELP {
            strHelp = textField.text!
        }
        updateViewSize()
        isEdited = true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == TAG_FIRST_NAME {
            Utils.shared.animateFadeOut(alertFirstName)
        }
        else if textField.tag == TAG_LAST_NAME {
            Utils.shared.animateFadeOut(alertLastName)
        }
        else if textField.tag == TAG_HELP {
            strHelp = textField.text!
            Utils.shared.animateFadeOut(alertHelp)
        }
        else if textField.tag == TAG_QUALIFICATION {
            Utils.shared.animateFadeOut(alertQualification)
        }
        else if textField.tag == TAG_LINK {
            Utils.shared.animateFadeOut(alertLink)
        }
        else if textField.tag == TAG_MEMBER {
            Utils.shared.animateFadeOut(alertMember)
        }
        
        Utils.shared.animateFadeOut(alertCity)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.tag == TAG_EXPERIENCE {
            if textView.text!.isEmpty() {
                textView.text = PLACEHOLDER_EXP
            }
        }
//        else if textView.tag == TAG_WHY {
//            if textView.text!.isEmpty() {
//                textView.text = PLACEHOLDER_COMMENT
//            }
//        }
        isEdited = true
        updateViewSize()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.tag == TAG_EXPERIENCE {
            Utils.shared.animateFadeOut(alertExp)
            if textView.text == PLACEHOLDER_EXP  {
                textView.text = ""
            }
        }
//        else if textView.tag == TAG_WHY {
//            Utils.shared.animateFadeOut(alertAce)
//            if textView.text == PLACEHOLDER_COMMENT  {
//                textView.text = ""
//            }
//        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfFirstName {
            tfLastName.becomeFirstResponder()
        }
        else if textField == tfLastName {
            tfQualification.becomeFirstResponder()
        }
        else if textField == tfQualification {
            tvExperience.becomeFirstResponder()
        }
        else if textField == tfLink  {
            tfMember.becomeFirstResponder()
        }
//        else if textField == tfMember {
//            tfWhy.becomeFirstResponder()
//        }
        else {
            tfLink.becomeFirstResponder()
        }
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard text == "\n" else {
            return true
        }
        
        if textView == tvExperience {
            if let cell = tblHelp.cellForRow(at: IndexPath(row: 0, section: 0)) as? CellHelp {
                cell.tfComment.becomeFirstResponder()
            }
        }
//        else if textView == tfWhy {
//            textView.resignFirstResponder()
//        }
        return false
    }
    
}

//MARK:- ImagePicker delegate
//MARK:-

extension AceProfile : ImagePickerDelegate {
    func didSelect(image: UIImage) {
        isEdited = true
        if picker.tag == TAG_COVER {
            imgCover.contentMode = .scaleAspectFill
            imgCover.image = image
            imgCover.tag = TAG_COVER
        } else {
            imgProfile.contentMode = .scaleAspectFill
            imgProfile.image = image
            imgProfile.isHighlighted = false
            imgProfile.tag = TAG_IMAGE
        }
    }
}
