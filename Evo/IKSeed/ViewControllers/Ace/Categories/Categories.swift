//
//  Categories.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/9/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit

protocol SportDelegate : class {
    func didSelect(sport:Sport)
    func didSelectNutrition()
}

extension SportDelegate{
    func didSelect(sport:Sport) {    }
    func  didSelectNutrition() {     }
}

class Categories: UIViewController {
    
    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var collCategories: UICollectionView!
    @IBOutlet var btnDone: UIButton! {
        didSet {
            btnDone.dropShadow(radius: 5)
        }
    }
    @IBOutlet var viewContainer: UIView! {
        didSet {
//            viewContainer.setBorder(cornerRadius: 10, borderWidth: 0, borderColor: .clear)
        }
    }
    @IBOutlet var viewShadow: UIView!
        {
        didSet {
            viewShadow.dropShadow(radius: 18,color : .black)
        }
    }
    @IBOutlet var lblTitle: UILabel!
    
    //MARK:- Variable
    //MARK:-
    
    fileprivate var arrSports : [Sport] = []
    weak var delegate  : SportDelegate?
    var isBecomeAce : Bool = false
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        WebConnect.getSport(type: "A") { (status, arr) in
            self.arrSports = arr
            self.collCategories.reloadData()
            for i in 0..<self.arrSports.count {
                self.arrSports[i].isSelected = false
            }
        }
        
        guard isBecomeAce else { return  }
        lblTitle.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction Method
    //MARK:-
    
    @IBAction func clickDone(_ sender: UIButton) {
        let age = self.storyboard?.instantiateViewController(withIdentifier: "AgeGroup") as! AgeGroup
        self.navigationController?.pushViewController(age, animated: true)
    }
    
    //MARK:- Other Method
    //MARK:-
    
}

extension Categories : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSports.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCategory", for: indexPath) as! CellCategory
      //  cell.imgIcon.image = arrSports[indexPath.row].image
        cell.lblName.text = arrSports[indexPath.row].name
        
        cell.lblName.textColor = arrSports[indexPath.row].isSelected ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1)
        cell.viewBorder.backgroundColor = arrSports[indexPath.row].isSelected ? #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width / 3,
                      height: collectionView.bounds.height  / 8.3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        for i in 0..<arrSports.count {
            arrSports[i].isSelected = false
        }
        
        arrSports[indexPath.row].isSelected = true
        collectionView.reloadData()
        
        delegate?.didSelect(sport:  arrSports[indexPath.row])
        _ = navigationController?.popViewController(animated: true)
    }
}


//MARK:- Cell
//MARK:-



class CellCategory : UICollectionViewCell {
    
    @IBOutlet var imgIcon : UIImageView!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var viewBorder: UIView!
    
    override func awakeFromNib() {
        viewBorder.setBorder(cornerRadius: 0, borderWidth: 1.5, borderColor: #colorLiteral(red: 0.9137254902, green: 0.1725490196, blue: 0.4, alpha: 1))
    }
    
}
