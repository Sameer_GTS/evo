//
//  AddPaymentDetail.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/26/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.

import UIKit
import Spring

class AddBankDetail: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    enum Tags : Int {
        case firstName = 101
        case lastName
        case address
        // case city
        case postalCode
        case iban
        case dob
        case idNo
    }
    
    let picker = MediaPicker()
    let arrIdentityType = [(id:1,name:"Førekort"),
                           (id:2,name:"Annet identifiseringsdokument"),
                           (id:3,name:"Pass"),
                           (id:4,name:"Personnr")]
    
    //MARK:- IBOutlet
    //MARK:-
    @IBOutlet var tblPayment: UITableView!
    @IBOutlet var viewContainer : UIView!
    
    @IBOutlet var tfFirstName: UITextField! {
        didSet {
            tfFirstName.tag = Tags.firstName.rawValue
        }
    }
    @IBOutlet var tfLastName: UITextField! {
        didSet {
            tfLastName.tag = Tags.lastName.rawValue
        }
    }
    @IBOutlet var tfAddress: UITextField!{
        didSet {
            tfAddress.tag = Tags.address.rawValue
        }
    }
    //    @IBOutlet var tfCity: UITextField!{
    //        didSet {
    //            tfCity.tag = Tags.city.rawValue
    //        }
    //    }
    @IBOutlet var tfPostalCode: UITextField!{
        didSet {
            tfPostalCode.tag = Tags.postalCode.rawValue
        }
    }
    @IBOutlet var tfIBAN: UITextField!{
        didSet {
            tfIBAN.tag = Tags.iban.rawValue
        }
    }
    @IBOutlet var tfDOB: UITextField!{
        didSet {
            tfDOB.tag = Tags.dob.rawValue
            tfDOB.inputView = datePicker
        }
    }
    @IBOutlet var tfIDNo: UITextField!{
        didSet {
            tfIDNo.tag = Tags.idNo.rawValue
        }
    }
    @IBOutlet var btnDone: UIButton! {
        didSet {
            btnDone.dropShadow()
        }
    }
    @IBOutlet var btnIdentity: UIButton!
    @IBOutlet var btnImage: UIButton!
    @IBOutlet var btnCountry: UIButton!
    @IBOutlet var btnCity: UIButton!
    
    @IBOutlet var conNavHeight: NSLayoutConstraint!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var btnBack: UIButton!
    
    ///---- Alerts
    
    @IBOutlet var alertFirstName: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertFirstName,0.0)
        }
    }
    @IBOutlet var alertAddress: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertAddress,0.0)
        }
    }
    @IBOutlet var alertLastName: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertLastName,0.0)
        }
    }
    @IBOutlet var alertCity: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertCity,0.0)
        }
    }
    @IBOutlet var alertCountry: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertCountry,0.0)
        }
    }
    @IBOutlet var alertPostalCode: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertPostalCode,0.0)
        }
    }
    @IBOutlet var alertIBAN: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertIBAN,0.0)
        }
    }
    @IBOutlet var alertDob: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertDob,0.0)
        }
    }
    @IBOutlet var alertIdentityType: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertIdentityType,0.0)
        }
    }
    @IBOutlet var alertIDNo: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertIDNo,0.0)
        }
    }
    @IBOutlet var alertDocument: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertDocument,0.0)
        }
    }
    
    //MARK:- Variable
    //MARK:-
    fileprivate var datePicker : UIDatePicker! = UIDatePicker()
    /// Get updated view height whenever layover list updated and height increase for table.
    fileprivate var viewHeight : CGFloat {
        return btnDone.frame.origin.y + btnDone.frame.height + 20
    }
    
    var isForword = false
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.datePickerMode = .date
        if #available(iOS 11.0, *) {
            let top = UIApplication.shared.keyWindow?.safeAreaInsets.top
            conNavHeight.constant = conNavHeight.constant + top!
            var frame = viewHeader.frame
            frame.size.height += top!
            viewHeader.frame = frame
        }
        else {
            let top = UIApplication.shared.statusBarFrame.size.height
            tblPayment.contentInset = UIEdgeInsets(top: CGFloat(-Float(top)), left: 0, bottom: 0, right: 0)
        }
        
        if isForword {
            conNavHeight.constant = 0
            self.view.setNeedsDisplay()
        }
        
        WebConnect.getBankingDetail { (details, message) in
            if let details = details {
                self.tfFirstName.text = details.firstName
                self.tfLastName.text = details.lastName
                self.tfAddress.text = details.address
                self.btnCity.setTitle(details.city, for: .normal)
                self.btnCity.setTitleColor(.black, for: .normal)
                self.tfPostalCode.text = details.postalCode
                self.tfDOB.text = details.dob
                self.tfIBAN.text = details.ibanNumber
                self.btnIdentity.setTitle(details.idType, for: .normal)
                self.btnIdentity.setTitleColor(.black, for: .normal)
                self.tfIDNo.text = details.idNumber
                
                self.btnImage.imageView?.kf.setImage(with: URL(string: details.docImage),
                                                     placeholder: nil,
                                                     options: nil,
                                                     progressBlock: nil,
                                                     completionHandler: { (image, error, _, _) in
                                                        guard image != nil else {return}
                                                        self.btnImage.setImage(image, for: .normal)
                })
            } else {
                Utils.shared.alert(on: self, message: message, affirmButton: "OK", cancelButton: nil)
            }
            
            self.updateViewSize()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickCity(_ sender: UIButton) {
        let picker =  ListPicker.showPicker(on: self, arrList: arrCities)
        picker.onSelectItem = { obj in
            Utils.shared.animateFadeOut(self.alertCity)
            sender.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
            sender.setTitleColor(.black, for: .normal)
        }
    }
    
    @IBAction func clickCountry(_ sender: UIButton) {
        let picker =  ListPicker.showPicker(on: self, arrList: arrCountry)
        picker.onSelectItem = { obj in
            Utils.shared.animateFadeOut(self.alertCountry)
            sender.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
            sender.setTitleColor(.black, for: .normal)
        }
    }
    
    @IBAction func clickIdentityType(_ sender: UIButton) {
        let picker =  ListPicker.showPicker(on: self, title: "Select Identity Type", arrList: arrIdentityType)
        picker.onSelectItem = { obj in
            Utils.shared.animateFadeOut(self.alertIdentityType)
            sender.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
            sender.setTitleColor(.black, for: .normal)
        }
    }
    
    @IBAction func clickDocument(_ sender: UIButton) {
        //Fahad
        picker.targetVC = self
        picker.isSquared = true
        
        picker.showPicker(vc: self)
        picker.delegate = self
    }
    
    @IBAction func clickSubmit(_ sender: UIButton) {
        guard validation() else {  return  }
        
        self.view.endEditing(true)
        
        let param = ["user_id":IK_USER.profile_id,
                     "name":self.tfFirstName.text!.trimString + " " + tfLastName.text!.trimString,
                     "email":IK_USER.email,
                     "account_number":tfIBAN.text!.trimString,
                     "bank_country":"NO" ,
                     "city":btnCity.titleLabel!.text! ,
                     "address":tfAddress.text!.trimString ,
                     "postal":tfPostalCode.text!.trimString,
                     "doc_type":btnIdentity.titleLabel!.text! ,
                     "doc_number":tfIDNo.text!.trimString,
                     "dob":datePicker.date.string(format: "yyyy-MM-dd"),
                     "first_name":tfFirstName.text!.trimString,
                     "last_name":tfLastName.text!.trimString] as [String : Any]
        
        WebConnect.addAcePaymentDetail(param: param, identity: btnImage.currentImage!) { (status, message) in
            Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
            
            guard  self.isForword  else { return }
            
            /*
            let home = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! Home
            let navigation = UINavigationController(rootViewController:home )
            navigation.isNavigationBarHidden = true
            
            UIView.transition(from: IK_DELEGATE.window!.rootViewController!.view,
                              to: navigation.view,
                              duration: 0.45,
                              options: .transitionCrossDissolve,
                              completion: { (_) in
                                IK_DELEGATE.window!.rootViewController = navigation
            })*/
            
            IK_DELEGATE.TabBarConfiguration(true)

        }
    }
    
    //MARK:- Other
    //MARK:-
    
    /// Function to updated view size and table view content size whenever new layover added or deleted.
    fileprivate func updateViewSize() {
        var frame = self.viewContainer.frame
        frame.size.height = self.viewHeight
        self.viewContainer.frame = frame
        self.tblPayment.contentSize = frame.size
        self.tblPayment.setNeedsDisplay()
        self.viewContainer.setNeedsDisplay()
    }
    
    fileprivate func validation() -> Bool {
        
        var valid = true
        
        if tfFirstName.text!.isEmpty() { Utils.shared.animatePop(alertFirstName); valid = false }
        
        if tfLastName.text!.isEmpty() { Utils.shared.animatePop(alertLastName);  valid = false }
        
        if tfAddress.text!.isEmpty() { Utils.shared.animatePop(alertAddress);  valid = false }
        
        if btnCity.tag == 0 { Utils.shared.animatePop(alertCity); valid = false }
        
        if tfPostalCode.text!.isEmpty() { Utils.shared.animatePop(alertPostalCode); valid = false }
        
        if tfDOB.text!.isEmpty() { Utils.shared.animatePop(alertDob);  valid = false  }
        
        if tfIBAN.text!.isEmpty() { Utils.shared.animatePop(alertIBAN); valid = false }
        
        if btnIdentity.tag == 0  { Utils.shared.animatePop(alertIdentityType); valid = false }
        
        if btnImage.tag == 0 { Utils.shared.animatePop(alertDocument);  valid = false }
        
        return valid
    }
}

//MARK:- Textfield delegate
//MARK:-

extension AddBankDetail : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == Tags.dob.rawValue {
            textField.text = datePicker.date.string(format: DATE_FORMAT)
        }
        updateViewSize()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let tfTag = Tags(rawValue: textField.tag)!
        
        switch tfTag {
        case .firstName:
            Utils.shared.animateFadeOut(alertFirstName)
            break
        case .lastName:
            Utils.shared.animateFadeOut(alertLastName)
            break
        case .address:
            Utils.shared.animateFadeOut(alertAddress)
            break
            //        case .city:
            //            Utils.shared.animateFadeOut(alertCity)
        //            break
        case .postalCode:
            Utils.shared.animateFadeOut(alertPostalCode)
            break
        case .iban:
            if textField.text?.contains("****") ?? false {
                textField.text = ""
            }
            Utils.shared.animateFadeOut(alertIBAN)
            break
        case .dob:
            Utils.shared.animateFadeOut(alertDob)
            break
        case .idNo:
            Utils.shared.animateFadeOut(alertIDNo)
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        updateViewSize()
        let tfTag = Tags(rawValue: textField.tag)!
        
        switch tfTag {
        case .firstName:
            tfLastName.becomeFirstResponder()
            break
        case .lastName:
            tfAddress.becomeFirstResponder()
            break
        case .address:
            tfPostalCode.becomeFirstResponder()
            break
            //        case .city:
            //            tfPostalCode.becomeFirstResponder()
        //            break
        case .postalCode:
            tfDOB.becomeFirstResponder()
            break
        case .dob:
            tfIBAN.becomeFirstResponder()
        case .iban:
            tfIDNo.becomeFirstResponder()
            break
        case .idNo:
            tfIDNo.resignFirstResponder()
            break
        }
        return true
    }
}

//MARK:- ImagePicker delegate
//MARK:-

extension AddBankDetail : ImagePickerDelegate {
    func didSelect(image: UIImage) {
        btnImage.setImage(image, for: .normal)
        btnImage.tag = 123
    }
}

