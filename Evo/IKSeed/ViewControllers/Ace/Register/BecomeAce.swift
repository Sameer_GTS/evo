//
//  BecomeAce.swift
//  IKSeed
//
//  Created by Chanchal Warde on 3/13/18.
//  Copyright © 2018 Chanchal Warde. All rights reserved.
//

import UIKit
import Spring

class BecomeAce: UIViewController {
    
    //MARK:- Constant
    //MARK:-
    
    let PLACEHOLDER_EXP = "Skriv litt mer detaljert om din erfaring, og hvorfor en kunde bør bestille deg. Husk at dette viser vi kundene."
    let PLACEHOLDER_COMMENT  = "Jo mer du deler med oss, desto bedre..."
    let PLACEHOLDER_MEMBER = "Ant. år som PT, andre erfaringer.."
    let PLACEHOLDER_LINK = "Utdanning, sertifikater o.l.."
    
    let sportList = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Categories") as! Categories
    
    let ROW_HEIGHT : CGFloat = 50
    let TAG_HELP = 1111
    let TAG_LINK = 1121
    let TAG_WHY = 1131
    let TAG_EXPERIENCE = 1141
    let TAG_QUALIFICATION = 1151
    let TAG_MEMBER = 1161
    let TAG_COVER = 4512
    let picker = MediaPicker()

    //MARK:- IBOutlet
    //MARK:-
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var btnSport: UIButton! {
        didSet {
//            btnSport.setBorder(cornerRadius: 5, borderWidth: 1)
            btnSport.setBorder(cornerRadius: 0, borderWidth: 1)
        }
    }
    @IBOutlet var lblSport: UILabel!
    @IBOutlet var lblExp: UILabel! {
        didSet {
            lblExp.text = "0"
        }
    }
    
    @IBOutlet var tblHelp: UITableView!
    @IBOutlet var tblContainer: UITableView!
    @IBOutlet var conTblHeight: NSLayoutConstraint!
    
    @IBOutlet var tfMember: UITextField! {
        didSet {
            tfMember.tag = TAG_MEMBER
        }
    }
    //Fahad
    @IBOutlet var tfMemberTv: UITextView! {
        didSet {
            tfMemberTv.tag = TAG_MEMBER
        }
    }
    
    @IBOutlet var tfLink: UITextField! {
        didSet {
            tfLink.tag = TAG_LINK
        }
    }
    
    //Fahad
    
    @IBOutlet var tfLinkTv: UITextView! {
        didSet {
            tfLinkTv.tag = TAG_LINK
        }
    }
    
    @IBOutlet var tfWhy: UITextView! {
        didSet {
            tfWhy.tag = TAG_WHY
        }
    }
    @IBOutlet var tfQualification: UITextField! {
        didSet {
            tfQualification.tag = TAG_QUALIFICATION
        }
    }
    @IBOutlet var tvExperience: UITextView! {
        didSet {
            tvExperience.tag = TAG_EXPERIENCE
        }
    }
    
    @IBOutlet var btnSubmit: UIButton! {
        didSet {
            btnSubmit.dropShadow()
        }
    }
    @IBOutlet var btnCity: UIButton!
    @IBOutlet var btnNutrition: UIButton!
    @IBOutlet var btnTerms: UIButton!
    
    @IBOutlet var btnPT: UIButton!
    @IBOutlet var btnCenter: UIButton!
    
    @IBOutlet var btnCover: UIButton!
    @IBOutlet var imgVuCover: UIImageView!
    
    
    //---Alerts
    
    @IBOutlet var alertSport: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertSport,0.0)
        }
    }
    
    @IBOutlet var alertPT: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertPT,0.0)
        }
    }
    @IBOutlet var alertQualification: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertQualification,0.0)
        }
    }
    @IBOutlet var alertExp: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertExp,0.0)
        }
    }
    @IBOutlet var alertExpDetail: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertExpDetail,0.0)
        }
    }
    @IBOutlet var alertHelp: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertHelp,0.0)
        }
    }
    @IBOutlet var alertLink: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertLink,0.0)
        }
    }
    @IBOutlet var alertAce: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertAce,0.0)
        }
    }
    @IBOutlet var alertCity: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertCity,0.0)
        }
    }
    @IBOutlet var alertMember: SpringImageView!{
        didSet{
            Utils.shared.animateFadeOut(alertMember,0.0)
        }
    }
    
    /// Get updated view height whenever layover list updated and height increase for table.
    fileprivate var viewHeight : CGFloat {
        return btnSubmit.frame.origin.y + btnSubmit.frame.height + 50
    }
    
    fileprivate var arrHelp : [String] = []
    fileprivate var strHelp = ""
    fileprivate var experience = 0
    
    //MARK:- Variable
    //MARK:-
    
    //MARK:- Life Cycle
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sportList.delegate = self
        
        Utils.shared.logEventWithFlurry(EventName: "become_ace", EventParam: [:])
        
        self.edgesForExtendedLayout = UIRectEdge()
        self.extendedLayoutIncludesOpaqueBars = false
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBAction
    //MARK:-
    
    @IBAction func clickCoverImage(_ sender: UIButton) {
        //Fahad
        picker.isSquared = false
        picker.targetVC = self
        picker.showPicker(vc: self)
        picker.delegate = self
    }
    
    @IBAction func clickIncrease(_ sender: UIButton) {
        
        guard experience < 50 else {
            return
        }
        experience += 1
        lblExp.text = String(describing:experience)
        Utils.shared.animateFadeOut(alertAce)
    }
    
    @IBAction func clickDecrease(_ sender: UIButton) {
        guard experience > 0  else {
            return
        }
        
        experience -= 1
        lblExp.text = String(describing:experience)
    }
    
    @IBAction func clickAcceptTerms(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func clickTerms(_ sender: UIButton) {
        let term = self.storyboard?.instantiateViewController(withIdentifier: "Terms") as! Terms
        term.url = WebConnect.AceTerms
        term.strTitle = "Avtalen"
        navigationController?.present(term, animated: true, completion: {
        })
        //navigationController?.pushViewController(term, animated: true)
    }
    
    @IBAction func clickSubmit(_ sender: UIButton) {
        
        guard validation() else {  return  }
        self.view.endEditing(true)
        
        if arrHelp.count == 1 && arrHelp.first!.isEmpty() {
            arrHelp[0] = strHelp
        }
        else if !strHelp.isEmpty() {
            arrHelp.append(strHelp)
            if arrHelp.first!.isEmpty() {
                arrHelp.removeFirst()
            }
        }
        
        let param : [String : Any] = [  "nutritionist" : btnNutrition.isSelected ? "Y" : "N",
                                        "nutritionist_degree" : btnNutrition.isSelected ? tfQualification.text! : "",
                                        "zip_code": "1",
                                        "user_id": IK_USER.profile_id!,
                                       // "cat_id": "\(btnSport.tag)",
                                        "experience": "\(experience)" ,
                                        "about_experience_work": tvExperience.text! ,
                                        "help_inputs":arrHelp.joined(separator: ","),
                                        "ssno":"ssno",
                                        "links":tfLinkTv.text!, //tfLink.text!,
                                        "become_ace_reasons":tfWhy.text!,
                                        "country":"Norway",
                                        "member_of":tfMemberTv.text!, //tfMember.text! ,
                                        "gym_id" : "\(btnCenter.tag)",
                                        "pt_level_id" : "\(btnPT.tag)" ]
        
        WebConnect.becomeAce(param: param, coverImage:btnCover.tag == TAG_COVER ? imgVuCover?.image : nil) { (status, message) in
            guard status else {
                Utils.shared.alert(on: self, message: message, affirmButton: "Ok", cancelButton: nil)
                return
            }
            
            if let dict = IK_DEFAULTS.object(forKey: USER_DETAIL) as? NSDictionary {
                let newDic = NSMutableDictionary(dictionary: dict)
                newDic.setValue(3, forKey: "role")
                
                IK_DEFAULTS.set(newDic, forKey: USER_DETAIL)
                IK_DEFAULTS.synchronize()
            }
            
            Utils.shared.alert(on: self, title: "Søknad er sendt", message: ALERT_APPLIED, affirmButton: "OK", cancelButton: nil, dismissWith: { (_) in
                self.navigationController?.popToRootViewController(animated: true)
            })
        }
    }
    
    @IBAction func clickBack(_ sender: UIButton) {
        
        let count = self.navigationController?.viewControllers.count
        if let vc = self.navigationController?.viewControllers[count!-3] {
            navigationController?.popToViewController(vc, animated: true)
        }
        else {
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func clickSport(_ sender: UIButton) {
        sportList.isBecomeAce = true
        navigationController?.pushViewController(sportList, animated: true)
    }
    
    @IBAction func clickNutrition(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func clickAddHelp(_ sender: UIButton) {
        
        if let cell = tblHelp.cellForRow(at: IndexPath(row: 0, section: 0)) as? CellHelp {

            guard !cell.tfComment.text!.isEmpty()  else {
                Utils.shared.alert(on: self, message: ERROR_HELP, affirmButton: "Ok", cancelButton: nil)
                return
            }
            
            guard !arrHelp.contains(cell.tfComment.text!) else {
                Utils.shared.alert(on: self, message: ERROR_HELP_EXISTS, affirmButton: "Ok", cancelButton: nil)
                return
            }
            
            guard arrHelp.count < 5 else {
                return
            }
            
            Utils.shared.animateFadeOut(alertHelp)
            arrHelp.append(cell.tfComment.text!)
            cell.tfComment.text = nil
            tblHelp.reloadData()
            
            self.conTblHeight.constant = CGFloat(arrHelp.count+1) * self.ROW_HEIGHT
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                self.updateViewSize()
            }
        }
    }
    
    @IBAction func clickDeleteHelp(_ sender: UIButton) {
        arrHelp.remove(at: sender.tag)
        self.conTblHeight.constant = CGFloat(arrHelp.count+1) * self.ROW_HEIGHT
        tblHelp.reloadData()
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.updateViewSize()
        }
    }
    
    @IBAction func clickCity(_ sender: UIButton) {
        let picker =  ListPicker.showPicker(on: self, arrList: arrCities)
        picker.onSelectItem = { obj in
            Utils.shared.animateFadeOut(self.alertCity)
            sender.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
        }
    }
    
    @IBAction func clickPtLevel(_ sender: UIButton) {
        let picker =  ListPicker.showPicker(on: self,title : "PT Nivå" , arrList: arrPTLevel)
        picker.onSelectItem = { obj in
            Utils.shared.animateFadeOut(self.alertPT)
            sender.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
        }
    }
    
    @IBAction func clickCenter(_ sender: UIButton) {
        let gym = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GymList") as! GymList
        
        // jitendra
        if sender.tag != 0 {
            gym.selectedGymId = sender.tag
        }
        
        gym.isHandler = true
        gym.showAll = true
        gym.onSelect = { obj in 
            sender.setTitle(obj.name, for: .normal)
            sender.tag = obj.id
            Utils.shared.animateFadeOut(self.alertCity)

        }
        self.navigationController?.pushViewController(gym , animated: true)
    }
    
    //MARK:- Other
    //MARK:-
    
    /// Function to updated view size and table view content size whenever new layover added or deleted.
    fileprivate func updateViewSize() {
        var frame = self.viewContainer.frame
        frame.size.height = self.viewHeight
        self.viewContainer.frame = frame
        self.tblContainer.contentSize = frame.size
        self.tblContainer.setNeedsDisplay()
        self.viewContainer.setNeedsDisplay()
    }
    
    fileprivate func validation() -> Bool {
        var isValid = true
//
//        if btnSport.tag == 0 {
//            Utils.shared.animatePop(alertSport)
//            isValid = false
//        }
        
        if btnPT.tag == 0 {
            Utils.shared.animatePop(alertPT)
            isValid = false
        }
        
        if btnNutrition.isSelected && tfQualification.text!.isEmpty() {
            Utils.shared.animatePop(alertQualification)
            isValid = false
        }
        
        if experience  == 0 {
            Utils.shared.animatePop(alertExp)
            isValid = false
        }
        
        if tvExperience.text!.isEmpty()  || tvExperience.text! == PLACEHOLDER_EXP {
            Utils.shared.animatePop(alertExpDetail)
            isValid = false
        }
        
        if arrHelp.count == 1 && arrHelp.first!.isEmpty() && strHelp.isEmpty() {
            Utils.shared.animatePop(alertHelp)
            isValid = false
        }
        //Fahad
//        if tfLink.text!.isEmpty() {
//            Utils.shared.animatePop(alertLink)
//            isValid = false
//        }
//
        if tfLinkTv.text!.isEmpty() {
            Utils.shared.animatePop(alertLink)
            isValid = false
        }
        
        if tfWhy.text!.isEmpty() || tfWhy.text! ==  PLACEHOLDER_COMMENT {
            Utils.shared.animatePop(alertAce)
            isValid = false
        }
        
        if btnCenter.tag == 0 {
            Utils.shared.animatePop(alertCity)
            isValid = false
        }
        
        if tfMemberTv.text!.isEmpty() {
            Utils.shared.animatePop(alertMember)
            isValid = false
        }
//        if tfMember.text!.isEmpty() {
//            Utils.shared.animatePop(alertMember)
//            isValid = false
//        }
        
//        if !btnTerms.isSelected {
//            isValid = false
//        }
        
        return isValid
    }
}

extension BecomeAce : SportDelegate {
    func didSelect(sport: Sport) {
        Utils.shared.animateFadeOut(alertSport)
        btnSport.tag = sport.id
        lblSport.text = sport.name
        btnSport.setImage(#imageLiteral(resourceName: "check"), for: .normal)
    }
}

//MARK:- Tableview Delegate
//MARK:-

extension BecomeAce : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : arrHelp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellHelp", for: indexPath) as! CellHelp
        
        guard indexPath.section == 1 else {
            cell.tfComment.isUserInteractionEnabled =  true
            cell.tfComment.delegate = self
            cell.tfComment.tag = TAG_HELP
            
            cell.btnAdd.isHidden = false
            cell.btnCancel.isHidden = true
            cell.btnAdd.addTarget(self, action: #selector(clickAddHelp), for: .touchUpInside)
            return cell
        }
        
        cell.btnAdd.isHidden = true
        cell.btnCancel.isHidden = false
        
        cell.btnCancel.tag = indexPath.row
        cell.btnCancel.addTarget(self, action: #selector(clickDeleteHelp), for: .touchUpInside)
        
        cell.tfComment.text = arrHelp[indexPath.row]
        cell.tfComment.isUserInteractionEnabled =  false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ROW_HEIGHT
    }
}

//MARK:- ImagePicker delegate
//MARK:-

extension BecomeAce : ImagePickerDelegate {
    func didSelect(image: UIImage) {
//        btnCover.setImage(image, for: .normal)
        imgVuCover.contentMode = .scaleAspectFill
        imgVuCover.image = image
        btnCover.tag = TAG_COVER
    }
}

//MARK:- Textfield Delegate
//MARK:-

extension BecomeAce : UITextFieldDelegate, UITextViewDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == TAG_HELP {
            strHelp = textField.text!
        }
        updateViewSize()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == TAG_HELP {
            strHelp = textField.text!
            Utils.shared.animateFadeOut(alertHelp)
        }
        else if textField.tag == TAG_QUALIFICATION {
            Utils.shared.animateFadeOut(alertQualification)
        }
        else if textField.tag == TAG_LINK {
            Utils.shared.animateFadeOut(alertLink)
        }
        else if textField.tag == TAG_MEMBER {
            Utils.shared.animateFadeOut(alertMember)
        }
        
        Utils.shared.animateFadeOut(alertCity)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.tag == TAG_EXPERIENCE {
            if textView.text!.isEmpty() {
                textView.text = PLACEHOLDER_EXP
            }
        }
        else if textView.tag == TAG_WHY {
            if textView.text!.isEmpty() {
                textView.text = PLACEHOLDER_COMMENT
            }
        }
        else if textView.tag == TAG_MEMBER {
            if textView.text!.isEmpty() {
                textView.text = PLACEHOLDER_MEMBER
            }
        }
        else if textView.tag == TAG_LINK {
            if textView.text!.isEmpty() {
                textView.text = PLACEHOLDER_LINK
            }
        }
        updateViewSize()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.tag == TAG_EXPERIENCE {
            Utils.shared.animateFadeOut(alertExpDetail)
        }
        else if textView.tag == TAG_WHY {
            Utils.shared.animateFadeOut(alertAce)
            if textView.text == PLACEHOLDER_COMMENT  {
                textView.text = ""
            }
        }
        else if textView.tag == TAG_MEMBER {
            Utils.shared.animateFadeOut(alertAce)
            if textView.text == PLACEHOLDER_MEMBER  {
                textView.text = ""
            }
        }
        else if textView.tag == TAG_LINK {
            Utils.shared.animateFadeOut(alertLink)
            if textView.text == PLACEHOLDER_LINK {
                textView.text = ""
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfQualification {
            tvExperience.becomeFirstResponder()
        }
        else if textField == tfLink  {
//            tfMember.becomeFirstResponder()
            tfMemberTv.becomeFirstResponder()
        }
        else if textField == tfMember {
            tfWhy.becomeFirstResponder()
        }
        else {
//            tfLink.becomeFirstResponder()
            tfLinkTv.becomeFirstResponder()
        }
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard text == "\n" else {
            return true
        }
        
        if textView == tvExperience {
            if let cell = tblHelp.cellForRow(at: IndexPath(row: 0, section: 0)) as? CellHelp {
                cell.tfComment.becomeFirstResponder()
            }
        }
        else if textView == tfWhy {
            textView.resignFirstResponder()
        }
        
        
        return false
    }
    
}

class CellHelp : UITableViewCell {
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var btnCancel: UIButton!
    
    @IBOutlet var viewBorder: UIView!
    @IBOutlet var tfComment: UITextField!
    
    override func awakeFromNib() {
        //viewBorder.setBorder(cornerRadius: 5, borderWidth: 0)
    }
}
